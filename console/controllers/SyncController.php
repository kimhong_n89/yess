<?php

namespace console\controllers;

use common\models\Category;
use common\models\Product;
use console\models\C_BusinessField;
use console\models\C_Category;
use console\models\C_DiseaseFeatureValue;
use console\models\C_DiseaseGroup;
use console\models\C_Feature;
use console\models\C_FeatureValue;
use console\models\C_Product;
use console\models\C_ProductCategory;
use console\models\C_ProductFeature;
use console\models\C_Store;
use console\models\C_StoreField;
use console\models\C_StoreStaff;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use yii\helpers\Inflector;
use yii\console\ExitCode;
use Faker\Factory;
use common\models\Article;
use common\models\ArticleCategory;
use common\models\User;

/**
 * Class SyncController
 * @package console\controllers
 */
class SyncController extends Controller
{
    const API_DOMAIN = "https://chothuoctay.com/module/api/";
    const API_KEY = "20aa382e356b25fac39ee113498df8b4affa588a";
    const END_POINTS = [
        'BusinessField'         => "public?method=businessFields",  // page
        'Categories'            => "public?method=categories",      // page, id_parent
        'Stores'                => "sync?method=suppliers",         // page
        'Disease'                => "sync?method=disease",          // page
        'Products'              => "sync?method=products",          // page, last (integer)
        'ProductFeatures'       => "public?method=productFeatures",
    ];

    /**
     * Sync from the last data updated
     */
    public function actionLast()
    {
        $syn_last = 0;
        /** @var Product $item */
        $item = Product::find()->orderBy('last_sync desc')->one();
        if(!empty($item)) {
            $syn_last = $item->last_sync;
            echo "Last sync interval $syn_last: " . date("Y-m-d H:i:s",$syn_last) . PHP_EOL;
        }
        $this->actionProduct($syn_last, 1);
    }

    /**
     * Sync common list
     * @throws \yii\db\Exception
     */
    public function actionList()
    {
        $this->actionField();
        $this->actionFeature();
        $this->actionDisease(1);
    }

    /**
     * For master data only
     * @throws \yii\db\Exception
     */
    public function actionMaster()
    {
        $this->actionField();
        $this->actionFeature();
        $this->actionDisease(1);
        $this->actionCategory();
        $this->actionStore(1);
    }

    /**
     * Execute all (Refresh)
     * @throws \yii\db\Exception
     */
    public function actionAll()
    {
        $this->actionField();
        $this->actionFeature();
        $this->actionDisease(1);
        $this->actionCategory();
        $this->actionStore(1);
        $this->actionProduct(0, 1);
    }

    /**
     * Sync product all
     * @param int $lastTime
     * @param int $page
     * @throws \yii\db\Exception
     */
    public function actionProduct($lastTime = 0, $page = 1)
    {
        $products = $this->loadApi(self::END_POINTS['Products'] . "&page={$page}&last={$lastTime}");
        if(!empty($products) && is_array($products)) {
            $model = new C_Product();
            $products = $products['response'];
            echo "Syncing Product Info data with ".count($products)." records...." . PHP_EOL;
            $sql = $model->buildSyncInsertQuery($products);
            if(!empty($sql) && !empty($products)) {
                Yii::$app->db->createCommand($sql)->execute();

                //Sync category
                $lstCategory = [];
                foreach ($products as $product) {
                    if(!empty($product['categories'])) {
                        $lstCategory = array_merge($lstCategory, $product['categories']);
                    }
                }
                if(!empty($lstCategory)) {
                    $model = new C_ProductCategory();
                    echo "Syncing Product Categories data with ".count($lstCategory)." records...." . PHP_EOL;
                    $sql = $model->buildSyncInsertQuery($lstCategory);
                    if(!empty($sql)) {
                        Yii::$app->db->createCommand($sql)->execute();
                    }
                }
                // Sync product Features
                $lstFeatureValues = [];
                foreach ($products as $product) {
                    if(!empty($product['feature_values'])) {
                        $lstFeatureValues = array_merge($lstFeatureValues, $product['feature_values']);
                    }
                }
                if(!empty($lstFeatureValues)) {
                    $model = new C_ProductFeature();
                    echo "Syncing Product Features data with ".count($lstFeatureValues)." records...." . PHP_EOL;
                    $sql = $model->buildSyncInsertQuery($lstFeatureValues);
                    if(!empty($sql)) {
                        Yii::$app->db->createCommand($sql)->execute();
                    }
                }
                // Clear broken data
                Yii::$app->db->createCommand("DELETE FROM {{%product}} WHERE id = 0")->execute();
                $this->actionProduct($lastTime, $page + 1);
            }
        }
    }

    /**
     * Sync feature and feature values
     * @param int $page
     * @throws \yii\db\Exception
     */
    public function actionFeature()
    {
        $model = new C_Feature();
        $features = $this->loadApi(self::END_POINTS['ProductFeatures']);
        if(is_array($features) && !empty($features['response']))
        {
            $features = $features['response'];
            $sql = $model->buildSyncInsertQuery($features);
            if(!empty($sql))
            {
                echo "Syncing Feature Info data with ".count($features)." records...." . PHP_EOL;
                Yii::$app->db->createCommand($sql)->execute();
                foreach ($features as $feature)
                {
                    if(!empty($feature['items']))
                    {
                        echo "Syncing Feature Value Info data with ".$feature['name']." records...." . PHP_EOL;
                        $sub_model = new C_FeatureValue();
                        $sub_model->feature_id = $feature['id'];
                        $sql = $sub_model->buildSyncInsertQuery($feature['items']);
                        if(!empty($sql)) {
                            Yii::$app->db->createCommand($sql)->execute();
                        }
                    }
                }
            }
        }
    }


    /**
     * Sync feature and feature values
     * @param int $page
     * @throws \yii\db\Exception
     */
    public function actionDisease($page = 1)
    {
        $model = new C_DiseaseGroup();
        $diseases = $this->loadApi(self::END_POINTS['Disease'] . "&page={$page}");
        if(is_array($diseases) && !empty($diseases['response']))
        {
            $diseases = $diseases['response'];
            $sql = $model->buildSyncInsertQuery($diseases);
            if(!empty($sql) && !empty($diseases))
            {
                echo "Syncing Disease Group Info data with ".count($diseases)." records...." . PHP_EOL;
                Yii::$app->db->createCommand($sql)->execute();
                foreach ($diseases as $disease)
                {
                    if(!empty($disease['feature_values']))
                    {
                        Yii::$app->db->createCommand("DELETE FROM {{%disease_feature_value}} WHERE disease_id = {$disease['id']}")->execute();
                        echo "Syncing Disease Feature Value Info data with ".$disease['name']."...." . PHP_EOL;
                        $sub_model = new C_DiseaseFeatureValue();
                        $sql = $sub_model->buildSyncInsertQuery($disease['feature_values']);
                        if(!empty($sql)) {
                            Yii::$app->db->createCommand($sql)->execute();
                        }
                    }
                }

                $this->actionDisease($page + 1);
            }
        }
    }

    /**
     * Sync Store and Staff data
     * @param int $page
     * @throws \yii\db\Exception
     */
    public function actionStore($page = 1)
    {
        $model = new C_Store();
        echo "===========> Page: $page" . PHP_EOL;
        $stores = $this->loadApi(self::END_POINTS['Stores'] . "&page={$page}");
        if(is_array($stores))
        {
            $stores = $stores['response'];
            $sql = $model->buildSyncInsertQuery($stores);
            if(!empty($sql) && !empty($stores)) {
                echo "Syncing Store Info data with ".count($stores)." records...." . PHP_EOL;
                Yii::$app->db->createCommand($sql)->execute();
                foreach ($stores as $store)
                {
                    // Sync Store Fields
                    echo "Syncing Store Field and Store Staff data for {$store['shop_name']} ...." . PHP_EOL;
                    Yii::$app->db->createCommand("DELETE FROM {{%store_field}} WHERE store_id = {$store['id']}")->execute();
                    if(!empty($store['field_items'])) {
                        $field_model = new C_StoreField();
                        $field_model->store_id = $store['id'];
                        $sql = $field_model->buildSyncInsertQuery($store['field_items']);
                        if(!empty($sql)) {
                            Yii::$app->db->createCommand($sql)->execute();
                        }
                    }
                    // Sync store staff
                    if(!empty($store['doctor_name'])) {
                        $staff_model = new C_StoreStaff();
                        Yii::$app->db->createCommand("DELETE FROM {{%store_staff}} WHERE store_id = {$store['id']}")->execute();
                        $staff_model->store_id = $store['id'];
                        $sql = $staff_model->buildSyncInsertQuery([$store]);
                        if(!empty($sql)) {
                            Yii::$app->db->createCommand($sql)->execute();
                        }
                    }
                }
                $this->actionStore($page + 1);
            }
        }
    }

    /**
     * Sync category
     * @throws \yii\db\Exception
     */
    public function actionCategory()
    {
        $root = Category::findOne(2);
        if(empty($root)) {
            $root = new Category();
            $root->id = 2;
            $root->name = "Yess";
            $root->active = 1;
            $root->description = $root->name;
            $root->position = 0;
            $root->rewrite = "yess";
            $root->last_sync = time();
            $root->save();
        }
        $all_categories = $this->loadApi(self::END_POINTS['Categories']);
        if(is_array($all_categories))
        {
            $this->syncCategories($all_categories['response']);
        }
    }

    /**
     * Sync business fields
     * @throws \yii\db\Exception
     */
    public function actionField()
    {
        $model = new C_BusinessField();
        $all_business_fields = $this->loadApi(self::END_POINTS['BusinessField']);
        if(is_array($all_business_fields)) {
            $data = $all_business_fields['response'];
            $sql = $model->buildSyncInsertQuery($data);
            if(!empty($sql)) {
                echo "Syncing Business Field data with ".count($data)." records...." . PHP_EOL;
                Yii::$app->db->createCommand($sql)->execute();
            }
        }
    }

    /**
     * Sync list category
     * @param $lstData
     * @throws \yii\db\Exception
     */
    private function syncCategories($lstData)
    {
        $model = new C_Category();
        $sql = $model->buildSyncInsertQuery($lstData);
        if(!empty($sql)) {
            echo "Syncing Category data with ".count($lstData)." records...." . PHP_EOL;
            Yii::$app->db->createCommand($sql)->execute();
        }
        foreach ($lstData as &$item) {
            if(!empty($item['subs'])) {
                $this->syncCategories($item['subs']);
            }
        }
    }

    /**
     * Load API result
     * @param $endpoint
     * @param array $params
     * @param string $method
     * @return bool|string
     */
    private function loadApi($endpoint, $params = array(), $method = "GET")
    {
        $url = self::API_DOMAIN . $endpoint;
        $headers = array(
            'Content-Type: application/json',
        );
        $headers[] = "api_key: " . self::API_KEY;
        $post_data = json_encode($params);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if($method == "POST") {
            curl_setopt($ch, CURLOPT_POST, true);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        $result = curl_exec($ch);
        if(empty($result)) return false;
        curl_close($ch);
        return json_decode($result, true);
    }
}