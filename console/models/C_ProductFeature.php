<?php
namespace console\models;

use Yii;
class C_ProductFeature extends \common\models\ProductFeature
{
    /**
     * @inheritDoc
     * @return string
     */
    protected function syncTableName()
    {
        return "product_feature";
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeIgnored()
    {
        return array_merge(['id'], parent::attributeIgnored());
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeMapped()
    {
        return [
            'feature_id' => 'id_feature',
            'product_id' => 'id_product',
            'feature_value_id' => 'id_feature_value'
        ];
    }
    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeDefaults()
    {
        return [
            'created_at' => time(),
            'updated_at' => time(),
            'last_sync' => time(),
        ];
    }
}