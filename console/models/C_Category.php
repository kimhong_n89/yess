<?php
namespace console\models;

use Yii;
class C_Category extends \common\models\Category
{
    /**
     * @inheritDoc
     * @return string
     */
    protected function syncTableName()
    {
        return "category";
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeMapped()
    {
        return [
            'description' => 'name',
            'parent_id' => 'id_parent',
            'rewrite' => 'link_rewrite'
        ];
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeIgnored()
    {
        return []; //array_merge(['id'], parent::attributeIgnored());
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeDefaults()
    {
        return [
            'active' => 1,
            'position' => 0,
            'created_at' => time(),
            'updated_at' => time(),
            'last_sync' => time(),
        ];
    }
}