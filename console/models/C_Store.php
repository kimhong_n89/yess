<?php
namespace console\models;

use Yii;
class C_Store extends \common\models\Store
{
    /**
     * @inheritDoc
     * @return string
     */
    protected function syncTableName()
    {
        return "store";
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeMapped()
    {
        return [
            'name' => 'shop_name',
            'phone' => 'phone_number',
            'email' => 'business_email',
            'province_id' => 'id_state',
            'district_id' => 'id_district',
            'ward_id' => 'id_ward',
            'address' => 'company_address',
            'image' => 'img_logo',
            'status' => 'active'
        ];
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeIgnored()
    {
        return array_merge(['doctor_id'], parent::attributeIgnored());
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeDefaults()
    {
        return [
            'active' => 1,
            'created_at' => time(),
            'updated_at' => time(),
            'last_sync' => time(),
        ];
    }
}