<?php
namespace console\models;

use Yii;
class C_Product extends \common\models\Product
{
    /**
     * @inheritDoc
     * @return string
     */
    protected function syncTableName()
    {
        return "product";
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeIgnored()
    {
        return [];//array_merge(['id'], parent::attributeIgnored());
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeMapped()
    {
        return [
            'last_sync' => 'last_update',
            'store_id' => 'id_seller',
            'rewrite' => 'link'
        ];
    }
    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeDefaults()
    {
        return [
            'status' => 1,
            'created_at' => time(),
            'updated_at' => time(),
            'last_sync' => 0,
        ];
    }
}