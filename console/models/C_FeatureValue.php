<?php
namespace console\models;

use Yii;
class C_FeatureValue extends  \common\models\FeatureValue
{
    public $feature_id;
    /**
     * @inheritDoc
     * @return string
     */
    protected function syncTableName()
    {
        return "feature_value";
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeIgnored()
    {
        return [];//array_merge(['id'], parent::attributeIgnored());
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeMapped()
    {
        return [
            'value' => 'name'
        ];
    }
    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeDefaults()
    {
        return [
            'feature_id' => $this->feature_id,
        ];
    }
}