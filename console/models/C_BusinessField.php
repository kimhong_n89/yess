<?php
namespace console\models;

use Yii;

class C_BusinessField extends \common\models\BusinessField
{
    /**
     * @inheritDoc
     * @return string
     */
    protected function syncTableName()
    {
        return "business_field";
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeMapped()
    {
        return [];
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeIgnored()
    {
        return []; //array_merge(['id'], parent::attributeIgnored());
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeDefaults()
    {
        return [
            'status' => 1,
            'created_at' => time(),
            'updated_at' => time(),
            'last_sync' => time(),
        ];
    }
}