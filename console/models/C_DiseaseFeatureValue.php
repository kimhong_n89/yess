<?php
namespace console\models;

use Yii;
class C_DiseaseFeatureValue extends  \common\models\DiseaseFeatureValue
{
    /**
     * @inheritDoc
     * @return string
     */
    protected function syncTableName()
    {
        return "disease_feature_value";
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeIgnored()
    {
        return [];//array_merge(['id'], parent::attributeIgnored());
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeMapped()
    {
        return [
            'disease_id' => 'id_disease_group',
            'feature_value_id' => 'id_feature_value'
        ];
    }
    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeDefaults()
    {
        return [
            'created_at' => time(),
            'updated_at' => time(),
        ];
    }
}