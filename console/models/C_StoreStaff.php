<?php
namespace console\models;

use Yii;
class C_StoreStaff extends \common\models\StoreStaff
{
    public $store_id;

    /**
     * @inheritDoc
     * @return string
     */
    protected function syncTableName()
    {
        return "store_staff";
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeIgnored()
    {
        return array_merge(['id'], parent::attributeIgnored());
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeMapped()
    {
        return [
            'name' => 'doctor_name',
            'phone' => 'doctor_phone',
            'email' => 'doctor_email',
            'province_id' => 'id_state',
            'district_id' => 'id_district',
            'ward_id' => 'id_ward',
            'address' => 'company_address',
            'image' => 'doctor_image',
            'status' => 'active'
        ];
    }
    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeDefaults()
    {
        return [
            'store_id' => $this->store_id,
            'created_at' => time(),
            'updated_at' => time(),
            'last_sync' => time(),
        ];
    }
}