<?php
namespace console\models;

use Yii;
class C_StoreField extends \common\models\StoreField
{
    public $store_id;

    /**
     * @inheritDoc
     * @return string
     */
    protected function syncTableName()
    {
        return "store_field";
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeIgnored()
    {
        return array_merge(['id'], parent::attributeIgnored());
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeMapped()
    {
        return [
            'field_id' => 'id',
        ];
    }
    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeDefaults()
    {
        return [
            'store_id' => $this->store_id,
            'created_at' => time(),
            'updated_at' => time(),
            'last_sync' => time(),
        ];
    }
}