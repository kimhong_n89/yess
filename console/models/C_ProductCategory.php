<?php
namespace console\models;

use Yii;
class C_ProductCategory extends \common\models\ProductCategory
{
    /**
     * @inheritDoc
     * @return string
     */
    protected function syncTableName()
    {
        return "product_category";
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeIgnored()
    {
        return array_merge(['id'], parent::attributeIgnored());
    }

    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeMapped()
    {
        return [
            'category_id' => 'id_category',
            'product_id' => 'id_product',
        ];
    }
    /**
     * @inheritDoc
     * @return array
     */
    protected function attributeDefaults()
    {
        return [
            'created_at' => time(),
            'updated_at' => time(),
            'last_sync' => time(),
        ];
    }
}