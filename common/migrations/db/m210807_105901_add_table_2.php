<?php

use yii\db\Migration;

/**
 * Class m210807_105901_add_table_2
 */
class m210807_105901_add_table_2 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn("{{%business_field}}", "id", $this->integer(11)->notNull());
        $this->alterColumn("{{%store}}", "id", $this->integer(11)->notNull());
        $this->alterColumn("{{%feature}}", "id", $this->integer(11)->notNull());
        $this->alterColumn("{{%feature_value}}", "id", $this->integer(11)->notNull());
        $this->alterColumn("{{%category}}", "id", $this->integer(11)->notNull());
        $this->alterColumn("{{%product}}", "id", $this->integer(11)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210807_105901_add_table_2 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210807_105901_add_table_2 cannot be reverted.\n";

        return false;
    }
    */
}
