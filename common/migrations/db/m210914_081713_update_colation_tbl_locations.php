<?php

use yii\db\Migration;

/**
 * Class m210914_081713_update_colation_tbl_locations
 */
class m210914_081713_update_colation_tbl_locations extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `province` CHANGE COLUMN `name` `name` VARCHAR(512) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_vietnamese_ci' NULL DEFAULT NULL ;");
        $this->execute("ALTER TABLE `district` CHANGE COLUMN `name` `name` VARCHAR(512) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_vietnamese_ci' NULL DEFAULT NULL ;");
        $this->execute("ALTER TABLE `ward` CHANGE COLUMN `name` `name` VARCHAR(512) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_vietnamese_ci' NULL DEFAULT NULL ;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210914_081713_update_colation_tbl_locations cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210914_081713_update_colation_tbl_locations cannot be reverted.\n";

        return false;
    }
    */
}
