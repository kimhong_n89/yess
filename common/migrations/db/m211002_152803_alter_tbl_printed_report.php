<?php

use yii\db\Migration;

/**
 * Class m211002_152803_alter_tbl_printed_report
 */
class m211002_152803_alter_tbl_printed_report extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("{{%printed_report}}", 'version', $this->integer(11));
        $this->addColumn("{{%printed_report}}", 'mail_sent', $this->smallInteger(6)->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211002_152803_alter_tbl_printed_report cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211002_152803_alter_tbl_printed_report cannot be reverted.\n";

        return false;
    }
    */
}
