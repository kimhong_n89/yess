<?php

use yii\db\Migration;

/**
 * Class m210812_151702_alter_printed
 */
class m210812_151702_alter_printed extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("{{%printed_report}}", 'hash', $this->string(50)->unique()->null(false));
        $this->addColumn("{{%prescription}}", 're_examination', $this->smallInteger(6)->defaultValue(0));
        $this->addColumn("{{%prescription}}", 're_examination_date', $this->date());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210812_151702_alter_printed cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210812_151702_alter_printed cannot be reverted.\n";

        return false;
    }
    */
}
