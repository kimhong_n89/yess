<?php

use yii\db\Migration;

/**
 * Class m210815_051948_add_table_schedule
 */
class m210815_051948_add_table_schedule extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("{{%examination_schedule}}", [
            'id' => $this->primaryKey(),
            'prescription_id' => $this->integer(11),
            'doctor_id' => $this->integer(11),
            'patient_id' => $this->integer(11),
            'note' => $this->string(1024),
            'date' => $this->date(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210815_051948_add_table_schedule cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210815_051948_add_table_schedule cannot be reverted.\n";

        return false;
    }
    */
}
