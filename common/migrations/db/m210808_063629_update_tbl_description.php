<?php

use yii\db\Migration;

/**
 * Class m210808_063629_update_tbl_description
 */
class m210808_063629_update_tbl_description extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn("{{%prescription}}", 'note', $this->string(1024)->null());
        $this->alterColumn("{{%prescription}}", 'insurance_rate', $this->float()->null());
        $this->alterColumn("{{%prescription}}", 'insurance_number', $this->string(50)->null());

        $this->dropColumn("{{%prescription_detail}}", 'update_at');
        $this->addColumn("{{%prescription_detail}}", 'updated_at', $this->integer(11)->notNull());
        $this->alterColumn("{{%prescription_detail}}", 'status', $this->smallInteger(6)->defaultValue(1)->null());
        $this->alterColumn("{{%prescription_detail}}", 'amount_morning', $this->float()->null());
        $this->alterColumn("{{%prescription_detail}}", 'amount_noon', $this->float()->null());
        $this->alterColumn("{{%prescription_detail}}", 'amount_afternoon', $this->float()->null());
        $this->alterColumn("{{%prescription_detail}}", 'time_of_use', $this->smallInteger(6)->null());
        $this->alterColumn("{{%prescription_detail}}", 'drugged_time', $this->integer(11)->null());

        $this->alterColumn("{{%patient}}", 'status', $this->smallInteger(6)->defaultValue(1)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210808_063629_update_tbl_description cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210808_063629_update_tbl_description cannot be reverted.\n";

        return false;
    }
    */
}
