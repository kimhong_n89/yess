<?php

use yii\db\Migration;

/**
 * Class m210816_083856_sms_history_table
 */
class m210816_083856_sms_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
        CREATE TABLE `sms_history` (
          `id` int(11) NOT NULL,
          `phone_number` varchar(50) DEFAULT NULL,
          `content` varchar(1024) DEFAULT NULL,
          `params` text DEFAULT NULL,
          `result` text DEFAULT NULL,
          `status` varchar(20) DEFAULT NULL,
          `sms_type` varchar(20) NOT NULL DEFAULT 'OTP' COMMENT 'OTP, REF',
          `user_id` int(10) UNSIGNED DEFAULT NULL,
          `updated_at` int(11) DEFAULT NULL,
          `created_at` int(11) DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
        $this->execute("ALTER TABLE `sms_history` ADD PRIMARY KEY (`id`);");
        $this->execute("ALTER TABLE `sms_history` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210816_083856_sms_history_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210816_083856_sms_history_table cannot be reverted.\n";

        return false;
    }
    */
}
