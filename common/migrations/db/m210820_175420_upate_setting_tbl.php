<?php

use yii\db\Migration;

/**
 * Class m210820_175420_upate_setting_tbl
 */
class m210820_175420_upate_setting_tbl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn("{{%doctor_setting}}", 'value', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210820_175420_upate_setting_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210820_175420_upate_setting_tbl cannot be reverted.\n";

        return false;
    }
    */
}
