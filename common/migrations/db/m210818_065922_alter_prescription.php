<?php

use yii\db\Migration;

/**
 * Class m210818_065922_alter_prescription
 */
class m210818_065922_alter_prescription extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn("{{%examination_schedule}}", 'date', $this->dateTime());
        $this->alterColumn("{{%prescription}}", 're_examination_date', $this->dateTime());
        $this->addColumn("{{%doctor}}", 'clinic_name', $this->string(255));
        $this->execute("
            ALTER TABLE `examination_schedule` 
            ADD CONSTRAINT `schedule-fk-doctor`
              FOREIGN KEY (`doctor_id`)
              REFERENCES `doctor` (`id`)
              ON DELETE CASCADE
              ON UPDATE NO ACTION,
            ADD CONSTRAINT `schedule-fk-prescription`
              FOREIGN KEY (`prescription_id`)
              REFERENCES `prescription` (`id`)
              ON DELETE CASCADE
              ON UPDATE NO ACTION,
            ADD CONSTRAINT `schedule-fk-patient`
              FOREIGN KEY (`patient_id`)
              REFERENCES `patient` (`id`)
              ON DELETE CASCADE
              ON UPDATE NO ACTION;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210818_065922_alter_prescription cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210818_065922_alter_prescription cannot be reverted.\n";

        return false;
    }
    */
}
