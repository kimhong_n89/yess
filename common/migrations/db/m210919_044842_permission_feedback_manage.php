<?php

use yii\db\Migration;

/**
 * Class m210919_044842_permission_feedback_manage
 */
class m210919_044842_permission_feedback_manage extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("INSERT INTO `sys_auth_route` (`route`) VALUES
            ('feedback/index'),
            ('feedback/view'),
            ('feedback/update'),
            ('feedback/delete');
        ");

        $this->execute("INSERT INTO `rbac_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
            ('feedbackManage', 2, 'Manage feedback', NULL, NULL, 1623228158, 1623228158);
        ");

        $this->execute("INSERT INTO `sys_auth_perm_route` (`auth_item`, `route`, `created`) VALUES
            ('feedbackManage', 'feedback/index', '2021-09-18 09:18:30'),
            ('feedbackManage', 'feedback/view', '2021-09-18 09:18:30'),
            ('feedbackManage', 'feedback/update', '2021-09-18 09:19:22'),
            ('feedbackManage', 'feedback/delete', '2021-09-18 09:19:22');
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210919_044842_permission_feedback_manage cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210919_044842_permission_feedback_manage cannot be reverted.\n";

        return false;
    }
    */
}
