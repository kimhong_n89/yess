<?php

use yii\db\Migration;

/**
 * Class m210918_081012_create_tbl_feedback
 */
class m210918_081012_create_tbl_feedback extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("{{%feedback}}", [
            'id' => $this->primaryKey(),
            'doctor_id' => $this->integer(11),
            'user_id' => $this->integer(11),
            'title' => $this->string(1024),
            'content' => $this->text(),
            'answer' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->execute("
            ALTER TABLE `feedback` 
            ADD CONSTRAINT `feedback_doctor`
              FOREIGN KEY (`doctor_id`)
              REFERENCES `yess`.`doctor` (`id`)
              ON DELETE CASCADE
              ON UPDATE NO ACTION,
            ADD CONSTRAINT `feedback_user`
              FOREIGN KEY (`user_id`)
              REFERENCES `yess`.`user` (`id`)
              ON DELETE CASCADE
              ON UPDATE NO ACTION;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210918_081012_create_tbl_feedback cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210918_081012_create_tbl_feedback cannot be reverted.\n";

        return false;
    }
    */
}
