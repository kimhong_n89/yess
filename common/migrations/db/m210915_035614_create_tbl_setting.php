<?php

use yii\db\Migration;

/**
 * Class m210915_035614_create_tbl_setting
 */
class m210915_035614_create_tbl_setting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%template}}', [
            'id' => $this->primaryKey(),
            'code' => $this->string(100)->notNull(),
            'title' => $this->string(255)->null(),
            'content' => $this->text(),
            'type' => $this->char(20)->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createTable('{{%setting}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string(512)->notNull(),
            'value' => $this->text()->null(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210915_035614_create_tbl_setting cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210915_035614_create_tbl_setting cannot be reverted.\n";

        return false;
    }
    */
}
