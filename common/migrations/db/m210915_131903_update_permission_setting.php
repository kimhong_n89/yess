<?php

use yii\db\Migration;

/**
 * Class m210915_131903_update_permission_setting
 */
class m210915_131903_update_permission_setting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("INSERT INTO `sys_auth_route` (`route`) VALUES
            ('setting/index'),
            ('setting/create-template'),
            ('setting/update-template'),
            ('setting/delete-template');
        ");

        $this->execute("INSERT INTO `rbac_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
            ('settingManage', 2, 'Manage setting', NULL, NULL, 1623228158, 1623228158);
        ");

        $this->execute("INSERT INTO `sys_auth_perm_route` (`auth_item`, `route`, `created`) VALUES
            ('settingManage', 'setting/index', '2021-09-15 09:18:30'),
            ('settingManage', 'setting/create-template', '2021-09-15 09:18:30'),
            ('settingManage', 'setting/update-template', '2021-09-15 09:19:22'),
            ('settingManage', 'setting/delete-template', '2021-09-15 09:19:22');
        ");


        //setting data
        $this->execute("
            INSERT INTO `setting` (`key`,`value`,`created_at`,`updated_at`) VALUES ('smtp_host','smtp.gmail.com',1631681308,1631681308);
            INSERT INTO `setting` (`key`,`value`,`created_at`,`updated_at`) VALUES ('smtp_port','587',1631681309,1631681309);
            INSERT INTO `setting` (`key`,`value`,`created_at`,`updated_at`) VALUES ('smtp_encryption','tls',1631681309,1631681309);
            INSERT INTO `setting` (`key`,`value`,`created_at`,`updated_at`) VALUES ('smtp_username','kimhongbnb@gmail.com',1631681309,1631712593);
            INSERT INTO `setting` (`key`,`value`,`created_at`,`updated_at`) VALUES ('smtp_password','ezomqjezriwtpcyw',1631681309,1631712593);
            INSERT INTO `setting` (`key`,`value`,`created_at`,`updated_at`) VALUES ('server_send_email','cskh@yersin.org',1631681309,1631681309);
            INSERT INTO `setting` (`key`,`value`,`created_at`,`updated_at`) VALUES ('server_send_name','Chăm sóc khách hàng - Yersin',1631681309,1631681309);
        ");

        $this->execute("
            INSERT INTO `template` (`code`,`title`,`content`,`type`,`created_at`,`updated_at`) VALUES ('email_send_prescription','Thông tin toa thuốc {ma_don_thuoc}','<p>Chào {ten_benh_nhan},</p><p>Bạn có toa thuốc \bsố {ma_don_thuoc} từ bác sĩ {ten_bac_si}</p><p>Vui lòng truy cập vào link bên dưới để xem thông tin toa thuốc của bạn.\r\n</p><p><a href=\"{url_view}\">{url_view}</a>\r\n</p><p>Chúc bạn 1 ngày vui vẻ.\r\n</p><p><br>\r\n</p>','email',1631680374,1631693546);
            INSERT INTO `template` (`code`,`title`,`content`,`type`,`created_at`,`updated_at`) VALUES ('email_approve_doctor','Thông báo trạng thái tài khoản Yersin.org','<p>Chào {ten_bac_si},</p><p>Tài khoản của bạn đã được duyệt.</p><p>Chúc bạn 1 ngày vui vẻ.</p>','email',1631695890,1631695944);
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210915_131903_update_permission_setting cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210915_131903_update_permission_setting cannot be reverted.\n";

        return false;
    }
    */
}
