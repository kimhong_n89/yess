<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_sys_auth_perm_route`.
 */
class m160517_104316_create_table_sys_auth_perm_route extends Migration
{
    /**
     * @inheritdoc
     */
public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%sys_auth_perm_route%}}',[
            'id' => $this->primaryKey(11),
            'auth_item' => $this->string()->notNull(),
            'route' => $this->string()->notNull(),
            'created' => $this->timestamp()
        ], $tableOptions);

        $this->addForeignKey('fk_sys_auth_perm_route_route', '{{%sys_auth_perm_route%}}', 'route', '{{%sys_auth_route%}}', 'route', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_sys_auth_perm_route_auth_item', '{{%sys_auth_perm_route%}}', 'auth_item', '{{%rbac_auth_item%}}', 'name', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%sys_auth_perm_route%}}');
    }
}
