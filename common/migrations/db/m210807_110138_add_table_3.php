<?php

use yii\db\Migration;

/**
 * Class m210807_110138_add_table_3
 */
class m210807_110138_add_table_3 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn("{{%province}}", "id_state", $this->integer(11)->notNull());
        $this->alterColumn("{{%district}}", "id_district", $this->integer(11)->notNull());
        $this->alterColumn("{{%ward}}", "id_ward", $this->integer(11)->notNull());
        $this->addColumn("{{%product}}", "store_id", $this->integer(11)->after("unity"));


        $this->addPrimaryKey("fk_product_feature", "{{%product_feature}}", ["product_id", "feature_id", "feature_value_id"]);
        $this->addPrimaryKey("fk_product_category", "{{%product_category}}", ["product_id", "category_id"]);
        $this->createIndex("idx_product_last_sync", "{{%product}}", ["last_sync"]);
        $this->createIndex("idx_store_last_sync", "{{%store}}", ["last_sync"]);
        $this->createIndex("idx_product_store_id", "{{%product}}", ["store_id"]);
        $this->createIndex("idx_store_address", "{{%store}}", ["province_id", "district_id", "ward_id"]);
        $this->createIndex("idx_store_name", "{{%store}}", ["name"]);
        $this->createIndex("idx_product_name", "{{%product}}", ["name"]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210807_110138_add_table_3 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210807_110138_add_table_3 cannot be reverted.\n";

        return false;
    }
    */
}
