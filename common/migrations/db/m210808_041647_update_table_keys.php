<?php

use yii\db\Migration;

/**
 * Class m210808_041647_update_table_keys
 */
class m210808_041647_update_table_keys extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn("{{%business_field}}", "id", $this->integer(11)->notNull()->comment("Synced with CTT"));
        $this->alterColumn("{{%store}}", "id", $this->integer(11)->notNull()->comment("Synced with CTT"));
        $this->alterColumn("{{%feature}}", "id", $this->integer(11)->notNull()->comment("Synced with CTT"));
        $this->alterColumn("{{%feature_value}}", "id", $this->integer(11)->notNull()->comment("Synced with CTT"));
        $this->alterColumn("{{%category}}", "id", $this->integer(11)->notNull()->comment("Synced with CTT"));
        $this->alterColumn("{{%product}}", "id", $this->integer(11)->notNull()->comment("Synced with CTT"));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210808_041647_update_table_keys cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210808_041647_update_table_keys cannot be reverted.\n";

        return false;
    }
    */
}
