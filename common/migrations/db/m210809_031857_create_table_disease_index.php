<?php

use yii\db\Migration;

/**
 * Class m210809_031857_create_table_disease_index
 */
class m210809_031857_create_table_disease_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex("idx_disease_group_name", "{{%disease_group}}", ["name"]);
        $this->createIndex("idx_disease_feature_value", "{{%disease_feature_value}}", ["disease_id", "feature_value_id"]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210809_031857_create_table_disease_index cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210809_031857_create_table_disease_index cannot be reverted.\n";

        return false;
    }
    */
}
