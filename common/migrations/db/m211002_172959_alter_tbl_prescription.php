<?php

use yii\db\Migration;

/**
 * Class m211002_172959_alter_tbl_prescription
 */
class m211002_172959_alter_tbl_prescription extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("{{%prescription}}", 'store_id', $this->integer(11));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211002_172959_alter_tbl_prescription cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211002_172959_alter_tbl_prescription cannot be reverted.\n";

        return false;
    }
    */
}
