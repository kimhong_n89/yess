<?php

use yii\db\Migration;

/**
 * Class m210616_100309_update_rbac_data
 */
class m210616_100309_update_rbac_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('{{%user}}', 'is_superadmin', $this->smallInteger(6));
        $this->execute("UPDATE `user` SET `is_superadmin` = 1 WHERE `user`.`username` = 'webmaster';");

        //add route
        $this->execute("INSERT INTO `sys_auth_route` (`route`) VALUES
            ('dashboard/index'),
            ('disease-group/index'),
            ('doctor/create'),
            ('doctor/delete'),
            ('doctor/index'),
            ('doctor/update'),
            ('doctor/view'),
            ('location/index'),
            ('log/clear-logs'),
            ('log/delete'),
            ('log/index'),
            ('log/view'),
            ('login-history/index'),
            ('manage-file/delete'),
            ('manage-file/download'),
            ('master-data/create-dosage-form'),
            ('master-data/create-drugged-time'),
            ('master-data/delete-dosage-form'),
            ('master-data/delete-drugged-time'),
            ('master-data/index'),
            ('master-data/update-dosage-form'),
            ('master-data/update-drugged-time'),
            ('patient/create'),
            ('patient/delete'),
            ('patient/index'),
            ('patient/update'),
            ('patient/view'),
            ('permission/create'),
            ('permission/delete'),
            ('permission/index'),
            ('permission/update'),
            ('permission/view'),
            ('prescription/create'),
            ('prescription/delete'),
            ('prescription/index'),
            ('prescription/print'),
            ('prescription/update'),
            ('prescription/view'),
            ('product/create'),
            ('product/delete'),
            ('product/index'),
            ('product/update'),
            ('rbac-auth-assignment/create'),
            ('rbac-auth-assignment/delete'),
            ('rbac-auth-assignment/index'),
            ('rbac-auth-assignment/update'),
            ('rbac-auth-assignment/view'),
            ('rbac-auth-item-child/create'),
            ('rbac-auth-item-child/delete'),
            ('rbac-auth-item-child/index'),
            ('rbac-auth-item-child/update'),
            ('rbac-auth-item-child/view'),
            ('rbac-auth-rule/create'),
            ('rbac-auth-rule/delete'),
            ('rbac-auth-rule/index'),
            ('rbac-auth-rule/update'),
            ('rbac-auth-rule/view'),
            ('role/create'),
            ('role/delete'),
            ('role/index'),
            ('role/update'),
            ('role/view'),
            ('route/auto-insert-routes'),
            ('route/create'),
            ('route/delete'),
            ('route/index'),
            ('route/update'),
            ('route/view'),
            ('sign-in/account'),
            ('sign-in/login'),
            ('sign-in/logout'),
            ('sign-in/profile'),
            ('storage/delete'),
            ('storage/index'),
            ('storage/view'),
            ('store/index'),
            ('timeline-event/index'),
            ('translate/index'),
            ('user/create'),
            ('user/delete'),
            ('user/index'),
            ('user/login'),
            ('user/update'),
            ('user/view');
        ");
        
        //add permission
        $this->execute("INSERT INTO `rbac_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
            ('chartView', 2, 'View Chart on Dashboard', NULL, NULL, 1630753915, 1630753915),
            ('dashboardView', 2, 'View Dashboard ', NULL, NULL, 1622798781, 1622798781),
            ('diseasegroupView', 2, 'View Disease Group', NULL, NULL, 1630750527, 1630849895),
            ('doctorCreate', 2, 'Create Doctor', NULL, NULL, 1622797877, 1622798674),
            ('doctorDelete', 2, 'Delete Doctor', NULL, NULL, 1622797933, 1622798723),
            ('doctorExport', 2, 'Export Doctor', NULL, NULL, 1630826408, 1630849844),
            ('doctorUpdate', 2, 'Update Doctor', NULL, NULL, 1622797913, 1622798701),
            ('doctorView', 2, 'View Doctor', NULL, NULL, 1622796419, 1622798655),
            ('fileManage', 2, 'Manage File', NULL, NULL, 1630834831, 1630834831),
            ('locationView', 2, 'View Location', NULL, NULL, 1630750574, 1630849877),
            ('loginHistoryView', 2, 'View Login History', NULL, NULL, 1630753953, 1630849864),
            ('logManage', 2, 'Manage Log', NULL, NULL, 1623675391, 1623675391),
            ('masterDataManage', 2, 'Manage Master Data', NULL, NULL, 1630750646, 1630849906),
            ('newPrescription', 2, 'View new Prescription on Dashboard', NULL, NULL, 1630753892, 1630753892),
            ('patientCreate', 2, 'Create Patient', NULL, NULL, 1622797877, 1622798674),
            ('patientDelete', 2, 'Delete Patient', NULL, NULL, 1622797933, 1622798723),
            ('patientExport', 2, 'Export Patient', NULL, NULL, 1630826420, 1630849836),
            ('patientUpdate', 2, 'Update Patient', NULL, NULL, 1622797913, 1622798701),
            ('patientView', 2, 'View Patient', NULL, NULL, 1622796419, 1630747110),
            ('prescriptionCreate', 2, 'Create Prescription', NULL, NULL, 1622797877, 1622798674),
            ('prescriptionDelete', 2, 'Delete Prescription', NULL, NULL, 1622797933, 1622798723),
            ('prescriptionExport', 2, 'Export Prescription', NULL, NULL, 1630826390, 1630849852),
            ('prescriptionPrint', 2, 'Print Prescription', NULL, NULL, 1630849798, 1630849828),
            ('prescriptionUpdate', 2, 'Update Prescription', NULL, NULL, 1622797913, 1622798701),
            ('prescriptionView', 2, 'View Prescription', NULL, NULL, 1622796419, 1622798655),
            ('productView', 2, 'View Product', NULL, NULL, 1630750422, 1630849884),
            ('rbacManage', 2, 'Manage rbac', NULL, NULL, 1623226986, 1623226986),
            ('statisticView', 2, 'View statistic on Dashboard', NULL, NULL, 1630752588, 1630752588),
            ('storeView', 2, 'View Store', NULL, NULL, 1630750390, 1630773121),
            ('timelineView', 2, 'View Timeline', NULL, NULL, 1623675228, 1623675228),
            ('userCreate', 2, 'Create User', NULL, NULL, 1623228176, 1623228176),
            ('userDelete', 2, 'Delete User', NULL, NULL, 1623228211, 1623228211),
            ('userUpdate', 2, 'Update User', NULL, NULL, 1623228191, 1623228191),
            ('userView', 2, 'View User', NULL, NULL, 1623228158, 1623228158);
        ");
        
        //add route to permission
        $this->execute("INSERT INTO `sys_auth_perm_route` (`auth_item`, `route`, `created`) VALUES
            ('patientView', 'patient/index', '2021-09-04 09:18:30'),
            ('patientView', 'patient/view', '2021-09-04 09:18:30'),
            ('doctorView', 'doctor/index', '2021-09-04 09:19:22'),
            ('doctorView', 'doctor/view', '2021-09-04 09:19:22'),
            ('prescriptionView', 'prescription/index', '2021-09-04 09:19:40'),
            ('prescriptionView', 'prescription/view', '2021-09-04 09:19:40'),
            ('doctorCreate', 'doctor/create', '2021-09-04 09:19:51'),
            ('patientCreate', 'patient/create', '2021-09-04 09:20:00'),
            ('prescriptionCreate', 'prescription/create', '2021-09-04 09:20:08'),
            ('doctorUpdate', 'doctor/update', '2021-09-04 09:20:20'),
            ('patientUpdate', 'patient/update', '2021-09-04 09:20:33'),
            ('prescriptionUpdate', 'prescription/update', '2021-09-04 09:20:42'),
            ('prescriptionPrint', 'prescription/print', '2021-09-05 13:49:58'),
            ('doctorDelete', 'doctor/delete', '2021-09-04 09:20:59'),
            ('patientDelete', 'patient/delete', '2021-09-04 09:21:08'),
            ('prescriptionDelete', 'prescription/delete', '2021-09-04 09:21:15'),
            ('dashboardView', 'dashboard/index', '2021-09-04 09:21:23'),
            ('rbacManage', 'permission/create', '2021-06-09 01:23:06'),
            ('rbacManage', 'permission/delete', '2021-06-09 01:23:06'),
            ('rbacManage', 'permission/index', '2021-06-09 01:23:06'),
            ('rbacManage', 'permission/update', '2021-06-09 01:23:06'),
            ('rbacManage', 'permission/view', '2021-06-09 01:23:06'),
            ('rbacManage', 'rbac-auth-rule/create', '2021-06-09 01:23:06'),
            ('rbacManage', 'rbac-auth-rule/delete', '2021-06-09 01:23:06'),
            ('rbacManage', 'rbac-auth-rule/index', '2021-06-09 01:23:06'),
            ('rbacManage', 'rbac-auth-rule/update', '2021-06-09 01:23:06'),
            ('rbacManage', 'rbac-auth-rule/view', '2021-06-09 01:23:06'),
            ('rbacManage', 'role/create', '2021-06-09 01:23:06'),
            ('rbacManage', 'role/delete', '2021-06-09 01:23:06'),
            ('rbacManage', 'role/index', '2021-06-09 01:23:06'),
            ('rbacManage', 'role/update', '2021-06-09 01:23:06'),
            ('rbacManage', 'role/view', '2021-06-09 01:23:06'),
            ('rbacManage', 'route/auto-insert-routes', '2021-06-09 01:23:06'),
            ('rbacManage', 'route/create', '2021-06-09 01:23:06'),
            ('rbacManage', 'route/delete', '2021-06-09 01:23:06'),
            ('rbacManage', 'route/index', '2021-06-09 01:23:06'),
            ('rbacManage', 'route/update', '2021-06-09 01:23:06'),
            ('rbacManage', 'route/view', '2021-06-09 01:23:06'),
            ('userView', 'user/index', '2021-06-09 01:42:38'),
            ('userView', 'user/view', '2021-06-09 01:42:38'),
            ('userCreate', 'user/create', '2021-06-09 01:42:56'),
            ('userUpdate', 'user/update', '2021-06-09 01:43:11'),
            ('userDelete', 'user/delete', '2021-06-09 01:43:31'),
            ('timelineView', 'timeline-event/index', '2021-06-14 05:53:48'),
            ('logManage', 'log/clear-logs', '2021-06-14 05:56:31'),
            ('logManage', 'log/delete', '2021-06-14 05:56:31'),
            ('logManage', 'log/index', '2021-06-14 05:56:31'),
            ('logManage', 'log/view', '2021-06-14 05:56:31'),
            ('storeView', 'store/index', '2021-09-04 10:13:10'),
            ('productView', 'product/index', '2021-09-04 10:13:42'),
            ('diseasegroupView', 'disease-group/index', '2021-09-04 10:15:27'),
            ('locationView', 'location/index', '2021-09-04 10:16:14'),
            ('masterDataManage', 'master-data/create-dosage-form', '2021-09-04 10:17:26'),
            ('masterDataManage', 'master-data/create-drugged-time', '2021-09-04 10:17:26'),
            ('masterDataManage', 'master-data/delete-dosage-form', '2021-09-04 10:17:26'),
            ('masterDataManage', 'master-data/delete-drugged-time', '2021-09-04 10:17:26'),
            ('masterDataManage', 'master-data/index', '2021-09-04 10:17:26'),
            ('masterDataManage', 'master-data/update-dosage-form', '2021-09-04 10:17:26'),
            ('loginHistoryView', 'login-history/index', '2021-09-04 11:18:21'),
            ('fileManage', 'manage-file/delete', '2021-09-05 09:40:31'),
            ('fileManage', 'manage-file/download', '2021-09-05 09:40:31');
        ");
        
        //add rbac_auth_item_child
        $this->execute("INSERT INTO `rbac_auth_item_child` (`parent`, `child`) VALUES
            ('manager', 'chartView'),
            ('manager', 'dashboardView'),
            ('manager', 'diseasegroupView'),
            ('manager', 'doctorCreate'),
            ('manager', 'doctorExport'),
            ('manager', 'doctorUpdate'),
            ('manager', 'doctorView'),
            ('manager', 'fileManage'),
            ('manager', 'locationView'),
            ('manager', 'loginHistoryView'),
            ('manager', 'masterDataManage'),
            ('manager', 'newPrescription'),
            ('manager', 'patientCreate'),
            ('manager', 'patientExport'),
            ('manager', 'patientUpdate'),
            ('manager', 'patientView'),
            ('manager', 'prescriptionCreate'),
            ('manager', 'prescriptionExport'),
            ('manager', 'prescriptionPrint'),
            ('manager', 'prescriptionUpdate'),
            ('manager', 'prescriptionView'),
            ('manager', 'productView'),
            ('manager', 'statisticView'),
            ('manager', 'storeView');
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210616_100309_update_rbac_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210616_100309_update_rbac_data cannot be reverted.\n";

        return false;
    }
    */
}
