<?php

use yii\db\Migration;

/**
 * Class m210809_030439_create_table_disease
 */
class m210809_030439_create_table_disease extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("{{%disease_group}}", [
           'id' => $this->primaryKey(),
           'name' => $this->string(1024),
           'status' => $this->tinyInteger(2)->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'last_sync' => $this->integer()
        ]);
        $this->createTable("{{%disease_feature_value}}", [
            'id' => $this->primaryKey(),
            'disease_id' => $this->integer(11),
            'feature_value_id' => $this->integer(11),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
        $this->alterColumn("{{%disease_group}}", "id", $this->integer(11)->notNull()->comment("Synced with CTT"));
        $this->alterColumn("{{%disease_feature_value}}", "id", $this->integer(11)->notNull()->comment("Synced with CTT"));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210809_030439_create_table_disease cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210809_030439_create_table_disease cannot be reverted.\n";

        return false;
    }
    */
}
