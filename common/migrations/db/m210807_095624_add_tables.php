<?php

use yii\db\Migration;

/**
 * Class m210807_095624_add_tables
 */
class m210807_095624_add_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("{{%sync_log}}", [
            'id' => $this->primaryKey(11),
            'table_name' => $this->string(512)->null(),
            'min_id' => $this->integer(11),
            'max_id' => $this->integer(11),
            'status' => $this->smallInteger()->notNull(),
            'message' => $this->text()->null(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createTable("{{%business_field}}", [
            'id' => $this->primaryKey(11)->comment('synced with CTT'),
            'name' => $this->string(512)->null(),
            'status' => $this->smallInteger()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'last_sync' => $this->integer()
        ]);

        $this->createTable("{{%store}}", [
            'id' => $this->primaryKey(11)->comment('synced with CTT'),
            'name' => $this->string(512)->null(),
            'phone' => $this->string(20)->null(),
            'email' => $this->string(255)->null(),
            'province_id' => $this->integer(11)->null(),
            'district_id' => $this->integer(11)->null(),
            'ward_id' => $this->integer(11)->null(),
            'address' => $this->string(2048)->null(),
            'image' => $this->string(2048)->null(),
            'doctor_id' => $this->integer(11)->null(),
            'status' => $this->smallInteger()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'last_sync' => $this->integer()
        ]);

        $this->createTable("{{%store_field}}", [
            'id' => $this->primaryKey(11),
            'store_id'=>  $this->integer(11)->notNull(),
            'field_id'=>  $this->integer(11)->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'last_sync' => $this->integer()
        ]);

        $this->createTable("{{%store_staff}}", [
            'id' => $this->primaryKey(11),
            'store_id'=>  $this->integer(11)->notNull(),
            'name' => $this->string(512)->null(),
            'phone' => $this->string(20)->null(),
            'email' => $this->string(255)->null(),
            'province_id' => $this->integer(11)->null(),
            'district_id' => $this->integer(11)->null(),
            'ward_id' => $this->integer(11)->null(),
            'address' => $this->string(2048)->null(),
            'image' => $this->string(2048)->null(),
            'status' => $this->smallInteger()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createTable("{{%feature}}", [
            'id' => $this->primaryKey(11)->comment('synced with CTT'),
            'name' => $this->string(512)->null(),
            'position' => $this->integer(2),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'last_sync' => $this->integer()
        ]);
        $this->createTable("{{%feature_value}}", [
            'id' => $this->primaryKey(11)->comment('synced with CTT'),
            'feature_id' => $this->integer(11),
            'value' => $this->string(255),
        ]);
        $this->createTable("{{%category}}", [
            'id' => $this->primaryKey(11)->comment('synced with CTT'),
            'name' => $this->string(512)->null(),
            'description' => $this->text(),
            'rewrite' => $this->string(512)->null(),
            'parent_id' => $this->integer(11),
            'position' => $this->integer(2),
            'image' => $this->string(2048)->null(),
            'active' => $this->tinyInteger(2),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'last_sync' => $this->integer()
        ]);
        $this->createTable("{{%product}}", [
            'id' => $this->primaryKey(11)->comment('synced with CTT'),
            'ean13' => $this->string(50),
            'name' => $this->string(512)->null(),
            'description' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext')->null(),
            'rewrite' => $this->string(512)->null(),
            'unity' => $this->string(255),
            'price' => $this->float(2)->defaultValue(0)->notNull(),
            'images' => $this->text()->comment("array string of image urls"),
            'status' => $this->smallInteger()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'last_sync' => $this->integer()
        ]);

        $this->createTable("{{%product_category}}", [
            'product_id' => $this->integer(11)->notNull(),
            'category_id' => $this->integer(11)->notNull()
        ]);

        $this->createTable("{{%product_feature}}", [
            'product_id' => $this->integer(11)->notNull(),
            'feature_id' => $this->integer(11)->notNull(),
            'feature_value_id' => $this->integer(11)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210807_095624_add_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210807_095624_add_tables cannot be reverted.\n";

        return false;
    }
    */
}
