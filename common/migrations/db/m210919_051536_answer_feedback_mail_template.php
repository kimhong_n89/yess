<?php

use yii\db\Migration;

/**
 * Class m210919_051536_answer_feedback_mail_template
 */
class m210919_051536_answer_feedback_mail_template extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            INSERT INTO `template` (`code`,`title`,`content`,`type`,`created_at`,`updated_at`) VALUES ('email_answer_feedback','Góp ý của bạn đã được phản hồi','<p>Chào {ten_bac_si},</p><p>Admin đã phản hồi góp ý của bạn</p><p>Tiêu đề: {tieu_de_gop_y}</p><p>Nội  dung:</p><p>{noi_dung_gop_y}</p><p>Chúc bạn 1 ngày vui vẻ.</p>','email',1631680374,1631693546);
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210919_051536_answer_feedback_mail_template cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210919_051536_answer_feedback_mail_template cannot be reverted.\n";

        return false;
    }
    */
}
