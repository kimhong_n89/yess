<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_sys_auth_route`.
 */
class m160517_103953_create_table_sys_auth_route extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%sys_auth_route%}}',[
            'id' => $this->primaryKey(11),
            'route' => $this->string(255)->notNull()->unique()
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%sys_auth_route%}}');
    }
}
