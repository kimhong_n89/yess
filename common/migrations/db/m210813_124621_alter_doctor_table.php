<?php

use yii\db\Migration;

/**
 * Class m210813_124621_alter_doctor_table
 */
class m210813_124621_alter_doctor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("{{%doctor}}", 'auth_key', $this->string(32));
        $this->addColumn("{{%doctor}}", 'access_token', $this->string(40));
        $this->addColumn("{{%doctor}}", 'logged_at', $this->integer(11));
        $this->alterColumn("{{%prescription}}", 'status', $this->smallInteger(6)->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210813_124621_alter_doctor_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210813_124621_alter_doctor_table cannot be reverted.\n";

        return false;
    }
    */
}
