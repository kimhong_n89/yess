<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */


namespace common\components\utils;

/**
 * Html is an enhanced version of [[\yii\helpers\Html]] helper class dedicated to the Bootstrap needs.
 * This class inherits all functionality available at [[\yii\helpers\Html]] and can be used as substitute.
 *
 * Attention: do not confuse [[\yii\bootstrap\Html]] and [[\yii\helpers\Html]], be careful in which class
 * you are using inside your views.
 *
 * @author Alex
 */
class Html extends \yii\helpers\Html
{
   /**
     * Generates a hyperlink tag.
     * @param string $text link body. It will NOT be HTML-encoded. Therefore you can pass in HTML code
     * such as an image tag. If this is coming from end users, you should consider [[encode()]]
     * it to prevent XSS attacks.
     * @param array|string|null $url the URL for the hyperlink tag. This parameter will be processed by [[Url::to()]]
     * and will be used for the "href" attribute of the tag. If this parameter is null, the "href" attribute
     * will not be generated.
     *
     * If you want to use an absolute url you can call [[Url::to()]] yourself, before passing the URL to this method,
     * like this:
     *
     * ```php
     * Html::a('link text', Url::to($url, true))
     * ```
     *
     * @param array $options the tag options in terms of name-value pairs. These will be rendered as
     * the attributes of the resulting tag. The values will be HTML-encoded using [[encode()]].
     * If a value is null, the corresponding attribute will not be rendered.
     * See [[renderTagAttributes()]] for details on how attributes are being rendered.
     * @param array $permission name of permisson
     * @return string the generated hyperlink
     * @see \yii\helpers\Url::to()
     */
    
    public static function a($text, $url = null, $options = array(), $permission = [])
    {
        if (!empty($permission)) {
            foreach ($permission as $value) {
                // CASE DENY
                if ($value == PermissionConstant::PERMISSION_DENY) {
                    return '';
                }
                if (rbac_is_allow_exec($value) || $value == PermissionConstant::PERMISSION_ALLOW) {
                    return parent::a($text, $url, $options);
                }
            }
            return '';
        } else {
            // not have permission
            return parent::a($text, $url, $options);
        }
    }
    
    public static function button($content = 'Button', $options = [], $permission = [])
    {
        if (!empty($permission)) {
            foreach ($permission as $value) {
                // CASE DENY
                if ($value == PermissionConstant::PERMISSION_DENY) {
                    return '';
                }
                if (rbac_is_allow_exec($value) || $value == PermissionConstant::PERMISSION_ALLOW) {
                    return parent::button($content, $options);
                }
            }
            return '';
        } else {
            return parent::button($content, $options);
        }
    }
    
    public static function submitButton($content = 'Button', $options = [], $permission = [])
    {
        if (!empty($permission)) {
            foreach ($permission as $value) {
                // CASE DENY
                if ($value == PermissionConstant::PERMISSION_DENY) {
                    return '';
                }
                if (rbac_is_allow_exec($value) || $value == PermissionConstant::PERMISSION_ALLOW) {
                    return parent::submitButton($content, $options);
                }
            }
            return '';
        } else {
            return parent::submitButton($content, $options);
        }
    }
    
    public static function aWithPermission($text, $url = null, $options = array(), $permission = [], $params = [])
    {
        if (!empty($permission)) {
            foreach ($permission as $value) {
                // CASE DENY
                if ($value == PermissionConstant::PERMISSION_DENY) {
                    return '';
                }
                if (rbac_is_allow_exec($value, $params) || $value == PermissionConstant::PERMISSION_ALLOW) {
                    return parent::a($text, $url, $options);
                }
            }
            return '';
        } else {
            // not have permission
            return parent::a($text, $url, $options);
        }
    }
}
