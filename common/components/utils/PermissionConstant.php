<?php
namespace common\components\utils;

class PermissionConstant
{
    const PERMISSION_ALLOW = 'permission_allow';
    const PERMISSION_DENY = 'permission_deny';
    
    const LOGIN_TO_BACKEND = 'loginToBackend';
    
    /* Dashboard */
    const VIEW_DASHBOARD = 'dashboardView';
    const VIEW_STATISTIC = 'statisticView';
    const VIEW_NEW_PRESCRIPTION= 'newPrescription';
    const VIEW_CHART = 'chartView';
    const VIEW_LOGIN_HISTORY = 'loginHistoryView';
    
    /* Prescription */
    const VIEW_PRESCRIPTION = 'prescriptionView';
    const CREATE_PRESCRIPTION = 'prescriptionCreate';
    const UPDATE_PRESCRIPTION = 'prescriptionUpdate';
    const DELETE_PRESCRIPTION = 'prescriptionDelete';
    const PRINT_PRESCRIPTION = 'prescriptionPrint';
    const EXPORT_PRESCRIPTION = 'prescriptionExport';

    /* Doctor */
    const VIEW_DOCTOR = 'doctorView';
    const CREATE_DOCTOR = 'doctorCreate';
    const UPDATE_DOCTOR = 'doctorUpdate';
    const DELETE_DOCTOR = 'doctorDelete';
    const EXPORT_DOCTOR = 'doctorExport';
    
    /* Patient */
    const VIEW_PATIENT = 'patientView';
    const CREATE_PATIENT = 'patientCreate';
    const UPDATE_PATIENT = 'patientUpdate';
    const DELETE_PATIENT = 'patientDelete';
    const EXPORT_PATIENT = 'patientExport';
    
    /* RBAC */
    const MANAGE_RBAC = 'rbacManage';

    const MANAGE_FILE = 'fileManage';

    /*Danh muc*/
    const VIEW_STORE = 'storeView';
    const VIEW_PRODUCT = 'productView';
    const VIEW_DISEASE_GROUP = 'diseasegroupView';
    const VIEW_LOCATION = 'locationView';

    const MANAGE_MASTER_DATA = 'masterDataManage';

    /* User */
    const VIEW_USER = 'userView';
    const CREATE_USER = 'userCreate';
    const UPDATE_USER = 'userUpdate';
    const DELETE_USER = 'userDelete';
    
    /* Translation */
    const MANAGE_TRANSLATION = 'translationManage';

    /* Timeline Event */
    const VIEW_TIMELINE = 'timelineView';
    
    /* Manage Log */
    const MANAGE_LOG = 'logManage';

    const MANAGE_SETTING = 'settingManage';
    const MANAGE_FEEDBACK = 'feedbackManage';
}
