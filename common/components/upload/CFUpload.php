<?php

namespace common\components\upload;

use backend\modules\system\models\SystemLog;
use Yii;
use yii\base\DynamicModel;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\UploadedFile;

class CFUpload extends \trntv\filekit\actions\UploadAction
{
    static $blackList = ['text/x-php', 'text/plain', 'text/html'];

    //All uploads go through this function (frontend+backend)
    public function run()
    {
        $result = [];
        $uploadedFiles = UploadedFile::getInstancesByName($this->fileparam);

        foreach ($uploadedFiles as $uploadedFile) {
            /* @var \yii\web\UploadedFile $uploadedFile */
            $uploadedMime = FileHelper::getMimeType($uploadedFile->tempName);   //image/png
            //If someone intentionally upload unallowed mime files -> log him
            if (in_array($uploadedMime, self::$blackList) ||
                (!empty($this->validationRules[0]['mimeTypes']) && !in_array($uploadedMime, explode(', ', $this->validationRules[0]['mimeTypes'])))) {
                Yii::error('-->HACK UPLOAD mime:' . $uploadedMime . ' --Filename: ' . $uploadedFile->name . ' --from IP:' . getUserIP() . ' --userId:' . Yii::$app->user->identity->id, SystemLog::CATEGORY_HACKATTEMPT);
                copy($uploadedFile->tempName, Yii::getAlias('@frontend/runtime/wrongmime/') . $uploadedFile->name);

                //TODO add ip into blacklist.
            }

            $output = [
                $this->responseNameParam => Html::encode($uploadedFile->name),
                $this->responseMimeTypeParam => $uploadedFile->type,
                $this->responseSizeParam => $uploadedFile->size,
                $this->responseBaseUrlParam => $this->getFileStorage()->baseUrl
            ];

            if ($uploadedFile->error === UPLOAD_ERR_OK) {
                $validationModel = DynamicModel::validateData(['file' => $uploadedFile], $this->validationRules);
                if (!$validationModel->hasErrors()) {

                    $path = $this->getFileStorage()->save($uploadedFile);

                    if ($path) {
                        $output[$this->responsePathParam] = $path;
                        $output[$this->responseUrlParam] = $this->getFileStorage()->baseUrl . '/' . $path;
                        $output[$this->responseDeleteUrlParam] = Url::to([$this->deleteRoute, 'path' => $path]);
                        $paths = \Yii::$app->session->get($this->sessionKey, []);
                        $paths[] = $path;
                        \Yii::$app->session->set($this->sessionKey, $paths);
                        $this->afterSave($output);

                    } else {
                        $output['error'] = true;
                        $output['errors'] = [];
                    }

                } else {
                    $output['error'] = true;
                    $output['errors'] = $validationModel->getFirstError('file');
                }
            } else {
                $output['error'] = true;
                $output['errors'] = $this->resolveErrorMessage($uploadedFile->error);
            }

            $result['files'][] = $output;
        }
        return $this->multiple ? $result : array_shift($result);
    }
}