<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\widgets;

use yii\bootstrap4\Modal;

class MyModal extends Modal
{
    public function init()
    {
        parent::init();
        echo $this->run();
    }
}
