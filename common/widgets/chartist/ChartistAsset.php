<?php
/**
 * @package okeanos\chartist
 * @author Nikolas Grottendieck <github@nikolasgrottendieck.com>
 * @copyright Copyright &copy; Nikolas Grottendieck
 * @license BSD-3-Clause
 * @version 1.0
 */

namespace common\widgets\chartist;

use yii;
use yii\web\AssetBundle;
/**
 * Asset Manager for Chartist
 *
 * @author Nikolas Grottendieck <github@nikolasgrottendieck.com>
 * @since  1.0
 */
class ChartistAsset extends AssetBundle
{
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        parent::init();
    }

    /**
     * @var array
     */
    public $css = [
        'https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css',
        'chartist-plugin-tooltip.css'
    ];
    /**
     * @var array
     */
    public $js = [
        'https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js',
        'chartist-plugin-pointlabels.js',
        'chartist-plugin-tooltip.js',
    ];

    public $publishOptions = [
        'only' => [
            '*.css',
            '*.js',
            '../img/*'
        ],
        "forceCopy" => YII_ENV_DEV,
    ];

    /**
     * Sets the source path if empty
     * @param string $path the path to be set
     */
    protected function setSourcePath($path)
    {
        if (empty($this->sourcePath)) {
            $this->sourcePath = $path;
        }
    }

    /**
     * @var array
     */
    public $depends = [
        //BackendAsset::class
    ];

}