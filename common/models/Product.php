<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string|null $ean13
 * @property string|null $name
 * @property string|null $description
 * @property string|null $rewrite
 * @property string|null $unity
 * @property int|null $store_id
 * @property float $price
 * @property string|null $images array string of image urls
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $last_sync
 *
 * @property ProductCategory[] $productCategories
 * @property ProductFeature[] $productFeatures
 * @property Store $store
 */
class Product extends \common\models\BaseSyncModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'required'],
            [['id', 'store_id', 'status', 'created_at', 'updated_at', 'last_sync'], 'integer'],
            [['description', 'images'], 'string'],
            [['price'], 'number'],
            [['ean13'], 'string', 'max' => 50],
            [['name', 'rewrite'], 'string', 'max' => 512],
            [['unity'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['store_id'], 'exist', 'skipOnError' => true, 'targetClass' => Store::className(), 'targetAttribute' => ['store_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ean13' => Yii::t('app', 'Ean13'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'rewrite' => Yii::t('app', 'Rewrite'),
            'unity' => Yii::t('app', 'Unity'),
            'store_id' => Yii::t('app', 'Store ID'),
            'price' => Yii::t('app', 'Price'),
            'images' => Yii::t('app', 'Images'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'last_sync' => Yii::t('app', 'Last Sync'),
        ];
    }

    /**
     * Gets query for [[ProductCategories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategories()
    {
        return $this->hasMany(ProductCategory::className(), ['product_id' => 'id']);
    }

    /**
     * Gets query for [[ProductFeatures]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductFeatures()
    {
        return $this->hasMany(ProductFeature::className(), ['product_id' => 'id']);
    }

    /**
     * Gets query for [[Store]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStore()
    {
        return $this->hasOne(Store::className(), ['id' => 'store_id']);
    }
}
