<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ward".
 *
 * @property int $id_ward
 * @property int $id_district
 * @property string|null $name
 * @property int|null $vt_ward_id
 * @property District $district
 */
class Ward extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ward';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_district'], 'required'],
            [['id_district', 'vt_ward_id'], 'integer'],
            [['name'], 'string', 'max' => 512],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_ward' => Yii::t('common', 'Id'),
            'id_district' => Yii::t('common', 'Quận/Huyện'),
            'name' => Yii::t('common', 'Tên'),
            'vt_ward_id' => Yii::t('common', 'Vt Ward ID'),
        ];
    }

    public function getDistrict()
    {
        return $this->hasOne(District::className(), ['id_district' => 'id_district']);
    }
}
