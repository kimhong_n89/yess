<?php

namespace common\models;

use common\commands\SendEmailCommand;
use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "doctor".
 *
 * @property int $id
 * @property string|null $fullname
 * @property int|null $gender 0: Nữ, 1: Nam, 2: Other
 * @property int|null $day_of_birth
 * @property int|null $month_of_birth
 * @property int|null $year_of_birth
 * @property string $phone
 * @property string|null $email
 * @property string|null $avatar_path
 * @property int|null $province_id
 * @property int|null $district_id
 * @property int|null $ward_id
 * @property string|null $address
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property int $logged_at
 * @property string $auth_key
 * @property string $access_token
 * @property string|null $clinic_name
 *
 * @property Certification[] $certifications
 * @property District $district
 * @property DoctorSetting[] $doctorSettings
 * @property FavoriteStore[] $favoriteStores
 * @property Prescription[] $prescriptions
 * @property Province $province
 * @property Ward $ward
 * @property PrescriptionDiagnose[] $prescriptionDiagnoses
 */
class Doctor extends GeneralModel
{
    const SCENARIO_UPDATE_PROFILE = 'update-profile';
    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;

    const GENDER_FEMALE = 0;
    const GENDER_MALE = 1;
    const GENDER_OTHER = 2;

    public $avatar;

    public $attachments;

    public $arr_business_field;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doctor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fullname', 'email', 'gender', 'day_of_birth', 'month_of_birth', 'year_of_birth', 'province_id', 'district_id', 'ward_id'], 'required', 'on' => self::SCENARIO_UPDATE_PROFILE],
            [['gender', 'day_of_birth', 'month_of_birth', 'year_of_birth', 'province_id', 'district_id', 'ward_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['phone', 'status'], 'required'],
            [['fullname', 'email'], 'string', 'max' => 100],
            [['phone'], 'unique'],
            ['phone', 'match', 'pattern' => Yii::$app->params['phone_pattern']],
            [['phone'], 'string', 'max' => 20],
            [['clinic_name'], 'string', 'max' => 255],
            [['avatar_path'], 'string', 'max' => 1024],
            [['address'], 'string', 'max' => 255],
            [['district_id'], 'exist', 'skipOnError' => true, 'targetClass' => District::className(), 'targetAttribute' => ['district_id' => 'id_district']],
            [['province_id'], 'exist', 'skipOnError' => true, 'targetClass' => Province::className(), 'targetAttribute' => ['province_id' => 'id_state']],
            [['ward_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ward::className(), 'targetAttribute' => ['ward_id' => 'id_ward']],
            [['avatar', 'attachments', 'arr_business_field'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'fullname' => Yii::t('common', 'Họ và tên'),
            'phone' => Yii::t('common', 'Số điện thoại'),
            'email' => Yii::t('common', 'Email'),
            'gender' => Yii::t('common', 'Giới tính'),
            'province_id' => Yii::t('common', 'Khu vực'),
            'district_id' => Yii::t('common', 'Quận/Huyện'),
            'ward_id' => Yii::t('common', 'Phường/Xã'),
            'address' => Yii::t('common', 'Chi tiết địa chỉ'),
            'day_of_birth' => Yii::t('common', 'Ngày sinh'),
            'month_of_birth' => Yii::t('common', 'Tháng sinh'),
            'year_of_birth' => Yii::t('common', 'Năm sinh'),
            'avatar_path' => Yii::t('common', 'Avatar'),
            'status' => Yii::t('common', 'Trạng thái'),
            'created_at' => Yii::t('common', 'Tạo lúc'),
            'updated_at' => Yii::t('common', 'Cập nhật lúc'),
            'attachments' => Yii::t('common', 'Hình ảnh văn bằng'),
            'arr_business_field' => Yii::t('common', 'Chuyên khoa'),
            'clinic_name' => Yii::t('common', 'Tên phòng khám'),

        ];
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => UploadBehavior::class,
                'attribute' => 'avatar',
                'pathAttribute' => 'avatar_path',
            ],
            [
                'class' => UploadBehavior::class,
                'attribute' => 'attachments',
                'multiple' => true,
                'uploadRelation' => 'certifications',
                'pathAttribute' => 'path',
                'orderAttribute' => 'order',
                'typeAttribute' => 'type',
                'sizeAttribute' => 'size',
                'nameAttribute' => 'name',
            ],
            'auth_key' => [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'auth_key'
                ],
                'value' => Yii::$app->getSecurity()->generateRandomString()
            ],
            'access_token' => [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'access_token'
                ],
                'value' => function () {
                    return Yii::$app->getSecurity()->generateRandomString(40);
                }
            ]
        ]);
    }

    /**
     * @return array statuses list
     */
    public static function statuses()
    {
        return [
            self::STATUS_PENDING => Yii::t('common', 'Chờ Duyệt'),
            self::STATUS_APPROVED => Yii::t('common', 'Đã Duyệt'),
            self::STATUS_DELETED => Yii::t('common', 'Đã Xoá')
        ];
    }

    /**
     * Gets query for [[Certifications]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCertifications()
    {
        return $this->hasMany(Certification::className(), ['doctor_id' => 'id']);
    }

    /**
     * Gets query for [[District]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(District::className(), ['id_district' => 'district_id']);
    }

    /**
     * Gets query for [[DoctorSettings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDoctorSettings()
    {
        return $this->hasMany(DoctorSetting::className(), ['doctor_id' => 'id']);
    }

    /**
     * Gets query for [[FavoriteStores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFavoriteStores()
    {
        return $this->hasMany(FavoriteStore::className(), ['doctor_id' => 'id']);
    }

    /**
     * Gets query for [[Prescriptions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPrescriptions()
    {
        return $this->hasMany(Prescription::className(), ['doctor_id' => 'id']);
    }

    /**
     * Gets query for [[Province]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvince()
    {
        return $this->hasOne(Province::className(), ['id_state' => 'province_id']);
    }

    public static function genders()
    {
        return [
            self::GENDER_MALE => Yii::t('common', 'Nam'),
            self::GENDER_FEMALE => Yii::t('common', 'Nữ'),
            self::GENDER_OTHER => Yii::t('common', 'Khác'),
        ];
    }
    /**
     * Gets query for [[Ward]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWard()
    {
        return $this->hasOne(Ward::className(), ['id_ward' => 'ward_id']);
    }

    public function getAvatarUrl($default = null)
    {
        if(!empty($this->avatar_path)) {
            return \Yii::$app->params['baseUrl'] . '/' . $this->avatar_path;
        }
        return $default;
    }

    public function getFullAddress()
    {
        $full_address = $this->address;
        $ward = Ward::find()->where(['id_ward' => $this->ward_id])->one();
        $district = District::find()->where(['id_district' => $this->district_id])->one();
        $province = Province::find()->where(['id_state' => $this->province_id])->one();
        $full_address .= (empty($full_address) ? "" : ", ") . (empty($ward) ? "" : $ward->name);
        $full_address .= (empty($full_address) ? "" : ", ") . (empty($district) ? "" : $district->name);
        $full_address .= (empty($full_address) ? "" : ", ") . (empty($province) ? "" : $province->name);
        return $full_address;
    }

    public function getFullBirthday()
    {
        return "{$this->day_of_birth}/{$this->month_of_birth}/{$this->year_of_birth}";
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();
        $settings = ArrayHelper::index(DoctorSetting::find()->where(['doctor_id' => $this->id])->asArray()->all(), 'id', 'key');
        $business_field = @$settings[DoctorSetting::KEY_BUSINESS_FIELD];
        $this->arr_business_field = $business_field ? ArrayHelper::getColumn($business_field, 'value') : [];
    }

    public function afterSave ( $insert, $changedAttributes )
    {
        parent::afterSave($insert, $changedAttributes);
        $this->saveBusinessField();
        if (!$insert && in_array('status', $changedAttributes) && $this->status == self::STATUS_APPROVED) {
            //send_mail
            if ($this->email) {
                $template = Template::find()->where(['code' => Template::EMAIL_APPROVE_DOCTOR])->one();
                if ($template) {
                    $search = ['{ten_bac_si}'];
                    $replace = [$this->fullname];
                    $body = str_replace($search, $replace, $template->content);
                    try {
                        return Yii::$app->commandBus->handle(new SendEmailCommand([
                            'to' => $this->email,
                            'subject' => str_replace($search, $replace, $template->title),
                            'view' => 'approveDoctor',
                            'params' => [
                                'body' => $body
                            ]
                        ]));
                    } catch (\Swift_TransportException $exc) {
                        Yii::error("email sending failed due to error: " . $exc->getMessage());
                    }
                }
            }
        }
    }
    public function saveBusinessField()
    {
        $this->arr_business_field = is_array($this->arr_business_field) ? $this->arr_business_field : [];
        if (!$this->isNewRecord) {
            // update // delete all
            DoctorSetting::deleteAll(['doctor_id' => $this->id, 'key' => DoctorSetting::KEY_BUSINESS_FIELD]);
        }
        // insert it.
        foreach ($this->arr_business_field as $id) {
            $model = new DoctorSetting();
            $model->doctor_id = $this->id;
            $model->value = $id;
            $model->key = DoctorSetting::KEY_BUSINESS_FIELD;
            $model->save();
        }
    }

    public function getBusinessFields()
    {
        return BusinessField::find()->innerJoin('doctor_setting', '`business_field`.`id` = `doctor_setting`.`value` AND `doctor_setting`.`key` = :key', [':key' => DoctorSetting::KEY_BUSINESS_FIELD])->asArray()->all();
    }
}
