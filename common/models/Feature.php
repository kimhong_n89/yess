<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "feature".
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $position
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $last_sync
 *
 * @property ProductFeature[] $productFeatures
 */
class Feature extends \common\models\BaseSyncModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feature';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'position', 'created_at', 'updated_at', 'last_sync'], 'integer'],
            [['name'], 'string', 'max' => 512],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'position' => Yii::t('app', 'Position'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'last_sync' => Yii::t('app', 'Last Sync'),
        ];
    }

    /**
     * Gets query for [[ProductFeatures]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductFeatures()
    {
        return $this->hasMany(ProductFeature::className(), ['feature_id' => 'id']);
    }
}
