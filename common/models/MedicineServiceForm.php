<?php

namespace common\models;

use common\models\PrescriptionDetail;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Create medicine form
 */
class MedicineServiceForm extends MedicineForm
{
    public $type = PrescriptionDetail::TYPE_MEDICINE_SERVICE;
}
