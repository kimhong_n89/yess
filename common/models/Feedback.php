<?php

namespace common\models;

use common\commands\SendEmailCommand;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "feedback".
 *
 * @property int $id
 * @property int|null $doctor_id
 * @property int|null $user_id
 * @property string|null $title
 * @property string|null $content
 * @property string|null $answer
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Doctor $doctor
 * @property User $user
 */
class Feedback extends ActiveRecord
{
    //for search
    public $doctor_name;
    public $user_name;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content', 'title'], 'required'],
            [['doctor_id', 'user_id', 'created_at', 'updated_at'], 'integer'],
            [['content', 'answer'], 'string'],
            [['title'], 'string', 'max' => 1024],
            [['doctor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Doctor::className(), 'targetAttribute' => ['doctor_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'doctor_id' => Yii::t('common', 'Bác sĩ'),
            'user_id' => Yii::t('common', 'Người trả lời'),
            'title' => Yii::t('common', 'Tiêu đề'),
            'content' => Yii::t('common', 'Nội dung'),
            'answer' => Yii::t('common', 'Phản hồi'),
            'created_at' => Yii::t('common', 'Tạo lúc'),
            'updated_at' => Yii::t('common', 'Trả lời lúc'),
        ];
    }

    /**
     * Gets query for [[Doctor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDoctor()
    {
        return $this->hasOne(Doctor::className(), ['id' => 'doctor_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function beforeSave($insert) {
        if (Yii::$app->user->identity instanceof Doctor) {
            $this->doctor_id = Yii::$app->user->identity->id;
        } else if(Yii::$app->user->identity instanceof User){
            $this->user_id = Yii::$app->user->identity->id;
        }
        ;
        return parent::beforeSave($insert);
    }

    public function sendMailNotify()
    {
        if ($this->answer) {
            $template = Template::find()->where(['code' => Template::EMAIL_SEND_ANSWER_FEEDBACK])->one();
            if ($this->doctor->email && $template) {
                $search = ['{ten_bac_si}', '{tieu_de_gop_y}', '{noi_dung_gop_y}'];
                $replace = [$this->doctor->fullname, $this->title, $this->content];
                $body = str_replace($search, $replace, $template->content);
                try {
                    return Yii::$app->commandBus->handle(new SendEmailCommand([
                        'to' => $this->doctor->email,
                        'subject' => str_replace($search, $replace, $template->title),
                        'view' => 'answerFeedback',

                        'params' => [
                            'body' => $body
                        ]
                    ]));
                } catch (\Swift_TransportException $exc) {
                    Yii::error("email sending failed due to error: " . $exc->getMessage());
                }
            }
        }

        return false;
    }
}
