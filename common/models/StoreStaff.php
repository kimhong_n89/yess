<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "store_staff".
 *
 * @property int $id
 * @property int $store_id
 * @property string|null $name
 * @property string|null $phone
 * @property string|null $email
 * @property int|null $province_id
 * @property int|null $district_id
 * @property int|null $ward_id
 * @property string|null $address
 * @property string|null $image
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Store $store
 */
class StoreStaff extends \common\models\BaseSyncModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'store_staff';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['store_id', 'status'], 'required'],
            [['store_id', 'province_id', 'district_id', 'ward_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 512],
            [['phone'], 'string', 'max' => 20],
            [['email'], 'string', 'max' => 255],
            [['address', 'image'], 'string', 'max' => 2048],
            [['store_id'], 'exist', 'skipOnError' => true, 'targetClass' => Store::className(), 'targetAttribute' => ['store_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'store_id' => Yii::t('app', 'Store ID'),
            'name' => Yii::t('app', 'Name'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'province_id' => Yii::t('app', 'Province ID'),
            'district_id' => Yii::t('app', 'District ID'),
            'ward_id' => Yii::t('app', 'Ward ID'),
            'address' => Yii::t('app', 'Address'),
            'image' => Yii::t('app', 'Image'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Store]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStore()
    {
        return $this->hasOne(Store::className(), ['id' => 'store_id']);
    }
}
