<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "favorite_store".
 *
 * @property int $id
 * @property int $doctor_id
 * @property int $store_id
 *
 * @property Doctor $doctor
 */
class FavoriteStore extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'favorite_store';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['doctor_id', 'store_id'], 'required'],
            [['doctor_id', 'store_id'], 'integer'],
            [['doctor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Doctor::className(), 'targetAttribute' => ['doctor_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'doctor_id' => Yii::t('common', 'Doctor ID'),
            'store_id' => Yii::t('common', 'Store ID'),
        ];
    }

    /**
     * Gets query for [[Doctor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDoctor()
    {
        return $this->hasOne(Doctor::className(), ['id' => 'doctor_id']);
    }
}
