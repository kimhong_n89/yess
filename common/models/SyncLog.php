<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sync_log".
 *
 * @property int $id
 * @property string|null $table_name
 * @property int|null $min_id
 * @property int|null $max_id
 * @property int $status
 * @property string|null $message
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class SyncLog extends \common\models\GeneralModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sync_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['min_id', 'max_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['status'], 'required'],
            [['message'], 'string'],
            [['table_name'], 'string', 'max' => 512],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'table_name' => Yii::t('app', 'Table Name'),
            'min_id' => Yii::t('app', 'Min ID'),
            'max_id' => Yii::t('app', 'Max ID'),
            'status' => Yii::t('app', 'Status'),
            'message' => Yii::t('app', 'Message'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
