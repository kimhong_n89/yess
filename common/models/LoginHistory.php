<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "login_history".
 *
 * @property int $id
 * @property string $phone_number
 * @property string $ip
 * @property int $otp_code
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property string $device_agent
 */
class LoginHistory extends GeneralModel
{
    public $fullname;
    public $email;

    const STATUS_NOT_ACTIVATE_YET = 1;
    const STATUS_ACTIVATED = 2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'login_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['phone_number', 'ip', 'otp_code', 'status', 'created_at', 'updated_at', 'device_agent'], 'required'],
            [['otp_code', 'status', 'created_at', 'updated_at'], 'integer'],
            [['phone_number', 'ip'], 'string', 'max' => 20],
            [['device_agent'], 'string', 'max' => 1024],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'phone_number' => Yii::t('common', 'Số điện thoại'),
            'ip' => Yii::t('common', 'Ip'),
            'otp_code' => Yii::t('common', 'Otp Code'),
            'status' => Yii::t('common', 'Trạng thái'),
            'created_at' => Yii::t('common', 'Lúc'),
            'updated_at' => Yii::t('common', 'Updated At'),
            'device_agent' => Yii::t('common', 'Device Agent'),
        ];
    }

    public static function statuses()
    {
        return [
            self::STATUS_NOT_ACTIVATE_YET => Yii::t('common', 'Chưa đăng nhập'),
            self::STATUS_ACTIVATED => Yii::t('common', 'Thành công'),
            self::STATUS_DELETED => Yii::t('common', 'Đã xoá'),
        ];
    }
}
