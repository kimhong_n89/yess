<?php

namespace common\models;

use common\commands\SendEmailCommand;
use Yii;

/**
 * This is the model class for table "printed_report".
 *
 * @property int $id
 * @property int $prescription_id
 * @property string $html
 * @property int $created_at
 * @property int $version
 * @property int $mail_sent
 * @property string|null $hash
 */
class PrintedReport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'printed_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prescription_id', 'html'], 'required'],
            [['prescription_id', 'created_at', 'version', 'mail_sent'], 'integer'],
            [['html'], 'string'],
            [['hash'], 'string', 'max' => 50],
            [['hash'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'prescription_id' => Yii::t('common', 'Prescription ID'),
            'html' => Yii::t('common', 'Html'),
            'created_at' => Yii::t('common', 'Created At'),
            'hash' => Yii::t('common', 'Hash'),
            'version' => Yii::t('common', 'Version'),
            'mail_sent' => Yii::t('common', 'Mail Sent'),
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($insert) {
            $this->created_at = time();
            $this->hash = uniqid();
        }
        return true;
    }

    public function sendEmailPrescription()
    {
        $prescription = Prescription::findOne($this->prescription_id);
        if ($prescription) {
            $template = Template::find()->where(['code' => Template::EMAIL_SEND_PRESCRIPTION])->one();
            if ($prescription->patient->email && $template) {
                $url = Yii::$app->urlManager->createAbsoluteUrl(['site/view-prescription', 'token' => PrintedReport::getEncodeToken($this->hash)]);
                $search = ['{ma_don_thuoc}', '{ten_benh_nhan}', '{ten_bac_si}', '{url_view}'];
                $replace = [$prescription->getCode(), $prescription->patient->fullname, $prescription->doctor->fullname, $url];
                $body = str_replace($search, $replace, $template->content);
                try {
                    $result = Yii::$app->commandBus->handle(new SendEmailCommand([
                        'to' => $prescription->patient->email,
                        'subject' => str_replace($search, $replace, $template->title),
                        'view' => 'sendPrescription',

                        'params' => [
                            'body' => $body
                        ]
                    ]));
                    if ($result) {
                        $this->mail_sent = 1;
                        $this->save(false);
                    }
                    return $result;
                } catch (\Swift_TransportException $exc) {
                    Yii::error("email sending failed due to error: " . $exc->getMessage());
                }
            }
        }

        return false;
    }

    public static function getEncodeToken($string)
    {
        return base64_encode($string);
    }

    public static function getDecodeToken($string)
    {
        return base64_decode($string);
    }

}
