<?php
namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class BaseSyncModel extends GeneralModel
{
    /**
     * Table will be synced
     * @return string
     */
    protected function syncTableName()
    {
        return self::tableName();
    }

    /**
     * Fields not be synced or get default values
     * @return array
     */
    protected function attributeIgnored()
    {
        return [];
    }

    /**
     * Convert key from model & input
     * [ModelKey => InputArrayKey, ModelKey => InputArrayKey, ...]
     * @return array
     */
    protected function attributeMapped()
    {
        return [];
    }

    /**
     * Fields with default values
     * @return array
     */
    protected function attributeDefaults()
    {
        return [
            'created_at' => time(),
            'updated_at' => time(),
            'last_sync' => time(),
        ];
    }

    /**
     * Build the query
     * @param $jsonArray
     * @return string
     */
    protected function buildItemQuery($jsonArray)
    {
        if(!empty($jsonArray))
        {
            foreach ($this->attributeDefaults() as $key => $val)
            {
                if(!isset($jsonArray[$key]) || empty($jsonArray[$key])) $jsonArray[$key] = $val;
            }
            $query = "";
            foreach ($this->attributeLabels() as $key => $label)
            {
                if(!in_array($key, $this->attributeIgnored()))
                {
                    $query .= empty($query) ? "" : ",";
                    $query .= ":{$key}";
                }
            }
            $command = Yii::$app->db->createCommand("({$query})");
            $mappedKeys = $this->attributeMapped();
            foreach ($this->attributeLabels() as $key => $label)
            {
                $value = isset($jsonArray[$key]) ? $jsonArray[$key] : null;
                if(isset($mappedKeys[$key])) {
                    $value = isset($jsonArray[$mappedKeys[$key]]) ? $jsonArray[$mappedKeys[$key]] : $value;
                }
                $command = $command->bindValue(":{$key}", $value);
            }
            return $command->getRawSql();
        }
    }

    /**
     * Build the list of Query
     * @param $listArray
     * @return string
     */
    public function buildSyncInsertQuery($listArray)
    {
        $query = "";
        foreach ($this->attributeLabels() as $key => $label)
        {
            if(!in_array($key, $this->attributeIgnored()))
            {
                $query .= empty($query) ? "" : ",";
                $query .= "{$key}";
            }
        }
        $query = "REPLACE INTO " . $this->syncTableName() . " (" . $query . ") VALUES";
        $itemQuery = "";
        foreach ($listArray as $array)
        {
            $itemQuery .= empty($itemQuery) ? "" : ",";
            $itemQuery .= $this->buildItemQuery($array);
        }
        return $query . $itemQuery. ";";
    }
}