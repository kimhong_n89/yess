<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "feature_value".
 *
 * @property int $id
 * @property int|null $feature_id
 * @property string|null $value
 *
 * @property ProductFeature[] $productFeatures
 */
class FeatureValue extends \common\models\BaseSyncModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feature_value';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'feature_id'], 'integer'],
            [['value'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'feature_id' => Yii::t('app', 'Feature ID'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

    /**
     * Gets query for [[ProductFeatures]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductFeatures()
    {
        return $this->hasMany(ProductFeature::className(), ['feature_value_id' => 'id']);
    }
}
