<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "district".
 *
 * @property int $id_district
 * @property int $id_state
 * @property string|null $name
 * @property int|null $vt_district_id
 * @property Province $province
 */
class District extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'district';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_state'], 'required'],
            [['id_state', 'vt_district_id'], 'integer'],
            [['name'], 'string', 'max' => 512],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_district' => Yii::t('common', 'Id'),
            'id_state' => Yii::t('common', 'Tỉnh/Thành phố'),
            'name' => Yii::t('common', 'Tên'),
            'vt_district_id' => Yii::t('common', 'Vt District ID'),
        ];
    }

    public function getProvince()
    {
        return $this->hasOne(Province::className(), ['id_state' => 'id_state']);
    }
}
