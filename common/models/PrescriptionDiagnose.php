<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prescription_diagnose".
 *
 * @property int $id
 * @property int $prescription_id
 * @property int $diagnose_id
 */
class PrescriptionDiagnose extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prescription_diagnose';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prescription_id', 'diagnose_id'], 'required'],
            [['prescription_id', 'diagnose_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'prescription_id' => Yii::t('common', 'Prescription ID'),
            'diagnose_id' => Yii::t('common', 'Diagnose ID'),
        ];
    }
}
