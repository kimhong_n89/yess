<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "store_field".
 *
 * @property int $id
 * @property int $store_id
 * @property int $field_id
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $last_sync
 *
 * @property BusinessField $field
 * @property Store $store
 */
class StoreField extends \common\models\BaseSyncModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'store_field';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['store_id', 'field_id'], 'required'],
            [['store_id', 'field_id', 'created_at', 'updated_at', 'last_sync'], 'integer'],
            [['field_id'], 'exist', 'skipOnError' => true, 'targetClass' => BusinessField::className(), 'targetAttribute' => ['field_id' => 'id']],
            [['store_id'], 'exist', 'skipOnError' => true, 'targetClass' => Store::className(), 'targetAttribute' => ['store_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'store_id' => Yii::t('app', 'Store ID'),
            'field_id' => Yii::t('app', 'Field ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'last_sync' => Yii::t('app', 'Last Sync'),
        ];
    }

    /**
     * Gets query for [[Field]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getField()
    {
        return $this->hasOne(BusinessField::className(), ['id' => 'field_id']);
    }

    /**
     * Gets query for [[Store]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStore()
    {
        return $this->hasOne(Store::className(), ['id' => 'store_id']);
    }
}
