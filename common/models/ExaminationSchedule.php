<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "examination_schedule".
 *
 * @property int $id
 * @property int|null $prescription_id
 * @property int|null $doctor_id
 * @property int|null $patient_id
 * @property string|null $note
 * @property string|null $date
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Doctor $doctor
 * @property Patient $patient
 * @property Prescription $prescription
 */
class ExaminationSchedule extends GeneralModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'examination_schedule';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prescription_id', 'doctor_id', 'patient_id', 'created_at', 'updated_at'], 'integer'],
            [['date'], 'safe'],
            [['note'], 'string', 'max' => 1024],
            [['doctor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Doctor::className(), 'targetAttribute' => ['doctor_id' => 'id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Patient::className(), 'targetAttribute' => ['patient_id' => 'id']],
            [['prescription_id'], 'exist', 'skipOnError' => true, 'targetClass' => Prescription::className(), 'targetAttribute' => ['prescription_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'prescription_id' => Yii::t('common', 'Toa thuốc'),
            'doctor_id' => Yii::t('common', 'Bác sĩ'),
            'patient_id' => Yii::t('common', 'Bệnh nhân'),
            'note' => Yii::t('common', 'Ghi chú'),
            'date' => Yii::t('common', 'Ngày tái khám'),
            'created_at' => Yii::t('common', 'Created At'),
            'updated_at' => Yii::t('common', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Doctor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDoctor()
    {
        return $this->hasOne(Doctor::className(), ['id' => 'doctor_id']);
    }

    /**
     * Gets query for [[Patient]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    /**
     * Gets query for [[Prescription]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPrescription()
    {
        return $this->hasOne(Prescription::className(), ['id' => 'prescription_id']);
    }
}
