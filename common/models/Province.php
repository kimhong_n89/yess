<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "province".
 *
 * @property int $id_state
 * @property int $id_country
 * @property int $id_zone
 * @property string $name
 * @property string $iso_code
 * @property int $tax_behavior
 * @property int $active
 * @property int|null $vt_state_id
 */
class Province extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'province';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_country', 'id_zone', 'name', 'iso_code'], 'required'],
            [['id_country', 'id_zone', 'tax_behavior', 'active', 'vt_state_id'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['iso_code'], 'string', 'max' => 7],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_state' => Yii::t('common', 'Id'),
            'id_country' => Yii::t('common', 'Id Country'),
            'id_zone' => Yii::t('common', 'Id Zone'),
            'name' => Yii::t('common', 'Tên'),
            'iso_code' => Yii::t('common', 'Iso Code'),
            'tax_behavior' => Yii::t('common', 'Tax Behavior'),
            'active' => Yii::t('common', 'Active'),
            'vt_state_id' => Yii::t('common', 'Vt State ID'),
        ];
    }
}
