<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sms_history".
 *
 * @property int $id
 * @property string|null $phone_number
 * @property string|null $content
 * @property string|null $params
 * @property string|null $result
 * @property string|null $status
 * @property string $sms_type OTP, REF
 * @property int|null $user_id
 * @property int|null $updated_at
 * @property int|null $created_at
 */
class SmsHistory extends \common\models\GeneralModel
{
    const SMS_TYPE_OTP = "OTP";
    const SMS_TYPE_SMS = "SMS";

    const SMS_STATUS_SUCCESS = "SUCCESS";
    const SMS_STATUS_FAILED = "FAILED";
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sms_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'updated_at', 'created_at'], 'integer'],
            [['phone_number'], 'string', 'max' => 50],
            [['content', 'result'], 'string', 'max' => 1024],
            [['params'], 'string', 'max' => 2048],
            [['status', 'sms_type'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'phone_number' => Yii::t('app', 'Phone Number'),
            'content' => Yii::t('app', 'Content'),
            'params' => Yii::t('app', 'Params'),
            'result' => Yii::t('app', 'Result'),
            'status' => Yii::t('app', 'Status'),
            'sms_type' => Yii::t('app', 'Sms Type'),
            'user_id' => Yii::t('app', 'User ID'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * Send OTP SMS Over Brandname
     * @param $phone
     * @param $otp_code
     * @return bool
     */
    public static function sendOTPSMS($phone, $otp_code)
    {
        $content = env("SMS_TEMPLATE_OTP");
        if(!empty($content)) {
            $content = str_replace("{{otp_code}}", $otp_code, $content);
            self::sendSMS($phone, $content);
            return true;
        }
        return false;
    }

    /**
     * Send SMS
     * @param $phone
     * @param $content
     * @param string $sms_type
     * @param null $user_id
     * @return array|\SimpleXMLElement
     */
    public static function sendSMS($phone, $content, $sms_type = SmsHistory::SMS_TYPE_OTP, $user_id = null)
    {
        $clientNo = "clientNo=".env('SMS_CLIENT_NUMBER')."&";
        $clientPass = "clientPass=".env('SMS_CLIENT_PASSWORD')."&";
        $senderName = "senderName=".env('SMS_BRAND_NAME')."&";
        $phoneNumber = "phoneNumber=" . $phone . "&";
        $smsMessage = "smsMessage=" . $content . "&";
        $default = "&smsGUID=0&serviceType=0";
        $url_header = "http://center.fibosms.com/service.asmx/SendMaskedSMS?";
        $url_content = $clientNo . $clientPass . $senderName . $phoneNumber . str_replace(array('+', ' '), '%20', $smsMessage) . $default;
        $history = new SmsHistory();
        $history->phone_number = $phone;
        $history->content = $content;
        $history->params = json_encode(explode("&", $url_content));
        $history->status = self::SMS_STATUS_SUCCESS;
        $history->sms_type = $sms_type;
        $history->user_id = $user_id;
        $history->save();
        try {
            $json_response = self::execGetRequest($url_header, $url_content);
            $encode_result = simplexml_load_string($json_response);
            $history->result = $json_response;
            $history->save();
            return $encode_result;
        } catch (\Exception $e) {
            $history->status = self::SMS_STATUS_FAILED;
            $history->result = $e->getMessage();
            $history->save();
            return array($e->getMessage(), $url_content);
        }
    }

    /**
     * Make request connection to URL
     * @param $url_header
     * @param $url_content
     * @return mixed
     */
    private static function execGetRequest($url_header,$url_content)
    {
        // open connection
        $ch = curl_init();

        $final_url = $url_header. $url_content;
        curl_setopt($ch, CURLOPT_URL, $final_url);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // execute post
        $result = curl_exec($ch);

        // close connection
        curl_close($ch);
        return $result;
    }
}
