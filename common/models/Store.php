<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "store".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $phone
 * @property string|null $email
 * @property int|null $province_id
 * @property int|null $district_id
 * @property int|null $ward_id
 * @property string|null $address
 * @property string|null $image
 * @property int|null $doctor_id
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $last_sync
 *
 * @property Product[] $products
 * @property StoreField[] $storeFields
 * @property StoreStaff[] $storeStaff
 * @property District $district
 * @property Province $province
 * @property Ward $ward
 */
class Store extends \common\models\BaseSyncModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'store';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'required'],
            [['id', 'province_id', 'district_id', 'ward_id', 'doctor_id', 'status', 'created_at', 'updated_at', 'last_sync'], 'integer'],
            [['name'], 'string', 'max' => 512],
            [['phone'], 'string', 'max' => 20],
            [['email'], 'string', 'max' => 255],
            [['address', 'image'], 'string', 'max' => 2048],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'province_id' => Yii::t('app', 'Province ID'),
            'district_id' => Yii::t('app', 'District ID'),
            'ward_id' => Yii::t('app', 'Ward ID'),
            'address' => Yii::t('app', 'Address'),
            'image' => Yii::t('app', 'Image'),
            'doctor_id' => Yii::t('app', 'Doctor ID'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'last_sync' => Yii::t('app', 'Last Sync'),
        ];
    }

    /**
     * Gets query for [[Products]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['store_id' => 'id']);
    }

    /**
     * Gets query for [[StoreFields]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStoreFields()
    {
        return $this->hasMany(StoreField::className(), ['store_id' => 'id']);
    }

    /**
     * Gets query for [[StoreStaff]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStoreStaff()
    {
        return $this->hasMany(StoreStaff::className(), ['store_id' => 'id']);
    }

    /**
     * Gets query for [[District]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(District::className(), ['id_district' => 'district_id']);
    }

    /**
     * Gets query for [[Province]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvince()
    {
        return $this->hasOne(Province::className(), ['id_state' => 'province_id']);
    }

    /**
     * Gets query for [[Ward]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWard()
    {
        return $this->hasOne(Ward::className(), ['id_ward' => 'ward_id']);
    }
}
