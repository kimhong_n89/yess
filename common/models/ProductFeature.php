<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_feature".
 *
 * @property int $id
 * @property int $product_id
 * @property int $feature_id
 * @property int $feature_value_id
 *
 * @property Feature $feature
 * @property FeatureValue $featureValue
 * @property Product $product
 */
class ProductFeature extends \common\models\BaseSyncModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_feature';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'feature_id', 'feature_value_id'], 'required'],
            [['product_id', 'feature_id', 'feature_value_id'], 'integer'],
            [['feature_id'], 'exist', 'skipOnError' => true, 'targetClass' => Feature::className(), 'targetAttribute' => ['feature_id' => 'id']],
            [['feature_value_id'], 'exist', 'skipOnError' => true, 'targetClass' => FeatureValue::className(), 'targetAttribute' => ['feature_value_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'feature_id' => Yii::t('app', 'Feature ID'),
            'feature_value_id' => Yii::t('app', 'Feature Value ID'),
        ];
    }

    /**
     * Gets query for [[Feature]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFeature()
    {
        return $this->hasOne(Feature::className(), ['id' => 'feature_id']);
    }

    /**
     * Gets query for [[FeatureValue]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFeatureValue()
    {
        return $this->hasOne(FeatureValue::className(), ['id' => 'feature_value_id']);
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
