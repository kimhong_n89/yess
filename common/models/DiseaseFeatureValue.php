<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "disease_feature_value".
 *
 * @property int $id Synced with CTT
 * @property int|null $disease_id
 * @property int|null $feature_value_id
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class DiseaseFeatureValue extends \common\models\BaseSyncModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'disease_feature_value';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'disease_id', 'feature_value_id', 'created_at', 'updated_at'], 'integer'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'disease_id' => Yii::t('app', 'Disease ID'),
            'feature_value_id' => Yii::t('app', 'Feature Value ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
