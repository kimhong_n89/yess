<?php

namespace common\models;

use common\models\PrescriptionDetail;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Create medicine form
 */
class MedicineForm extends Model
{
    public $type = PrescriptionDetail::TYPE_MEDICINE_INSURANCE;
    public $id;
    public $medicine_id;
    public $dosage_form_id;
    public $prescription_id;
    public $amount;
    public $amount_morning;
    public $amount_noon;
    public $amount_afternoon;
    public $time_of_use;
    public $drugged_time;

    private $model;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['medicine_id', 'dosage_form_id', 'amount'], 'required'],
            [['prescription_id', 'medicine_id', 'dosage_form_id', 'time_of_use', 'drugged_time'], 'integer'],
            [['amount', 'amount_morning', 'amount_noon', 'amount_afternoon'], 'number', 'integerOnly'=>true, 'min'=>1],
        ];
    }

    /**
     * @return PrescriptionDetail
     */
    public function getModel()
    {
        if (!$this->model) {
            $this->model = new PrescriptionDetail();
        }
        return $this->model;
    }

    /**
     * @param PrescriptionDetail $model
     * @return mixed
     */
    public function setModel($model)
    {
        $this->id = $model->id;
        $this->prescription_id = $model->prescription_id;
        $this->medicine_id = $model->medicine_id;
        $this->dosage_form_id = $model->dosage_form_id;
        $this->time_of_use = $model->time_of_use;
        $this->drugged_time = $model->drugged_time;
        $this->amount = $model->amount;
        $this->amount_morning = $model->amount_morning;
        $this->amount_noon = $model->amount_noon;
        $this->amount_afternoon = $model->amount_afternoon;

        $this->model = $model;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'prescription_id' => Yii::t('common', 'Đơn thuốc'),
            'type' => Yii::t('common', 'Loại'),
            'medicine_id' => Yii::t('common', 'Tên thuốc'),
            'dosage_form_id' => Yii::t('common', 'Quy cách'),
            'amount' => Yii::t('common', 'Số lượng'),
            'amount_morning' => Yii::t('common', 'Sáng'),
            'amount_noon' => Yii::t('common', 'Trưa'),
            'amount_afternoon' => Yii::t('common', 'Chiều'),
            'time_of_use' => Yii::t('common', 'Thời gian dùng'),
            'drugged_time' => Yii::t('common', 'Thời điểm'),
        ];
    }

    /**
     * @return PrescriptionDetail|null the saved model or null if saving fails
     * @throws Exception
     */
    public function save()
    {
        if ($this->validate()) {
            $model = $this->getModel();
            $isNewRecord = $model->getIsNewRecord();
            $model->type = $this->type;
            $model->prescription_id = $this->prescription_id;
            $model->medicine_id = $this->medicine_id;
            $model->dosage_form_id = $this->dosage_form_id;
            $model->time_of_use = $this->time_of_use;
            $model->drugged_time = $this->drugged_time;
            $model->amount = $this->amount;
            $model->amount_morning = $this->amount_morning;
            $model->amount_noon = $this->amount_noon;
            $model->amount_afternoon = $this->amount_afternoon;
            $model->save();

            return !$model->hasErrors();
        }
        return null;
    }
}
