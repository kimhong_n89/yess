<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "patient".
 *
 * @property int $id
 * @property string $fullname
 * @property string|null $phone
 * @property string|null $email
 * @property int|null $gender
 * @property int|null $province_id
 * @property int|null $district_id
 * @property int|null $ward_id
 * @property string|null $address
 * @property int|null $day_of_birth
 * @property int|null $month_of_birth
 * @property int|null $year_of_birth
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property District $district
 * @property Prescription[] $prescriptions
 * @property Province $province
 * @property Ward $ward
 */
class Patient extends GeneralModel
{
    const GENDER_FEMALE = 0;
    const GENDER_MALE = 1;
    const GENDER_OTHER = 2;

    public $count_prescription;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'patient';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fullname', 'gender', 'year_of_birth'], 'required'],
            [['gender', 'province_id', 'district_id', 'ward_id', 'day_of_birth', 'month_of_birth', 'year_of_birth', 'status', 'created_at', 'updated_at'], 'integer'],
            [['fullname', 'email'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 20],
            ['phone', 'match', 'pattern' => Yii::$app->params['phone_pattern']],
            [['phone'], 'unique'],
            [['address'], 'string', 'max' => 255],
            [['district_id'], 'exist', 'skipOnError' => true, 'targetClass' => District::className(), 'targetAttribute' => ['district_id' => 'id_district']],
            [['province_id'], 'exist', 'skipOnError' => true, 'targetClass' => Province::className(), 'targetAttribute' => ['province_id' => 'id_state']],
            [['ward_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ward::className(), 'targetAttribute' => ['ward_id' => 'id_ward']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'fullname' => Yii::t('common', 'Họ và tên'),
            'phone' => Yii::t('common', 'Điện thoại'),
            'email' => Yii::t('common', 'Email'),
            'gender' => Yii::t('common', 'Giới tính'),
            'province_id' => Yii::t('common', 'Khu vực'),
            'district_id' => Yii::t('common', 'Quận/Huyện'),
            'ward_id' => Yii::t('common', 'Phường/Xã'),
            'address' => Yii::t('common', 'Chi tiết địa chỉ'),
            'day_of_birth' => Yii::t('common', 'Ngày sinh'),
            'month_of_birth' => Yii::t('common', 'Tháng sinh'),
            'year_of_birth' => Yii::t('common', 'Năm sinh'),
            'status' => Yii::t('common', 'Trạng thái'),
            'created_at' => Yii::t('common', 'Tạo lúc'),
            'updated_at' => Yii::t('common', 'Cập nhật lúc'),
            'count_prescription' => Yii::t('common', 'Số toa'),
        ];
    }

    /**
     * Gets query for [[District]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(District::className(), ['id_district' => 'district_id']);
    }

    /**
     * Gets query for [[Prescriptions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPrescriptions()
    {
        return $this->hasMany(Prescription::className(), ['patient_id' => 'id']);
    }

    /**
     * Gets query for [[Province]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvince()
    {
        return $this->hasOne(Province::className(), ['id_state' => 'province_id']);
    }

    /**
     * Gets query for [[Ward]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWard()
    {
        return $this->hasOne(Ward::className(), ['id_ward' => 'ward_id']);
    }

    public static function genders()
    {
        return [
            self::GENDER_MALE => Yii::t('common', 'Nam'),
            self::GENDER_FEMALE => Yii::t('common', 'Nữ'),
            self::GENDER_OTHER => Yii::t('common', 'Khác'),
        ];
    }

    public function getFullAddress()
    {
        $full_address = $this->address;
        $ward = Ward::find()->where(['id_ward' => $this->ward_id])->one();
        $district = District::find()->where(['id_district' => $this->district_id])->one();
        $province = Province::find()->where(['id_state' => $this->province_id])->one();
        $full_address .= (empty($full_address) ? "" : ", ") . (empty($ward) ? "" : $ward->name);
        $full_address .= (empty($full_address) ? "" : ", ") . (empty($district) ? "" : $district->name);
        $full_address .= (empty($full_address) ? "" : ", ") . (empty($province) ? "" : $province->name);
        return $full_address;
    }

    public function getFullBirthday()
    {
        return "{$this->day_of_birth}/{$this->month_of_birth}/{$this->year_of_birth}";
    }

    public function getYearOld()
    {
        return $this->year_of_birth ? date('Y') - $this->year_of_birth : '';
    }

    public function getAllDiagnoses()
    {
        return DiseaseGroup::find()
                    ->innerJoin('prescription_diagnose', 'prescription_diagnose.diagnose_id = disease_group.id')
                    ->innerJoin('prescription', 'prescription_diagnose.prescription_id = prescription.id')
                    ->where(['patient_id' => $this->id])->all();
    }
}
