<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "template".
 *
 * @property int $id
 * @property string $code
 * @property string|null $title
 * @property string|null $content
 * @property string $type
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class Template extends GeneralModel
{
    const EMAIL_SEND_PRESCRIPTION = 'email_send_prescription';
    const EMAIL_APPROVE_DOCTOR = 'email_approve_doctor';
    const EMAIL_SEND_ANSWER_FEEDBACK = 'email_answer_feedback';
    
    const TYPE_EMAIL = 'email';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'template';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'type'], 'required'],
            [['content'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['code'], 'string', 'max' => 100],
            [['title'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'title' => Yii::t('app', 'Tiêu đề'),
            'content' => Yii::t('app', 'Nội dung'),
            'type' => Yii::t('app', 'loại'),
            'created_at' => Yii::t('app', 'Tạo lúc'),
            'updated_at' => Yii::t('app', 'Cập nhật lúc'),
        ];
    }
    
    /**
     * @return array statuses list
     */
    public static function types()
    {
        return [
            self::TYPE_EMAIL => Yii::t('common', 'Email'),
        ];
    }
}
