<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $rewrite
 * @property int|null $parent_id
 * @property int|null $position
 * @property string|null $image
 * @property int|null $active
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $last_sync
 *
 * @property ProductCategory[] $productCategories
 */
class Category extends \common\models\BaseSyncModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'parent_id', 'position', 'active', 'created_at', 'updated_at', 'last_sync'], 'integer'],
            [['description'], 'string'],
            [['name', 'rewrite'], 'string', 'max' => 512],
            [['image'], 'string', 'max' => 2048],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'rewrite' => Yii::t('app', 'Rewrite'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'position' => Yii::t('app', 'Position'),
            'image' => Yii::t('app', 'Image'),
            'active' => Yii::t('app', 'Active'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'last_sync' => Yii::t('app', 'Last Sync'),
        ];
    }

    /**
     * Gets query for [[ProductCategories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategories()
    {
        return $this->hasMany(ProductCategory::className(), ['category_id' => 'id']);
    }
}
