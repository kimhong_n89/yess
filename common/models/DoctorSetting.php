<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "doctor_setting".
 *
 * @property int $id
 * @property int $doctor_id
 * @property string $key
 * @property string $value
 */
class DoctorSetting extends \yii\db\ActiveRecord
{
    const KEY_BUSINESS_FIELD = 'business_field';
    const KEY_TEMPLATE_HEADER = 'template_header';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doctor_setting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['doctor_id', 'key'], 'required'],
            [['doctor_id'], 'integer'],
            [['key'], 'string', 'max' => 100],
            [['value'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'doctor_id' => Yii::t('common', 'Doctor ID'),
            'key' => Yii::t('common', 'Key'),
            'value' => Yii::t('common', 'Value'),
        ];
    }
}
