<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "prescription".
 *
 * @property int $id
 * @property int $doctor_id
 * @property int $patient_id
 * @property string $insurance_number
 * @property int $is_use_insurance 0: Khong, 1: Co
 * @property float $insurance_rate
 * @property int|null $total_amount
 * @property string $note
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property int|null $re_examination
 * @property string|null $re_examination_date
 * @property int $store_id
 *
 * @property Doctor $doctor
 * @property Store $store
 * @property Patient $patient
 * @property PrescriptionDetail[] $prescriptionDetails
 * @property PrescriptionDiagnose[] $prescriptionDiagnoses
 */
class Prescription extends GeneralModel
{
    const STATUS_DRAFT = 0; // đang tạo
    const STATUS_DELIVERY_TO_STORE = 1; // đã chuyển cho nhà thuốc
    const STATUS_DONE = 2; // hoàn thành||đã giao

    public $arr_diagnoses;

    //for search
    public $doctor_name;
    public $doctor_phone;
    public $patient_name;
    public $patient_province;
    public $patient_district;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prescription';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['doctor_id', 'patient_id', 'is_use_insurance'], 'required'],
            [['doctor_id', 'patient_id', 'is_use_insurance', 'total_amount', 'status', 'created_at', 'updated_at', 're_examination'], 'integer'],
            [['insurance_rate'], 'number'],
            [['re_examination_date'], 'safe'],
            [['insurance_number'], 'string', 'max' => 50],
            [['note'], 'string', 'max' => 1024],
            [['doctor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Doctor::className(), 'targetAttribute' => ['doctor_id' => 'id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Patient::className(), 'targetAttribute' => ['patient_id' => 'id']],
            [['arr_diagnoses'], 'safe'],
            ['insurance_number', 'required', 'when' => function ($model) {
                return $model->is_use_insurance == 1;
            }, 'whenClient' => "function (attribute, value) {
                return $('#sel-insurance').val() == 1;
            }"],
            ['insurance_rate', 'required', 'when' => function ($model) {
                            return $model->is_use_insurance == 1;
                        }, 'whenClient' => "function (attribute, value) {
                return $('#sel-insurance').val() == 1;
            }"],
            ['re_examination_date', 'required', 'when' => function ($model) {
                return $model->re_examination == 1;
            },
             'whenClient' => "function (attribute, value) {
                return $('input[name=\"Prescription[re_examination]\"]:checked').val() == 1;
            }"
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'doctor_id' => Yii::t('common', 'Bác sĩ'),
            'patient_id' => Yii::t('common', 'Khách hàng'),
            'insurance_number' => Yii::t('common', 'Số BHYT'),
            'is_use_insurance' => Yii::t('common', 'BHYT'),
            'insurance_rate' => Yii::t('common', '% BHYT'),
            'total_amount' => Yii::t('common', 'Tổng tiền'),
            'note' => Yii::t('common', 'Lời dặn của bác sĩ'),
            'status' => Yii::t('common', 'Trạng thái'),
            'created_at' => Yii::t('common', 'Tạo lúc'),
            'updated_at' => Yii::t('common', 'Cập nhật lúc'),
            'arr_diagnoses' => Yii::t('common', 'Chẩn đoán'),
            're_examination' => Yii::t('app', 'Hẹn tái khám'),
            're_examination_date' => Yii::t('app', 'Ngày tái khám'),
            'store_id' => Yii::t('app', 'Nhà thuốc'),
        ];
    }

    public static function statuses()
    {
        return [
            self::STATUS_DRAFT => Yii::t('common', 'Đang tạo'),
            self::STATUS_DELIVERY_TO_STORE => Yii::t('common', 'Đã chuyển nhà thuốc'),
            self::STATUS_DONE => Yii::t('common', 'Hoàn thành'),
            self::STATUS_DELETED => Yii::t('common', 'Đã huỷ'),
        ];
    }

    public static function statusNotes()
    {
        return [
            self::STATUS_DRAFT => Yii::t('common', 'Bạn chưa điền đầy đủ thông tin của toa thuốc, hãy “Sửa” để cập nhật hoàn chỉnh và in toa hoặc “Xóa”'),
            self::STATUS_DELIVERY_TO_STORE => Yii::t('common', 'Đã chuyển nhà thuốc'),
            self::STATUS_DONE => Yii::t('common', 'Đơn thuốc đã in, đang chờ lấy thuốc trực tiếp hoặc thông qua Nhà thuốc'),
            self::STATUS_DELETED => Yii::t('common', 'Đơn thuốc đã bị hủy'),
        ];
    }

    public static function insuranceRates()
    {
        return [10 => '10%', 20 => '20%', 30 => '30%', 40 => '40%', 50 => '50%', 60 => '60%', 70 => '70%', 80 => '80%', 90 => '90%', 100 => '100%'];
    }
    /**
     * Gets query for [[Doctor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDoctor()
    {
        return $this->hasOne(Doctor::className(), ['id' => 'doctor_id']);
    }

    /**
     * Gets query for [[Patient]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['id' => 'patient_id']);
    }

    public function getStore()
    {
        return $this->hasOne(Store::className(), ['id' => 'store_id']);
    }

    /**
     * Gets query for [[PrescriptionDetails]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPrescriptionDetails()
    {
        return $this->hasMany(PrescriptionDetail::className(), ['prescription_id' => 'id']);
    }

    /**
     * Gets query for [[PrescriptionDiagnoses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPrescriptionDiagnoses()
    {
        return $this->hasMany(PrescriptionDiagnose::className(), ['prescription_id' => 'id']);
    }

    public function getDiagnoses() {
        return $this->hasMany(DiseaseGroup::className(), ['id' => 'diagnose_id'])
                    ->via('prescriptionDiagnoses');
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->arr_diagnoses = ArrayHelper::getColumn($this->prescriptionDiagnoses, 'diagnose_id');
        $this->re_examination_date = $this->re_examination_date ? date("d/m/Y H:i", strtotime($this->re_examination_date)) : null;
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $re_examination_date = \DateTime::createFromFormat('d/m/Y H:i', $this->re_examination_date);
        if ($re_examination_date) {
            $this->re_examination_date = $re_examination_date->format('Y-m-d H:i:s');
        }
        $action = Yii::$app->request->post('btn-submit', 'save');
        if ($action == 'done') {
            $this->status = self::STATUS_DONE;
        }
        return true;
    }

    public function afterSave ( $insert, $changedAttributes )
    {
        parent::afterSave($insert, $changedAttributes);
        $this->saveSchedule();
        $this->saveDiagnoses();
    }

    public function saveSchedule()
    {
        $model = ExaminationSchedule::findOne(['prescription_id' => $this->id]);
        if ($this->re_examination) {
            if (!$model) {
                $model = new ExaminationSchedule();
                $model->prescription_id = $this->id;
                $model->doctor_id = $this->doctor_id;
                $model->patient_id = $this->patient_id;
            }
            $model->date = $this->re_examination_date;
            $model->note = $this->note;
            $model->save();
        } else { //delete schedule
            if ($model && $model->delete());
        }
    }

    public function saveDiagnoses()
    {
        $this->arr_diagnoses = is_array($this->arr_diagnoses) ? $this->arr_diagnoses : [];
        if ((isset($this->relatedRecords['prescriptionDiagnoses']) && diff_2array(ArrayHelper::getColumn($this->relatedRecords['prescriptionDiagnoses'], 'diagnose_id'), $this->arr_diagnoses)) || empty($this->relatedRecords['prescriptionDiagnoses'])) {
            if (!$this->isNewRecord) {
                // update // delete all
                PrescriptionDiagnose::deleteAll('prescription_id = :id', array('id' => $this->id));
            }
            // insert it.
            foreach ($this->arr_diagnoses as $id) {
                $model = new PrescriptionDiagnose();
                $model->prescription_id = $this->id;
                $model->diagnose_id = $id;
                $model->save();
            }
        }
    }

    public function getCode()
    {
        return "DT{$this->id}-" . date('dmY', $this->created_at);
    }
}
