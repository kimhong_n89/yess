<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prescription_detail".
 *
 * @property int $id
 * @property int $prescription_id
 * @property int $type 1: BHYT; 2: Dich vu
 * @property int $medicine_id
 * @property int $dosage_form_id Quy cách
 * @property float $amount
 * @property float $amount_morning
 * @property float $amount_noon
 * @property float $amount_afternoon
 * @property int $time_of_use 1: truoc an; 2: sau an
 * @property int $drugged_time thời điểm
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Product $medicine
 */
class PrescriptionDetail extends GeneralModel
{
    const TYPE_MEDICINE_INSURANCE = 1;
    const TYPE_MEDICINE_SERVICE = 2;

    const TIME_BEFORE_MEAL = 1;
    const TIME_AFTER_MEAL = 2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prescription_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'medicine_id', 'dosage_form_id', 'amount'], 'required'],
            [['prescription_id', 'type', 'medicine_id', 'dosage_form_id', 'time_of_use', 'drugged_time', 'status', 'created_at', 'updated_at'], 'integer'],
            [['amount', 'amount_morning', 'amount_noon', 'amount_afternoon'], 'number', 'integerOnly'=>true, 'min'=>1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'prescription_id' => Yii::t('common', 'Đơn thuốc'),
            'type' => Yii::t('common', 'Loại'),
            'medicine_id' => Yii::t('common', 'Tên thuốc'),
            'dosage_form_id' => Yii::t('common', 'Quy cách'),
            'amount' => Yii::t('common', 'Số lượng'),
            'amount_morning' => Yii::t('common', 'Sáng'),
            'amount_noon' => Yii::t('common', 'Trưa'),
            'amount_afternoon' => Yii::t('common', 'Chiều'),
            'time_of_use' => Yii::t('common', 'Thời gian dùng'),
            'drugged_time' => Yii::t('common', 'Thời điểm'),
            'status' => Yii::t('common', 'Trạng thái'),
            'created_at' => Yii::t('common', 'Tạo lúc'),
            'updated_at' => Yii::t('common', 'Cập nhật lúc'),
        ];
    }

    public static function types()
    {
        return [
            self::TYPE_MEDICINE_INSURANCE => Yii::t('common', 'Toa thuốc BHYT'),
            self::TYPE_MEDICINE_SERVICE => Yii::t('common', 'Toa thuốc dịch vụ'),
        ];
    }

    public static function timeUses()
    {
        return [
            self::TIME_AFTER_MEAL => Yii::t('common', 'Sau khi ăn'),
            self::TIME_BEFORE_MEAL => Yii::t('common', 'Trước khi ăn'),
        ];
    }

    public function getMedicine()
    {
        return $this->hasOne(Product::className(), ['id' => 'medicine_id']);
    }
}
