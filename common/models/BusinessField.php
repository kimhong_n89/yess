<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "business_field".
 *
 * @property int $id
 * @property string|null $name
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $last_sync
 *
 * @property StoreField[] $storeFields
 */
class BusinessField extends \common\models\BaseSyncModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'business_field';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'required'],
            [['id', 'status', 'created_at', 'updated_at', 'last_sync'], 'integer'],
            [['name'], 'string', 'max' => 512],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'last_sync' => Yii::t('app', 'Last Sync'),
        ];
    }

    /**
     * Gets query for [[StoreFields]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStoreFields()
    {
        return $this->hasMany(StoreField::className(), ['field_id' => 'id']);
    }
}
