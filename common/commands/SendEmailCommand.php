<?php

namespace common\commands;

use backend\models\GeneralSettingForm;
use trntv\bus\interfaces\SelfHandlingCommand;
use yii\base\BaseObject;
use yii\swiftmailer\Message;

/**
 * @author Eugene Terentev <eugene@terentev.net>
 */
class SendEmailCommand extends BaseObject implements SelfHandlingCommand
{
    /**
     * @var mixed
     */
    public $from;
    /**
     * @var mixed
     */
    public $to;
    /**
     * @var string
     */
    public $subject;
    /**
     * @var string
     */
    public $view;
    /**
     * @var array
     */
    public $params;
    /**
     * @var string
     */
    public $body;
    /**
     * @var bool
     */
    public $html = true;

    /**
     * @param \common\commands\SendEmailCommand $command
     * @return bool
     */
    public function handle($command)
    {
        $settings = new GeneralSettingForm();
        $settings->loadFromDB();
        \Yii::$app->mailer->setTransport(
            [
                'class' => 'Swift_SmtpTransport',
                'host' => $settings->smtp_host,
                'username' => $settings->smtp_username,
                'password' => $settings->smtp_password,
                'port' => $settings->smtp_port,
                'encryption' => $settings->smtp_encryption,
            ]
        );
        $this->from = $this->from ?: [$settings->server_send_email => $settings->server_send_name];
        if (!$command->body) {
            $message = \Yii::$app->mailer->compose($command->view, $command->params);
        } else {
            $message = new Message();
            if ($command->isHtml()) {
                $message->setHtmlBody($command->body);
            } else {
                $message->setTextBody($command->body);
            }
        }
        $message->setCharset('UTF-8');
        $message->setFrom($command->from);
        $message->setTo($command->to ?: \Yii::$app->params['robotEmail']);
        $message->setSubject($command->subject);
        return $message->send();
    }

    /**
     * @return bool
     */
    public function isHtml()
    {
        return (bool)$this->html;
    }
}
