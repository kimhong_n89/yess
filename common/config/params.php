<?php
return [
    'dateFormatDisplay' => [
        'phpdatetime' => 'php:Y-m-d H:i:s',
        'phpdate' => 'php:Y-m-d',
        'datetime' => 'Y-m-d H:i:s',
        'date' => 'Y-m-d'
    ],
    'baseUrl' => \Yii::getAlias('@storageUrl/source'),
    'phone_pattern' => '/(84|0[3|5|7|8|9])+([0-9]{8})\b/',
    'max_send_otp' => 3
];