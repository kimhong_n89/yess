<?php
/**
 * Yii2 Shortcuts
 * @author Eugene Terentev <eugene@terentev.net>
 * @author Victor Gonzalez <victor@vgr.cl>
 * -----
 * This file is just an example and a place where you can add your own shortcuts,
 * it doesn't pretend to be a full list of available possibilities
 * -----
 */

 use yii\helpers\ArrayHelper;
 use yii\helpers\Html;

/**
 * @return int|string
 */
function getMyId()
{
    return Yii::$app->user->getId();
}

/**
 * @param string $view
 * @param array $params
 * @return string
 */
function render($view, $params = [])
{
    return Yii::$app->controller->render($view, $params);
}

/**
 * @param $url
 * @param int $statusCode
 * @return \yii\web\Response
 */
function redirect($url, $statusCode = 302)
{
    return Yii::$app->controller->redirect($url, $statusCode);
}

/**
 * @param string $key
 * @param mixed $default
 * @return mixed
 */
function env($key, $default = null)
{

    $value = getenv($key) ?? $_ENV[$key] ?? $_SERVER[$key];

    if ($value === false) {
        return $default;
    }

    switch (strtolower($value)) {
        case 'true':
        case '(true)':
            return true;

        case 'false':
        case '(false)':
            return false;
    }

    return $value;
}

/**
 * Renders any data provider summary text.
 *
 * @param \yii\data\DataProviderInterface $dataProvider
 * @param array $options the HTML attributes for the container tag of the summary text
 * @return string the HTML summary text
 */
function getDataProviderSummary($dataProvider, $options = [])
{
    $count = $dataProvider->getCount();
    if ($count <= 0) {
        return '';
    }
    $tag = ArrayHelper::remove($options, 'tag', 'div');
    if (($pagination = $dataProvider->getPagination()) !== false) {
        $totalCount = $dataProvider->getTotalCount();
        $begin = $pagination->getPage() * $pagination->pageSize + 1;
        $end = $begin + $count - 1;
        if ($begin > $end) {
            $begin = $end;
        }
        $page = $pagination->getPage() + 1;
        $pageCount = $pagination->pageCount;
        return Html::tag($tag, Yii::t('yii', 'Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}.', [
                'begin' => $begin,
                'end' => $end,
                'count' => $count,
                'totalCount' => $totalCount,
                'page' => $page,
                'pageCount' => $pageCount,
            ]), $options);
    } else {
        $begin = $page = $pageCount = 1;
        $end = $totalCount = $count;
        return Html::tag($tag, Yii::t('yii', 'Total <b>{count, number}</b> {count, plural, one{item} other{items}}.', [
            'begin' => $begin,
            'end' => $end,
            'count' => $count,
            'totalCount' => $totalCount,
            'page' => $page,
            'pageCount' => $pageCount,
        ]), $options);
    }
}

function diff_2array($arr1=array(), $arr2=array(), $type = 'count')
{
    if ($type != 'count') {
        return array_merge(array_diff($arr1, $arr2), array_diff($arr2, $arr1));
    }
    return count(array_merge(array_diff($arr1, $arr2), array_diff($arr2, $arr1))) > 0 ? true : false;
}

function d1()
{
    //     return;
    $args = func_get_args();
    foreach ($args as $arg) {
        echo '<pre>';
        print_r($arg);
        echo '</pre>';
    }
}

function dd1()
{
    //     return;
    $args = func_get_args();
    foreach ($args as $arg) {
        echo '<pre>';
        print_r($arg);
        echo '</pre>';
    }
    die;
}

function j()
{
    $args = func_get_args();
    foreach ($args as $arg) {
        echo '<pre>';
        echo json_encode($arg, JSON_PRETTY_PRINT, 10);
        echo '</pre>';
    }
}

function m()
{
    $args = func_get_args();
    foreach ($args as $arg) {
        echo '<pre>';
        var_dump($arg);
        echo '</pre>';
    }
}

function mm()
{
    $args = func_get_args();
    foreach ($args as $arg) {
        echo '<pre>';
        var_dump($arg);
        echo '</pre>';
        echo '<hr>';
    }
    die;
}

function q()
{
    $args = func_get_args();
    foreach ($args as $arg) {
        echo '<pre>';
        print_r($arg->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
        echo '</pre>';
        echo '<hr>';
    }
}

function qd()
{
    $args = func_get_args();
    foreach ($args as $arg) {
        echo '<pre>';
        print_r($arg->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
        echo '</pre>';
        echo '<hr>';
    }
    die;
}

$rbac_cache = [];

/**
 * Function to check current user can perform a specific permission.
 * For special functions admin can perform without determine a permission. (e.g admin can see deleted project)
 * @author Wayne
 * @param string $permission_name . This must be a name defined in table `auth_item` (e.g AuthItem::EDIT_TASK_STATUS)
 * @return boolean
 */
function rbac_is_allow_exec($permission_name = null, $params = [])
{
    global $rbac_cache;
    //    comment it because we have case for same permission name but different situation
    //     if (!array_key_exists($permission_name, $rbac_cache)) {
    //         $rbac_cache[$permission_name] = userCan($permission_name, $params);
    //     }
        $rbac_cache[$permission_name] = userCan($permission_name, $params);
        if ($rbac_cache[$permission_name]) {
            return true;
        }
        return false;
}

/**
 * @param $permission_name
 * @return bool
 */
function userCan($permission_name, $params = [])
{
    return rbac_is_super_admin() || Yii::$app->user->can($permission_name, ['noRecursive' => true]);
}

function rbac_is_super_admin()
{
    global $rbac_cache;
    if (isset(Yii::$app->user->identity->is_superadmin) && Yii::$app->user->identity->is_superadmin === 1) {
        return true;
    }
}

function load_post_value($model, $attribute, $post)
{
    if (!empty($post)) {
        if (is_array($post)) {
            foreach ($post as $key => $value) {
                if ($key == $attribute) {
                    $model->$attribute = $value;
                } else if (is_array($value)) {
                    load_post_value($model, $attribute, $value);
                }
            }
        }
    }
}

/**
 * Check if the string give in is a valid JSON format
 *
 * @author Wayne
 * @param string $string
 */
function isJSON($string)
{
    return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
}

/**
 * Check if the string give in is a valid date or not
 *
 * @author Wayne
 * @param string $string
 */
function tm_checkIsValidDate($string)
{
    if (empty(strtotime($string))) {
        return 0;
    }
    $string = strtotime($string);
    $month = date('m', $string);
    $day = date('d', $string);
    $year = date('Y', $string);
    return checkdate($month, $day, $year) ? 1 : 0;
}

/**
 * Strip tags but keep content inside
 * @param string $string
 * @param array $allow_tags
 */
function stripTags($string, $allow_tags = [])
{
    $allow_tags = !empty($allow_tags) ? $allow_tags : '<strong><p><b><li><ul><u><i><ol><a><hr><div><span>';
    return strip_tags($string, $allow_tags);
}

/**
 *Strip all of tags and clear content inside
 * @param string $text
 * @param string $allow_tags => this will keep content inside but still remove tag
 * @return mixed|unknown
 */
function strip_tags_with_content($text, $allow_tags = '')
{
    $allow_tags = !empty($allow_tags) ? $allow_tags : '<strong><p><b><li><ul><u><i><ol><a><hr><div><span>';
    
    preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($allow_tags), $allow_tags);
    $allow_tags = array_unique($allow_tags[1]);
    
    if (is_array($allow_tags) && count($allow_tags) > 0) {
        return preg_replace('@<(?!(?:' . implode('|', $allow_tags) . ')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
    } else {
        return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
    }
    return $text;
}

function makeLink($string)
{
    $regExUrl = '@(http|https)?(s)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
    $string = preg_replace($regExUrl, '<a href="$1$2://$4" target="_blank" title="$0">$0</a>', $string);
    return $string;
}

/**
 * @author: Peter
 * @param: string with <br>
 */
function br2nl($source)
{
    return $out = str_replace("<br />", PHP_EOL, $source);
}

function pnl2br($source)
{
    return $out = str_replace(PHP_EOL, "<br />", $source);
}

function encode_display_html($html)
{
    $html = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $html);
    return $html;
}

/**
 * Function format time to working day
 *
 * @author Alex
 * @param int $minute
 * @return string
 */
function workperday($minute)
{
    return floor($minute / (8 * 60)) . "d " . floor(($minute % (8 * 60)) / 60) . "h " . floor((($minute % (8 * 60))) % 60) . "m";
}

/**
 * Function format time by type
 * @author Alex
 * @param int $minute
 * @param text $type
 * @param $isRound : whether round down the decimal number or not.
 * @return string
 */
function workperhour($minute, $type = 'all', $hourChar = 'h', $minChar = 'm', $isRound = true)
{
    $number_prefix = 1;
    if ($minute < 0) $number_prefix = -1;
    // Calculate as number greater than 0
    
    $minute = abs($minute);
    if ($type == 'h') {
        if (!$isRound) {
            if (floatval($minute / 60) != 0.00) {
                $value = ($minute % 60 == 0) ? floatval($minute / 60) : floatval($minute / 60);
                return $number_prefix * $value;
            }
            return "";
        }
        return floor($minute / 60) == 0 ? "" : ($number_prefix) * floor($minute / 60);
    }
    if ($type == 'm') {
        return floor($minute % (8 * 60) % 60) == 0 ? "" : ($number_prefix) * floor($minute % (8 * 60) % 60);
    }
    
    $h = floor($minute / 60) == 0 ? '' : ($number_prefix) * floor($minute / 60) . $hourChar;
    $m = floor($minute % (8 * 60) % 60) == 0 ? '' : ($number_prefix) * floor($minute % (8 * 60) % 60) . $minChar;
    
    return !empty(trim($h . ' ' . $m)) ? trim($h . ' ' . $m) : '0h';
}

/**
 * @author Alex, Peter
 * For function date(strtotime('+getDateDiff() day'))
 * @param string|int $start Date start in string or timestamp
 * @param string|int $end End start in string or timestamp
 * @param bool $isCount : if true then it return the number of days COUNTING from start to end.
 */

function getDateDiff($start, $end, $isCount = false)
{
    $dStart = (!ctype_digit($start)) ? new DateTime(date("Y-m-d 00:00:00", strtotime($start))) : new DateTime(date("Y-m-d 00:00:00", $start));
    $dEnd = (!ctype_digit($end)) ? new DateTime(date("Y-m-d 00:00:00", strtotime($end))) : new DateTime(date("Y-m-d 00:00:00", $end));
    $dDiff = $dStart->diff($dEnd);
    return ($isCount) ? $dDiff->days + 1 : $dDiff->days;
}

/**
 * Get Monday - Sunday date of current week (if $custom_date is null)/ M-S of $custom_date's week.
 * @author Peter
 * @param string $custom_date
 * @return array $out: [monday, sunday]
 */
function getFirstLastDateOfWeek($custom_date = null)
{
    //     $custom_date = '2016-05-02';
    $custom_date = (!empty($custom_date)) ? strtotime($custom_date) : strtotime("now");
    $today = date('l', $custom_date);
    if ($today == 'Sunday') {
        $week_start = date('Y-m-d', strtotime('monday last week', $custom_date));
        $week_end = date('Y-m-d', strtotime('sunday last week', $custom_date));
    } else {
        $week_start = date('Y-m-d', strtotime('monday this week', $custom_date));
        $week_end = date('Y-m-d', strtotime('sunday this week', $custom_date));
    }
    $out = [$week_start, $week_end];
    return $out;
}

function getFirstLastDate2Week($custom_date = null)
{
    $custom_date = (!empty($custom_date)) ? strtotime($custom_date) : strtotime("now");
    $week_start = date('Y-m-d', strtotime('monday this week', $custom_date));
    $week_end = date('Y-m-d', strtotime('sunday next week', $custom_date));
    
    $out = [$week_start, $week_end];
    return $out;
}


/**
 * Get Monday - Sunday date of current month (if $custom_date is null)/ M-S of $custom_date's week.
 * @author Peter
 * @param string $custom_date
 * @return array $out: [monday, sunday]
 */
function getFirstLastDateOfMonth()
{
    return [
        date('Y-m-d', strtotime('first day of last month', strtotime("now"))), date('Y-m-d', strtotime('last day of last month', strtotime("now"))),
        date('Y-m-d', strtotime('first day of this month', strtotime("now"))), date('Y-m-d', strtotime('last day of this month', strtotime("now"))),
        date('Y-m-d', strtotime('first day of next month', strtotime("now"))), date('Y-m-d', strtotime('last day of next month', strtotime("now"))),
    ];
}

/**
 * Unset parent attributes which shouldn't be cloned/copy from child
 * @author Peter
 * @param array $fieldNames : field name which value will be get from parent
 * @param ActiveRecord $model : child object
 * @param ActiveRecord $parent : parent object
 */
function unsetModelAttribute($fieldNames, &$model, &$parent)
{
    $atts = $model->attributes;
    $patts = $parent->attributes;
    
    foreach ($fieldNames as $name) {
        // unset from child to get these field from parent
        unset($atts[$name]);
    }
    return array_merge($patts, $atts);
}

function rangeOfStartEndDate($start_range, $end_range, $format = 'Y-m-d')
{
    $period = new \DatePeriod(
        new \DateTime($start_range), new \DateInterval('P1D'), new \DateTime($end_range)
        );
    $range_week = [date($format, strtotime($start_range)), date($format, strtotime($end_range))];
    foreach ($period as $day) {
        $range_week[] = $day->format($format);
    }
    return array_unique($range_week);
}

function rangeOfStartEndDateCalendar($start_range, $end_range, $format = 'Y-m-d', $plus = 0)
{
    $is_sat = !empty(Yii::$app->params['task_plan']['not_working_day'][0]) ? true : false;
    $is_sun = !empty(Yii::$app->params['task_plan']['not_working_day'][6]) ? true : false;
    
    // Function caculator end date
    $d = new \DateTime($start_range);
    $t = $d->getTimestamp();
    if ($plus > 0) {
        // loop for X days
        for ($i = 0; $i < $plus; $i++) {
            
            // add 1 day to timestamp
            $addDay = 86400;
            
            // get what day it is next day
            $nextDay = date('w', ($t + $addDay));
            
            // if it's Saturday or Sunday get $i-1
            if ($nextDay == 0 || $nextDay == 6) {
                $i--;
            }
            
            // modify timestamp, add 1 day
            $t = $t + $addDay;
        }
        $d->setTimestamp($t);
        $end_range = $d->format($format);
    }
    // End caculator end date
    
    
    $period = new \DatePeriod(
        new \DateTime($start_range), new \DateInterval('P1D'), new \DateTime($end_range)
        );
    $range_week = [date($format, strtotime($start_range)), date($format, strtotime($end_range))];
    foreach ($period as $day) {
        
        // Discard Sun and Sat
        $this_day = $day->format("D");
        if ($is_sat && $this_day == 'Sat') {
            continue;
        }
        if ($is_sun && $this_day == 'Sun') {
            continue;
        }
        // End discard Sun and Sat
        $range_week[] = $day->format($format);
    }
    return array_unique($range_week);
}


// ------------------------------------------------------------------------

/**
 * Character Limiter
 *
 * Limits the string based on the character count.  Preserves complete words
 * so the character count may not be exactly as specified.
 *
 * @access    public
 * @param    string
 * @param    integer
 * @param    string    the end character. Usually an ellipsis
 * @return    string
 */
function characterLimiter($str, $n = 500, $end_char = '&#8230;', $match_array = null)
{
    if (strlen($str) < $n) {
        return $str;
    }
    
    $str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));
    
    if (strlen($str) <= $n) {
        return $str;
    }
    $str = @mb_convert_encoding($str, 'UTF-8');
    $out = "";
    foreach (explode(' ', trim($str)) as $val) {
        $out .= $val . ' ';
        if (strlen($out) >= $n) {
            $out = trim($out);
            $out = (strlen($out) == strlen($str)) ? $out : $out . $end_char;
            break;
        }
    }
    $matched = false;
    foreach (explode(' ', trim($match_array)) as $match_value) {
        if (in_array($match_value, explode(' ', trim($out)))) {
            $matched = true;
            break;
        }
    }
    if (!$matched) {
        foreach (explode(' ', trim($match_array)) as $match_value) {
            if (in_array($match_value, explode(' ', trim($str)))) {
                $out = $out . $match_value . $end_char;
                break;
            }
        }
    }
    return $out;
}

function arr_move_to_top(&$array, $key)
{
    if (isset($array[$key])) {
        return array_merge(array($key => $array[$key]), $array);
    }
    return $array;
}

/**
 * Super function to calculate weeks between two dates with YEAR included.
 * $weekArr = getNoOfWeek('2013-12-01', '2014-02-10');
 * Array
 * (
 * [Week 48 '13] => 0
 * [Week 49 '13] => 0
 * [Week 50 '13] => 0
 * [Week 51 '13] => 0
 * [Week 52 '13] => 0
 * [Week 1 '14] => 0
 * [Week 2 '14] => 0
 * [Week 3 '14] => 0
 * [Week 4 '14] => 0
 * [Week 5 '14] => 0
 * [Week 6 '14] => 0
 * [Week 7 '14] => 0
 * )
 * @param string $startDate
 * @param string $endDate
 * @return array $output like the following:
 */
function getNoOfWeek($startDate, $endDate)
{
    // convert date in valid format
    $startDate = date("Y-m-d", strtotime($startDate));
    $endDate = date("Y-m-d", strtotime($endDate));
    $yearEndDay = 31;
    $weekArr = array();
    $startYear = date("Y", strtotime($startDate));
    $endYear = date("Y", strtotime($endDate));
    
    if ($startYear != $endYear) {
        $newStartDate = $startDate;
        
        for ($i = $startYear; $i <= $endYear; $i++) {
            if ($endYear == $i) {
                $newEndDate = $endDate;
            } else {
                $newEndDate = $i . "-12-" . $yearEndDay;
            }
            $startWeek = date("W", strtotime($newStartDate));
            $endWeek = date("W", strtotime($newEndDate));
            if ($endWeek == 1) {
                $endWeek = date("W", strtotime($i . "-12-" . ($yearEndDay - 7)));
            }
            $tempWeekArr = range($startWeek, $endWeek);
            array_walk($tempWeekArr, "week_text_alter",
                array('pre' => 'Week ', 'post' => " '" . substr($i, 2, 2)));
            $weekArr = array_merge($weekArr, $tempWeekArr);
            
            $newStartDate = date("Y-m-d", strtotime($newEndDate . "+1 days"));
        }
    } else {
        $startWeek = date("W", strtotime($startDate));
        $endWeek = date("W", strtotime($endDate));
        $endWeekMonth = date("m", strtotime($endDate));
        if ($endWeek == 1 && $endWeekMonth == 12) {
            $endWeek = date("W", strtotime($endYear . "-12-" . ($yearEndDay - 7)));
        }
        $weekArr = range($startWeek, $endWeek);
        array_walk($weekArr, "week_text_alter",
            array('pre' => 'Week ', 'post' => " '" . substr($startYear, 2, 2)));
    }
    $weekArr = array_fill_keys($weekArr, 0);
    return $weekArr;
}

function week_text_alter(&$item1, $key, $prefix)
{
    $item1 = $prefix['pre'] . $item1 . $prefix['post'];
}

function make_links_clickable($text)
{
    return preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+,.~#?&;//=]+)!i', '<a data-pjax="0" target="_blank" href="$1">$1</a>', $text);
}

/*
 * @author michael
 
 */
function isUserAccessResource()
{
    
}

function convertToHoursMins($time, $format = '%02dh %02dm')
{
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}

function removeTagInPreCallback($matches) {
    return str_replace($matches[1], trim(strip_tags(html_entity_decode($matches[1]))),$matches[0]);
}

function removeTagInPre($string) {
    return preg_replace_callback('/<pre.*?>(.*?)<\/pre>/ims',"removeTagInPreCallback", $string);
}

function numberToRomanRepresentation($number) {
    $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
    $returnValue = '';
    while ($number > 0) {
        foreach ($map as $roman => $int) {
            if($number >= $int) {
                $number -= $int;
                $returnValue .= $roman;
                break;
            }
        }
    }
    return $returnValue;
}

function adjustBrightness($hex, $percent) {
    // Work out if hash given
    $hash = '';
    if (stristr($hex, '#')) {
        $hex = str_replace('#', '', $hex);
        $hash = '#';
    }
    /// HEX TO RGB
    $rgb = [hexdec(substr($hex, 0, 2)), hexdec(substr($hex, 2, 2)), hexdec(substr($hex, 4, 2))];
    //// CALCULATE
    for ($i = 0; $i < 3; $i++) {
        // See if brighter or darker
        if ($percent > 0) {
            // Lighter
            $rgb[$i] = round($rgb[$i] * $percent) + round(255 * (1 - $percent));
        } else {
            // Darker
            $positivePercent = $percent - ($percent * 2);
            $rgb[$i] = round($rgb[$i] * (1 - $positivePercent)); // round($rgb[$i] * (1-$positivePercent));
        }
        // In case rounding up causes us to go to 256
        if ($rgb[$i] > 255) {
            $rgb[$i] = 255;
        }
    }
    //// RBG to Hex
    $hex = '';
    for ($i = 0; $i < 3; $i++) {
        // Convert the decimal digit to hex
        $hexDigit = dechex($rgb[$i]);
        // Add a leading zero if necessary
        if (strlen($hexDigit) == 1) {
            $hexDigit = "0" . $hexDigit;
        }
        // Append to the hex string
        $hex .= $hexDigit;
    }
    return $hash . $hex;
}

function timeLeft($time, $space = 4) {
    $current_time = time();
    if($current_time > $time) {
        return 0;
    }
    $dtCurrent = DateTime::createFromFormat('U', $current_time);
    $dtCreate = DateTime::createFromFormat('U', $time);
    $diff = $dtCurrent->diff($dtCreate);

    $interval = $diff->format("%y years %m months %d days %h hours %i minutes %s seconds");
    $interval = preg_replace('/(^0| 0) (years|months|days|hours|minutes|seconds)/', '', $interval);
    $interval = trim($interval);
    if ($space) {
        $interval = '2 months 23 hours 38 minutes 28 seconds';
        $arr = explode(' ', $interval);
        $arr = array_slice($arr, 0, 4);
        $interval = implode(" ", $arr);
    }
    return $interval;
}

function translate_day_name($string) {
    $dayName = [
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thusday',
        'Friday',
        'Saturday',
        'Sunday'
    ];
    $dayNameVi = [
        'Thứ 2',
        'Thứ 3',
        'Thứ 4',
        'Thứ 5',
        'Thứ 6',
        'Thứ 7',
        'Chủ nhật'
    ];

    return str_replace($dayName, $dayNameVi, $string);
}

function addParamToUrl($url, $strParam) {
    $query = parse_url($url, PHP_URL_QUERY);

    // Returns a string if the URL has parameters or NULL if not
    if ($query) {
        $url .= "&$strParam";
    } else {
        $url .= "?$strParam";
    }
    return $url;
}
