<?php

namespace api\components;

use Yii;
use yii\web\Response;

class ApiResponse extends Response
{
    public function send()
    {
        /*
        if(!isset($this->data['data'])) {
            $response_data = $this->data;
            $this->data = [];
            $this->data['data'] = $response_data;
        }
        */

        $this->data = [
            'status' =>$this->statusCode,
            'response' => $this->data
        ];
        parent::send();
    }
}

