<?php

namespace api\resources;

use yii\helpers\Url;
use yii\web\Link;
use yii\web\Linkable;

/**
 * @author Tyson <tyson@itoideal.com>
 */
class District extends \common\models\District
{
    /**
     * @inheritdoc
     * @return array
     */
    public function fields()
    {
        return ['district_id', 'name', 'province_id'];
    }
}
