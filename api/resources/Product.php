<?php

namespace api\resources;

use yii\helpers\Url;
use yii\web\Link;
use yii\web\Linkable;

/**
 * Class Product
 * @package api\resources
 */
class Product extends \common\models\Product
{
    /**
     * @inheritdoc
     * @return array
     */
    public function fields()
    {
        return ['id', 'ean13', 'name', 'unity'];
    }
}
