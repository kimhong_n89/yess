<?php

namespace api\resources;

use common\models\DosageForm;
use yii\helpers\Url;
use yii\web\Link;
use yii\web\Linkable;

/**
 * Class PrescriptionDetail
 * @package api\resources
 */
class PrescriptionDetail extends \common\models\PrescriptionDetail
{
    /**
     * @inheritdoc
     * @return array
     */
    public function fields()
    {
        return ['id', 'service_type', 'product', 'dosage', 'amount', 'amount_morning', 'amount_noon', 'amount_afternoon', 'time_of_meal', ];
    }

    /**
     * @return Product|null
     */
    function getProduct()
    {
        return Product::findOne($this->medicine_id);
    }

    /**
     * @return int
     */
    function getService_type()
    {
        return $this->type; //1: BHYT, 2:DV
    }

    /**
     * @return string
     */
    function getDosage()
    {
        if(!empty($this->dosage_form_id)) {
            $item = DosageForm::findOne($this->dosage_form_id);
            if(!empty($item)) return $item->name;
        }
        return "";
    }

    /**
     * @return int
     */
    function getTime_of_meal()
    {
        return $this->time_of_use; //1: trước khi ăn, 2: sau khi ăn
    }
}
