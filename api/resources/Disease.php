<?php

namespace api\resources;

use yii\helpers\Url;
use yii\web\Link;
use yii\web\Linkable;

class Disease extends \common\models\DiseaseGroup
{
    /**
     * @inheritdoc
     * @return array
     */
    public function fields()
    {
        return ['id', 'name'];
    }
}
