<?php

namespace api\resources;

use yii\helpers\Url;
use yii\web\Link;
use yii\web\Linkable;

/**
 * @author Tyson <tyson@itoideal.com>
 */
class Article extends \common\models\Article
{
    /**
     * @inheritdoc
     * @return array
     */
    public function fields()
    {
        return ['id', 'title', 'view', 'category_id'];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function extraFields()
    {
        return [
            'body',
            'category_title' => function($item) {
                return $item->category->title;
            }
        ];
    }
}
