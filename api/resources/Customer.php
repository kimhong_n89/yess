<?php

namespace api\resources;

use yii\helpers\Url;
use yii\web\Link;
use yii\web\Linkable;

/**
 * Class Customer
 * @package api\resources
 */
class Customer extends \common\models\Patient
{
    /**
     * @inheritdoc
     * @return array
     */
    public function fields()
    {
        return ['id', 'name', 'phone', 'email', 'year_of_birth', 'address'];
    }

    /**
     * @return string
     */
    function getName()
    {
        return $this->fullname;
    }

    /**
     * @return string|null
     */
    function getAddress()
    {
        return $this->getFullAddress();
    }
}
