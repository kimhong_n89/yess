<?php

namespace api\resources;

use yii\helpers\Url;
use yii\web\Link;
use yii\web\Linkable;

/**
 * @author Tyson <tyson@itoideal.com>
 */
class Province extends \common\models\Province
{
    /**
     * @inheritdoc
     * @return array
     */
    public function fields()
    {
        return ['province_id', 'name'];
    }


    /**
     * @inheritdoc
     * @return array
     */
    public function extraFields()
    {
        return ['districts'];
    }

    /***
     * Get list of district
     * @return mixed
     */
    public function getDistricts()
    {
        return District::find()->where(['province_id' => $this->province_id])->orderBy("name asc")->all();
    }
}

