<?php

namespace api\resources;

use common\models\PrescriptionDiagnose;
use yii\helpers\Url;
use yii\web\Link;
use yii\web\Linkable;

/**
 * Class Prescription
 * @package api\resources
 */
class Prescription extends \common\models\Prescription
{
    /**
     * @inheritdoc
     * @return array
     */
    public function fields()
    {
        return ['id', 'diseases', 'doctor', 'customer', 'items', 'total_amount', 'insurance_number', 'is_insurance', 'note'];
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    function getItems()
    {
        return PrescriptionDetail::find()->where(["prescription_id" => $this->id])->all();
    }

    /**
     * @return array
     */
    function getDiseases()
    {
        $result = [];
        $items = PrescriptionDiagnose::find()->where(["prescription_id" => $this->id])->all();
        /** @var PrescriptionDiagnose $item */
        foreach ($items as $item) {
            $result[] = Disease::findOne($item->diagnose_id);
        }
        return $result;
    }

    /**
     * @return Customer|null
     */
    function getCustomer()
    {
        return Customer::findOne($this->patient_id);
    }

    /**
     * @return Doctor|\yii\db\ActiveQuery|null
     */
    function getDoctor()
    {
        return Doctor::findOne($this->doctor_id);
    }

    /**
     * @return int
     */
    function getIs_insurance()
    {
        return $this->is_use_insurance;
    }
}

