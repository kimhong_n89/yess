<?php

namespace api\resources;

use yii\helpers\Url;
use yii\web\Link;
use yii\web\Linkable;

/**
 * Class Doctor
 * @package api\resources
 */
class Doctor extends \common\models\Doctor
{
    /**
     * @inheritdoc
     * @return array
     */
    public function fields()
    {
        return ['id', 'name', 'phone', 'email', 'image'];
    }

    /**
     * @return string|null
     */
    function getName()
    {
        return $this->fullname;
    }

    /**
     * @return string|null
     */
    function getImage()
    {
        return $this->getAvatarUrl();
    }
}
