<?php

namespace api\resources;

use yii\helpers\Url;
use yii\web\Link;
use yii\web\Linkable;

/**
 * @author Tyson <tyson@itoideal.com>
 */
class ArticleCategory extends \common\models\ArticleCategory
{
    /**
     * @inheritdoc
     * @return array
     */
    public function fields()
    {
        return ['id', 'title'];
    }
}
