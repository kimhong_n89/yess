<?php

namespace api\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\rest\Serializer;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class ApiBaseController
 */

// This API key alway send from client in request header
define('API_KEY', '8f08c41546ffa44a39c593a430239e8');
class ApiBaseController extends EmptyBaseController
{
    /**
     * Customer token when logged in to API
     * This param will be available if user logged in
     * @var string
     */
    protected $customer_token;

    protected $current_language;
    /**
     * Init controller
     * {@inheritDoc}
     * @see \yii\base\Object::init()
     */
    public function init()
    {
        parent::init();
        // Check API key
        if(!isset($this->header['api_key']) || $this->header['api_key'] != API_KEY) {
            throw new HttpException(self::ERROR_API_GENERAL_ERROR_STATUS, \Yii::t("api","Api key is missing."), self::ERROR_API_GENERAL_ERROR_CODE);
        }
        // Check language key
        $this->current_language = "en";
        if(isset($this->header['lang'])) {
            $this->current_language = $this->header['lang'];
            \Yii::$app->language = $this->current_language;
        }
    }
}
