<?php

namespace api\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\rest\Serializer;
use yii\web\Response;

/**
 * Class EmptyBaseController
 */
class EmptyBaseController extends ActiveController
{
    /**
     * Paging size
     */
    const PAGE_SIZE = 20;
    /**
     * Error code
     */
    const ERROR_API_GENERAL_ERROR_CODE = 405;
    /**
     * Error status
     */
    const ERROR_API_GENERAL_ERROR_STATUS = 400;

    /**
     * Header object
     */
    protected $header;

    /**
     * Request object
     * @var \yii\web\Request
     */
    public $request;

    /**
     * @var array
     */
    public $serializer = [
        'class' => Serializer::class,
        'collectionEnvelope' => 'data'
    ];

    /**
     * Default behaviors
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ]
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['POST', 'PUT', 'PATCH'],
            'delete' => ['DELETE'],
        ];
    }
    /**
     * Init controller
     * {@inheritDoc}
     * @see \yii\base\Object::init()
     */
    public function init()
    {
        // Assign environment params
        $this->header = Yii::$app->request->getHeaders();
        $this->request = Yii::$app->request;
    }

    /**
     * Get request params
     */
    public function getParam($key, $default = "")
    {
        $this->request = \Yii::$app->request;
        if($this->request->getIsGet()) {
            return $this->request->getQueryParam($key, $default);
        }
        return $this->request->getBodyParam($key, $default);
    }
}
