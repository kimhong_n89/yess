<?php

namespace api\modules\v1\controllers;

use api\controllers\ApiBaseController;
use api\resources\District;
use api\resources\Province;
use yii\rest\OptionsAction;

/**
 * Class PublicController
 * @author Tyson <tyson@itoideal.com>
 */
class PublicController extends ApiBaseController
{
    public $modelClass = 'Array';
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'options' => [
                'class' => OptionsAction::class
            ]
        ];
    }

    /**
     * Get list of provinces
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionProvinces()
    {
        return Province::find()->orderBy("name asc")->all();
    }

    /**
     * Get list of distrits
     * @param $id
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionDistricts($id)
    {
        return District::find()->where(['province_id' => $id])->orderBy("name asc")->all();
    }
}
