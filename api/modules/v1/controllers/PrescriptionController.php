<?php

use api\controllers\ApiBaseController;
use api\resources\Prescription;
use yii\rest\OptionsAction;

class PublicController extends ApiBaseController
{
    public $modelClass = 'Array';
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'options' => [
                'class' => OptionsAction::class
            ]
        ];
    }

    /**
     * @return Prescription|null
     * @throws \yii\web\HttpException
     */
    public function actionDetail()
    {
        $id = $this->getParam("id", 0);
        if(!empty($id)) {
            $item = Prescription::findOne($id);
            if(empty($item)) {
                throw new \yii\web\HttpException(self::ERROR_API_GENERAL_ERROR_STATUS, "Thông tin toa thuốc không hợp lệ.");
            }
            return $item;
        } else {
            throw new \yii\web\HttpException(self::ERROR_API_GENERAL_ERROR_STATUS, "Thông tin toa thuốc không hợp lệ.");
        }
    }

    public function actionLoadByDoctor()
    {

    }

    public function actionLoadByCustomer()
    {

    }
}