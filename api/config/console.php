<?php
return [
    'id' => 'api_console',
    'bootstrap' => [
        'customer'
    ],
    'modules' => [
        'customer' => \api\modules\v1\Module::class
    ],
    'components' => [
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => yii\web\User::class,
        ],
        'errorHandler' => [
            'class'  => 'yii\console\ErrorHandler',
        ],
    ]
];
