<?php

namespace backend\controllers;

use backend\models\search\PrescriptionSearch;
use common\components\utils\PermissionConstant;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Yii;
use common\models\Patient;
use backend\models\search\PatientSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PatientController implements the CRUD actions for Patient model.
 */
class PatientController extends BackendController
{
    const ACTION_EXPORT = 'export';

    /** @inheritdoc */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    /**
     * Lists all Patient models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PatientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (rbac_is_allow_exec(PermissionConstant::EXPORT_PATIENT) && Yii::$app->request->get('act') == self::ACTION_EXPORT) {
            return $this->exportData($dataProvider->getModels());
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Patient model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $searchModel = new PrescriptionSearch();
        $params = ArrayHelper::merge(Yii::$app->request->queryParams, ['PrescriptionSearch' => ['patient_id' => $id]]);
        $dataProvider = $searchModel->search($params);
        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Patient model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Patient();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Patient model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Patient model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->softDelete();
        if ($url = Yii::$app->request->get('redirect')) {
            return $this->redirect($url);
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Patient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Patient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Patient::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionFind($term)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $row_set = [];
        $patients = Patient::find()->where([
            'OR',
            ['LIKE', 'fullname', $term],
            ['LIKE', 'phone', $term]
        ])->asArray()->all();
        foreach ($patients as $patient) {
            $row_set[] = [
                'id' => $patient['id'],
                'label' => $patient['fullname'] . ' - ' . $patient['phone'],
                'value' => $patient['fullname'],
                'phone' => $patient['phone'],
                'email' => $patient['email'],
                'year' => $patient['year_of_birth'],
                'gender' => $patient['gender'],
                'province' => $patient['province_id'],
                'district' => $patient['district_id'],
                'ward' => $patient['ward_id'],
                'address' => $patient['address'],
            ];
        }
        //$row_set = ArrayHelper::map($patients,'id', 'fullname');
        return $row_set;
    }

    public function exportData( $dataModel, $file_name = null)
    {
        if (!$file_name) {
            $file_name = "DanhSachBenhNhan-" . date('d-m-Y');
        }

        $spreadsheet = new Spreadsheet();
        // Set document properties
        $alphas = range('A', 'Z');
        $index = 0;
        $row = 2;
        $array = [];
        $columns = ['ID', "Họ và tên", "Số điện thoại", "Email", "Năm sinh", "Giới tính", "Khu vực", "Quận", "Tổng số toa", "Trạng thái", "Tạo lúc"];

        $spreadsheet->getActiveSheet()->mergeCells("A1:J1");
        $spreadsheet->setActiveSheetIndex(0)->setCellValue("A1" , 'Danh sách Khách hàng');
        $spreadsheet->getActiveSheet()->getStyle("A1:J1")->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'vertical' => Alignment::VERTICAL_CENTER,
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ]);

        // Add some data
        foreach ($columns as $column) {
            $spreadsheet->setActiveSheetIndex(0)->setCellValue($alphas[$index] . $row , $column);
            $array[$alphas[$index] . $row] = $column;
            $spreadsheet->getActiveSheet()->getStyle("A{$row}:K{$row}")->applyFromArray([
                'alignment' => [
                    'vertical' => Alignment::VERTICAL_CENTER,
                ],
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ]
                ],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'startColor' => [
                        'argb' => adjustBrightness('00A9A0', 0.9),
                    ],
                ],
            ]);
            $index++;
        }

        $row = 3;
        // Miscellaneous glyphs, UTF-8

        foreach ($dataModel as $model) {
            /**
             * @var Patient $model
             */
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("A{$row}", $model->id);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("B{$row}", Html::encode($model->fullname));
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("B")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("C{$row}", Html::encode($model->phone));
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("C")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("D{$row}", Html::encode($model->email));
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("D")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("E{$row}", $model->year_of_birth);
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("E")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("F{$row}", @Patient::genders()[$model->gender]);
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("F")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("G{$row}", @$model->province->name);
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("G")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("H{$row}", @$model->district->name);
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("H")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("I{$row}", $model->count_prescription);
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("I")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("J{$row}", @Patient::statuses()[$model->status]);
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("J")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("K{$row}", Yii::$app->formatter->asDatetime($model->created_at, 'medium'));
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("K")->setAutoSize(true);
            $row++;
        }
        //dd1($spreadsheet->getActiveSheet()->toArray(null, true, true, true));
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename=$file_name.xlsx");
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }
}
