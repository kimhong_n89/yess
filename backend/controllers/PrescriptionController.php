<?php

namespace backend\controllers;

use common\components\utils\PermissionConstant;
use common\models\MedicineForm;
use common\models\GeneralModel;
use common\models\MedicineServiceForm;
use common\models\Patient;
use common\models\PrescriptionDetail;
use common\models\PrintedReport;
use common\models\Product;
use Yii;
use common\models\Prescription;
use backend\models\search\PrescriptionSearch;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

/**
 * PrescriptionController implements the CRUD actions for Prescription model.
 */
class PrescriptionController extends BackendController
{
    const TAB_ALL = 'all';
    const TAB_DONE = 'done';
    const TAB_DRAFT = 'draft';
    const TAB_DELETED = 'deleted';
    const TAB_DELIVERY_TO_STORE = 'delivery';

    const ACTION_EXPORT = 'export';

    /** @inheritdoc */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    /**
     * Lists all Prescription models.
     * @return mixed
     */
    public function actionIndex($tab = self::TAB_ALL)
    {
        $searchModel = new PrescriptionSearch();
        switch ($tab) {
            case self::TAB_DONE:
                $params = ArrayHelper::merge(Yii::$app->request->queryParams, ['PrescriptionSearch' => ['status' => Prescription::STATUS_DONE]]);
                break;
            case self::TAB_DRAFT:
                $params = ArrayHelper::merge(Yii::$app->request->queryParams, ['PrescriptionSearch' => ['status' => Prescription::STATUS_DRAFT]]);
                break;
            case self::TAB_DELETED:
                $params = ArrayHelper::merge(Yii::$app->request->queryParams, ['PrescriptionSearch' => ['status' => Prescription::STATUS_DELETED]]);
                break;
            case self::TAB_DELIVERY_TO_STORE:
                $params = ArrayHelper::merge(Yii::$app->request->queryParams, ['PrescriptionSearch' => ['status' => Prescription::STATUS_DELIVERY_TO_STORE]]);
                break;
            default:
                $params = Yii::$app->request->queryParams;
        }

        $dataProvider = $searchModel->search($params);
        if (rbac_is_allow_exec(PermissionConstant::EXPORT_PRESCRIPTION) && Yii::$app->request->get('act') == self::ACTION_EXPORT) {
            return $this->exportData($dataProvider->getModels());
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tab' => $tab
        ]);
    }

    /**
     * Displays a single Prescription model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Prescription model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Prescription();
        $model->doctor_id = Yii::$app->user->id;
        $modelPatient = new Patient();
        $modelMedicineInsurance = [new MedicineForm()];
        $modelMedicineService = [new MedicineServiceForm()];

        if ($model->load(Yii::$app->request->post()) && $modelPatient->load(Yii::$app->request->post())) {

            $modelMedicineInsurance = GeneralModel::createMultiple(MedicineForm::classname(), $modelMedicineInsurance);
            Model::loadMultiple($modelMedicineInsurance, Yii::$app->request->post());

            $modelMedicineService = GeneralModel::createMultiple(MedicineServiceForm::classname(), $modelMedicineService);
            Model::loadMultiple($modelMedicineService, Yii::$app->request->post());

            $valid = true;
            $transaction = \Yii::$app->db->beginTransaction();
            if (!$model->patient_id) {
                //create new patient
                $valid = $modelPatient->save();
                $model->patient_id = $modelPatient->id;
            }

            // validate all models
            $valid = $model->validate() && $valid;
            if ($modelMedicineInsurance) {
                $valid = Model::validateMultiple($modelMedicineInsurance) && $valid;
            }
            if ($modelMedicineService) {
                $valid = Model::validateMultiple($modelMedicineService) && $valid;
            }
            if ($valid) {
                try {
                    if ($flag = $model->save()) {
                        //calculate total_amount
                        $total_amount = 0;
                        foreach ($modelMedicineInsurance as $modelDetail) {
                            $modelDetail->prescription_id = $model->id;
                            $price = Product::findOne($modelDetail->medicine_id)->price ;
                            $total_amount += $price * $modelDetail->amount;
                            if (! ($flag = $modelDetail->save())) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                        foreach ($modelMedicineService as $modelDetail) {
                            $modelDetail->prescription_id = $model->id;
                            $price = Product::findOne($modelDetail->medicine_id)->price ;
                            $total_amount += $price * $modelDetail->amount;
                            if (! ($flag = $modelDetail->save())) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                        $model->total_amount = $total_amount;
                        $model->save(false);
                    }
                    if ($flag) {
                        $transaction->commit();

                        $action = Yii::$app->request->post('btn-submit', 'save');
                        if ($action == 'save') {
                            Yii::$app->session->setFlash('alert', [
                                'options' => ['class' => 'alert-success'],
                                'body' => Yii::t('frontend', 'Toa thuốc của bạn đã được tạo thành công')
                            ]);
                            return $this->redirect(['update', 'id' => $model->id]);
                        }
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            } else {
                $transaction->rollBack();
            }
        }

        $min_insurance = count($modelMedicineInsurance) > 0 ? 1 : 0;
        $min_service = count($modelMedicineService) > 0 ? 1 : 0;
        return $this->render('create', [
            'model' => $model,
            'modelPatient' => $modelPatient,
            'modelMedicineInsurance' => (empty($modelMedicineInsurance)) ? [new MedicineForm()] : $modelMedicineInsurance,
            'modelMedicineService' => (empty($modelMedicineService)) ? [new MedicineServiceForm()] : $modelMedicineService,
            'min_insurance' => $min_insurance,
            'min_service' => $min_service,
        ]);
    }

    /**
     * Updates an existing Prescription model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $min_insurance = $min_service = 0;
        $model = $this->findModel($id);
        $modelPatient = $model->patient;
        $prescriptionDetails = $model->prescriptionDetails;
        $modelDetails = ArrayHelper::index($prescriptionDetails, null, 'type');
        if (!empty($modelDetails[PrescriptionDetail::TYPE_MEDICINE_INSURANCE])) {
            foreach ($modelDetails[PrescriptionDetail::TYPE_MEDICINE_INSURANCE] as $modelDetail) {
                $modelMedicineInsurance[] = (new MedicineForm())->setModel($modelDetail);
            }
            $min_insurance = 1;
        } else {
            $modelMedicineInsurance = [new MedicineForm()];
        }
        if (!empty($modelDetails[PrescriptionDetail::TYPE_MEDICINE_SERVICE])) {
            foreach ($modelDetails[PrescriptionDetail::TYPE_MEDICINE_SERVICE] as $modelDetail) {
                $modelMedicineService[] = (new MedicineServiceForm())->setModel($modelDetail);
            }
            $min_service = 1;
        } else {
            $modelMedicineService = [new MedicineServiceForm()];
        }

        $oldDetails = ArrayHelper::getColumn($prescriptionDetails, 'id');

        if ($model->load(Yii::$app->request->post()) && $modelPatient->load(Yii::$app->request->post())) {

            $modelMedicineInsurance = GeneralModel::createMultiple(MedicineForm::classname(), $modelMedicineInsurance);
            Model::loadMultiple($modelMedicineInsurance, Yii::$app->request->post());

            $modelMedicineService = GeneralModel::createMultiple(MedicineServiceForm::classname(), $modelMedicineService);
            Model::loadMultiple($modelMedicineService, Yii::$app->request->post());

            $min_insurance = count($modelMedicineInsurance) > 0 ? 1 : 0;
            $min_service = count($modelMedicineService) > 0 ? 1 : 0;

            $valid = true;
            $transaction = \Yii::$app->db->beginTransaction();
            if (!$model->patient_id) {
                //create new patient
                $valid = $modelPatient->save();
                $model->patient_id = $modelPatient->id;
            }

            // validate all models
            $valid = $model->validate() && $valid;
            if ($modelMedicineInsurance) {
                $valid = Model::validateMultiple($modelMedicineInsurance) && $valid;
            }
            if ($modelMedicineService) {
                $valid = Model::validateMultiple($modelMedicineService) && $valid;
            }

            if ($valid) {
                try {
                    if ($flag = $model->save()) {
                        //calculate total_amount
                        $total_amount = 0;
                        foreach ($modelMedicineInsurance as $modelDetail) {
                            $modelDetail->prescription_id = $model->id;
                            $price = Product::findOne($modelDetail->medicine_id)->price ;
                            $total_amount += $price * $modelDetail->amount;
                            if (! ($flag = $modelDetail->save())) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                        foreach ($modelMedicineService as $modelDetail) {
                            $modelDetail->prescription_id = $model->id;
                            $price = Product::findOne($modelDetail->medicine_id)->price ;
                            $total_amount += $price * $modelDetail->amount;
                            if (! ($flag = $modelDetail->save())) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                        $model->total_amount = $total_amount;
                        $model->save(false);
                    }
                    //delete old prescription detail
                    PrescriptionDetail::deleteAll(['IN', 'id', $oldDetails]);
                    if ($flag) {
                        $transaction->commit();
                        $action = Yii::$app->request->post('btn-submit', 'save');
                        if ($action == 'save') {
                            Yii::$app->session->setFlash('alert', [
                                'options' => ['class' => 'alert-success'],
                                'body' => Yii::t('frontend', 'Toa thuốc của bạn đã được cập nhật thành công')
                            ]);
                            return $this->refresh();
                        }
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            } else {
                $transaction->rollBack();
            }
        }
        return $this->render('update', [
            'model' => $model,
            'modelPatient' => $modelPatient,
            'modelMedicineInsurance' => (empty($modelMedicineInsurance)) ? [new MedicineForm()] : $modelMedicineInsurance,
            'modelMedicineService' => (empty($modelMedicineService)) ? [new MedicineServiceForm()] : $modelMedicineService,
            'min_insurance' => $min_insurance,
            'min_service' => $min_service,
        ]);
    }

    /**
     * Deletes an existing Prescription model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->softDelete();
        if ($url = Yii::$app->request->get('redirect')) {
            return $this->redirect($url);
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Prescription model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Prescription the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Prescription::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Save print html to db
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionPrint($id)
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = Prescription::findOne(['id' => $id]);
        if ($model) {
            $printed = new PrintedReport();
            $printed->prescription_id = $id;
            $printed->html = $this->renderPartial('view/_view', ['model' => $model, 'print' => true]);
            $printed->save();
            if($printed->sendEmailPrescription()) {
                return [
                    'title'=> Yii::t('checkin', 'Thành công!'),
                    'content' => 'Toa thuốc của bạn đã được IN và gửi đến khách hàng',
                    'footer' => ''
                ];
            } else {
                return [
                    'title'=> Yii::t('checkin', 'Thất bại!'),
                    'content' => 'Đã có lỗi trong quá trình gửi toa, vui lòng thử lại sau',
                    'footer' => ''
                ];
            }
            exit();
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function exportData( $dataModel, $file_name = null)
    {
        if (!$file_name) {
            $file_name = "DanhSachToaThuoc-" . date('d-m-Y');
        }

        $spreadsheet = new Spreadsheet();
        // Set document properties
        $alphas = range('A', 'Z');
        $index = 0;
        $row = 2;
        $array = [];
        $columns = ['ID', "Bác sĩ", "Bệnh nhân", "Khu vực", "Quận", "BHYT", "Chẩn đoán", "Trạng thái"];

        $spreadsheet->getActiveSheet()->mergeCells("A1:J1");
        $spreadsheet->setActiveSheetIndex(0)->setCellValue("A1" , 'Danh sách Toa Thuốc');
        $spreadsheet->getActiveSheet()->getStyle("A1:J1")->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'vertical' => Alignment::VERTICAL_CENTER,
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ]);

        // Add some data
        foreach ($columns as $column) {
            $spreadsheet->setActiveSheetIndex(0)->setCellValue($alphas[$index] . $row , $column);
            $array[$alphas[$index] . $row] = $column;
            $spreadsheet->getActiveSheet()->getStyle("A{$row}:H{$row}")->applyFromArray([
                'alignment' => [
                    'vertical' => Alignment::VERTICAL_CENTER,
                ],
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ]
                ],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'startColor' => [
                        'argb' => adjustBrightness('00A9A0', 0.9),
                    ],
                ],
            ]);
            $index++;
        }

        $row = 3;
        // Miscellaneous glyphs, UTF-8

        foreach ($dataModel as $model) {
            /**
             * @var Prescription $model
             */
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("A{$row}", $model->id);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("B{$row}", Html::encode(@$model->doctor->fullname));
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("B")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("C{$row}", Html::encode(@$model->patient->fullname));
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("C")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("D{$row}", @$model->patient->province->name);
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("D")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("E{$row}", @$model->patient->district->name);
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("E")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("F{$row}", @Prescription::yesno()[$model->is_use_insurance]);
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("F")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("G{$row}" , implode(' / ', \yii\helpers\ArrayHelper::getColumn($model->diagnoses, 'name')));
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("G")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("H{$row}", @Prescription::statuses()[$model->status]);
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("H")->setAutoSize(true);
            $row++;
        }
        //dd1($spreadsheet->getActiveSheet()->toArray(null, true, true, true));
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename=$file_name.xlsx");
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }
}
