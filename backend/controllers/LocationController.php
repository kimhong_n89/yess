<?php

namespace backend\controllers;

use backend\models\search\BusinessFieldSearch;
use backend\models\search\DistrictSearch;
use backend\models\search\DosageFormSearch;
use backend\models\search\DruggedTimeSearch;
use backend\models\search\ProvinceSearch;
use backend\models\search\WardSearch;
use common\models\DosageForm;
use common\models\DruggedTime;
use Yii;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use rmrevin\yii\fontawesome\FAS;

class LocationController extends BackendController
{
    const TAB_PROVINCE = 'province';
    const TAB_DISTRICT = 'district';
    const TAB_WARD = 'ward';

    /** @inheritdoc */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }
    
    /**
     * Lists all SpaSetting models.
     * @return mixed
     */
    public function actionIndex($tab = self::TAB_PROVINCE)
    {
        switch ($tab) {
            case self::TAB_WARD :
                $searchModel = new WardSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                break;
            case self::TAB_DISTRICT :
                $searchModel = new DistrictSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                break;
            case self::TAB_PROVINCE:
            default:
            $searchModel = new ProvinceSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tab' => $tab
        ]);
    }
}
