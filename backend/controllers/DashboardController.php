<?php

namespace backend\controllers;

use backend\models\search\LoginHistorySearch;
use backend\models\search\PrescriptionSearch;
use common\models\Doctor;
use common\models\LoginHistory;
use common\models\Patient;
use common\models\Prescription;
use Yii;
use yii\web\Controller;
use yii\helpers\ArrayHelper;

class DashboardController extends BackendController
{
    public $layout = 'common';

    public function actionIndex($month = null, $year = null)
    {
        if (empty($month) || empty($year)) {
            $month = date('m');
            $year = date('Y');
        }
        $doctor_count = Doctor::find()->count();
        $customer_count = Patient::find()->count();
        $prescription_count = Prescription::find()->count();
        
        $prescriptionSearch = new PrescriptionSearch();
        $prescriptionProvider = $prescriptionSearch->newest();
        $searchLoginHistory = new LoginHistorySearch();
        $loginHistoryProvider = $searchLoginHistory->newest();
        $chartData = $this->getPrescriptionData($month, $year);
        
        return $this->render('index', [
            'doctor_count' => $doctor_count,
            'customer_count' => $customer_count,
            'prescription_count' => $prescription_count,
            'prescriptionProvider' => $prescriptionProvider,
            'loginHistoryProvider' => $loginHistoryProvider,
            'chartData' => $chartData,
            'month' => $month,
            'year' => $year,
            'previous' => $this->getPrevious($month, $year),
            'next' => $this->getNext($month, $year)
        ]);
    }
    
    public function getPrescriptionData($month, $year) {
        //cal_days_in_month();
        $select = "SELECT count(id) as prescription, DAY(FROM_UNIXTIME(created_at)) as `day` FROM " . Prescription::tableName();
        $where = sprintf("WHERE MONTH(FROM_UNIXTIME(created_at)) = %1s AND YEAR(FROM_UNIXTIME(created_at)) = %2s", $month, $year);
        //dd1($where);
        $groupBy = 'GROUP BY `created_at`';
        $result = ArrayHelper::map(Yii::$app->db->createCommand("$select $where $groupBy")->queryAll(), 'day', 'prescription');
        $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $highest = 0;
        $labels = $series = [];
        for ($i = 1; $i <= $days; $i++) {
            $value = isset($result[$i]) ? $result[$i] : 0;
            $labels[] = $i;
            $series[] = ['meta' => "$i-$month", 'value' => $value];
            $highest = ($highest < $value) ? $value : $highest;
        }
        
        return [
            'labels' => $labels,
            'series' => $series,
            'highest' => $highest
        ];
    }
    
    public function getPrevious($month, $year) {
        if ($month == 1) {
            $month = 12;
            $year--;
        } else {
            $month--;
        }
        return ['month' => $month, 'year' => $year];
    }
    public function getNext($month, $year) {
        if ($month == 12) {
            $month = 1;
            $year++;
        } else {
            $month++;
        }
        return ['month' => $month, 'year' => $year];
    }
}
