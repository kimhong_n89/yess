<?php

namespace backend\controllers;

use common\models\FavoriteStore;
use common\models\Patient;
use common\models\Product;
use common\models\Ward;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use common\models\District;

/**
 * AddressController implements the CRUD actions for Attendee model.
 */
class AjaxController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // Assuming admin can do all actions while editor only do "create" function
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ]
            ]
        ];
    }
    /**
     * District API
     * @return array
     */
    public function actionGetDistricts()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $province_id = $parents[0];
                $lists = District::find()->where(['id_state' => $province_id])->orderBy('name')->all();
                foreach ($lists as $district) {
                    $out[] = ['id' => $district->id_district, 'name' => $district->name];
                }
                return ['output'=>$out, 'selected'=>''];
            }
        }
        return ['output'=>'', 'selected'=>''];
    }

    /**
     * List of wards
     * @return array
     */
    public function actionGetWards()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $id_district = $parents[0];
                $lists = Ward::find()->where(['id_district' => $id_district])->orderBy('name')->all();
                foreach ($lists as $ward) {
                    $out[] = ['id' => $ward->id_ward, 'name' => $ward->name];
                }
                return ['output'=>$out, 'selected'=>''];
            }
        }
        return ['output'=>'', 'selected'=>''];
    }

    /**
     * @param $term
     * @return array
     */
    public function actionFindPatient($term)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $row_set = [];
        $patients = Patient::find()->where([
            'OR',
            ['LIKE', 'fullname', $term],
            ['LIKE', 'phone', $term]
        ])->orderBy('fullname')->asArray()->all();
        foreach ($patients as $patient) {
            $row_set[] = [
                'id' => $patient['id'],
                'label' => $patient['fullname'] . ' - ' . $patient['phone'],
                'value' => $patient['fullname'],
                'phone' => $patient['phone'],
                'email' => $patient['email'],
                'year' => $patient['year_of_birth'],
                'gender' => $patient['gender'],
                'province' => $patient['province_id'],
                'district' => $patient['district_id'],
                'ward' => $patient['ward_id'],
                'address' => $patient['address'],
            ];
        }

        return $row_set;
    }

    /**
     * @return array
     */
    public function actionGetProductsByDiagnose()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_all_params'])) {
            $parents = $_POST['depdrop_all_params'];
            if (!empty($parents['sel-diagnose'])) {
                $id_diagnoses = $parents['sel-diagnose'];
                $query = Product::find()->join('INNER JOIN', 'product_feature', 'product.id=product_feature.product_id')
                    ->join('INNER JOIN', 'disease_feature_value', 'product_feature.feature_value_id=disease_feature_value.feature_value_id')
                    ->where(['>', 'price', 0])
                    ->andWhere(['IN', 'disease_feature_value.disease_id', $id_diagnoses]);
                $lists = $query->all();
                foreach ($lists as $product) {
                    $out[] = [
                        'id' => $product->id,
                        'name' => $product->name,
                        'options' => ['data-price' => $product->price]
                    ];
                }
                return ['output'=>$out, 'selected'=>''];
            }
        }
        return ['output'=>'', 'selected'=>''];
    }

    public function actionSearchProducts($q = null, $page = 1) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $total_count = 0;
        $results = [];
        $ignoreIDs = [];
        $id_diagnoses = Yii::$app->request->get('diagnoses');
        $limit = 30;
        $offset = ($page - 1) * $limit;
        if ($id_diagnoses) {
            $query = Product::find()->join('INNER JOIN', 'product_feature', 'product.id=product_feature.product_id')
                            ->join('INNER JOIN', 'disease_feature_value', 'product_feature.feature_value_id=disease_feature_value.feature_value_id')
                            ->where(['>', 'price', 0])
                            ->andWhere(['IN', 'disease_feature_value.disease_id', $id_diagnoses]);
            $query->orderBy(['name' => SORT_ASC]);
            if ($q) {
                $query->andWhere(['LIKE', 'name', $q]);
                $ignoreIDs = ArrayHelper::getColumn($query->all(), 'id');

                //set new query
                $query = Product::find()->select([
                    'id',
                    'name',
                    'price',
                    'IF (FIND_IN_SET(id, "' . implode(',', $ignoreIDs) . '") > 0, 0, 1) AS ordinal'
                ])->where(['>', 'price', 0])->andWhere(['like', 'name', $q])->orderBy(['ordinal' => SORT_ASC, 'name' => SORT_ASC]);
            }
        } else {
            if ($q) {
                $query = Product::find()->where(['>', 'price', 0])->andWhere(['like', 'name', $q]);
                $query->orderBy(['name' => SORT_ASC]);
            }
        }

        if (isset($query)) {
            $query->limit($limit)->offset($offset);
            $total_count = $query->count();
            $lists = $query->all();
            foreach ($lists as $product) {
                $results[] = [
                    'id' => $product->id,
                    'text' => $product->name,
                    'options' => ['price' => $product->price]
                ];
            }
        }
        $out = ['total_count' => $total_count, 'items' => $results];
        return $out;
    }
}
