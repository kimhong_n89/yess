<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use backend\modules\rbac\models\SysAuthRoute;

class BackendController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // Assuming admin can do all actions while editor only do "create" function
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (rbac_is_super_admin()) {
                                return true;
                            }
                            // Check if user is deactivated or not
                            if (\Yii::$app->user->identity->status != 2) {
                                return false;
                            }
                            // Get route path at first
                            // $action = Yii::$app->controller->action->id;
                            $controller = Yii::$app->controller->id;
                            // Make a route path
                            $route = "$controller/$action->id"; // = pages/index or menu/index
                            // Find the route id
                            $sys_auth_route = SysAuthRoute::find()->where([
                                'route' => $route
                            ])->one();
                            // Declared $check for checking permission
                            $check = false;
                            /**
                             * Check if route path is not empty:
                             * - Find the permission-id throught route-path
                             * - Check if permission-id is not empty
                             * - If yes, continue to check if user have that permission
                             * - If user have that permission, $check = true and break out of foreach loop
                             */
                            if (! empty($sys_auth_route)) {
                                $_sys_routes = $sys_auth_route->sysAuthPermRoutes;
                                if (! empty($_sys_routes)) {
                                    foreach ($_sys_routes as $_route) {
                                        if (Yii::$app->user->can($_route->auth_item)) {
                                            $check = true;
                                            break;
                                        }
                                        $check = false;
                                    }
                                }
                            }
                            // Allow access for user with permission or super admins are defined in params
                            if ($check || in_array($route, Yii::$app->params['ignoreRoute']) || in_array($controller . '/*', Yii::$app->params['ignoreRoute'])) {
                                return true;
                            }
                        }
                    ]
                ]
            ]
        ];
    }
}
