<?php

namespace backend\controllers;

use common\models\Certification;
use common\models\Doctor;
use common\models\DoctorSetting;
use common\models\FileStorageItem;
use common\models\MedicineForm;
use common\models\MedicineServiceForm;
use common\models\Patient;
use common\models\Prescription;
use frontend\models\TemplateForm;
use trntv\filekit\actions\DeleteAction;
use trntv\filekit\actions\UploadAction;
use Yii;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use rmrevin\yii\fontawesome\FAS;

class ManageFileController extends BackendController
{

    /** @inheritdoc */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    /** @inheritdoc */
    public function actions()
    {
        return [
            'upload' => [
                'class' => UploadAction::class,
                'deleteRoute' => 'delete',
                'on afterSave' => function($event) {
                    /* @var $file \League\Flysystem\File */
                    $file = $event->file;
                    $model = new DoctorSetting();
                    $model->doctor_id = Yii::$app->user->identity->id;
                    $model->key = Yii::$app->request->get('key');
                    $model->value = $file->getPath();
                    $model->save();

                }
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'on afterDelete' => function($event) {
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    Certification::deleteAll(['path' => Yii::$app->request->get('path')]);
                }
            ],
        ];
    }

    public function actionDownload($path)
    {
        $file = FileStorageItem::find()->where(['path' => $path])->one();

        if ($file) {
            // Send a header saying we'll be displaying a picture
            header('Content-type: ' . $file->type);

            // Name the file
            header('Content-Disposition: attachment; filename="' . str_replace('1/', '', $file->path) . '"');
            readfile($file->base_url .'/'. $file->path);
            die;
        }
        throw new NotFoundHttpException('The requested page does not exist.');

    }
}
