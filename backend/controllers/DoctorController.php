<?php

namespace backend\controllers;

use backend\models\search\CertificationSearch;
use backend\models\search\LoginHistorySearch;
use backend\models\search\PatientSearch;
use backend\models\search\PrescriptionSearch;
use common\components\utils\PermissionConstant;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Yii;
use common\models\Doctor;
use backend\models\search\DoctorSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DoctorController implements the CRUD actions for Doctor model.
 */
class DoctorController extends BackendController
{
    const TAB_ALL = 'all';
    const TAB_PENDING = 'pending';
    const TAB_APPROVED = 'approved';

    const TAB_PRESCRIPTION = 'prescription';
    const TAB_PATIENT = 'patient';
    const TAB_CERTIFICATION = 'certification';
    const TAB_LOGIN = 'login';

    const ACTION_EXPORT = 'export';

    /** @inheritdoc */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'approve' => ['post'],
                ],
            ],
        ]);
    }

    /**
     * Lists all Doctor models.
     * @return mixed
     */
    public function actionIndex($tab = self::TAB_ALL)
    {
        $searchModel = new DoctorSearch();

        switch ($tab) {
            case self::TAB_PENDING:
                $params = ArrayHelper::merge(Yii::$app->request->queryParams, ['DoctorSearch' => ['status' => Doctor::STATUS_PENDING]]);
                break;
            case self::TAB_APPROVED:
                $params = ArrayHelper::merge(Yii::$app->request->queryParams, ['DoctorSearch' => ['status' => Doctor::STATUS_APPROVED]]);
                break;
            default:
                $params = Yii::$app->request->queryParams;
        }

        $dataProvider = $searchModel->search($params);
        if (rbac_is_allow_exec(PermissionConstant::EXPORT_DOCTOR) && Yii::$app->request->get('act') == self::ACTION_EXPORT) {
            return $this->exportData($dataProvider->getModels());
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tab' => $tab
        ]);
    }

    /**
     * Displays a single Doctor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $tab = self::TAB_PRESCRIPTION)
    {
        $model = $this->findModel($id);
        switch ($tab) {
            case self::TAB_PATIENT:
                if (!userCan(PermissionConstant::VIEW_PATIENT)) {
                    $this->redirect(['view', 'id' => $id, 'tab' => self::TAB_CERTIFICATION]);
                }
                $searchModel = new PatientSearch();
                $dataProvider = $searchModel->search(ArrayHelper::merge(Yii::$app->request->queryParams, ['PatientSearch' => ['doctor_id' => $id]]));
                break;
            case self::TAB_CERTIFICATION:
                $searchModel = null;
                $dataProvider = null;
                break;
            case self::TAB_LOGIN:
                if (!userCan(PermissionConstant::VIEW_LOGIN_HISTORY)) {
                    $this->redirect(['view', 'id' => $id, 'tab' => self::TAB_CERTIFICATION]);
                }
                $searchModel = new LoginHistorySearch();
                $dataProvider = $searchModel->search(ArrayHelper::merge(Yii::$app->request->queryParams, ['LoginHistorySearch' => ['phone_number' => $model->phone]]));
                break;
            default:
                if (!userCan(PermissionConstant::VIEW_PRESCRIPTION)) {
                    $this->redirect(['view', 'id' => $id, 'tab' => self::TAB_CERTIFICATION]);
                }
                $searchModel = new PrescriptionSearch();
                $dataProvider = $searchModel->search(ArrayHelper::merge(Yii::$app->request->queryParams, ['PrescriptionSearch' => ['doctor_id' => $id]]));
        }
        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tab' => $tab
        ]);
    }

    /**
     * Creates a new Doctor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Doctor();
        $model->scenario = Doctor::SCENARIO_UPDATE_PROFILE;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Doctor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = Doctor::SCENARIO_UPDATE_PROFILE;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Doctor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->softDelete();
        if ($url = Yii::$app->request->get('redirect')) {
            return $this->redirect($url);
        }
        return $this->redirect(['index']);
    }

    public function actionApprove($id)
    {
        $model = $this->findModel($id);
        $model->status = $model::STATUS_APPROVED;
        $model->save();
        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * Finds the Doctor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Doctor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Doctor::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function exportData( $dataModel, $file_name = null)
    {
        if (!$file_name) {
            $file_name = "DanhSachBacSi-" . date('d-m-Y');
        }

        $spreadsheet = new Spreadsheet();
        // Set document properties
        $alphas = range('A', 'Z');
        $index = 0;
        $row = 2;
        $array = [];
        $columns = ['ID', "Avatar", "Số điện thoại", "Họ và tên", "Email", "Giới tính", "Khu vực", "Quận", "Tạo lúc", "Trạng thái"];

        $spreadsheet->getActiveSheet()->mergeCells("A1:J1");
        $spreadsheet->setActiveSheetIndex(0)->setCellValue("A1" , 'Danh sách Bác sĩ');
        $spreadsheet->getActiveSheet()->getStyle("A1:J1")->applyFromArray([
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'vertical' => Alignment::VERTICAL_CENTER,
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ]);

        // Add some data
        foreach ($columns as $column) {
            $spreadsheet->setActiveSheetIndex(0)->setCellValue($alphas[$index] . $row , $column);
            $array[$alphas[$index] . $row] = $column;
            $spreadsheet->getActiveSheet()->getStyle("A{$row}:J{$row}")->applyFromArray([
                'alignment' => [
                    'vertical' => Alignment::VERTICAL_CENTER,
                ],
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ]
                ],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'startColor' => [
                        'argb' => adjustBrightness('00A9A0', 0.9),
                    ],
                ],
            ]);
            $index++;
        }

        $row = 3;
        // Miscellaneous glyphs, UTF-8

        foreach ($dataModel as $doctor) {
            /**
             * @var Doctor $doctor
             */
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("A{$row}" , $doctor->id);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("B{$row}" , Html::encode($doctor->getAvatarUrl()));
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("B")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("C{$row}" , Html::encode($doctor->phone));
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("C")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("D{$row}" , Html::encode($doctor->fullname));
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("D")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("E{$row}" , Html::encode($doctor->email));
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("E")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("F{$row}" , @Doctor::genders()[$doctor->gender]);
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("F")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("G{$row}" , @$doctor->province->name);
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("G")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("H{$row}" , @$doctor->district->name);
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("H")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("I{$row}" , Yii::$app->formatter->asDatetime($doctor->created_at, 'medium'));
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("I")->setAutoSize(true);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue("J{$row}" , @Doctor::statuses()[$doctor->status]);
            $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("J")->setAutoSize(true);
            $row++;
        }
        //dd1($spreadsheet->getActiveSheet()->toArray(null, true, true, true));
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename=$file_name.xlsx");
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }
}
