<?php

namespace backend\controllers;

use backend\models\search\BusinessFieldSearch;
use backend\models\search\DosageFormSearch;
use backend\models\search\DruggedTimeSearch;
use common\models\DosageForm;
use common\models\DruggedTime;
use Yii;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use rmrevin\yii\fontawesome\FAS;

class MasterDataController extends BackendController
{
    const TAB_BUSINESS_FIELD = 'business-field';
    const TAB_DRUGGEDTIME = 'drugged-time';
    const TAB_DOSAGE_FORM = 'dosage-form';

    /** @inheritdoc */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }
    
    /**
     * Lists all SpaSetting models.
     * @return mixed
     */
    public function actionIndex($tab = self::TAB_BUSINESS_FIELD)
    {
        switch ($tab) {
            case self::TAB_DOSAGE_FORM :
                $searchModel = new DosageFormSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                break;
            case self::TAB_DRUGGEDTIME :
                $searchModel = new DruggedTimeSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                break;
            case self::TAB_BUSINESS_FIELD:
            default:
            $searchModel = new BusinessFieldSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tab' => $tab
        ]);
    }

    public function actionCreateDruggedTime()
    {

        $request = Yii::$app->request;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new DruggedTime();
        $model->status = $model::STATUS_ACTIVE;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return [
                'forceReload' => $request->get('pjax-container', '#crud-datatable-pjax'),
                'forceClose' => true,
                'push' => false,
                'replace' => false
            ];
            exit();
        }
        return [
            'title'=> Yii::t('checkin', 'Tạo thời điểm'),
            'content'=>$this->renderAjax('form/_form_drugged_time', [
                'model' => $model,
            ]),
            'footer' => Html::submitButton(FAS::icon('save').' '.Yii::t('backend', 'Tạo'), ['class' => 'btn btn-success'])
        ];
    }

    public function actionUpdateDruggedTime($id)
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = $this->findDruggedTimeModel($id);
        if ($model) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return [
                    'forceReload' => $request->get('pjax-container', '#crud-datatable-pjax'),
                    'forceClose' => true,
                    'push' => false,
                    'replace' => false
                ];
                exit();
            }
            return [
                'title'=> Yii::t('checkin', 'Cập nhật thời điểm'),
                'content'=>$this->renderAjax('form/_form_drugged_time', [
                    'model' => $model,
                ]),
                'footer' => Html::submitButton(FAS::icon('save').' '.Yii::t('backend', 'Lưu'), ['class' => 'btn btn-success'])
            ];
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDeleteDruggedTime($id)
    {
        $request = Yii::$app->request;

        $this->findDruggedTimeModel($id)->delete();

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['forceClose' => true,'forceReload' => $request->get('pjax-container', '#crud-datatable-pjax')];
    }

    public function actionCreateDosageForm()
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new DosageForm();
        $model->status = $model::STATUS_ACTIVE;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return [
                'forceReload' => $request->get('pjax-container', '#crud-datatable-pjax'),
                'forceClose' => true,
                'push' => false,
                'replace' => false
            ];
            exit();
        }
        return [
            'title'=> Yii::t('checkin', 'Tạo quy cách'),
            'content'=>$this->renderAjax('form/_form_dosage_form', [
                'model' => $model,
            ]),
            'footer' => Html::submitButton(FAS::icon('save').' '.Yii::t('backend', 'Tạo'), ['class' => 'btn btn-success'])
        ];
    }

    public function actionUpdateDosageForm($id)
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = $this->findDosageFormModel($id);
        if ($model) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return [
                    'forceReload' => $request->get('pjax-container', '#crud-datatable-pjax'),
                    'forceClose' => true,
                    'push' => false,
                    'replace' => false
                ];
                exit();
            }
            return [
                'title'=> Yii::t('checkin', 'Cập nhật quy cách'),
                'content'=>$this->renderAjax('form/_form_dosage_form', [
                    'model' => $model,
                ]),
                'footer' => Html::submitButton(FAS::icon('save').' '.Yii::t('backend', 'Lưu'), ['class' => 'btn btn-success'])
            ];
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDeleteDosageForm($id)
    {
        $request = Yii::$app->request;

        $this->findDosageFormModel($id)->delete();

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['forceClose' => true,'forceReload' => $request->get('pjax-container', '#crud-datatable-pjax')];
    }


    protected function findDruggedTimeModel($id)
    {
        if (($model = DruggedTime::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findDosageFormModel($id)
    {
        if (($model = DosageForm::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
