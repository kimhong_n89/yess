<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use backend\models\GeneralSettingForm;
use backend\models\search\TemplateSearch;
use yii\helpers\ArrayHelper;
use common\models\Template;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use rmrevin\yii\fontawesome\FAS;
use PharIo\Manifest\Type;

/**
 * SettingController implements the CRUD actions for SpaSetting model.
 */
class SettingController extends Controller
{
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    /**
     * Lists all SpaSetting models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new GeneralSettingForm();
        $model->loadFromDB();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('/setting/index');
        }
        
        $searchTemplateModel = new TemplateSearch();
        $emailProvider = $searchTemplateModel->search(ArrayHelper::merge(Yii::$app->request->queryParams, ['TemplateSearch' => ['type' => Template::TYPE_EMAIL]]));
        
        return $this->render('index', [
            'model' => $model,
            'searchTemplateModel' => $searchTemplateModel,
            'emailProvider' => $emailProvider
        ]);
    }
    
    /**
     * Creates a new Template model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateTemplate($type = Template::TYPE_EMAIL)
    {
        
        $request = Yii::$app->request;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new Template();
        $model->type = $type;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return [
                'forceReload' => $request->get('pjax-container', '#crud-datatable-pjax'),
                'forceClose' => true,
                'push' => false,
                'replace' => false
            ];
            exit();
        }
        return [
            'title'=> Yii::t('checkin', 'Tạo mẫu {type}', ['type' => Template::types()[$type]]),
            'content'=>$this->renderAjax('template/_form', [
                'model' => $model,
            ]),
            'footer' => Html::submitButton(FAS::icon('save').' '.Yii::t('backend', 'Tạo'), ['class' => 'btn btn-success'])
        ];
    }
    
    /**
     * Updates an existing Template model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateTemplate($id)
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $model = $this->findModel($id);
        if ($model) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return [
                    'forceReload' => $request->get('pjax-container', '#crud-datatable-pjax'),
                    'forceClose' => true,
                    'push' => false,
                    'replace' => false
                ];
                exit();
            }
            return [
                'title'=> Yii::t('checkin', 'Cập nhật mẫu {type}', ['type' => Template::types()[$model->type]]),
                'content'=>$this->renderAjax('template/_form', [
                    'model' => $model,
                ]),
                'footer' => Html::submitButton(FAS::icon('save').' '.Yii::t('backend', 'Lưu'), ['class' => 'btn btn-success'])
            ];
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Deletes an existing Template model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteTemplate($id)
    {
        $request = Yii::$app->request;
        
        $this->findModel($id)->delete();
        
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['forceClose' => true,'forceReload' => $request->get('pjax-container', '#crud-datatable-pjax')];
    }
    
    /**
     * Finds the Template model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Template the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Template::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
