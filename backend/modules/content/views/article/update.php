<?php

/**
 * @var yii\web\View $this
 * @var common\models\Article $model
 * @var common\models\ArticleCategory[] $categories
 */
$this->title = Yii::t('backend', 'Cập nhật {modelClass}: ', [
        'modelClass' => 'Bài viết',
    ]) . ' ' . $model->title;

$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Bài viết'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Cập nhật');

?>

<?php echo $this->render('_form', [
    'model' => $model,
    'categories' => $categories,
]) ?>
