<?php

/**
 * @var $this       yii\web\View
 * @var $model      common\models\Article
 * @var $categories common\models\ArticleCategory[]
 */

$this->title = Yii::t('backend', 'Tạo {modelClass}', [
    'modelClass' => 'Bài viết',
]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Bài viết'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<?php echo $this->render('_form', [
    'model' => $model,
    'categories' => $categories,
]) ?>
