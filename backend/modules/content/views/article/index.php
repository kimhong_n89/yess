<?php

use common\grid\EnumColumn;
use common\models\Article;
use common\models\ArticleCategory;
use kartik\date\DatePicker;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use rmrevin\yii\fontawesome\FAS;

/**
 * @var yii\web\View $this
 * @var backend\modules\content\models\search\ArticleSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('backend', 'Bài viết');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header">
        <div class="row align-items-center">
            <div class="col text-right">
                <?= Html::a('<i class="fas fa-plus"></i>', ['create'], ['class' => 'float-right btn btn-sm btn-primary'])?>
            </div>
        </div>
    </div>
        <?php echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{pager}",
            'options' => [
                'class' => 'table-responsive'
            ],
            'tableOptions' => [
                'class' => [
                    'table align-items-center table-flush',
                ]
            ],
            'headerRowOptions' => [
                'class' => [
                    'thead-light'
                ]
            ],
            'columns' => [
                [
                    'attribute' => 'id',
                    'options' => ['style' => 'width: 5%'],
                ],
                [
                    'attribute' => 'thumbnail',
                    'options' => ['style' => 'width: 20%'],
                    'value' => function($model) {
                        return Html::img($model->getThumbnailUrl(), ['width' => '200px']) ;
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'title',
                    'value' => function ($model) {
                        return Html::a(Html::encode($model->title), ['update', 'id' => $model->id]);
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'category_id',
                    'options' => ['style' => 'width: 10%'],
                    'value' => function ($model) {
                        return $model->category ? $model->category->title : null;
                    },
                    'filter' => ArrayHelper::map(ArticleCategory::find()->all(), 'id', 'title'),
                    ],
                [
                    'attribute' => 'created_by',
                    'options' => ['style' => 'width: 10%'],
                    'value' => function ($model) {
                        return @$model->author->username;
                    },
                ],
                [
                    'class' => EnumColumn::class,
                    'attribute' => 'status',
                    'options' => ['style' => 'width: 10%'],
                    'enum' => Article::statuses(),
                    'filter' => Article::statuses(),
                ],
                [
                    'attribute' => 'created_at',
                    'options' => ['style' => 'width: 10%'],
                    'format' => 'date',
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created_at',
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pluginOptions' => [
                            'format' => 'dd-mm-yyyy',
                            'showMeridian' => true,
                            'todayBtn' => true,
                            'endDate' => '0d',
                        ]
                    ]),
                ],
                [
                    'class' => \common\widgets\ActionColumn::class,
                    'options' => ['style' => 'width: 70px'],
                    'template' => '{update}{delete}',
                    'contentOptions' => [
                        'class' => 'td-actions'
                    ]
                ],
            ],
        ]); ?>
</div>