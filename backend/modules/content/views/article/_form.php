<?php
use trntv\filekit\widget\Upload;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\select2\Select2;

/**
 *
 * @var yii\web\View $this
 * @var common\models\Article $model
 * @var common\models\ArticleCategory[] $categories
 */
?>

<?php

$form = ActiveForm::begin([
    'enableClientValidation' => false,
    'enableAjaxValidation' => true
])?>
<div class="row">
	<div class="col-md-8">
		<div class="card">
			<div class="card-body">
                            <?= $form->field($model, "title")->textInput(['maxlength' => true]);?>
                            <?php
                            echo $form->field($model, "body")->widget(\yii\imperavi\Widget::class, [
                                'plugins' => [
                                    'fullscreen',
                                    'fontcolor',
                                    'video'
                                ],
                                'options' => [
                                    'minHeight' => 200,
                                    'maxHeight' => 200,
                                    'buttonSource' => true,
                                    'convertDivs' => false,
                                    'removeEmptyTags' => true,
                                    'imageUpload' => Yii::$app->urlManager->createUrl([
                                        '/file/storage/upload-imperavi'
                                    ])
                                ]
                            ])?>
                            <?php
                            echo $form->field($model, "thumbnail")->widget(Upload::class, [
                                'url' => [
                                    '/file/storage/upload'
                                ],
                                'maxFileSize' => 5000000, // 5 MiB,
                                'acceptFileTypes' => new JsExpression('/(\.|\/)(gif|jpe?g|png)$/i')
                            ])?>
                            <?php echo $form->field($model, 'attachments')->widget(
                                Upload::class,
                                [
                                    'url' => ['/file/storage/upload'],
                                    'sortable' => true,
                                    'maxFileSize' => 10000000, // 10 MiB
                                    'maxNumberOfFiles' => 10,
                                ]
                            ) ?>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card card-full-height">
			<div class="card-header card-header-rose">
				<h4 class="card-title mb-0"><?= Yii::t('checkin', 'Cài đặt') ?></h4>
			</div>
			<div class="card-body">
                            <?php

echo $form->field($model, 'category_id')->dropDownList(\yii\helpers\ArrayHelper::map($categories, 'id', 'title'), [
    'prompt' => Yii::t('checkin', 'Chọn danh mục ...')
                            ])?>
                <?php
                echo $form->field($model, 'status')->dropDownList($model::statuses());
                ?>
            </div>
		</div>
	</div>
</div>
<div class="text-right">
    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('checkin', 'Tạo') : Yii::t('checkin', 'Cập nhật'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end() ?>
