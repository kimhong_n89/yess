<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use rmrevin\yii\fontawesome\FAS;
use trntv\filekit\widget\Upload;
use yii\web\JsExpression;

/**
 * @var yii\web\View $this
 * @var common\models\ArticleCategory $model
 * @var common\models\ArticleCategory[] $categories
 */

?>


<?php $form = ActiveForm::begin() ?>
    <div class="row">
		<div class="col-md-6 mx-auto">
    <div class="card">
        <div class="card-body">
            <?= $form->field($model, "title")->textInput(['maxlength' => true]);?>
            <?php echo $form->field($model, 'status')->checkbox() ?>
        </div>
        <div class="card-footer text-right">
            <?php echo Html::submitButton($model->isNewRecord ? Yii::t('checkin', 'Tạo') : Yii::t('checkin', 'Cập nhật'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    </div>
    </div>
<?php ActiveForm::end() ?>
