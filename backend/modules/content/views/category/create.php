<?php

/**
 * @var $this       yii\web\View
 * @var $model      common\models\ArticleCategory
 * @var $categories common\models\ArticleCategory[]
 */

$this->title = Yii::t('backend', 'Tạo {modelClass}: ', [
        'modelClass' => 'Danh mục bài viết',
    ]) . ' ' . $model->title;

$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Danh mục bài viết'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Tạo');

?>

<?php echo $this->render('_form', [
    'model' => $model
]) ?>
