<?php

namespace backend\modules\translation\traits;


use Yii;

trait ModuleTrait
{

    /**
     * @return array
     */
    public function getLanguages($all=false)
    {
        $languages = [];
        foreach (Yii::$app->params['availableLocales'] as $locale => $name) {
            if ($locale !== Yii::$app->sourceLanguage || $all)
                $languages[substr($locale, 0, 2)] = $name;
        }

        return $languages;
    }

    /**
     * @return array
     */
    public function getPrefixedLanguages()
    {
        $languages = [];
        foreach ($this->getLanguages() as $lang => $name) {
            $languages[$lang] = $name;
        }

        return $languages;
    }

    /**
     * @return array
     */
    public function getAllPrefixedLanguages()
    {
        $languages = [];
        foreach ($this->getLanguages(true) as $lang => $name) {
            $languages[$lang] = $name;
        }
        
        return $languages;
    }
}