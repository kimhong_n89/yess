<?php

use backend\modules\translation\models\Source;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @var $this               yii\web\View
 * @var $searchModel        backend\modules\translation\models\search\SourceSearch
 * @var $dataProvider       yii\data\ActiveDataProvider
 * @var $model              \common\base\MultiModel
 * @var $languages          array
 */

$this->title = Yii::t('backend', 'Translation');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="card">
    <div class="card-header">
      <div class="row align-items-center">
        <div class="col">
          <h4 class="card-title"><?= Yii::t('checkin', 'Translate') ?></h4>
        </div>
        <div class="col text-right">
            <?= Html::a('<i class="fas fa-plus"></i>', ['create'], ['class' => 'float-right btn btn-sm btn-primary'])?>
        </div>
      </div>
    </div>
    <?php
    $translationColumns = [];
    foreach ($languages as $language => $name) {
        $translationColumns[] = [
            'attribute' => $language,
            'header' => $name,
            'value' => $language . '.translation',
        ];
    }
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'class' => 'table-responsive'
        ],
        'summary' => '',
        'tableOptions' => [
            'class' => [
                'table align-items-center table-flush',
            ]
        ],
        'headerRowOptions' => [
            'class' => [
                'thead-light'
            ]
        ],
        'columns' => ArrayHelper::merge([
            [
                'attribute' => 'id',
                'options' => ['style' => 'width: 50px'],
            ],
            [
                'attribute' => 'category',
                'options' => ['style' => 'width: 50px'],
                'filter' => ArrayHelper::map(Source::find()->select('category')->distinct()->all(), 'category', 'category'),
            ],
            'message:ntext'
        ], $translationColumns, [[
            'class' => \common\widgets\ActionColumn::class,
            'options' => ['style' => 'width: 5%'],
            'contentOptions' => [
                'class' => 'td-actions'
            ],
            'template' => '{update} {delete}',
        ]]),
    ]); ?>
</div>