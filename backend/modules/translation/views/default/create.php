<?php

/**
 * @var $this      yii\web\View
 * @var $model     \common\base\MultiModel
 * @var $languages array
 */

$this->title = Yii::t('backend', 'Create {modelClass}: ', [
        'modelClass' => 'I18n Source Message',
    ]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'I18n Source Messages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Create');

?>

<?php echo $this->render('_form', [
    'model' => $model,
    'languages' => $languages,
]) ?>


