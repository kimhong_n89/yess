<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use backend\modules\translation\models\Source;

/**
 * @var $this      yii\web\View
 * @var $model     \common\base\MultiModel
 * @var $languages array
 */

?>
<div class="card">
    <div class="card-body">
        <?php $form = ActiveForm::begin(); ?>
        
        <?php echo $form->field($model->getModel('source'), 'category')->dropDownList(ArrayHelper::map(Source::find()->select('category')->distinct()->all(), 'category', 'category')) ?>
        
        <?php echo $form->field($model->getModel('source'), 'message')->textInput() ?>
        
        <?php if (!$model->getModel('source')->isNewRecord) { ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo Yii::t('backend', 'Translations') ?></h3>
                </div>
                <div class="panel-body">
                    <?php foreach ($languages as $language => $name) {
                        echo $form->field($model->getModel($language), 'translation')->textInput([
                            'id' => $language . '-translation',
                            'name' => $language . '[translation]',
                        ])->label($name);
                    } ?>
                </div>
            </div>
        <?php } ?>
        
        <div class="form-group">
            <?php echo Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-primary']) ?>
        </div>
        
        <?php ActiveForm::end(); ?>
    </div>
</div>