<?php

namespace backend\modules\rbac\models;

use Yii;

/**
 * This is the model class for table "sys_auth_perm_route".
 *
 * @property int $id
 * @property string $auth_item
 * @property string $route
 * @property string $created
 *
 * @property RbacAuthItem $authItem
 * @property SysAuthRoute $route
 */
class SysAuthPermRoute extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sys_auth_perm_route';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['auth_item', 'route'], 'required'],
            [['created'], 'safe'],
            [['auth_item', 'route'], 'string', 'max' => 255],
            [['auth_item'], 'exist', 'skipOnError' => true, 'targetClass' => RbacAuthItem::className(), 'targetAttribute' => ['auth_item' => 'name']],
            [['route'], 'exist', 'skipOnError' => true, 'targetClass' => SysAuthRoute::className(), 'targetAttribute' => ['route' => 'route']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('spa', 'ID'),
            'auth_item' => Yii::t('spa', 'Auth Item'),
            'route' => Yii::t('spa', 'Route'),
            'created' => Yii::t('spa', 'Created'),
        ];
    }

    /**
     * Gets query for [[AuthItem]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItem()
    {
        return $this->hasOne(RbacAuthItem::className(), ['name' => 'auth_item']);
    }

    /**
     * Gets query for [[Route]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRoute()
    {
        return $this->hasOne(SysAuthRoute::className(), ['route' => 'route']);
    }
}
