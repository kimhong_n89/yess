<?php

namespace backend\modules\rbac\models;

use Yii;

/**
 * This is the model class for table "sys_auth_route".
 *
 * @property int $id
 * @property string $route
 *
 * @property SysAuthPermRoute[] $sysAuthPermRoutes
 */
class SysAuthRoute extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sys_auth_route';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['route'], 'required'],
            [['route'], 'string', 'max' => 255],
            [['route'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('spa', 'ID'),
            'route' => Yii::t('spa', 'Route'),
        ];
    }

    /**
     * Gets query for [[SysAuthPermRoutes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSysAuthPermRoutes()
    {
        return $this->hasMany(SysAuthPermRoute::className(), ['route' => 'route']);
    }
}
