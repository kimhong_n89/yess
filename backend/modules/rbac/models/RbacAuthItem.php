<?php

namespace backend\modules\rbac\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "rbac_auth_item".
 *
 * @property string $name
 * @property integer $type
 * @property string $description
 * @property string $rule_name
 * @property resource $data
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property RbacAuthAssignment[] $rbacAuthAssignments
 * @property RbacAuthRule $ruleName
 * @property RbacAuthItemChild[] $rbacAuthItemChildren
 * @property RbacAuthItemChild[] $rbacAuthItemChildren0
 * @property RbacAuthItem[] $children
 * @property RbacAuthItem[] $parents
 * @property SysAuthPermRoute[] $sysAuthPermRoutes
 */
class RbacAuthItem extends \yii\db\ActiveRecord
{
    const TYPE_ROLE = 1;
    const TYPE_PERMISSION = 2;
    
    public $arr_routes;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%rbac_auth_item}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['name'], 'unique'],
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['description', 'data'], 'string'],
            [['name', 'rule_name'], 'string', 'max' => 64],
            [['description', 'rule_name', 'data'], 'default', 'value' => null],
            [['rule_name'], 'exist', 'skipOnError' => true, 'targetClass' => RbacAuthRule::class, 'targetAttribute' => ['rule_name' => 'name']],
            [['arr_routes'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('backend', 'Tên'),
            'type' => Yii::t('backend', 'Type'),
            'description' => Yii::t('backend', 'Mô tả'),
            'rule_name' => Yii::t('backend', 'Rule Name'),
            'data' => Yii::t('backend', 'Data'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'arr_routes' => Yii::t('backend', 'Routes')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRbacAuthAssignments()
    {
        return $this->hasMany(RbacAuthAssignment::class, ['item_name' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuleName()
    {
        return $this->hasOne(RbacAuthRule::class, ['name' => 'rule_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRbacAuthItemChildren()
    {
        return $this->hasMany(RbacAuthItemChild::class, ['parent' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRbacAuthItemChildren0()
    {
        return $this->hasMany(RbacAuthItemChild::class, ['child' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(RbacAuthItem::class, ['name' => 'child'])->viaTable('rbac_auth_item_child', ['parent' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParents()
    {
        return $this->hasMany(RbacAuthItem::class, ['name' => 'parent'])->viaTable('rbac_auth_item_child', ['child' => 'name']);
    }
    
    public function afterSave ( $insert, $changedAttributes )
    {
        if ($this->type == self::TYPE_PERMISSION) {
            $this->saveRoute();
        }
    }
    
    /**
     * Gets query for [[SysAuthPermRoutes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSysAuthPermRoutes()
    {
        return $this->hasMany(SysAuthPermRoute::className(), ['auth_item' => 'name']);
    }
    
    public function saveRoute()
    {
        $this->arr_routes = is_array($this->arr_routes) ? $this->arr_routes : [];
        
        if ((isset($this->relatedRecords['sysAuthPermRoutes']) && diff_2array(ArrayHelper::getColumn($this->relatedRecords['sysAuthPermRoutes'], 'route'), $this->arr_routes)) || empty($this->relatedRecords['sysAuthPermRoutes'])) {
            if (!$this->isNewRecord) {
                // update // delete all
                SysAuthPermRoute::deleteAll('auth_item = :name', array('name' => $this->name));
            }
            // insert it.
            foreach ($this->arr_routes as $route) {
                $model = new SysAuthPermRoute();
                $model->auth_item = $this->name;
                $model->route = $route;
                $model->save(false);
            }
        }
    }
    
    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();
        if ($this->type == self::TYPE_PERMISSION) {
            $this->arr_routes = ArrayHelper::getColumn($this->sysAuthPermRoutes, 'route');
        }
    }
}
