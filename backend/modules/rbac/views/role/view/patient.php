<?php 
use common\components\utils\PermissionConstant;

$title = 'Khách hàng';
$arr_pers = [
    PermissionConstant::VIEW_PATIENT,
    PermissionConstant::CREATE_PATIENT,
    PermissionConstant::UPDATE_PATIENT,
    PermissionConstant::DELETE_PATIENT,
    PermissionConstant::EXPORT_PATIENT
];
?>
<?= $this->render('_item', ['permissions' => $permissions, 'selected' => $selected, 'title' => $title, 'arr_pers' => $arr_pers]); ?>