<?php 
use common\components\utils\PermissionConstant;

$title = 'Bác sĩ';
$arr_pers = [
    PermissionConstant::VIEW_DOCTOR,
    PermissionConstant::CREATE_DOCTOR,
    PermissionConstant::UPDATE_DOCTOR,
    PermissionConstant::DELETE_DOCTOR,
    PermissionConstant::EXPORT_DOCTOR
    ];
?>
<?= $this->render('_item', ['permissions' => $permissions, 'selected' => $selected, 'title' => $title, 'arr_pers' => $arr_pers]); ?>