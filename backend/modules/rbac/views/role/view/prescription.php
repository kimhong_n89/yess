<?php 
use common\components\utils\PermissionConstant;

$title = 'Toa thuốc';
$arr_pers = [
    PermissionConstant::VIEW_PRESCRIPTION,
    PermissionConstant::CREATE_PRESCRIPTION,
    PermissionConstant::UPDATE_PRESCRIPTION,
    PermissionConstant::DELETE_PRESCRIPTION,
    PermissionConstant::PRINT_PRESCRIPTION,
    PermissionConstant::EXPORT_PRESCRIPTION
];
?>
<?= $this->render('_item', ['permissions' => $permissions, 'selected' => $selected, 'title' => $title, 'arr_pers' => $arr_pers]); ?>