<?php 
use common\components\utils\PermissionConstant;

$title = 'User';
$arr_pers = [
    PermissionConstant::VIEW_USER,
    PermissionConstant::CREATE_USER,
    PermissionConstant::UPDATE_USER,
    PermissionConstant::DELETE_USER,
]
?>
<?= $this->render('_item', ['permissions' => $permissions, 'selected' => $selected, 'title' => $title, 'arr_pers' => $arr_pers]); ?>