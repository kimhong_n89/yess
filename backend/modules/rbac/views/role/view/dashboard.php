<?php 
use common\components\utils\PermissionConstant;

$title = 'Dashboard';
$arr_pers = [
    PermissionConstant::VIEW_DASHBOARD,
    PermissionConstant::VIEW_STATISTIC,
    PermissionConstant::VIEW_NEW_PRESCRIPTION,
    PermissionConstant::VIEW_CHART,
    PermissionConstant::VIEW_LOGIN_HISTORY
]
?>
<?= $this->render('_item', ['permissions' => $permissions, 'selected' => $selected, 'title' => $title, 'arr_pers' => $arr_pers]); ?>