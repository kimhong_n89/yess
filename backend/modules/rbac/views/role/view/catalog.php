<?php 
use common\components\utils\PermissionConstant;

$title = 'Danh mục';
$arr_pers = [
    PermissionConstant::VIEW_STORE,
    PermissionConstant::VIEW_PRODUCT,
    PermissionConstant::VIEW_DISEASE_GROUP,
    PermissionConstant::VIEW_LOCATION,
    PermissionConstant::MANAGE_MASTER_DATA
]
?>
<?= $this->render('_item', ['permissions' => $permissions, 'selected' => $selected, 'title' => $title, 'arr_pers' => $arr_pers]); ?>