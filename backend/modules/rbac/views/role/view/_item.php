<div class="col-md-3">
  <div class="card card-full-height">
    <div class="card-header card-header-text card-header-warning">
      <div class="card-text">
        <h4 class="card-title"><?= $title; ?></h4>
      </div>
    </div>
    <div class="card-body table-responsive">
        <?php foreach ($arr_pers as $pers) :?>
         <?php if(isset($permissions[$pers])) : ?>
         <div class="form-check">
            <label class="form-check-label">
              <input class="form-check-input" name="permissionName[]" type="checkbox" value="<?= $pers;?>" <?= in_array($pers, $selected) ? 'checked' : '' ?>>
              <?= $permissions[$pers];?>
              <span class="form-check-sign">
                <span class="check"></span>
              </span>
            </label>
          </div>
          <?php endif;?>
        <?php endforeach;?>
    </div>
  </div>
</div>