<?php 
use common\components\utils\PermissionConstant;

$title = 'Hệ thống';
$arr_pers = [
    PermissionConstant::LOGIN_TO_BACKEND,
    PermissionConstant::MANAGE_RBAC,
    PermissionConstant::MANAGE_TRANSLATION,
    PermissionConstant::MANAGE_LOG,
    PermissionConstant::VIEW_TIMELINE,
    PermissionConstant::MANAGE_FILE,
    PermissionConstant::MANAGE_SETTING,
    PermissionConstant::MANAGE_FEEDBACK,
]
?>
<?= $this->render('_item', ['permissions' => $permissions, 'selected' => $selected, 'title' => $title, 'arr_pers' => $arr_pers]); ?>