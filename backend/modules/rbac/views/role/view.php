<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\components\utils\PermissionConstant;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\rbac\models\RbacAuthItem */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Nhóm người dùng'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="role-item-view">
<?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="fas fa-times"></i></button>
        <span><?= Yii::$app->session->getFlash('success') ?></span>
    </div>
<?php endif; ?>

<?php ActiveForm::begin();?>
    <div class="row">
        <?= $this->render('view/dashboard', ['permissions' => $permissions, 'selected' => $selected]); ?>
        <?= $this->render('view/prescription', ['permissions' => $permissions, 'selected' => $selected]); ?>
        <?= $this->render('view/doctor', ['permissions' => $permissions, 'selected' => $selected]); ?>
        <?= $this->render('view/patient', ['permissions' => $permissions, 'selected' => $selected]); ?>
        <?= $this->render('view/catalog', ['permissions' => $permissions, 'selected' => $selected]); ?>
        <?= $this->render('view/user', ['permissions' => $permissions, 'selected' => $selected]); ?>
        <?= $this->render('view/system', ['permissions' => $permissions, 'selected' => $selected]); ?>
    </div>
    <div class="text-right">
        <?php echo Html::submitButton(Yii::t('spa', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end()?>
</div>
