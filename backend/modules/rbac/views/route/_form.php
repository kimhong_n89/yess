<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/**
 * @var yii\web\View $this
 * @var backend\modules\rbac\models\SysAuthRoute $model
 * @var yii\bootstrap4\ActiveForm $form
 */
?>

<div class="sys-auth-route-form">
    <?php $form = ActiveForm::begin(); ?>
    <?php echo $form->errorSummary($model); ?>
	<?php echo $form->field($model, 'route')->textInput(['maxlength' => true]) ?>
<?php ActiveForm::end(); ?>
</div>
