<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 *
 * @var yii\web\View $this
 * @var backend\modules\rbac\models\SysAuthRouteSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('backend', 'Routes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sys-auth-route-index">
    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
    		<button aria-hidden="true" data-dismiss="alert" class="close"
    			type="button">×</button>
             <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php endif; ?>
	<div class="card">
    <div class="card-header">
      <div class="row">
        <div class="col text-right">
            <?= Html::a('<i class="fas fa-plus"></i>', ['create','pjax-container' => '#route-gridview-pjax'], ['class' => 'btn btn-sm mx-2 btn-primary', 'role' => 'modal-remote', 'data-toggle' => 'tooltip', 'title' => Yii::t('checkin', 'Add'),])?>
        </div>
      </div>
    </div>
		<div class="card-body">
    		<?php Pjax::begin(['id' => 'route-gridview-pjax']); ?>
            <?php
            echo GridView::widget([
                'layout' => "{items}\n{pager}",
                'options' => [
                    'class' => 'table-responsive table-striped'
                ],
                'tableOptions' => [
                    'class' => [
                        'table',
                        'dtr-inline'
                    ]
                ],
                'headerRowOptions' => [
                    'class' => [
                        'text-primary'
                    ]
                ],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'route',
                    [
                        'class' => \common\widgets\ActionColumn::class,
                        'options' => [
                            'style' => 'width: 100px'
                        ],
                        'contentOptions' => [
                            'class' => 'td-actions'
                        ],
                        'template' => "{update} {delete}",
                        'buttons' => [
                            'update' => function ($url, $model) {
                                $url = Url::to([
                                    'update',
                                    'id' => $model->id,
                                    'pjax-container' => '#route-gridview-pjax'
                                ]);
                                $attributes = [
                                    'class' => 'table-action text-warning',
                                    'role' => 'modal-remote',
                                    'data-toggle' => 'tooltip',
                                    'title' => Yii::t('spa', 'Update Route')
                                ];
                                return Html::a('<i class="fa-fw fas fa-edit" aria-hidden=""></i>', $url, $attributes);
                            },
                            'delete' => function ($url, $model) {
                                $url = Url::to([
                                    'delete',
                                    'id' => $model->id,
                                    'pjax-container' => '#route-gridview-pjax'
                                ]);
                                $attributes = [
                                    'class' => 'table-action text-danger',
                                    'role' => 'modal-remote',
                                    'data-request-method' => 'post',
                                    'data-toggle' => 'tooltip',
                                    'title' => Yii::t('spa', 'Remove'),
                                    'data-confirm-title' => Yii::t('spa', 'Are you sure?'),
                                    'data-confirm-message' => Yii::t('spa', 'Are you sure you want to delete this item ')
                                ];
                                return Html::a('<i class="fa-fw fas fa-trash" aria-hidden=""></i>', $url, $attributes);
                            }
                        ]
                    ]
                ]
            ]);
            ?>
            <?php Pjax::end();?>
        </div>
	</div>
</div>
