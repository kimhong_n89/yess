<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\modules\rbac\models\RbacAuthRule;
use rmrevin\yii\fontawesome\FAS;
use kartik\select2\Select2;
use backend\modules\rbac\models\SysAuthRoute;

/* @var $this yii\web\View */
/* @var $model backend\modules\rbac\models\RbacAuthItem */
/* @var $form yii\widgets\ActiveForm */
$routes = ArrayHelper::map(SysAuthRoute::find()->all(), 'route', 'route');
?>
<?php $form = ActiveForm::begin(); ?>
    <?php echo $form->errorSummary($model); ?>
		<?php echo $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
		<?php echo $form->field($model, 'description')->textarea(['rows' => 2]) ?>
        <?php 
            echo $form->field($model, 'arr_routes')->widget(Select2::classname(), [
                'data' => $routes,
                'options' => ['placeholder' => 'Select a route ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'multiple' => true,
                    'closeOnSelect' => false
                ],
            ]);
        ?>
        
        <?php echo $form->field($model, 'rule_name')->dropDownList(ArrayHelper::map(RbacAuthRule::find()->all(), 'name', 'name'), ['prompt' => Yii::t('backend', 'Please select a rule...')]) ?>
        <?php echo $form->field($model, 'data')->textInput() ?>
<?php ActiveForm::end(); ?>