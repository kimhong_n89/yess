<?php

namespace backend\modules\rbac\controllers;

use Yii;
use backend\modules\rbac\models\RbacAuthItem;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use rmrevin\yii\fontawesome\FAS;
use backend\modules\rbac\models\RbacAuthItemSearch;
use yii\helpers\ArrayHelper;
use backend\modules\rbac\models\RbacAuthItemChild;
use backend\controllers\BackendController;

/**
 * RbacAuthItemController implements the CRUD actions for RbacAuthItem model.
 */
class RoleController extends BackendController
{
    /** @inheritdoc */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    /**
     * Lists all RbacAuthItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RbacAuthItemSearch();
        $dataProvider = $searchModel->search(ArrayHelper::merge(Yii::$app->request->queryParams, ['RbacAuthItemSearch' => ['type' => RbacAuthItem::TYPE_ROLE]]));
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RbacAuthItem model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $role = $this->findModel($id);
        if (Yii::$app->request->post()) {
            $arrPemissions = Yii::$app->request->post('permissionName', []);
            RbacAuthItemChild::deleteAll(['parent' => $role->name]);
            $rows = [];
            foreach ($arrPemissions as $pers) {
                $rows[] = ['parent' => $role->name, 'child' => $pers];
            }
            if ($rows) {
                Yii::$app->db->createCommand()->batchInsert(RbacAuthItemChild::tableName(), ['parent', 'child'], $rows)->execute();
            }
            
            Yii::$app->session->setFlash('success', Yii::t('spa', "Cập nhật thành công."));
            return $this->redirect(['view', 'id' => $id]);
        }
        $permissions =  ArrayHelper::map(RbacAuthItem::find()->where(['type' => RbacAuthItem::TYPE_PERMISSION])->all(), 'name', 'description');
        $selected = ArrayHelper::getColumn($role->rbacAuthItemChildren, 'child');
        return $this->render('view', [
            'model' => $role,
            'permissions' => $permissions,
            'selected' => $selected
        ]);
    }
    
    /**
     * Creates a new RbacAuthItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RbacAuthItem();
        $model->type = RbacAuthItem::TYPE_ROLE;
        
        $request = Yii::$app->request;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return [
                'forceReload' => $request->get('pjax-container', '#crud-datatable-pjax'),
                'forceClose' => true,
                'push' => false,
                'replace' => false
            ];
            exit();
        }
        
        return [
            'title'=> Yii::t('spa', 'Tạo nhóm'),
            'content'=>$this->renderAjax('_form', [
                'model' => $model,
            ]),
            'footer' => Html::submitButton(FAS::icon('save').' '.Yii::t('backend', 'Save'), ['class' => 'btn btn-primary'])
        ];
    }

    /**
     * Updates an existing RbacAuthItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->type = RbacAuthItem::TYPE_ROLE;
        
        $request = Yii::$app->request;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return [
                'forceReload' => $request->get('pjax-container', '#crud-datatable-pjax'),
                'forceClose' => true,
                'push' => false,
                'replace' => false
            ];
            exit();
        }
        
        return [
            'title'=> Yii::t('spa', 'Cập nhật nhóm'),
            'content'=>$this->renderAjax('_form', [
                'model' => $model,
            ]),
            'footer' => Html::submitButton(FAS::icon('save').' '.Yii::t('backend', 'Save'), ['class' => 'btn btn-primary'])
        ];
    }

    /**
     * Deletes an existing RbacAuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        $request = Yii::$app->request;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['forceClose' => true,'forceReload' => $request->get('pjax-container', '#crud-datatable-pjax')];
    }

    /**
     * Finds the RbacAuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return RbacAuthItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RbacAuthItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
