<?php

namespace backend\modules\rbac\controllers;

use Yii;
use backend\modules\rbac\models\SysAuthRoute;
use backend\modules\rbac\models\SysAuthRouteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use rmrevin\yii\fontawesome\FAS;
use yii\helpers\ArrayHelper;
use backend\controllers\BackendController;

/**
 * RouteController implements the CRUD actions for SysAuthRoute model.
 */
class RouteController extends BackendController
{
    public $controllers;

    /** @inheritdoc */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    /**
     * Lists all SysAuthRoute models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SysAuthRouteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new SysAuthRoute model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SysAuthRoute();

        $request = Yii::$app->request;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return [
                'forceReload' => $request->get('pjax-container', '#crud-datatable-pjax'),
                'forceClose' => true,
                'push' => false,
                'replace' => false
            ];
            exit();
        }
        
        return [
            'title'=> Yii::t('spa', 'Create Route'),
            'content'=>$this->renderAjax('_form', [
                'model' => $model,
            ]),
            'footer' => Html::submitButton(FAS::icon('save').' '.Yii::t('backend', 'Save'), ['class' => 'btn btn-primary'])
        ];
    }

    /**
     * Updates an existing SysAuthRoute model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $request = Yii::$app->request;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return [
                'forceReload' => $request->get('pjax-container', '#crud-datatable-pjax'),
                'forceClose' => true,
                'push' => false,
                'replace' => false
            ];
            exit();
        }
        
        return [
            'title'=> Yii::t('spa', 'Update route'),
            'content'=>$this->renderAjax('_form', [
                'model' => $model,
            ]),
            'footer' => Html::submitButton(FAS::icon('save').' '.Yii::t('backend', 'Save'), ['class' => 'btn btn-primary'])
        ];
    }

    /**
     * Deletes an existing SysAuthRoute model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        $request = Yii::$app->request;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['forceClose' => true,'forceReload' => $request->get('pjax-container', '#crud-datatable-pjax')];
    }

    /**
     * Finds the SysAuthRoute model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SysAuthRoute the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SysAuthRoute::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    private function getController($path, $ignoreFolder = array('nbproject', 'vendor'), $ignoreController = array('appController', 'SiteController')) {
        $controllers = glob($path . '/*', GLOB_ONLYDIR);
        $controllerList = array();
        if (!empty($controllers)) {
            foreach ($controllers as $controller) {
                if (!in_array(basename($controller), $ignoreFolder)) {
                    if (basename($controller) == 'controllers') {
                        $controllerList = glob($controller . '/*Controller.php');
                        if (!empty($controllerList)) {
                            foreach ($controllerList as $controllerName) {
                                if (!in_array(substr(basename($controllerName), 0, -4), $ignoreController)) {
                                    $controllerContent = file_get_contents($controllerName);
                                    $namespace = substr($controllerContent, strpos($controllerContent, 'namespace'), strpos($controllerContent, ';') - strpos($controllerContent, 'namespace'));
                                    $namespace = explode(' ', $namespace);
                                    $this->controllers[substr(basename($controllerName), 0, -4)] = $namespace[1];
                                }
                            }
                        }
                    } else {
                        $this->getController($controller);
                    }
                }
            }
        }
        return $this->controllers;
    }
    
    public function actionAutoInsertRoutes()
    {
        $fdir = dirname(\Yii::getAlias('@backend')) . '\backend';
        $controllers = $this->getController($fdir);
        
        if (!empty($controllers)) {
            foreach ($controllers as $controller => $namespace) {
                if ($controller[0] !== '.') {
                    $controller = str_replace('.php', '', $controller);
                    $routers[$controller] = array();
                    $class = $namespace . '\\' . $controller;
                    $reflectorController = new \ReflectionClass($class);
                    
                    $methods = $reflectorController->getMethods(\ReflectionMethod::IS_PUBLIC);
                    if (!empty($methods)) {
                        foreach ($methods as $method) {
                            if (preg_match('/^action[A-Z]+/', $method->name) && $method->class == $class) {
                                // $routers[$controller][] = $method;
                                $_controller =  strtolower(preg_replace(
                                    '/(?<=[a-z])([A-Z]+)/',
                                    '-$1',
                                    str_replace('Controller', '', $controller)
                                    ));
                                $_action =  strtolower(preg_replace(
                                    '/(?<=[a-z])([A-Z]+)/',
                                    '-$1',
                                    str_replace('action', '', $method->name)));
                                
                                $route = $_controller . '/' . $_action;;
                                $rbac_permissions[] = $route;
                                
                                $auth_po_routes = SysAuthRoute::find()->where(['route' => $route])->one();
                                if(empty($auth_po_routes)) {
                                    $model = new SysAuthRoute();
                                    $model->route = $route;
                                    $model->save();
                                }
                            }
                        }
                    }
                }
            }
        }
        \Yii::$app->getSession()->setFlash('success', count($rbac_permissions) . ' routes was found');
        return $this->redirect('index');
    }
}
