<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\Setting;

/**
 * Login form
 */
class GeneralSettingForm extends Model
{
    //Mail server
    public $smtp_host;
    public $smtp_port;
    public $smtp_encryption;
    public $smtp_username;
    public $smtp_password;
    public $server_send_email;
    public $server_send_name;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['server_send_email'], 'email'],
            [['smtp_host', 'smtp_port', 'smtp_encryption', 'smtp_username', 'smtp_password', 'server_send_name'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'smtp_host' => Yii::t('checkin', 'SMTP host'),
            'smtp_port' => Yii::t('checkin', 'SMTP port'),
            'smtp_encryption' => Yii::t('checkin', 'SMTP Encryption'),
            'smtp_username' => Yii::t('checkin', 'Username'),
            'smtp_password' => Yii::t('checkin', 'Mật khẩu'),
            'server_send_email' => Yii::t('checkin', 'Email người gửi'),
            'server_send_name' => Yii::t('checkin', 'Tên người gửi'),
        ];
    }

    public function loadFromDB()
    {
        $settings = Setting::find()->all();
        foreach ($settings as $setting) {
            $this->{$setting->key} = $setting->value;
        }
    }
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }
        foreach ($this->attributeLabels() as $key => $name) {
            $model = Setting::findOne(['key' => $key]);
            if (!$model) {
                $model = new Setting();
                $model->key = $key;
            }
            $model->value = $this->$key;
            $model->save();
        }

        return true;
    }
}
