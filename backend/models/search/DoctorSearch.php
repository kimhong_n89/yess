<?php

namespace backend\models\search;

use common\models\District;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Doctor;

/**
 * DoctorSearch represents the model behind the search form about `common\models\Doctor`.
 */
class DoctorSearch extends Doctor
{
    public $keyword;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'gender', 'day_of_birth', 'month_of_birth', 'year_of_birth', 'province_id', 'district_id', 'ward_id', 'status'], 'integer'],
            [['fullname', 'phone', 'email', 'avatar_path', 'address'], 'safe'],
            [['created_at', 'updated_at'], 'default', 'value' => null],
            [['keyword'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Doctor::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        if (!empty($params['act'])){
            $dataProvider->setPagination(['pageSize' => -1]);
        }

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->keyword) {
            $query->andFilterWhere([
                'OR',
                ['like', 'fullname', $this->keyword],
                ['like', 'phone', $this->keyword],
                ['like', 'email', $this->keyword],
                ['like', 'address', $this->keyword]
            ]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'gender' => $this->gender,
            'day_of_birth' => $this->day_of_birth,
            'month_of_birth' => $this->month_of_birth,
            'year_of_birth' => $this->year_of_birth,
            'province_id' => $this->province_id,
            'ward_id' => $this->ward_id,
            'status' => $this->status,
            'updated_at' => $this->updated_at,
        ]);
        if ($this->created_at !== null) {
            $query->andFilterWhere(['between', 'created_at', strtotime($this->created_at), strtotime($this->created_at) + 3600 * 24]);
        }

        if ($this->district_id) {
            if ($this->province_id) {
                $check = District::find()->where(['id_district' => $this->district_id, 'id_state' => $this->province_id])->exists();
                if ($check) {
                    $query->andFilterWhere(['district_id' => $this->district_id]);
                }
            }

        }
        $query->andFilterWhere(['like', 'fullname', $this->fullname])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'address', $this->address]);

        return $dataProvider;
    }
}
