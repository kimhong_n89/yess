<?php

namespace backend\models\search;

use common\models\District;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Patient;

/**
 * PatientSearch represents the model behind the search form about `common\models\Patient`.
 */
class PatientSearch extends Patient
{
    public $doctor_id;
    public $keyword;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'gender', 'province_id', 'district_id', 'ward_id', 'day_of_birth', 'month_of_birth', 'year_of_birth', 'status'], 'integer'],
            [['fullname', 'phone', 'email', 'address'], 'safe'],
            [['created_at', 'updated_at'], 'default', 'value' => null],
            [['doctor_id', 'keyword'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Patient::find()->select([
            'patient.*',
            'COUNT(prescription.id) as count_prescription'
        ])->joinWith('prescriptions')->groupBy('patient.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'id',
                    'fullname',
                    'phone',
                    'email',
                    'year_of_birth',
                    'gender',
                    'status',
                    'created_at',
                    'district_id',
                    'province_id',
                    'count_prescription'
                ]
            ]
        ]);

        if (!empty($params['act'])){
            $dataProvider->setPagination(['pageSize' => -1]);
        }

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        if ($this->doctor_id) {
            $query->andFilterWhere([
                'prescription.doctor_id' => $this->doctor_id,
            ]);
        }

        if ($this->keyword) {
            $query->andFilterWhere([
                'OR',
                ['like', 'fullname', $this->keyword],
                ['like', 'phone', $this->keyword],
                ['like', 'email', $this->keyword],
                ['like', 'address', $this->keyword]
            ]);
        }
        $query->andFilterWhere([
            'patient.id' => $this->id,
            'patient.gender' => $this->gender,
            'patient.province_id' => $this->province_id,
            'patient.ward_id' => $this->ward_id,
            'patient.day_of_birth' => $this->day_of_birth,
            'patient.month_of_birth' => $this->month_of_birth,
            'patient.year_of_birth' => $this->year_of_birth,
            'patient.status' => $this->status,
            'patient.updated_at' => $this->updated_at,
        ]);

        if ($this->created_at !== null) {
            $query->andFilterWhere(['between', 'patient.created_at', strtotime($this->created_at), strtotime($this->created_at) + 3600 * 24]);
        }


        if ($this->district_id) {
            if ($this->province_id) {
                $check = District::find()->where(['id_district' => $this->district_id, 'id_state' => $this->province_id])->exists();
                if ($check) {
                    $query->andFilterWhere(['district_id' => $this->district_id]);
                }
            }

        }

        $query->andFilterWhere(['like', 'patient.fullname', $this->fullname])
            ->andFilterWhere(['like', 'patient.phone', $this->phone])
            ->andFilterWhere(['like', 'patient.email', $this->email])
            ->andFilterWhere(['like', 'patient.address', $this->address]);

        return $dataProvider;
    }
}
