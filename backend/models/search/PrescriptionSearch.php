<?php

namespace backend\models\search;

use common\models\District;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Prescription;

/**
 * PrescriptionSearch represents the model behind the search form about `common\models\Prescription`.
 */
class PrescriptionSearch extends Prescription
{
    public $keyword;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'doctor_id', 'patient_id', 'is_use_insurance', 'total_amount', 'status', 'created_at', 'updated_at'], 'integer'],
            [['insurance_number', 'note'], 'safe'],
            [['insurance_rate'], 'number'],
            [['keyword', 'arr_diagnoses', 'patient_name', 'doctor_name', 'doctor_phone', 'patient_province', 'patient_district'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Prescription::find()->select([
            'prescription.*',
            'doctor.fullname as doctor_name',
            'doctor.phone as doctor_phone',
            'patient.fullname as patient_name',
            'patient.province_id as patient_province',
            'patient.district_id as patient_district'
        ])->joinWith('doctor')->joinWith('patient')->joinWith('prescriptionDiagnoses');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'id',
                    'doctor_name',
                    'doctor_phone',
                    'patient_name',
                    'is_use_insurance',
                    'patient_province',
                    'patient_district',
                    'created_at',
                    'status',
                    'total_amount'
                ]
            ]
        ]);

        if (!empty($params['act'])){
            $dataProvider->setPagination(['pageSize' => -1]);
        }

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->keyword) {
            $query->andFilterWhere([
                'OR',
                ['like', 'doctor.fullname', $this->keyword],
                ['like', 'doctor.phone', $this->keyword],
                ['like', 'patient.fullname', $this->keyword],
                ['like', 'patient.phone', $this->keyword],
            ]);
        }

        $query->andFilterWhere([
            'prescription.id' => $this->id,
            'prescription.doctor_id' => $this->doctor_id,
            'prescription.patient_id' => $this->patient_id,
            'prescription.is_use_insurance' => $this->is_use_insurance,
            'prescription.insurance_rate' => $this->insurance_rate,
            'prescription.total_amount' => $this->total_amount,
            'prescription.status' => $this->status,
            'prescription.created_at' => $this->created_at,
            'prescription.updated_at' => $this->updated_at,
            'patient.province_id' => $this->patient_province,
        ]);

        if ($this->patient_district) {
            if ($this->patient_province) {
                $check = District::find()->where(['id_district' => $this->patient_district, 'id_state' => $this->patient_province])->exists();
                if ($check) {
                    $query->andFilterWhere(['patient.district_id' => $this->patient_district]);
                }
            }

        }
        if ($this->arr_diagnoses) {
            $query->andFilterWhere(['prescription_diagnose.diagnose_id' => $this->arr_diagnoses]);
        }

        $query->andFilterWhere(['like', 'insurance_number', $this->insurance_number])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'doctor.fullname', $this->doctor_name])
            ->andFilterWhere(['like', 'doctor.phone', $this->doctor_phone])
            ->andFilterWhere(['like', 'patient.fullname', $this->patient_name]);

        return $dataProvider;
    }

    public function newest()
    {
        $query = Prescription::find()->with(['doctor', 'patient'])->where([
            "NOT IN", 'prescription.status', [Prescription::STATUS_DRAFT, Prescription::STATUS_DELETED]
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at' => SORT_DESC]],
        ]);

        return $dataProvider;
    }
}
