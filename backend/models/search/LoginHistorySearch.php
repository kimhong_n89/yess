<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LoginHistory;

/**
 * LoginHistorySearch represents the model behind the search form about `common\models\LoginHistory`.
 */
class LoginHistorySearch extends LoginHistory
{
    public $perpage;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'otp_code', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'default', 'value' => null],
            [['phone_number', 'ip', 'device_agent', 'fullname', 'email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LoginHistory::find()->select([
            'login_history.*',
            'doctor.fullname as fullname',
            'doctor.email as email'
        ])->join("LEFT JOIN", 'doctor', 'doctor.phone = `login_history`.phone_number');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->perpage) {
            $dataProvider->setPagination([
                'pageSize' => 10,
            ]);
        }
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'login_history.id' => $this->id,
            'login_history.otp_code' => $this->otp_code,
            'login_history.status' => $this->status,
            'login_history.updated_at' => $this->updated_at,
        ]);

        if ($this->created_at !== null) {
            $query->andFilterWhere(['between', 'login_history.created_at', strtotime($this->created_at), strtotime($this->created_at) + 3600 * 24]);
        }

        $query->andFilterWhere(['like', 'login_history.phone_number', $this->phone_number])
            ->andFilterWhere(['like', 'login_history.ip', $this->ip])
            ->andFilterWhere(['like', 'login_history.device_agent', $this->device_agent]);

        return $dataProvider;
    }

    public function newest()
    {
        $query = LoginHistory::find()->select([
            'login_history.*',
            'doctor.fullname as fullname',
            'doctor.email as email'
        ])->join("LEFT JOIN", 'doctor', 'doctor.phone = `login_history`.phone_number');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

        return $dataProvider;
    }
}
