<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Attendee;

/**
 * AttendeeSearch represents the model behind the search form about `common\models\Attendee`.
 */
class AttendeeSearch extends Attendee
{
    public $keyword;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'district_id', 'province_id', 'gender', 'status', 'registration_date', 'last_login', 'created_at', 'updated_at'], 'integer'],
            [['first_name', 'last_name', 'phone', 'email', 'company_name', 'position_title', 'address', 'avatar_path'], 'safe'],
            [['keyword'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Attendee::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->keyword) {
            $query->andFilterWhere([
                'OR',
                ['like', 'attendee.phone', $this->keyword],
                ['like', 'attendee.first_name', $this->keyword],
                ['like', 'attendee.last_name', $this->keyword],
                ['like', 'attendee.company_name', $this->keyword]
            ]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'district_id' => $this->district_id,
            'province_id' => $this->province_id,
            'gender' => $this->gender,
            'status' => $this->status,
            'registration_date' => $this->registration_date,
            'last_login' => $this->last_login,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'company_name', $this->company_name])
            ->andFilterWhere(['like', 'position_title', $this->position_title])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'avatar_path', $this->avatar_path]);

        return $dataProvider;
    }
}
