<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Ward;

/**
 * WardSearch represents the model behind the search form about `common\models\Ward`.
 */
class WardSearch extends Ward
{
    public $id_state;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_ward', 'id_district', 'vt_ward_id', 'id_state'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ward::find()->joinWith('district');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->id_state) {
            $query->andFilterWhere([
                'district.id_state' => $this->id_state,
            ]);
        }
        $query->andFilterWhere([
            'ward.id_ward' => $this->id_ward,
            'ward.id_district' => $this->id_district,
            'ward.vt_ward_id' => $this->vt_ward_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
