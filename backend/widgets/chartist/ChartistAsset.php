<?php
/**
 * @package okeanos\chartist
 * @author Nikolas Grottendieck <github@nikolasgrottendieck.com>
 * @copyright Copyright &copy; Nikolas Grottendieck
 * @license BSD-3-Clause
 * @version 1.0
 */

namespace backend\widgets\chartist;

use yii;
use yii\web\AssetBundle;
use backend\assets\BackendAsset;

/**
 * Asset Manager for Chartist
 *
 * @author Nikolas Grottendieck <github@nikolasgrottendieck.com>
 * @since  1.0
 */
class ChartistAsset extends AssetBundle
{
    public $basePath = '@backend/web';
    
    /**
     * @var array
     */
    public $css = [
        'https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.css',
        '/css/chartist-plugin-tooltip.css'
    ];
    /**
     * @var array
     */
    public $js = [
        'https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js',
        '/js/chartist-plugin-pointlabels.js',
        '/js/chartist-plugin-tooltip.js',
    ];
    
    public $publishOptions = [
        'only' => [
            '*.css',
            '*.js',
            '../img/*'
        ],
        "forceCopy" => YII_ENV_DEV,
    ];
    
    /**
     * @var array
     */
    public $depends = [
        //BackendAsset::class
    ];

}