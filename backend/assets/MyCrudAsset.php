<?php
namespace backend\assets;

use yii\web\AssetBundle;

class MyCrudAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl  = '@web';
    
    public $css = [
        'css/ajaxcrud.css',
    ];
    public $js  = [
        'js/ModalRemote.js',
        'js/ajaxcrud.js',
    ];
    
    public $depends = [
        BackendAsset::class
    ];
}
?>