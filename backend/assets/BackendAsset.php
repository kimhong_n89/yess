<?php
/**
 * Created by PhpStorm.
 * User: zein
 * Date: 7/3/14
 * Time: 3:14 PM
 */

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\YiiAsset;
use yii\web\JqueryAsset;
use yii\bootstrap4\BootstrapPluginAsset;

class BackendAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $basePath = '@backend/web';
    
    /**
     * @var array
     */
    public $css = [
        'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700',
        //'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700',
        //'https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css',
        //'/css/style.css',
        //'/css/backend_v1.css',
        '/css/backend_v2.css',
    ];
    /**
     * @var array
     */
    public $js = [
//        '/js/multi-carousel.min.js',
        '/js/bootstrap-selectpicker.js',
        '/js/app.js'
    ];
    
    public $publishOptions = [
        'only' => [
            '*.css',
            '*.js',
            '../img/*'
        ],
        "forceCopy" => YII_ENV_DEV,
    ];
    
    /**
     * @var array
     */
    public $depends = [
        YiiAsset::class,
        JqueryAsset::class,
        BootstrapPluginAsset::class,
    ];
}
