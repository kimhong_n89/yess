<?php

/**
 * @var yii\web\View $this
 * @var common\models\ExaminationSchedule $model
 */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Examination Schedule',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Examination Schedules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="examination-schedule-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
