<?php

/**
 * @var yii\web\View $this
 * @var common\models\ExaminationSchedule $model
 */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Examination Schedule',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Examination Schedules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="examination-schedule-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
