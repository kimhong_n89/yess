<?php
use yii\grid\GridView;
use common\components\utils\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\grid\EnumColumn;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use common\components\utils\PermissionConstant;
use common\models\Prescription;
use kartik\widgets\Select2;

$diagnose = ['' => Yii::t('backend', 'Tất cả')] + ArrayHelper::map(\common\models\DiseaseGroup::find()->all(), 'id', 'name');

echo GridView::widget([
    'layout' => "{items}\n{pager}",
    'options' => [
        'class' => 'table-responsive'
    ],
    'tableOptions' => [
        'class' => [
            'table align-items-center table-striped table-flush',
        ]
    ],
    'headerRowOptions' => [
        'class' => [
            'thead-light'
        ]
    ],
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'attribute' => 'id',
            'contentOptions' => [
                'style' => 'width: 40px'
            ],
        ],
        [
            'attribute' => 'doctor_name',
            'contentOptions' => [
                'style' => 'word-break: break-word; white-space: normal;',
            ],
            'label' => Yii::t('backend', 'Bác sĩ'),
        ],
        [
            'attribute' => 'doctor_phone',
            'label' => 'SĐT Bác',
            'contentOptions' => [
                'style' => 'width: 100px'
            ],
        ],
        [
            'attribute' => 'is_use_insurance',
            'class' => \common\grid\EnumColumn::class,
            'enum' => Prescription::yesno(),
            'filter' => Select2::widget([
                'model' => $searchModel,
                'attribute' => 'is_use_insurance',
                'data' => ['' => Yii::t('backend', 'Tất cả')] + Prescription::yesno(),
            ]),
            'contentOptions' => [
                'width' => '10%',
                'style' => 'word-break: break-word; white-space: normal;',
            ],
        ],
        [
            'attribute' => 'arr_diagnoses',
            'contentOptions' => [
                'width' => '10%',
                'style' => 'word-break: break-word; white-space: normal;',
            ],
            'value' => function($model) {
                return implode(' / ', ArrayHelper::getColumn($model->diagnoses, 'name'));
            },
            'filter' => Select2::widget([
                'model' => $searchModel,
                'attribute' => 'arr_diagnoses',
                'data' => $diagnose,
                'options' => [
                    'id' => 'sel-diagnose'
                ],
            ]),
        ],
        [
            'class' => \common\grid\EnumColumn::class,
            'attribute' => 'status',
            'enum' => Prescription::statuses(),
            'filter' => Select2::widget([
                'model' => $searchModel,
                'attribute' => 'status',
                'data' => ['' => Yii::t('backend', 'Tất cả')] + Prescription::statuses(),
            ]),
            'contentOptions' => [
                'width' => '10%',
                'style' => 'word-break: break-word; white-space: normal;',
            ],
        ],
        [
            'class' => \common\widgets\ActionColumn::class,
            'contentOptions' => [
                'class' => 'table-actions',
                'style' => 'width: 32px'
            ],
            'template' => '<div class="dropdown">
                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                      {view}{update}{delete}
                    </div>
                  </div>',
            'urlCreator' => function( $action, $model, $key, $index ){
                if ($action == "view") {
                    return Url::to(['/prescription/view', 'id' => $key]);
                } else if ($action == "update") {
                    return Url::to(['/prescription/update', 'id' => $key]);
                } else if ($action == "delete") {
                    return Url::to(['/prescription/delete', 'id' => $key]);
                }
            },
            'buttons' => [
                'view' => function ($url, $model) {
                    $attributes = [
                        'class' => 'dropdown-item',
                        'data-toggle' => 'tooltip',
                        'title' => Yii::t('checkin', 'Xem')
                    ];
                    return Html::a(Yii::t('checkin', 'Xem'), $url, $attributes, [PermissionConstant::VIEW_PRESCRIPTION]);
                },
                'update' => function ($url, $model) {
                    $attributes = [
                        'class' => 'dropdown-item',
                        'data-toggle' => 'tooltip',
                        'title' => Yii::t('checkin', 'Cập nhật')
                    ];
                    return Html::a(Yii::t('checkin', 'Cập nhật'), $url, $attributes, [PermissionConstant::UPDATE_PRESCRIPTION]);
                },
                'delete' => function ($url, $model) {
                    $attributes = [
                        'class' => 'dropdown-item',
                        'data-toggle' => 'tooltip',
                        'title' => Yii::t('checkin', 'Xóa'),
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                    ];
                    return Html::a(Yii::t('checkin', 'Xóa'), $url, $attributes, [PermissionConstant::DELETE_PRESCRIPTION]);
                }
            ]
        ],
    ],
]);
?>
