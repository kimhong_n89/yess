<?php
use common\models\Patient;
use common\components\utils\Html;
use common\components\utils\PermissionConstant;

/**
 * @var yii\web\View $this
 * @var \common\models\Patient $model
 */
?>
<div class="card">
    <div class="card-body">
        <div class="pt-4 text-center">
          <h5 class="h3 title">
            <span class="d-block mb-1"><?= Html::encode($model->fullname);?></span>
            <span class="badge badge-pill badge-lg <?= $model->status == Patient::STATUS_ACTIVE ? 'badge-success': 'badge-warning';?>"><?= @$model::statuses()[$model->status];?></span>
          </h5>
          <div class="mt-3">
            <?= Html::a('<i class="fa-fw fas fa-edit"></i>', ['/patient/update', 'id' => $model->id], [
                'class' => 'btn btn-primary btn-sm',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('checkin', 'Cập nhật'),
            ], [PermissionConstant::UPDATE_PATIENT]);?>

            <?php
              if($model->status != Patient::STATUS_DELETED) {
                  echo Html::a('<i class="fa-fw fas fa-trash"></i>', ['/patient/delete', 'id' => $model->id], [
                      'class'        => 'btn btn-danger btn-sm',
                      'data-toggle'  => 'tooltip',
                      'data-confirm' => 'Bạn có chắc là sẽ xóa không',
                      'data-method'  => 'post',
                      'title'        => Yii::t('checkin', 'Xoá'),
                  ], [PermissionConstant::DELETE_PATIENT]);
              }
            ?>
          </div>
        </div>
    </div>
    <div class="card-body group-block">
        <div class="item-detail">
            <div class="item-label"><strong><?= Yii::t('checkin', 'Số điện thoại');?></strong></div>
            <div class="item-content"><?= Html::encode($model->phone);?></div>
        </div>
        <div class="item-detail">
            <div class="item-label"><strong><?= Yii::t('checkin', 'Email');?></strong></div>
            <div class="item-content"><?= Html::encode($model->email);?></div>
        </div>
        <div class="item-detail">
          <div class="item-label"><strong><?= Yii::t('checkin', 'Giới tính');?></strong></div>
          <div class="item-content"><?= Html::encode(@$model::genders()[$model->gender]);?></div>
        </div>
        <div class="item-detail">
          <div class="item-label"><strong><?= Yii::t('checkin', 'Năm sinh');?></strong></div>
          <div class="item-content"><?= Html::encode($model->year_of_birth);?></div>
        </div>
        <div class="item-detail">
            <div class="item-label"><strong><?= Yii::t('checkin', 'Địa chỉ');?></strong></div>
            <div class="item-content"><i class="fas fa-map-marker-alt"></i> <?= Html::encode($model->getFullAddress());?></div>
        </div>
        <div class="item-detail">
            <div class="item-label"><strong><?= Yii::t('checkin', 'Ngày tạo');?></strong></div>
            <div class="item-content"><?= Yii::$app->formatter->asDate($model->created_at, 'long');?></div>
        </div>
    </div>
</div>