<?php

use common\components\utils\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use backend\controllers\PatientController;
use common\components\utils\PermissionConstant;

/**
 * @var yii\web\View $this
 * @var common\models\Patient $model
 */

$this->title = $model->fullname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Khách hàng'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patient-view">
  <div class="row">
    <div class="col-md-3 order-sm-1">
        <?= $this->render('detail/_detail', ['model' => $model]); ?>
    </div>
    <?php if (userCan(PermissionConstant::VIEW_PRESCRIPTION)) :?>
    <div class="col-md-9 order-sm-0">
        <?php Pjax::begin(['id' => 'doctor-view-pjax']); ?>
      <div class="card">
        <div class="card-header">
          <div class="row align-items-center">
            <div class="col-md-6">
              <h4 class="mb-0">Toa thuốc</h4>
            </div>
            <div class="col-sm-6 text-right">
                <?php
                    echo Html::a('<i class="fas fa-plus"></i>', Url::to(['/prescription/create']), [
                        'class' => 'btn btn-primary btn-sm',
                        'data-toggle' => 'tooltip',
                        'data-pjax' => '0',
                        'title' => Yii::t('checkin', 'Tạo'),
                    ], [PermissionConstant::CREATE_PRESCRIPTION]);
//                    echo Html::a('<i class="fas fa-file-download"></i>', \Yii::$app->request->url . '&act=' . PatientController::ACTION_EXPORT, [
//                        'class' => 'btn btn-primary btn-sm',
//                        'data-toggle' => 'tooltip',
//                        'title' => Yii::t('checkin', 'Export'),
//                        'data-pjax' => 0
//                    ]);
                ?>
            </div>
          </div>
            <?php
              echo $this->render('_search', ['model' => $searchModel]);
            ?>
        </div>
        <div class="tab-content tab-space">
          <!-- attendee -->
          <div class="tab-pane active show">
              <?= $this->render('detail/_prescription', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);?>
          </div>
        </div>
      </div>
        <?php Pjax::end()?>
    </div>
    <?php endif;?>
  </div>
</div>
