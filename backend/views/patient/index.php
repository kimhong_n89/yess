<?php

use common\components\utils\Html;
use yii\grid\GridView;
use backend\controllers\PatientController;
use common\models\Patient;
use yii\helpers\ArrayHelper;
use common\models\District;
use common\models\Province;
use common\components\utils\PermissionConstant;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;

/**
 * @var yii\web\View $this
 * @var backend\models\search\PatientSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$arrDistrict = ['' => Yii::t('backend', 'Tất cả')];
if ($searchModel->province_id) {
    $arrDistrict = $arrDistrict + ArrayHelper::map(District::find()->where(['id_state' => $searchModel->province_id])->orderBy('name')->asArray()->all(), 'id_district', 'name');
}

$this->title = Yii::t('backend', 'Khách hàng');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php \yii\widgets\Pjax::begin(['id' => 'patient-pjax']);?>
<div class="patient-index">
  <div class="card">
    <div class="card-header">
      <div class="row">
        <div class="col text-right">
            <?= Html::a('<i class="fas fa-plus"></i>', ['create'], ['class' => 'btn btn-sm mx-2 btn-primary', 'data-pjax' => 0, 'data-toggle' => 'tooltip', 'title' => Yii::t('checkin', 'Add')], [PermissionConstant::CREATE_PATIENT])?>
            <?php
            echo Html::a('<i class="fas fa-file-download"></i>',  addParamToUrl(\Yii::$app->request->url, 'act=' . PatientController::ACTION_EXPORT), [
                'class' => 'btn btn-primary btn-sm',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('checkin', 'Export'),
                'data-pjax' => 0
            ], [PermissionConstant::EXPORT_PATIENT]);
            ?>
        </div>
      </div>
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>


      <?php echo GridView::widget([
          'layout' => "{items}\n{pager}",
          'options' => [
              'class' => 'table-responsive'
          ],
          'tableOptions' => [
              'class' => [
                  'table align-items-center table-striped table-flush',
              ]
          ],
          'headerRowOptions' => [
              'class' => [
                  'thead-light'
              ]
          ],
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
          'columns' => [
              [
                  'attribute' => 'id',
                  'contentOptions' => [
                      'style' => 'width: 40px'
                  ],
              ],
              [
                  'attribute' => 'fullname',
                  'contentOptions' => [
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
              ],
              [
                  'attribute' => 'phone',
              ],
              [
                  'attribute' => 'email',
                  'contentOptions' => [
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
              ],
              [
                  'attribute' => 'year_of_birth',
                  'contentOptions' => ['style' => 'width: 80px'],
              ],
              [
                  'class' => \common\grid\EnumColumn::class,
                  'attribute' => 'gender',
                  'enum' => Patient::genders(),
                  'filter' => Select2::widget([
                      'model' => $searchModel,
                      'attribute' => 'gender',
                      'data' => ['' => Yii::t('backend', 'Tất cả')] + Patient::genders(),
                  ]),
                  'contentOptions' => [
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
              ],
              [
                  'attribute' => 'province_id',
                  'label' => Yii::t('backend', 'Khu vực'),
                  'contentOptions' => [
                      'width' => '10%',
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
                  'value' => function($model){
                      return @$model->province->name;
                  },
                  'filter' => Select2::widget([
                      'model' => $searchModel,
                      'attribute' => 'province_id',
                      'data' => ['' => Yii::t('backend', 'Tất cả')] + ArrayHelper::map(Province::find()->orderBy('name')->asArray()->all(), 'id_state', 'name'),
                  ]),
              ],
              [
                  'label' => Yii::t('backend', 'Quận'),
                  'contentOptions' => [
                      'width' => '10%',
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
                  'attribute' => 'district_id',
                  'value' => function($model){
                      return @$model->district->name;
                  },
                  'filter' => Select2::widget([
                      'model' => $searchModel,
                      'attribute' => 'district_id',
                      'data' => $arrDistrict,
                  ]),
              ],
              [
                  'attribute' => 'count_prescription',
                  'contentOptions' => ['style' => 'width: 80px'],
              ],
              [
                  'class' => \common\grid\EnumColumn::class,
                  'attribute' => 'status',
                  'enum' => Patient::statuses(),
                  'filter' => Select2::widget([
                      'model' => $searchModel,
                      'attribute' => 'status',
                      'data' => ['' => Yii::t('backend', 'Tất cả')] + Patient::statuses(),
                  ]),
                  'contentOptions' => [
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
              ],
              [
                  'attribute' => 'created_at',
                  'filter' => DatePicker::widget([
                      'model' => $searchModel,
                      'attribute' => 'created_at',
                      'type' => DatePicker::TYPE_COMPONENT_APPEND,
                      'pluginOptions' => [
                          'format' => 'dd-mm-yyyy',
                          'showMeridian' => true,
                          'endDate' => '0d',
                          'todayHighlight' => true,
                          'autoclose'=>true,
                      ]
                  ]),
                  'value' => function($model){
                    return str_replace('{%1}', '<br/>', date('H:i:s{%1}d-m-Y', $model->created_at));
                  },
                  'contentOptions' => [
                      'width' => '10%',
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
                  'format' => 'html'
              ],
              [
                  'class' => \common\widgets\ActionColumn::class,
                  'contentOptions' => [
                      'class' => 'table-actions',
                      'style' => 'width: 32px'
                  ],
                  'template' => '<div class="dropdown">
                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                      {view}{update}{delete}
                    </div>
                  </div>',
                  'buttons' => [
                      'view' => function ($url, $model) {
                          $attributes = [
                              'class' => 'dropdown-item',
                              'data-toggle' => 'tooltip',
                              'title' => Yii::t('checkin', 'Xem'),
                              'data-pjax' => 0
                          ];
                          return Html::a(Yii::t('checkin', 'Xem'), $url, $attributes, [PermissionConstant::VIEW_PATIENT]);
                      },
                      'update' => function ($url, $model) {
                          $attributes = [
                              'class' => 'dropdown-item',
                              'data-toggle' => 'tooltip',
                              'title' => Yii::t('checkin', 'Cập nhật'),
                              'data-pjax' => 0
                          ];
                          return Html::a(Yii::t('checkin', 'Cập nhật'), $url, $attributes, [PermissionConstant::UPDATE_PATIENT]);
                      },
                      'delete' => function ($url, $model) {
                          $attributes = [
                              'class' => 'dropdown-item',
                              'data-toggle' => 'tooltip',
                              'title' => Yii::t('checkin', 'Xóa'),
                              'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                              'data-method' => 'post',
                          ];
                          return Html::a(Yii::t('checkin', 'Xóa'), $url, $attributes, [PermissionConstant::DELETE_PATIENT]);
                      }
                  ]
              ],
          ],
      ]); ?>
  </div>
</div>
<?php \yii\widgets\Pjax::end();?>
