<?php

/**
 * @var yii\web\View $this
 * @var common\models\Patient $model
 */

$this->title = Yii::t('backend', 'Cập nhật khách hàng') . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Khách hàng'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Cập nhật');
?>
<div class="patient-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
