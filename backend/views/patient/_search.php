<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\Doctor $model
 * @var yii\bootstrap4\ActiveForm $form
 */
?>

<div class="attendee-search my-4">
    <?php $form = ActiveForm::begin([
        'options' => [
            'class' => 'navbar-search navbar-search-light form-inline mr-sm-3 justify-content-center',
            'data-pjax' => true
        ],
        'method' => 'get',
    ]); ?>
      <div class="form-group mb-0">
        <div class="input-group input-group-alternative input-group-merge">
          <?= Html::activeInput('text', $model, 'keyword', ['class' => 'form-control', 'placeholder' => Yii::t('checkin', 'Tìm kiếm...')]);?>
          <div class="input-group-prepend">
              <?php echo Html::submitButton('<i class="fas fa-search"></i>', ['class' => 'input-group-text']); ?>
          </div>
        </div>
      </div>
    <?php ActiveForm::end(); ?>
</div>
