<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use common\models\District;
use common\models\Province;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use common\models\Ward;
/**
 * @var yii\web\View $this
 * @var common\models\Patient $model
 * @var yii\bootstrap4\ActiveForm $form
 */
$provinces = ArrayHelper::map(Province::find()->orderBy('name')->all(), 'id_state', 'name');

$listDistricts = $listWards = [];
if ($model->province_id) {
    $districts = District::find()->where(['id_state' => $model->province_id])->orderBy('name')->all();
    $listDistricts = ArrayHelper::map($districts, 'id_district', 'name');
}
if ($model->district_id) {
    $wards = Ward::find()->where(['id_district' => $model->district_id])->orderBy('name')->all();
    $listWards = ArrayHelper::map($wards, 'id_ward', 'name');
}
?>

<div class="patient-form">
  <?php $form = ActiveForm::begin(); ?>
      <div class="row">
        <div class="col-md-12">
          <div class="card my-4">
            <div class="card-header border-0">
              <h2 class="mb-0">Thông tin cá nhân</h2>
            </div>
            <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                      <?php echo $form->field($model, 'fullname')->textInput(['maxlength' => true]) ?>
                  </div>
                  <div class="col-md-3">
                      <?php echo $form->field($model, 'year_of_birth')->textInput(['type' => 'number', 'class' => 'form-control', 'min' => '1900', 'placeholder' => '']) ?>
                  </div>
                  <div class="col-md-3">
                      <?php echo $form->field($model, 'gender')->widget(Select2::classname(), [
                          'data' => $model::genders(),
                          'options' => [
                              'placeholder' => Yii::t('backend', 'Chọn'),
                          ],
                      ])?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                      <?php echo $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                  </div>
                  <div class="col-md-6">
                      <?php echo $form->field($model, 'email')->textInput(['maxlength' => true, 'type' => 'email']) ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                      <?php echo $form->field($model, 'province_id')->widget(Select2::classname(), [
                          'data' => $provinces,
                          'options' => [
                              'id' => 'sel-province',
                              'placeholder' => Yii::t('backend', 'Chọn Tỉnh/Thành')
                          ],
                      ]);?>
                  </div>
                  <div class="col-md-6">
                      <?php echo $form->field($model, 'district_id')->widget(DepDrop::classname(), [
                          'type' => DepDrop::TYPE_SELECT2,
                          'data' => $listDistricts,
                          'options' => [
                              'id' => 'sel-district',
                              'placeholder' => Yii::t('backend', 'Chọn Quận/Huyện')
                          ],
                          'pluginOptions' => [
                              'depends' => [
                                  'sel-province'
                              ],
                              'url' => Url::to([
                                  '/ajax/get-districts'
                              ])
                          ]
                      ]);?>
                  </div>
                  <div class="col-md-6">
                      <?php echo $form->field($model, 'ward_id')->widget(DepDrop::classname(), [
                          'type' => DepDrop::TYPE_SELECT2,
                          'data' => $listWards,
                          'options' => [
                              'id' => 'sel-ward',
                              'placeholder' => Yii::t('backend', 'Chọn Phường/Xã')
                          ],
                          'pluginOptions' => [
                              'depends' => [
                                  'sel-district'
                              ],
                              'url' => Url::to([
                                  '/ajax/get-wards'
                              ])
                          ]
                      ]);?>
                  </div>
                  <div class="col-md-6">
                      <?php echo $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
                  </div>
                  <div class="col-md-6"><?php echo $form->field($model, 'status')->widget(Select2::classname(), [
                          'data' => $model::statuses(),
                      ]);?>
                  </div>
                </div>
            </div>
            <div class="card-footer">
              <div class="text-left">
                  <?php echo Html::submitButton($model->isNewRecord ? Yii::t('checkin', 'Tạo mới') : Yii::t('checkin', 'Cập nhật'), ['class' => 'btn btn-primary']) ?>
              </div>
            </div>
          </div>
        </div>
      </div>
  <?php ActiveForm::end(); ?>
</div>

