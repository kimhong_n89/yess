<?php

/**
 * @var yii\web\View $this
 * @var common\models\Patient $model
 */

$this->title = Yii::t('backend', 'Tạo khách hàng');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Khách hàng'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patient-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
