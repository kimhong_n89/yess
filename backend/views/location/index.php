<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use backend\controllers\LocationController;
/**
 *
 * @var yii\web\View $this
 */

$this->title = Yii::t('checkin', 'Đơn vị hành chính');
$this->params['breadcrumbs'][] = $this->title;

$classProvince = $classDistrict = $classWard = 'nav-link';

if ($tab == LocationController::TAB_PROVINCE) {
    $classProvince = 'nav-link active';
    $view = '_province';
} else if ($tab == LocationController::TAB_DISTRICT) {
    $classDistrict = 'nav-link active';
    $view = '_district';
} else {
    $classWard = 'nav-link active';
    $view = '_ward';
}
?>
<div class="master-data-index">
    <?php \yii\widgets\Pjax::begin(['id' => 'location-data-gridview-pjax']); ?>
	<div class="row">
		<div class="col-md-12 ml-auto mr-auto">
			<div class="nav-tabs-navigation">
				<div class="nav-tabs-wrapper">
					<ul
						class="nav nav-pills nav-pills-warning nav-pills-icons justify-content-center mb-4"
						data-tabs="tabs">
						<li class="nav-item">
                <?= Html::a(Yii::t('checkin', 'Tỉnh/Thành phố'), ['index', 'tab' => LocationController::TAB_PROVINCE], ['class' => $classProvince])?>
						</li>
            <li class="nav-item">
                <?= Html::a(Yii::t('checkin', 'Quận/Huyện'), ['index', 'tab' => LocationController::TAB_DISTRICT], ['class' => $classDistrict])?>
            </li>
            <li class="nav-item">
                <?= Html::a(Yii::t('checkin', 'Phường/Xã'), ['index', 'tab' => LocationController::TAB_WARD], ['class' => $classWard])?>
            </li>
					</ul>
				</div>
			</div>
			<div class="tab-content">
        <div class="tab-pane active show">
            <?= $this->render($view, ['searchModel' => $searchModel, 'dataProvider' => $dataProvider]);?>
        </div>
			</div>
		</div>
	</div>
  <?php \yii\widgets\Pjax::end();?>
</div>
