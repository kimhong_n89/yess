<?php
use yii\helpers\Html;
use yii\grid\GridView;
use common\grid\EnumColumn;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 *
 * @var yii\web\View $this
 * @var backend\models\search\ProvinceSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */
?>
<div class="template-index">
	<div class="card">
		<div class="card-header card-header-rose card-header-icon">
			<div class="row align-items-center">
				<div class="col">
					<h4 class="card-title"><?= Yii::t('checkin', 'Tỉnh/Thành phố') ?></h4>
				</div>
			</div>
		</div>
            <?php
            echo GridView::widget([
                'layout' => "{items}\n{pager}",
                'options' => [
                    'class' => 'table-responsive'
                ],
                'tableOptions' => [
                    'class' => [
                        'table align-items-center table-striped table-flush',
                    ]
                ],
                'headerRowOptions' => [
                    'class' => [
                        'thead-light'
                    ]
                ],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'id_state',
                        'options' => ['style' => 'width: 5%'],
                    ],
                    'name',
                ],
            ]);
            ?>
    </div>
</div>
