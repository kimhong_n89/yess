<?php
use yii\helpers\Html;
use yii\grid\GridView;
use common\grid\EnumColumn;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\Province;
use kartik\widgets\Select2;
/**
 *
 * @var yii\web\View $this
 * @var backend\models\search\DistrictSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */
?>
<div class="template-index">
	<div class="card">
		<div class="card-header card-header-rose card-header-icon">
			<div class="row align-items-center">
				<div class="col">
					<h4 class="card-title"><?= Yii::t('checkin', 'Quận/Huyện') ?></h4>
				</div>
			</div>
		</div>
            <?php
            echo GridView::widget([
                'layout' => "{items}\n{pager}",
                'options' => [
                    'class' => 'table-responsive'
                ],
                'tableOptions' => [
                    'class' => [
                        'table align-items-center table-striped table-flush',
                    ]
                ],
                'headerRowOptions' => [
                    'class' => [
                        'thead-light'
                    ]
                ],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'id_district',
                        'options' => ['style' => 'width: 5%'],
                    ],
                    'name',
                    [
                        'attribute' => 'id_state',
                        'label' => Yii::t('backend', 'Khu vực'),
                        'contentOptions' => [
                            'width' => '15%',
                        ],
                        'value' => function($model){
                            return @$model->province->name;
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_state',
                            'data' => ['' => Yii::t('backend', 'Tất cả')] + ArrayHelper::map(Province::find()->orderBy('name')->asArray()->all(), 'id_state', 'name'),
                        ]),
                    ],
                ],
            ]);
            ?>
    </div>
</div>
