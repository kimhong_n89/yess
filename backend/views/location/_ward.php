<?php
use yii\helpers\Html;
use yii\grid\GridView;
use common\grid\EnumColumn;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\District;
use common\models\Province;
use kartik\widgets\Select2;
/**
 *
 * @var yii\web\View $this
 * @var backend\models\search\WardSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$arrDistrict = ['' => Yii::t('backend', 'Tất cả')];
if ($searchModel->id_state) {
    $arrDistrict = $arrDistrict + ArrayHelper::map(District::find()->where(['id_state' => $searchModel->id_state])->orderBy('name')->asArray()->all(), 'id_district', 'name');
}

?>

<div class="template-index">
	<div class="card">
		<div class="card-header card-header-rose card-header-icon">
			<div class="row align-items-center">
				<div class="col">
					<h4 class="card-title"><?= Yii::t('checkin', 'Phường/Xã') ?></h4>
				</div>
			</div>
		</div>
            <?php
            echo GridView::widget([
                'layout' => "{items}\n{pager}",
                'options' => [
                    'class' => 'table-responsive'
                ],
                'tableOptions' => [
                    'class' => [
                        'table align-items-center table-striped table-flush',
                    ]
                ],
                'headerRowOptions' => [
                    'class' => [
                        'thead-light'
                    ]
                ],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'id_ward',
                        'options' => ['style' => 'width: 5%'],
                    ],
                    'name',
                    [
                        'attribute' => 'id_state',
                        'label' => Yii::t('backend', 'Khu vực'),
                        'contentOptions' => [
                            'width' => '10%',
                            'style' => 'word-break: break-word; white-space: normal;',
                        ],
                        'value' => function($model){
                            return @$model->district->province->name;
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_state',
                            'data' => ['' => Yii::t('backend', 'Tất cả')] + ArrayHelper::map(Province::find()->orderBy('name')->asArray()->all(), 'id_state', 'name'),
                        ]),
                    ],
                    [
                        'label' => Yii::t('backend', 'Quận'),
                        'contentOptions' => [
                            'width' => '10%',
                            'style' => 'word-break: break-word; white-space: normal;',
                        ],
                        'attribute' => 'id_district',
                        'value' => function($model){
                            return @$model->district->name;
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'id_district',
                            'data' => $arrDistrict,
                        ]),
                    ],
                ],
            ]);
            ?>
    </div>
</div>
