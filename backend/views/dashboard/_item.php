<?php

use yii\helpers\Html;

/**
 * @var common\models\Prescription $model
 */

$attributes = [
    'role'=>'modal-remote',
    'data-request-method'=>'get',
    'data-confirm-title'=>Yii::t('spa', 'Are you sure?'),
    'data-confirm-message'=>Yii::t('spa', 'Are you sure want to checkin'),
    'title'=> Yii::t('backend', 'Checkin'),
    'class' => 'btn btn-success'
];
?>

<div class="card mb-0 h-100" style="background-color: rgba(255, 255, 255, 0.1)">
    <div class="card-body">
      <div class="card-category text-white mb-2"><i class="fas fa-file-medical-alt"></i> <?= $model->id; ?></div>
      <div class="card-category text-white mb-2"><i class="fas fa-user-md"></i> <?= Html::encode($model->doctor->fullname); ?></div>
      <div class="card-category text-white mb-2"><i class="fas fa-user"></i> <?= Html::encode($model->patient->fullname); ?></div>
      <div class="card-category text-white mb-2"><i class="fas fa-map-marker-alt"></i> <?= Html::encode($model->patient->getFullAddress())?></div>
    </div>
</div>