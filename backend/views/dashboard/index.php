<?php
use common\grid\EnumColumn;
use yii\widgets\Pjax;
use yii\widgets\ListView;
use yii\web\JsExpression;
use yii\helpers\Json;
use backend\widgets\chartist\Chartist;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap4\Dropdown;
use yii\widgets\ActiveForm;
use common\components\utils\Html;
use common\components\utils\PermissionConstant;
/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('spa', 'Dashboard');
$this->params['breadcrumbs'][] = $this->title;
$defaultHigh = 200;
?>
<style>
.icon-block {
	font-size: 80px;
}

.count-txt {
	font-size: 20px;
}
</style>
<div class="dashboard-index">
  <?php if (userCan(PermissionConstant::VIEW_STATISTIC)) :?>
    <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-6">
        <div class="card mb-0 h-100 card-stats">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0"><?= Yii::t('checkin', 'Bác sĩ') ?></h5>
                <span class="h2 font-weight-bold mb-0"><?= $doctor_count ?></span>
              </div>
              <div class="col-auto">
                <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                  <i class="fas fa-user-md"></i>
                </div>
              </div>
            </div>
            <p class="mt-3 mb-0 text-sm">
              <span class="text-nowrap"><?= Html::a(Yii::t('backend', 'Xem tất cả'), ['/doctor'], [], [
                      PermissionConstant::VIEW_DOCTOR
                  ]); ?></span>
            </p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-6">
        <div class="card mb-0 h-100 card-stats">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0"><?= Yii::t('checkin', 'Toa thuốc') ?></h5>
                <span class="h2 font-weight-bold mb-0"><?= $prescription_count ?></span>
              </div>
              <div class="col-auto">
                <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                  <i class="fas fa-file-medical-alt"></i>
                </div>
              </div>
            </div>
            <p class="mt-3 mb-0 text-sm">
              <span class="text-nowrap"><?= Html::a(Yii::t('backend', 'Xem tất cả'), ['/prescription'], [], [
                      PermissionConstant::VIEW_PRESCRIPTION
                  ]); ?></span>
            </p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-6">
        <div class="card mb-0 h-100 card-stats">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <h5 class="card-title text-uppercase text-muted mb-0"><?= Yii::t('checkin', 'Khách hàng') ?></h5>
                <span class="h2 font-weight-bold mb-0"><?= $customer_count ?></span>
              </div>
              <div class="col-auto">
                <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                  <i class="fas fa-users"></i>
                </div>
              </div>
            </div>
            <p class="mt-3 mb-0 text-sm">
              <span class="text-nowrap"><?= Html::a(Yii::t('backend', 'Xem tất cả'), ['/patient'], [], [
                      PermissionConstant::VIEW_PATIENT
                  ]); ?></span>
            </p>
          </div>
        </div>
      </div>
    </div>
  <?php endif;?>
  <?php if (userCan(PermissionConstant::VIEW_NEW_PRESCRIPTION)) :?>
    <div class="card my-4">
      <div class="card-header border-0">
        <div class="row align-items-center">
          <div class="col">
            <h3 class="mb-0"><?= Yii::t('checkin', 'Các đơn thuốc mới tạo') ?></h3>
          </div>
          <div class="col text-right">
              <?= Html::a(Yii::t('backend', 'Xem tất cả'), [
                  '/prescription'
              ], [
                  'class' => 'btn btn-sm btn-primary'
              ], [
                  PermissionConstant::VIEW_PRESCRIPTION
              ]);
              ?>
          </div>
        </div>
      </div>
      <div class="card-body">
          <?php
          echo ListView::widget([
              'dataProvider' => $prescriptionProvider,
              'itemView' => '_item',
              'options' => [
                  'class' => 'row',
                  'role' => 'listbox'
              ],
              'itemOptions' => [
                  'class' => 'col-md-3'
              ],
              'layout' => '{items}'
          ]);
          ?>
      </div>
    </div>
  <?php endif;?>
    <div class="row">
      <?php if (userCan(PermissionConstant::VIEW_CHART)) :?>
        <div class="col-lg-7 col-md-12">
        <?php Pjax::begin(['id' => 'revenue-chart']) ?>
        <?php
        $previous_params = ['dashboard/index', 'month' => sprintf("%02d", $previous['month']), 'year' => $previous['year']];
        $next_params = ['dashboard/index', 'month' => sprintf("%02d", $next['month']), 'year' => $next['year']];
        $previous_link = Html::a('<i class="fas fa-chevron-left"></i>', $previous_params, ['class' => 'mx-2']);
        $next_link = Html::a('<i class="fas fa-chevron-right"></i>', $next_params, ['class' => 'mx-2']);
        ?>
            <div class="card card-chart">
                <div class="card-header card-header-rose">
                <div class="row mb-2">
                    <div class="col-md-12">
                    <h3 class="text-right mt-0"> <?= $previous_link . $month .'/'. $year . $next_link?></h3>
                    </div>
                </div>
                <?=
                Chartist::widget([
                    'tagName' => 'div',
                    'data' => new JsExpression(Json::encode([
                        'labels' => $chartData['labels'],
                        'series' => [
                            $chartData['series'],
                        ]
                    ])),
                    'chartOptions' => [
                        'options' => [
                            'high' => @$chartData['highest'] > $defaultHigh ? @$chartData['highest'] : $defaultHigh,
                            'seriesBarDistance' => 15,
                            'fullWidth' => true,
                            'chartPadding' => [
                                'top' => 20,
                                'right' => 20,
                                'bottom' => 20,
                            ],
                            'height' => '380px',
                            'plugins' => [
                                new JsExpression("Chartist.plugins.tooltip()"),
                                new JsExpression("Chartist.plugins.ctPointLabels({
                                                    textAnchor: 'middle',
                                                    color: '#ffa726'
                                                  })")
                            ]
                        ],
                    ],
                    'widgetOptions' => [
                        'type' => 'Line', // Bar, Line, or Pie, i.e. the chart types supported by Chartist.js
                        'useClass' => 'chartist-chart' // optional parameter, needs to be included in the htmlOptions class string as well if set! Forces the widget to use this class name as reference point for Chartist.js instead of an id
                    ],
                    'htmlOptions' => [
                        'class' => 'chartist-chart ct-chart', // ct-chart for CSS references; size of the charting area needs to be assigned as well
                        //...
                    ]
                ]);
                ?>
                </div>
                <div class="card-body">
                  <h4><?= Yii::t('backend', 'Thống kê lượng đơn theo ngày trong tháng') ?></h4>
                </div>
            </div>
            <?php Pjax::end()?>
        </div>
      <?php endif;?>
      <?php if (userCan(PermissionConstant::VIEW_LOGIN_HISTORY)) :?>
        <div class="col-lg-5 col-md-12">
          <div class="card card-full-height">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0"><?= Yii::t('checkin', 'Bác sĩ đăng nhập') ?></h3>
                </div>
                <div class="col text-right">
                    <?= Html::a(Yii::t('backend', 'Xem tất cả'), [
                        '/login-history'
                    ], [
                        'class' => 'btn btn-sm btn-primary'
                    ]);
                    ?>
                </div>
              </div>
            </div>
            <div class="table-responsive">
                <?php
                echo \yii\grid\GridView::widget([
                    'layout' => "{items}",
                    'options' => [
                        'class' => 'table-responsive'
                    ],
                    'tableOptions' => [
                        'class' => [
                            'table align-items-center table-striped table-flush',
                        ]
                    ],
                    'headerRowOptions' => [
                        'class' => [
                            'thead-light'
                        ]
                    ],
                    'dataProvider' => $loginHistoryProvider,
                    'columns' => [
                        [
                            'class' => 'yii\grid\SerialColumn'
                        ],
                        [
                            'attribute' => 'phone_number',
                            'label' => 'Số ĐT',
                            'enableSorting' =>false
                        ],
                        'email',
                        [
                            'attribute' => 'fullname',
                            'label' => 'Bác sĩ'
                        ],
                        [
                            'attribute' => 'created_at',
                            'label' => 'Đăng nhập lúc',
                            'options' => [
                                // 'style' => 'width: 10%'
                            ],
                            'format' => [
                                'datetime',
                                'dd/MM/Y H:mm:ss'
                            ],
                            'enableSorting' =>false
                        ],
                    ]
                ]);
                ?>
            </div>
          </div>
        </div>
      <?php endif;?>
    </div>
</div>

<style>
    table tr td {
        overflow-wrap: break-word;
        word-wrap: break-word;
        white-space: normal !important;
    }
</style>