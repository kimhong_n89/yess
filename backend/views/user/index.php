<?php

use common\grid\EnumColumn;
use common\models\User;
use kartik\date\DatePicker;
use common\components\utils\Html;
use yii\grid\GridView;
use rmrevin\yii\fontawesome\FAS;
use common\components\utils\PermissionConstant;

/**
 * @var yii\web\View $this
 * @var backend\models\search\UserSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */
$this->title = Yii::t('backend', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-index">
    <div class="card">
        <div class="card-header">
          <div class="row align-items-center">
            <div class="col text-right">
                <?= Html::a('<i class="fas fa-plus"></i>', ['create'], ['class' => 'float-right btn btn-sm btn-primary'], [PermissionConstant::CREATE_USER])?>
            </div>
          </div>
        </div>
            <?php echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{pager}",
                'options' => [
                    'class' => 'table-responsive'
                ],
                'tableOptions' => [
                    'class' => [
                        'table align-items-center table-striped table-flush',
                    ]
                ],
                'headerRowOptions' => [
                    'class' => [
                        'thead-light'
                    ]
                ],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'options' => ['style' => 'width: 5%'],
                    ],
                    'username',
                    'email:email',
                    [
                        'class' => EnumColumn::class,
                        'attribute' => 'status',
                        'enum' => User::statuses(),
                        'filter' => User::statuses()
                    ],
                    [
                        'attribute' => 'created_at',
                        'format' => 'datetime',
                        'filter' => DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'created_at',
                            'type' => DatePicker::TYPE_COMPONENT_APPEND,
                            'pluginOptions' => [
                                'format' => 'dd-mm-yyyy',
                                'showMeridian' => true,
                                'endDate' => '0d',
                                'todayHighlight' => true
                            ]
                        ]),
                        'value' => function($model){
                            return str_replace('{%1}', '<br/>', date('H:i:s{%1}d-m-Y', $model->created_at));
                        },
                        'contentOptions' => [
                            'width' => '10%',
                            'style' => 'word-break: break-word; white-space: normal;',
                        ],
                        'format' => 'html'
                    ],
                    [
                        'attribute' => 'logged_at',
                        'format' => 'datetime',
                        'filter' => DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'logged_at',
                            'type' => DatePicker::TYPE_COMPONENT_APPEND,
                            'pluginOptions' => [
                                'format' => 'dd-mm-yyyy',
                                'showMeridian' => true,
                                'endDate' => '0d',
                                'todayHighlight' => true
                            ]
                        ]),
                        'value' => function($model){
                            return str_replace('{%1}', '<br/>', date('H:i:s{%1}d-m-Y', $model->logged_at));
                        },
                        'contentOptions' => [
                            'width' => '10%',
                            'style' => 'word-break: break-word; white-space: normal;',
                        ],
                        'format' => 'html'
                    ],
                    // 'updated_at',

                    [
                        'class' => \common\widgets\ActionColumn::class,
                        'contentOptions' => [
                            'class' => 'table-actions',
                            'style' => 'width: 32px'
                        ],
                        'template' => '<div class="dropdown">
                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-ellipsis-v"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                              {view}{update}{delete}
                            </div>
                          </div>',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                $attributes = [
                                    'class' => 'dropdown-item',
                                    'data-toggle' => 'tooltip',
                                    'title' => Yii::t('checkin', 'Xem'),
                                    'data-pjax' => 0
                                ];
                                return Html::a(Yii::t('checkin', 'Xem'), $url, $attributes, [PermissionConstant::VIEW_USER]);
                            },
                            'update' => function ($url, $model) {
                                $attributes = [
                                    'class' => 'dropdown-item',
                                    'data-toggle' => 'tooltip',
                                    'title' => Yii::t('checkin', 'Cập nhật'),
                                    'data-pjax' => 0
                                ];
                                return Html::a(Yii::t('checkin', 'Cập nhật'), $url, $attributes, [PermissionConstant::UPDATE_USER]);
                            },
                            'delete' => function ($url, $model) {
                                $attributes = [
                                    'class' => 'dropdown-item',
                                    'data-toggle' => 'tooltip',
                                    'title' => Yii::t('checkin', 'Xóa'),
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',
                                ];
                                return Html::a(Yii::t('checkin', 'Xóa'), $url, $attributes, [PermissionConstant::DELETE_USER]);
                            }
                        ]
                    ],
                ],
            ]); ?>
    </div>
</div>