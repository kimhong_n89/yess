<?php

use common\components\utils\Html;
use yii\widgets\DetailView;
use common\components\utils\PermissionConstant;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->getPublicIdentity();
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">
    <div class="card">
        <div class="card-header">
            <?php echo Html::a(Yii::t('backend', 'Cập nhật'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary'], [PermissionConstant::UPDATE_USER]) ?>
            <?php echo Html::a(Yii::t('backend', 'Xóa'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ], [PermissionConstant::DELETE_USER]) ?>
        </div>
        <div class="card-body p-0">
            <?php echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'username',
                    'auth_key',
                    'email:email',
                    [
                        'attribute' => 'status',
                        'value' => function($model) {
                            return @\common\models\User::statuses()[$model->status];
                        }
                    ],
                    'created_at:datetime',
                    'updated_at:datetime',
                    'logged_at:datetime',
                ],
                'options' => [
                    'class' => 'table'
                ]
            ]) ?>
        </div>
    </div>
</div>
