<?php

use yii\helpers\Html;
use rmrevin\yii\fontawesome\FAS;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
$statusCode = property_exists($exception, 'statusCode') ? $exception->statusCode : 500;
$textColor = $statusCode == 500? 'danger': 'warning';
?>
  <div class="d-flex align-items-center justify-content-center">
    <div class="error-page text-center" style="margin-top: 35px;">
        <div class="row justify-content-center">
            <img src="/img/logo.png" class="navbar-brand-img img-fluid" alt="Yess">
        </div>

        <div style="margin: 10px; font-weight: 500; font-size: 60px;opacity: 0.8;" class="headline text-<?php echo $textColor?>"><?php echo $statusCode ?></div>

        <div class="error-content">
            <h3 class="font-weight-normal"><?php echo FAS::icon('exclamation-triangle', ['class' => "text-$textColor"]).'    '.nl2br(Html::encode(empty($message) ? Yii::t('backend', 'Unknown error') : $message)) ?></h3>

            <ul class="list-inline" style="margin-top: 40px; margin-bottom: 40px;">
                <li class="list-inline-item">
                    <?php echo Html::a(FAS::icon('home').' '.Yii::t('backend', 'Trang chủ'), ['/'], ['class' => ['btn', 'btn-primary', 'btn-lg']]) ?>
                </li>
            </ul>
        </div>
    </div>
  </div>
