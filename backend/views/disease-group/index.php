<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var backend\models\search\DiseaseGroupSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('backend', 'Chẩn đoán bệnh');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php \yii\widgets\Pjax::begin(['id' => 'disease-group-pjax']);?>
  <div class="disease-group-index">
    <div class="card">
      <div class="card-header">
        <div class="row align-items-center">

        </div>
          <?php echo $this->render('_search', ['model' => $searchModel]); ?>
      </div>


        <?php echo GridView::widget([
            'layout' => "{items}\n{pager}",
            'options' => [
                'class' => 'table-responsive'
            ],
            'tableOptions' => [
                'class' => [
                    'table align-items-center table-striped table-flush',
                ]
            ],
            'headerRowOptions' => [
                'class' => [
                    'thead-light'
                ]
            ],
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute' => 'id',
                    'options' => ['style' => 'width: 60px'],
                ],
                [
                    'attribute' => 'name',
                ],
            ],
        ]); ?>
    </div>
  </div>
<?php \yii\widgets\Pjax::end();?>
