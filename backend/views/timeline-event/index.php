<?php
/**
 * @author Eugine Terentev <eugine@terentev.net>
 * @author Victor Gonzalez <victor@vgr.cl>
 * @var yii\web\View $this
 * @var common\models\TimelineEvent $model
 * @var yii\data\ActiveDataProvider $dataProvider
 */

use rmrevin\yii\fontawesome\FAS;

$this->title = Yii::t('backend', 'Timeline');
$icons = [
    'user' => FAS::icon('user', ['bg-blue'])
];
?>

<?php \yii\widgets\Pjax::begin() ?>
<div class="row">
    <div class="col-md-12">
        <?php if ($dataProvider->count > 0) : ?>
        <div class="card">
            <div class="card-header bg-transparent">
              <h3 class="mb-0">Timeline</h3>
            </div>
            <div class="card-body">
              <div class="timeline timeline-one-side" data-timeline-content="axis" data-timeline-axis-style="dashed">
                <?php foreach ($dataProvider->getModels() as $model) : ?>
                            <?php
                            try {
                                $viewFile = sprintf('%s/%s', $model->category, $model->event);
                                echo $this->render($viewFile, ['model' => $model]);
                            } catch (\yii\base\InvalidArgumentException $e) {
                                echo $this->render('_item', ['model' => $model]);
                            }
                            ?>
                    <?php endforeach; ?>
              </div>
            </div>
          </div>
        <?php else : ?>
            <?php echo Yii::t('backend', 'No events found') ?>
        <?php endif; ?>
    </div>
    <div class="col-md-12 text-center">
        <?php echo \yii\widgets\LinkPager::widget([
            'pagination'=>$dataProvider->pagination,
            'options' => ['class' => 'pagination']
        ]) ?>
    </div>
</div>
<?php \yii\widgets\Pjax::end() ?>

