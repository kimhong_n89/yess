<?php
/**
 * @author Eugene Terentev <eugene@terentev.net>
 * @author Victor Gonzalez <victor@vgr.cl>
 * @var common\models\TimelineEvent $model
 */

use yii\helpers\Html;
?>
<div class="timeline-block">
  <span class="timeline-step badge-success">
    <i class="fas fa-user-plus"></i>
  </span>
  <div class="timeline-content">
    <small class="text-muted font-weight-bold"><?php echo Yii::$app->formatter->asRelativeTime($model->created_at) ?></small>
    <h5 class=" mt-3 mb-0"><?php echo Yii::t('backend', 'User mới!')?></h5>
    <?php echo Yii::t('backend', '{identity} đã được tạo', [
        'identity' => Html::a($model->data['public_identity'], ['user/view', 'id' => $model->data['user_id']]),
        'created_at' => Yii::$app->formatter->asDatetime($model->data['created_at'])
    ]) ?>
  </div>
</div>