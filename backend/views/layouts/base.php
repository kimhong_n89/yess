<?php
use backend\assets\BackendAsset;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use backend\assets\MyCrudAsset;
use common\widgets\MyModal;

/**
 * @var yii\web\View $this
 * @var string $content
 */

$bundle = BackendAsset::register($this);
MyCrudAsset::register($this);

$this->params['body-class'] = $this->params['body-class'] ?? null;
$keyStorage = Yii::$app->keyStorage;
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
<head>
    <meta charset="<?php echo Yii::$app->charset ?>">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <?php echo Html::csrfMetaTags() ?>
    <title><?php echo Html::encode($this->title) ?> - Yersin - Republic of Doctors</title>
    <?php $this->head() ?>

</head>
<?php echo Html::beginTag('body', [
    'class' => implode(' ', [
        @$_COOKIE['sidenav-state'] == 'pinned' ? 'g-sidenav-pinned nav-open' : '',
        isset($this->context->bodyClass) ? $this->context->bodyClass : ''
    ]),
])?>
    <?php $this->beginBody() ?>
        <?php echo $content ?>
    <?php $this->endBody() ?>
<?php echo Html::endTag('body') ?>
<?php 
MyModal::begin([
    'id' => 'ajaxCrubModal',
    'size' => MyModal::SIZE_LARGE,
    'footer' => true
]);
?>
</html>
<?php $this->endPage() ?>
