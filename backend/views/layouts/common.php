<?php
/**
 * @author Eugine Terentev <eugine@terentev.net>
 * @author Victor Gonzalez <victor@vgr.cl>
 * @var yii\web\View $this
 * @var string $content
 */

use backend\assets\BackendAsset;
use backend\modules\system\models\SystemLog;
use backend\widgets\MainSidebarMenu;
use common\models\TimelineEvent;
use yii\bootstrap4\Alert;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\log\Logger;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use common\components\utils\Html;
use rmrevin\yii\fontawesome\FAR;
use rmrevin\yii\fontawesome\FAS;
use common\components\keyStorage\FormModel;
use common\components\keyStorage\FormWidget;
use common\components\utils\PermissionConstant;

//$bundle = BackendAsset::register($this);
Yii::info(Yii::$app->components["i18n"]["translations"]['*']['class'], 'test');

$keyStorage = Yii::$app->keyStorage;

$logEntries = [
    [
        'label' => Yii::t('backend', 'Có {num} log', ['num' => SystemLog::find()->count()]),
        'linkOptions' => ['class' => ['dropdown-item', 'dropdown-header']]
    ],
    '<div class="dropdown-divider"></div>',
];
foreach (SystemLog::find()->orderBy(['log_time' => SORT_DESC])->limit(5)->all() as $logEntry) {
    $logEntries[] = [
        'label' => FAS::icon('exclamation-triangle', ['class' => [$logEntry->level === Logger::LEVEL_ERROR ? 'text-red' : 'text-yellow'
        ]
        ]) . ' ' . $logEntry->category,
        'url' => [
            '/system/log/view',
            'id' => $logEntry->id
        ]
    ];
}
$logEntries[] = '<div class="dropdown-divider"></div>';
$logEntries[] = [
    'label' => Yii::t('backend', 'Xem tất cả'),
    'url' => [
        '/system/log/index'
    ],
    'linkOptions' => [
        'class' => [
            'dropdown-item',
            'dropdown-footer'
        ]
    ]
];
?>

<?php $this->beginContent('@backend/views/layouts/base.php'); ?>
<div class="wrapper">
  <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white " id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header  d-flex  align-items-center">
        <a class="navbar-brand" href="<?=Yii::$app->homeUrl;?>">
          <img src="/img/logo.png" class="navbar-brand-img" alt="Yess">
        </a>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">

        <?php
        $activeApplicationMenu = in_array(Yii::$app->controller->id, ['store', 'product', 'disease-group', 'location', 'master-data']);
        $showApplicationSub = $activeApplicationMenu ? 'show' : '';

        $activeSystemMenu = in_array(Yii::$app->controller->id, ['user', 'setting', 'category', 'article']) || (Yii::$app->controller->module->id == 'translation') || (Yii::$app->controller->module->id == 'rbac');
        $showSystemSub = $activeSystemMenu ? 'show' : '';

        $canStore = rbac_is_allow_exec(PermissionConstant::VIEW_STORE);
        $canProduct = rbac_is_allow_exec(PermissionConstant::VIEW_PRODUCT);
        $canDiseaseGroup = rbac_is_allow_exec(PermissionConstant::VIEW_DISEASE_GROUP);
        $canLocation = rbac_is_allow_exec(PermissionConstant::VIEW_LOCATION);
        $canMasterData = rbac_is_allow_exec(PermissionConstant::MANAGE_MASTER_DATA);
        $canCatalog = $canStore || $canProduct || $canDiseaseGroup || $canLocation || $canMasterData;

        $canUser = rbac_is_allow_exec(PermissionConstant::VIEW_USER);
        $canSetting = rbac_is_allow_exec(PermissionConstant::MANAGE_SETTING);
        $canRbac = rbac_is_allow_exec(PermissionConstant::MANAGE_RBAC);
        $canFeedback = rbac_is_allow_exec(PermissionConstant::MANAGE_FEEDBACK);
        $canSystem = $canSetting || $canUser || $canRbac || $canFeedback;

        echo MainSidebarMenu::widget([
                        'options' => [
                            'class' => ['navbar-nav']
                        ],
                        'linkTemplate' => '<a href="{url}" class="nav-link" {collapse}>{icon}<span class="nav-link-text">{label}</span>{dropdown-caret}{badge}</a>',
                        'linkTemplateActive' => '<a href="{url}" class="nav-link" {collapse}>{icon}<span class="nav-link-text">{label}</span>{dropdown-caret}{badge}</a>',
                        'activeCssClass' => 'active',
                        'dropdownCaret' => '<b class="caret"></b>',
                        'items' => [
                            [
                                'label' => Yii::t('backend', 'Dashboard'),
                                'icon' => '<i class="ni ni-shop text-primary"></i>',
                                'url' => ['/dashboard/index'],
                                'visible' => rbac_is_allow_exec(PermissionConstant::VIEW_DASHBOARD)
                            ],
                            [
                                'label' => Yii::t('backend', 'Toa thuốc'),
                                'icon' => '<i class="fas fa-file-medical-alt text-warning"></i>',
                                'url' => ['/prescription/index'],
                                'visible' => rbac_is_allow_exec(PermissionConstant::VIEW_PRESCRIPTION)
                            ],
                            [
                                'label' => Yii::t('backend', 'Bác sĩ'),
                                'icon' => '<i class="fas fa-user-md text-info"></i>',
                                'url' => ['/doctor/index'],
                                'visible' => rbac_is_allow_exec(PermissionConstant::VIEW_DOCTOR)
                            ],
                            [
                                'label' => Yii::t('backend', 'Khách hàng'),
                                'icon' => '<i class="fas fa-users text-success"></i>',
                                'url' => ['/patient/index'],
                                'visible' => rbac_is_allow_exec(PermissionConstant::VIEW_PATIENT)
                            ],
                            [
                                'label' => Yii::t('backend', 'Danh mục'),
                                'icon' => FAS::icon('th-list', ['class' => ['text-red']]),
                                'url' => '#menu-application',
                                'collapse' => true,
                                'submenuTemplate' => '<div class="collapse '.$showApplicationSub.'" id="menu-application"><ul class="nav nav-sm flex-column">{items}</ul></div>',
                                'active' => $activeApplicationMenu,
                                'visible' => $canCatalog,
                                'items' => [
                                    [
                                        'label' => Yii::t('backend', 'Nhà thuốc'),
                                        'icon' => '<span class="sidenav-mini-icon"><i class="ni ni-shop text-primary"></i></span>',
                                        'url' => ['/store/index'],
                                        'active' => 'store' === Yii::$app->controller->id,
                                        'visible' => $canStore
                                    ],
                                    [
                                        'label' => Yii::t('backend', 'Sản phẩm thuốc'),
                                        'icon' => '<span class="sidenav-mini-icon"><i class="fas fa-capsules"></i></span>',
                                        'url' => ['/product/index'],
                                        'active' => 'product' === Yii::$app->controller->id,
                                        'visible' => $canProduct
                                    ],
                                    [
                                        'label' => Yii::t('backend', 'Chẩn đoán bệnh'),
                                        'icon' => '<span class="sidenav-mini-icon"><i class="fas fa-clipboard-list"></i></span>',
                                        'url' => ['/disease-group/index'],
                                        'active' => 'disease-group' === Yii::$app->controller->id,
                                        'visible' => $canDiseaseGroup
                                    ],
                                    [
                                        'label' => Yii::t('backend', 'Đơn vị hành chính'),
                                        'icon' => '<span class="sidenav-mini-icon"><i class="fas fa-map-marked-alt"></i></span>',
                                        'url' => ['/location/index'],
                                        'active' => 'location' === Yii::$app->controller->id,
                                        'visible' => $canLocation
                                    ],
                                    [
                                        'label' => Yii::t('backend', 'Khác'),
                                        'icon' => '<span class="sidenav-mini-icon"><i class="fas fa-network-wired"></i></span>',
                                        'url' => ['/master-data/index'],
                                        'active' => 'master-data' === Yii::$app->controller->id,
                                        'visible' => $canMasterData
                                    ],
                                ],
                            ],
                            [
                                'label' => Yii::t('backend', 'Hệ thống'),
                                'url' => '#menu-system',
                                'collapse' => true,
                                'submenuTemplate' => '<div class="collapse '.$showSystemSub.'" id="menu-system"><ul class="nav nav-sm flex-column">{items}</ul></div>',
                                'icon' => FAS::icon('cogs', ['class' => ['nav-icon', 'text-primary']]),
                                'active' => $activeSystemMenu,
                                'visible' => $canSystem,
                                'items' => [
                                    [
                                        'label' => Yii::t('backend', 'Cài đặt'),
                                        'icon' => '<span class="sidenav-mini-icon"><i class="fas fa-cogs"></i></span>',
                                        'url' => ['/setting/index'],
                                        'visible' => $canSetting,
                                    ],
                                    [
                                        'label' => Yii::t('backend', 'Người dùng'),
                                        'icon' => '<span class="sidenav-mini-icon"><i class="fas fa-users"></i></span>',
                                        'url' => ['/user/index'],
                                        'visible' => $canUser,
                                    ],
                                    [
                                        'label' => Yii::t('backend', 'Nhóm người dùng'),
                                        'url' => ['/rbac/role/index'],
                                        'icon' => '<span class="sidenav-mini-icon"><i class="fas fa-users"></i></span>',
                                        'visible' => $canRbac,
                                    ],
                                    [
                                        'label' => Yii::t('backend', 'Góp ý'),
                                        'icon' => '<span class="sidenav-mini-icon"><i class="far fa-comments"></i></span>',
                                        'url' => ['/feedback/index'],
                                        'visible' => $canFeedback,
                                    ],
                                ],
                            ],
                        ],
                    ]) ?>
        </div>
      </div>
    </div>
  </nav>
    <!-- content wrapper -->
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <?php NavBar::begin([
        'innerContainerOptions' => ['class' => 'container-fluid'],
        'options' => [
            'class' => 'navbar navbar-top navbar-expand navbar-dark bg-gradient-header border-bottom',
        ],
        'togglerContent' => '',
        'brandLabel' => '<div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </div>',
    ]); ?>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- right navbar links -->
        <?php
          $avatar = Yii::$app->user->identity->userProfile->getAvatar('/img/anonymous.png');
          echo Nav::widget([
            'options' => ['class' => 'navbar-nav align-items-center  ml-auto mr-md-0'],
            'encodeLabels' => false,
            'items' => [
                [
                    // timeline events
                    'label' => '<i class="ni ni-bell-55"></i>' . ' <span class="notification">'.TimelineEvent::find()->today()->count().'</span>',
                    'url'  => ['/timeline-event/index'],
                    'visible' => rbac_is_allow_exec(PermissionConstant::VIEW_TIMELINE)
                ],
                [
                    // log events
                    'label' => '<i class="fas fa-exclamation-triangle"></i>' . ' <span class="notification">' . SystemLog::find()->count().'</span>',
                    'url' => '#',
                    'dropdownOptions' => [
                        'class' => 'dropdown-menu-right',
                    ],
                    'linkOptions' => ['class' => 'no-dropdown-toggle'],
                    'items' => $logEntries,
                    'visible' => rbac_is_allow_exec(PermissionConstant::MANAGE_LOG)
                ],
                '<li class="nav-item dropdown">
                    <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                          <img alt="Image placeholder" src="'. $avatar .'">
                        </span>
                        <div class="media-body  ml-2  d-none d-lg-block">
                          <span class="mb-0 text-sm  font-weight-bold">'.\Yii::$app->user->getIdentity()->username.'</span>
                        </div>
                      </div>
                    </a>
    
                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                    '.Html::a(Yii::t('backend', 'Thông tin cá nhân'), ['/sign-in/profile'], ['class' => 'dropdown-item']).'
                    '.Html::a(Yii::t('backend', 'Tài khoản'), ['/sign-in/account'], ['class' => 'dropdown-item']).'
                        <div class="dropdown-divider"></div>
                        '.Html::a(Yii::t('backend', 'Đăng xuất'), ['/sign-in/logout'], ['class' => 'dropdown-item', 'data-method' => 'post']).'
                      </div>
                </li>
                '
            ]
        ]); ?>
        <!-- /right navbar links -->
        </div>
    <?php NavBar::end(); ?>
    <!-- /navbar -->
    <div class="header pb-6 d-print-none">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-2">
            <div class="col-lg-6 col-7">
              <h6 class="h2 d-inline-block mb-0"><?php echo Html::encode($this->title) ?></h6>
            </div>
            <div class="col-lg-6 col-5 text-right">
                <?php echo Breadcrumbs::widget([
                    'tag' => 'ol',
                    'navOptions' => ['class' => ['d-none d-md-inline-block ml-md-4']],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'options' => ['class' => ['breadcrumb breadcrumb-links breadcrumb-light']]
                ]) ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- main content -->
    <section class="container-fluid mt--6">
          <?php if (Yii::$app->session->hasFlash('alert')) : ?>
              <?php echo Alert::widget([
                  'body' => ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
                  'options' => ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
              ]) ?>
          <?php endif; ?>
          <?php echo $content ?>
              <div class="copyright text-center text-muted d-print-none">
      Copyright © <?php echo date('Y') ?> Yersin
    </div>
    </section>
    <!-- /main content -->

        <?php
//         echo Html::a(FAS::icon('chevron-up'), '#', [
//             'class' => ['btn', 'btn-primary', 'back-to-top'],
//             'role' => 'button',
//             'aria-label' => 'Scroll to top',
//         ])
        ?>
    </div>
    <!-- /content wrapper -->

    <!-- footer -->
<!--   <footer class="footer pt-0"> -->
<!--     <div class="copyright text-center text-muted"> -->
<!--       Copyright @ 2021 ITO TECHNOLOGY Company All Right Reserved -->
<!--     </div> -->
<!--   </footer> -->
    <!-- /footer -->
</div>
<?php $this->endContent(); ?>
