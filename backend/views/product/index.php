<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var backend\models\search\ProductSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('backend', 'Sản phẩm thuốc');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php \yii\widgets\Pjax::begin(['id' => 'product-pjax']);?>
<div class="product-index">
  <div class="card">
    <div class="card-header">
      <div class="row align-items-center">

      </div>
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>


      <?php echo GridView::widget([
          'layout' => "{items}\n{pager}",
          'options' => [
              'class' => 'table-responsive'
          ],
          'tableOptions' => [
              'class' => [
                  'table align-items-center table-striped table-flush',
              ]
          ],
          'headerRowOptions' => [
              'class' => [
                  'thead-light'
              ]
          ],
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
          'columns' => [
              [
                  'attribute' => 'id',
                  'options' => ['style' => 'width: 60px'],
              ],
              [
                  'attribute' => 'images',
                  'value' => function($model) {
                      return Html::img(@json_decode($model->images)[0], ['class' => 'avatar avatar-xl']);
                  },
                  'options' => ['style' => 'width: 60px'],
                  'format' => 'raw',
              ],
              [
                  'attribute' => 'name',
              ],
//              [
//                  'attribute' => 'description',
//                  'format' => 'html'
//              ],
              [
                  'attribute' => 'price',
              ],
              'unity',
              //              [
              //                  'class' => \common\widgets\ActionColumn::class,
              //                  'contentOptions' => [
              //                      'class' => 'table-actions',
              //                      'style' => 'width: 30px'
              //                  ],
              //                  'template' => "{sync}",
              //              ],
          ],
      ]); ?>
  </div>
</div>
<?php \yii\widgets\Pjax::end();?>
