<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\grid\EnumColumn;
use yii\grid\GridView;
?>
<?php
echo GridView::widget([
    'layout' => "{items}\n{pager}",
    'options' => [
        'class' => ['table-responsive'],
    ],
    'tableOptions' => [
        'class' => [
            'table align-items-center table-striped table-flush',
        ]
    ],
    'headerRowOptions' => [
        'class' => [
            'thead-light'
        ]
    ],
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'phone_number',
        'ip',
        'otp_code',
        [
            'attribute' => 'device_agent',
            'contentOptions' => [
                'style' => 'word-break: break-word; white-space: normal;',
            ],
        ],
        [
            'class' => \common\grid\EnumColumn::class,
            'attribute' => 'status',
            'options' => ['style' => 'width: 10%'],
            'enum' => \common\models\LoginHistory::statuses(),
            'filter' => \common\models\LoginHistory::statuses(),
        ],
        [
            'attribute' => 'created_at',
            'options' => ['style' => 'width: 10%'],
            'value' => function($model){
                return str_replace('{%1}', '<br/>', date('H:i:s{%1}d-m-Y', $model->created_at));
            },
            'contentOptions' => [
                'style' => 'word-break: break-word; white-space: normal;',
            ],
            'format' => 'html'
        ],
    ],
]);
?>