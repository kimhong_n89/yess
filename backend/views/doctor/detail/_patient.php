<?php
use yii\grid\GridView;
use common\components\utils\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\grid\EnumColumn;
use kartik\date\DatePicker;
use common\models\Patient;
use common\models\District;
use common\models\Province;
use yii\helpers\ArrayHelper;
use common\components\utils\PermissionConstant;
use kartik\widgets\Select2;

$arrDistrict = ['' => Yii::t('backend', 'Tất cả')];
if ($searchModel->province_id) {
    $arrDistrict = $arrDistrict + ArrayHelper::map(District::find()->where(['id_state' => $searchModel->province_id])->asArray()->all(), 'id_district', 'name');
}

echo GridView::widget([
    'layout' => "{items}\n{pager}",
    'options' => [
        'class' => 'table-responsive'
    ],
    'tableOptions' => [
        'class' => [
            'table align-items-center table-striped table-flush',
        ]
    ],
    'headerRowOptions' => [
        'class' => [
            'thead-light'
        ]
    ],
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'attribute' => 'id',
            'options' => ['style' => 'width: 40px'],
        ],
        [
            'attribute' => 'fullname',
            'contentOptions' => [
                'style' => 'word-break: break-word; white-space: normal;',
            ],
        ],
        [
            'attribute' => 'phone',
        ],
        [
            'attribute' => 'email',
            'contentOptions' => [
                'style' => 'word-break: break-word; white-space: normal;',
            ],
        ],
        [
            'attribute' => 'year_of_birth',
            'contentOptions' => ['style' => 'width: 50px'],
            'headerOptions' => ['style' => 'width: 50px'],
        ],
        [
            'class' => \common\grid\EnumColumn::class,
            'attribute' => 'gender',
            'enum' => Patient::genders(),
            'filter' => Select2::widget([
                'model' => $searchModel,
                'attribute' => 'gender',
                'data' => ['' => Yii::t('backend', 'Tất cả')] + Patient::genders(),
            ]),
            'contentOptions' => [
                'style' => 'word-break: break-word; white-space: normal;',
            ],
        ],
        [
            'attribute' => 'province_id',
            'label' => Yii::t('backend', 'Khu vực'),
            'contentOptions' => [
                'width' => '10%',
                'style' => 'word-break: break-word; white-space: normal;',
            ],
            'value' => function($model){
                return @$model->province->name;
            },
            'filter' => Select2::widget([
                'model' => $searchModel,
                'attribute' => 'province_id',
                'data' => ['' => Yii::t('backend', 'Tất cả')] + ArrayHelper::map(Province::find()->asArray()->all(), 'id_state', 'name'),
            ]),
        ],
        [
            'label' => Yii::t('backend', 'Quận'),
            'contentOptions' => [
                'width' => '10%',
                'style' => 'word-break: break-word; white-space: normal;',
            ],
            'attribute' => 'district_id',
            'value' => function($model){
                return @$model->district->name;
            },
            'filter' => Select2::widget([
                'model' => $searchModel,
                'attribute' => 'district_id',
                'data' => $arrDistrict,
            ]),
        ],
        [
            'attribute' => 'count_prescription',
            'contentOptions' => ['style' => 'width: 80px'],
        ],
        [
            'class' => \common\grid\EnumColumn::class,
            'attribute' => 'status',
            'enum' => Patient::statuses(),
            'filter' => Select2::widget([
                'model' => $searchModel,
                'attribute' => 'status',
                'data' => ['' => Yii::t('backend', 'Tất cả')] + Patient::statuses(),
            ]),
            'contentOptions' => [
                'style' => 'word-break: break-word; white-space: normal;',
            ],
        ],
        [
            'class' => \common\widgets\ActionColumn::class,
            'contentOptions' => [
                'class' => 'table-actions',
                'style' => 'width: 32px'
            ],
            'visible' => rbac_is_allow_exec(PermissionConstant::VIEW_PATIENT) || rbac_is_allow_exec(PermissionConstant::UPDATE_PATIENT) || rbac_is_allow_exec(PermissionConstant::DELETE_PATIENT),
            'urlCreator' => function( $action, $model, $key, $index ){
                if ($action == "view") {
                    return Url::to(['/patient/view', 'id' => $key]);
                } else if ($action == "update") {
                    return Url::to(['/patient/update', 'id' => $key]);
                } else {
                    return Url::to(['/patient/delete', 'id' => $key, 'redirect' => Yii::$app->request->url]);
                }
            },
            'template' => '<div class="dropdown">
                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                      {view}{update}{delete}
                    </div>
                  </div>',
            'buttons' => [
                'view' => function ($url, $model) {
                    $attributes = [
                        'class' => 'dropdown-item',
                        'data-toggle' => 'tooltip',
                        'title' => Yii::t('checkin', 'Xem')
                    ];
                    return Html::a(Yii::t('checkin', 'Xem'), $url, $attributes, [PermissionConstant::VIEW_PATIENT]);
                },
                'update' => function ($url, $model) {
                    $attributes = [
                        'class' => 'dropdown-item',
                        'data-toggle' => 'tooltip',
                        'title' => Yii::t('checkin', 'Cập nhật')
                    ];
                    return Html::a(Yii::t('checkin', 'Cập nhật'), $url, $attributes, [PermissionConstant::UPDATE_PATIENT]);
                },
                'delete' => function ($url, $model) {
                    $attributes = [
                        'class' => 'dropdown-item',
                        'data-toggle' => 'tooltip',
                        'title' => Yii::t('checkin', 'Xóa'),
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post',
                    ];
                    return Html::a(Yii::t('checkin', 'Xóa'), $url, $attributes, [PermissionConstant::DELETE_PATIENT]);
                }
            ]
        ],
    ],
]);
?>
