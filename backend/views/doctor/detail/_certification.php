<?php 

use yii\web\JsExpression;
use common\models\DevicePushDetail;
use common\components\utils\Html;
use common\components\utils\PermissionConstant;

?>
<div class="card-body">
  <div class="row">
    <?php foreach ($doctorModel->attachments as $certificate): ?>
        <?php //dd1($certificate) ?>
      <div class="col-sm-4">
        <div class="card">
          <img style="height: 170px" class="card-img-top" src="<?= $certificate['base_url'] . '/' .$certificate['path']?>" alt="Card image cap">
          <div class="card-body text-center">
              <?php
                  echo Html::a('<i class="fa-fw fas fa-download"></i>', ['/manage-file/download', 'path' => $certificate['path']], [
                      'class' => 'btn btn-success btn-sm',
                      'data-toggle' => 'tooltip',
                      'data-pjax' => 0,
                      'title' => Yii::t('checkin', 'Download'),
                  ], [
                      PermissionConstant::MANAGE_FILE
                  ]);
                  echo Html::a('<i class="fa-fw fas fa-trash"></i>', 'javascript:void(0)', [
                      'class' => 'btn btn-danger btn-sm',
                      'data-href' => \yii\helpers\Url::to([
                          '/manage-file/delete',
                          'path' => $certificate['path']
                      ]) ,
                      'onclick' => 'deleteAjax(this)',
                      'data-pjax-container' => '#doctor-view-pjax',
                      'data-confirm-message' => 'Bạn có chắc là sẽ xóa không',
                      'data-toggle' => 'tooltip',
                      'title' => Yii::t('checkin', 'Xoá'),
                  ], [
                      PermissionConstant::MANAGE_FILE
                  ]);
              ?>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
</div>
