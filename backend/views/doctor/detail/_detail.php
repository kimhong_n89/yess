<?php
use common\models\Doctor;
use common\components\utils\Html;
use common\components\utils\PermissionConstant;

/**
 * @var yii\web\View $this
 * @var \common\models\Doctor $model
 */
?>
<div class="card">
    <div class="card-body">
        <img src="<?= $model->getAvatarUrl()?>" class="rounded-circle img-center img-fluid shadow shadow-lg--hover" style="width: 140px;">
        <div class="pt-4 text-center">
          <h5 class="h3 title">
            <span class="d-block mb-1"><?= Html::encode($model->fullname);?></span>
            <span class="badge badge-pill badge-lg <?= $model->status == Doctor::STATUS_APPROVED ? 'badge-success': 'badge-warning';?>"><?= @$model::statuses()[$model->status];?></span>
          </h5>
          <div class="mt-3">
            <?= Html::a('<i class="fa-fw fas fa-edit"></i>', ['/doctor/update', 'id' => $model->id], [
                'class' => 'btn btn-primary btn-sm',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('checkin', 'Cập nhật'),
            ], [PermissionConstant::UPDATE_DOCTOR]);?>
            <?php
                if($model->status != Doctor::STATUS_APPROVED) {
                    echo Html::a('<i class="fa-fw fas fa-check"></i>', ['/doctor/approve', 'id' => $model->id], [
                        'class' => 'btn btn-success btn-sm',
                        'data-toggle' => 'tooltip',
                        'data-method' => 'post',
                        'title' => Yii::t('checkin', 'Xét duyệt'),
                    ], [PermissionConstant::UPDATE_DOCTOR]);
                }
              ?>
            <?php
                if($model->status != Doctor::STATUS_DELETED) {
                    echo Html::a('<i class="fa-fw fas fa-trash"></i>', ['/doctor/delete', 'id' => $model->id], [
                        'class'        => 'btn btn-danger btn-sm',
                        'data-toggle'  => 'tooltip',
                        'data-confirm' => 'Bạn có chắc là sẽ xóa không',
                        'data-method'  => 'post',
                        'title'        => Yii::t('checkin', 'Xoá'),
                    ], [PermissionConstant::DELETE_DOCTOR]);
                }
            ?>
          </div>
        </div>
    </div>
    <div class="card-body group-block">
        <div class="item-detail">
            <div class="item-label"><strong><?= Yii::t('checkin', 'Số điện thoại');?></strong></div>
            <div class="item-content"><?= Html::encode($model->phone);?></div>
        </div>
        <div class="item-detail">
            <div class="item-label"><strong><?= Yii::t('checkin', 'Email');?></strong></div>
            <div class="item-content"><?= Html::encode($model->email);?></div>
        </div>
        <div class="item-detail">
          <div class="item-label"><strong><?= Yii::t('checkin', 'Giới tính');?></strong></div>
          <div class="item-content"><?= Html::encode(@$model::genders()[$model->gender]);?></div>
        </div>
        <div class="item-detail">
          <div class="item-label"><strong><?= Yii::t('checkin', 'Ngày sinh');?></strong></div>
          <div class="item-content"><?= Html::encode($model->getFullBirthday());?></div>
        </div>
        <div class="item-detail">
            <div class="item-label"><strong><?= Yii::t('checkin', 'Địa chỉ');?></strong></div>
            <div class="item-content"><i class="fas fa-map-marker-alt"></i> <?= Html::encode($model->getFullAddress());?></div>
        </div>
        <div class="item-detail">
            <div class="item-label"><strong><?= Yii::t('checkin', 'Ngày tạo');?></strong></div>
            <div class="item-content"><?= Yii::$app->formatter->asDate($model->created_at, 'long');?></div>
        </div>
    </div>
</div>