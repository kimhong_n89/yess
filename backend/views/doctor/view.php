<?php
use backend\assets\MyCrudAsset;
use yii\widgets\Pjax;
use common\components\utils\Html;
use backend\controllers\DoctorController;
use yii\helpers\Url;
use common\components\utils\PermissionConstant;

MyCrudAsset::register($this);
/**
 * @var yii\web\View $this
 * @var common\models\Doctor $model
 */

$this->title = $model->fullname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('checkin', 'Bác sĩ'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$classPrescription = $classPatient = $classCertification = $classLogin = 'nav-link';

switch ($tab) {
    case DoctorController::TAB_PATIENT:
        $classPatient .= ' active';
        $view = 'detail/_patient';
        break;
    case DoctorController::TAB_CERTIFICATION:
        $classCertification .= ' active';
        $view = 'detail/_certification';
        break;
    case DoctorController::TAB_LOGIN:
        $classLogin .= ' active';
        $view = 'detail/_login_history';
        break;
    case DoctorController::TAB_PRESCRIPTION:
    default:
        $classPrescription .= ' active';
        $view = 'detail/_prescription';
}
?>
<div class="doctor-view">
  <div class="row">
    <div class="col-md-3 order-sm-1">
        <?= $this->render('detail/_detail', ['model' => $model]); ?>
    </div>
    <div class="col-md-9 order-sm-0">
      <?php Pjax::begin(['id' => 'doctor-view-pjax']); ?>
      <div class="card">
        <div class="card-header">
          <div class="row align-items-center">
            <div class="col">
              <ul class="nav nav-pills nav-pills-warning" role="tablist">
                <?php if (userCan(PermissionConstant::VIEW_PRESCRIPTION)) :?>
                <li class="nav-item">
                    <?= Html::a(Yii::t('checkin', 'Toa thuốc'), ['doctor/view', 'id' => $model->id, 'tab' => DoctorController::TAB_PRESCRIPTION], ['class' => $classPrescription])?>
                </li>
                <?php endif;?>
                  <?php if (userCan(PermissionConstant::VIEW_PATIENT)) :?>
                <li class="nav-item">
                    <?= Html::a(Yii::t('checkin', 'Khách hàng'), ['doctor/view', 'id' => $model->id, 'tab' => DoctorController::TAB_PATIENT], ['class' => $classPatient])?>
                </li>
                  <?php endif;?>
                <li class="nav-item">
                    <?= Html::a(Yii::t('checkin', 'Giấy chứng nhận'), ['doctor/view', 'id' => $model->id, 'tab' => DoctorController::TAB_CERTIFICATION], ['class' => $classCertification])?>
                </li>
                  <?php if (userCan(PermissionConstant::VIEW_LOGIN_HISTORY)) :?>
                <li class="nav-item">
                    <?= Html::a(Yii::t('checkin', 'Lịch sử đăng nhập'), ['doctor/view', 'id' => $model->id, 'tab' => DoctorController::TAB_LOGIN], ['class' => $classLogin])?>
                </li>
                  <?php endif;?>
              </ul>
            </div>
            <div class="col-auto ml-auto text-right">
                <?php
                if($tab == DoctorController::TAB_PRESCRIPTION) {
                    echo Html::a('<i class="fas fa-plus"></i>', Url::to(['/prescription/create', 'doctor_id' => $model->id, 'data-pjax' => '0']), [
                        'class' => 'btn btn-primary btn-sm',
                        'data-toggle' => 'tooltip',
                        'title' => Yii::t('checkin', 'Tạo'),
                    ], [PermissionConstant::CREATE_PRESCRIPTION]);
                }
                if($tab == DoctorController::TAB_PRESCRIPTION || $tab == DoctorController::TAB_PATIENT ) {
//                    echo Html::a('<i class="fas fa-file-download"></i>', \Yii::$app->request->url . '&act=' . DoctorController::ACTION_EXPORT, [
//                        'class' => 'btn btn-primary btn-sm',
//                        'data-toggle' => 'tooltip',
//                        'title' => Yii::t('checkin', 'Export'),
//                        'data-pjax' => 0
//                    ]);
                }
                ?>
            </div>
          </div>
            <?php
            if($tab == DoctorController::TAB_PRESCRIPTION || $tab == DoctorController::TAB_PATIENT) {
                echo $this->render('detail/_search', ['model' => $searchModel, 'doctorId' => $model->id]);
            }
            ?>
        </div>
        <div class="tab-content tab-space">
          <!-- attendee -->
          <div class="tab-pane active show">
              <?= $this->render($view, ['doctorModel' => $model, 'dataProvider' => $dataProvider, 'searchModel' => $searchModel]);?>
          </div>
        </div>
      </div>
        <?php Pjax::end()?>
    </div>
  </div>
</div>
