<?php

/**
 * @var yii\web\View $this
 * @var common\models\Doctor $model
 */

$this->title = Yii::t('backend', 'Cập nhật Bác sĩ') . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Bác sĩ'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Cập nhật');
?>
<div class="doctor-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
