<?php

use common\components\utils\Html;
use \yii\helpers\Url;
use yii\grid\GridView;
use common\models\Doctor;
use backend\controllers\DoctorController;
use common\models\District;
use common\models\Province;
use yii\helpers\ArrayHelper;
use common\components\utils\PermissionConstant;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
/**
 * @var yii\web\View $this
 * @var backend\models\search\DoctorSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('backend', 'Bác sĩ');
$this->params['breadcrumbs'][] = $this->title;

$arrDistrict = ['' => Yii::t('backend', 'Tất cả')];
if ($searchModel->province_id) {
    $arrDistrict = $arrDistrict + ArrayHelper::map(District::find()->where(['id_state' => $searchModel->province_id])->orderBy('name')->asArray()->all(), 'id_district', 'name');
}
$classAll = $classPeding = $classApproved = 'nav-link nav-link mb-sm-3 mb-md-0';

switch ($tab) {
    case DoctorController::TAB_PENDING:
        $classPeding .= ' active';
        break;
    case DoctorController::TAB_APPROVED:
        $classApproved .= ' active';
        break;
    default:
        $classAll .= ' active';
}

?>
<?php \yii\widgets\Pjax::begin(['id' => 'doctor-pjax']);?>
<div class="doctor-index">
  <div class="card">
    <div class="card-header">
      <div class="row">
        <div class="col">
          <ul class="nav nav-pills" id="tabs-icons-text" role="tablist">
            <li class="nav-item">
                <?= Html::a(Yii::t('checkin', 'Tất cả'), ['doctor/index'], ['class' => $classAll])?>
            </li>
            <li class="nav-item">
                <?= Html::a(Yii::t('checkin', 'Chờ Duyệt'), ['doctor/index', 'tab' => DoctorController::TAB_PENDING], ['class' => $classPeding])?>
            </li>
            <li class="nav-item">
                <?= Html::a(Yii::t('checkin', 'Đã Duyệt'), ['doctor/index', 'tab' => DoctorController::TAB_APPROVED], ['class' => $classApproved])?>
            </li>
          </ul>
        </div>
        <div class="col-auto ml-auto">
            <?= Html::a('<i class="fas fa-plus"></i>', ['create'], ['class' => 'btn btn-sm mx-2 btn-primary', 'data-toggle' => 'tooltip', 'title' => Yii::t('checkin', 'Add')], [PermissionConstant::CREATE_DOCTOR])?>
            <?php
            echo Html::a('<i class="fas fa-file-download"></i>',  addParamToUrl(\Yii::$app->request->url, 'act=' . DoctorController::ACTION_EXPORT), [
                'class' => 'btn btn-primary btn-sm',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('checkin', 'Export'),
                'data-pjax' => 0
            ], [PermissionConstant::EXPORT_DOCTOR]);
            ?>
        </div>
      </div>
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>


      <?php echo GridView::widget([
          'layout' => "{items}\n{pager}",
          'options' => [
              'class' => 'table-responsive'
          ],
          'tableOptions' => [
              'class' => [
                  'table align-items-center table-striped table-flush',
              ]
          ],
          'headerRowOptions' => [
              'class' => [
                  'thead-light'
              ]
          ],
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
          'columns' => [
              [
                  'attribute' => 'id',
                  'contentOptions' => [
                      'style' => 'width: 40px'
                  ],
              ],
              [
                  'attribute' => 'avatar',
                  'contentOptions' => [
                      'style' => 'width: 80px'
                  ],
                  'value' => function($model) {
                      return Html::img($model->getAvatarUrl(), ['class' => 'avatar avatar-xl']);
                  },
                  'format' => 'raw',
              ],
              [
                  'attribute' => 'phone',
              ],
              [
                  'attribute' => 'fullname',
                  'contentOptions' => [
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
              ],
              [
                  'contentOptions' => [
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
                  'attribute' => 'email',
              ],
              [
                  'class' => \common\grid\EnumColumn::class,
                  'attribute' => 'gender',
                  'enum' => Doctor::genders(),
                  'filter' => Select2::widget([
                      'model' => $searchModel,
                      'attribute' => 'gender',
                      'data' => ['' => Yii::t('backend', 'Tất cả')] + Doctor::genders(),
                  ]),
                  'contentOptions' => [
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
              ],
              [
                  'attribute' => 'province_id',
                  'label' => Yii::t('backend', 'Khu vực'),
                  'contentOptions' => [
                      'width' => '10%',
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
                  'value' => function($model){
                      return @$model->province->name;
                  },
                  'filter' => Select2::widget([
                      'model' => $searchModel,
                      'attribute' => 'province_id',
                      'data' => ['' => Yii::t('backend', 'Tất cả')] + ArrayHelper::map(Province::find()->orderBy('name')->asArray()->all(), 'id_state', 'name'),
                  ]),
              ],
              [
                  'label' => Yii::t('backend', 'Quận'),
                  'contentOptions' => [
                      'width' => '10%',
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
                  'attribute' => 'district_id',
                  'value' => function($model){
                      return @$model->district->name;
                  },
                  'filter' => Select2::widget([
                      'model' => $searchModel,
                      'attribute' => 'district_id',
                      'data' => $arrDistrict,
                  ]),
              ],
              [
                  'attribute' => 'created_at',
                  'format' => 'datetime',
                  'filter' => DatePicker::widget([
                      'model' => $searchModel,
                      'attribute' => 'created_at',
                      'type' => DatePicker::TYPE_COMPONENT_APPEND,
                      'pluginOptions' => [
                          'format' => 'dd-mm-yyyy',
                          'showMeridian' => true,
                          'endDate' => '0d',
                          'todayHighlight' => true,
                          'autoclose'=>true,
                      ]
                  ]),
                  'value' => function($model){
                      return str_replace('{%1}', '<br/>', date('H:i:s{%1}d-m-Y', $model->created_at));
                  },
                  'contentOptions' => [
                      'width' => '10%',
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
                  'format' => 'html'
              ],
              [
                  'class' => \common\grid\EnumColumn::class,
                  'attribute' => 'status',
                  'enum' => Doctor::statuses(),
                  'filter' => Select2::widget([
                      'model' => $searchModel,
                      'attribute' => 'status',
                      'data' => ['' => Yii::t('backend', 'Tất cả')] + Doctor::statuses(),
                  ]),
                  'contentOptions' => [
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
              ],
              [
                  'class' => \common\widgets\ActionColumn::class,
                  'contentOptions' => [
                      'class' => 'table-actions',
                      'style' => 'width: 32px'
                  ],
                  'template' => '<div class="dropdown">
                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                      {view}{update}{delete}
                    </div>
                  </div>',
                  'buttons' => [
                      'view' => function ($url, $model) {
                          $attributes = [
                              'class' => 'dropdown-item',
                              'data-toggle' => 'tooltip',
                              'title' => Yii::t('checkin', 'Xem'),
                              'data-pjax' => 0
                          ];
                          return Html::a(Yii::t('checkin', 'Xem'), $url, $attributes, [PermissionConstant::VIEW_DOCTOR]);
                      },
                      'update' => function ($url, $model) {
                          $attributes = [
                              'class' => 'dropdown-item',
                              'data-toggle' => 'tooltip',
                              'title' => Yii::t('checkin', 'Cập nhật'),
                              'data-pjax' => 0
                          ];
                          return Html::a(Yii::t('checkin', 'Cập nhật'), $url, $attributes, [PermissionConstant::UPDATE_DOCTOR]);
                      },
                      'delete' => function ($url, $model) {
                          $attributes = [
                              'class' => 'dropdown-item',
                              'data-toggle' => 'tooltip',
                              'title' => Yii::t('checkin', 'Xóa'),
                              'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                              'data-method' => 'post',
                              'data-pjax' => 0
                          ];
                          return Html::a(Yii::t('checkin', 'Xóa'), $url, $attributes, [PermissionConstant::DELETE_DOCTOR]);
                      }
                  ]
              ],
          ],
      ]); ?>
  </div>
</div>
<?php \yii\widgets\Pjax::end();?>
