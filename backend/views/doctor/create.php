<?php

/**
 * @var yii\web\View $this
 * @var common\models\Doctor $model
 */

$this->title = Yii::t('backend', 'Tạo Bác sĩ');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Bác sĩ'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="doctor-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
