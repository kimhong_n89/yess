<?php
use yii\helpers\ArrayHelper;
use \yii\helpers\Html;
$prescriptionDetails = ArrayHelper::index($model->prescriptionDetails, null, 'type');
$drugged_time = ArrayHelper::map(\common\models\DruggedTime::find()->all(), 'id', 'name');
$dosage_form = ArrayHelper::map(\common\models\DosageForm::find()->all(), 'id', 'name');
$rate = @\common\models\Prescription::insuranceRates()[$model->insurance_rate];
$doctorSettings = ArrayHelper::map(\common\models\DoctorSetting::find()->where(['doctor_id' => $model->doctor->id])->asArray()->all(), 'key', 'value');
$showHeader = !empty($doctorSettings['logo']) + !empty($doctorSettings['medical_name']) + !empty($doctorSettings['clinic_name']) +
    !empty($doctorSettings['clinic_phone']) + !empty($doctorSettings['barcode']) + !empty($doctorSettings['medical_code']) + !empty($doctorSettings['no_of_record']);
?>
<div class="prescription-view">
  <?php if($showHeader): ?>
    <div class="form-header">
      <div class="row">
        <div class="col-7 col-sm-6 view-left-header">
          <div class="row align-items-center">
              <?php if(!empty($doctorSettings['logo'])): ?>
                <div class="col col-logo"><?= Html::img(\Yii::$app->params['baseUrl'] .'/'. $doctorSettings['logo'])?></div>
              <?php endif;?>
            <div class="col">
                <?php if(!empty($doctorSettings['medical_name'])): ?>
                  <div><label class="label-text"><?= Html::encode($doctorSettings['medical_name']);?></label></div>
                <?php endif;?>

                <?php if(!empty($doctorSettings['clinic_name'])): ?>
                  <div><label class="label-text"><?= Html::encode(@$doctorSettings['clinic_name']);?></label></div>
                <?php endif;?>

                <?php if(!empty($doctorSettings['clinic_phone'])): ?>
                  <div><label class="label-text">Liên hệ:</label><?= Html::encode(@$doctorSettings['clinic_phone']);?></div>
                <?php endif;?>
            </div>
          </div>
        </div>
        <div class="col-5 col-sm-6 text-center view-right-header">
          <div class="col-barcode">
              <?php if(!empty($doctorSettings['barcode'])): ?>
                  <?= Html::img(\Yii::$app->params['baseUrl'] .'/'. $doctorSettings['barcode'], ['class' => 'img-barcode'])?>
              <?php endif;?>

              <?php if(!empty($doctorSettings['medical_code'])): ?>
                <div><label class="label-text">Mã Y Tế</label><?= Html::encode(@$doctorSettings['medical_code']);?></div>
              <?php endif;?>

              <?php if(!empty($doctorSettings['no_of_record'])): ?>
                <div><label class="label-text">Số hồ sơ</label><?= Html::encode(@$doctorSettings['no_of_record']);?></div>
              <?php endif;?>
          </div>
        </div>
      </div>
    </div>
  <?php endif;?>
  <div class="row block-info">
    <div class="col-6 mb-10 text-left">
      <span class="label-text">Mã đơn thuốc</span>DT<?=$model->id?>-<?=date('dmY', $model->created_at);?>
    </div>
    <div class="col-6 mb-10 text-right">
      <span class="label-text">Ngày tạo</span><?=date('d/m/Y', $model->created_at);?><span class="ml-3"><?= str_replace('{%1}', 'h', date('H{%1}i', $model->created_at));?></span>
    </div>
    <div class="col-12">
      <h1 class="text-center font-weight-bold text-uppercase">Toa thuốc</h1>
    </div>
    <div class="<?= isset($print) ? 'col-6' : 'col-sm-6';?> mb-10"><label class="min-width label-text">Họ và tên</label><?=Html::encode($model->patient->fullname)?></div>
    <div class="<?= isset($print) ? 'col-3' : 'col-sm-3 col-6';?> mb-10"><label class="label-text">Độ tuổi</label><?=Html::encode($model->patient->getYearOld())?></div>
    <div class="<?= isset($print) ? 'col-3' : 'col-sm-3 col-6';?> mb-10"><label class="label-text">Giới tính</label><?=@\common\models\Patient::genders()[$model->patient->gender]?></div>

    <div class="<?= isset($print) ? 'col-6' : 'col-sm-6 col-12';?> mb-10"><label class="min-width label-text">Số điện thoại</label><?=Html::encode($model->patient->phone)?></div>
    <div class="<?= isset($print) ? 'col-6' : 'col-sm-6 col-12';?> mb-10"><label class="label-text">Email</label><?=Html::encode($model->patient->email)?></div>

    <div class="col-12 mb-10"><label class="min-width label-text">Địa chỉ</label><?=Html::encode($model->patient->getFullAddress())?></div>

    <div class="col-sm-4 col-6 mb-10"><label class="min-width label-text">Số BHYT</label><?=Html::encode($model->insurance_number)?></div>
    <div class="col-sm-4 col-6 mb-10"><label class="label-text"><?=$model->is_use_insurance ? "Có BHYT ({$rate})" : 'Không BHYT';?></label></div>
    <div class="col-12 mb-10"><label class="label-text">Chẩn đoán</label><?= implode(' / ', \yii\helpers\ArrayHelper::getColumn($model->diagnoses, 'name'));?></div>
  </div>
  <div class="block-medicine">
      <?php foreach ($prescriptionDetails as $type => $prescription): ?>
        <h4 class="heading-prescription"><?= \common\models\PrescriptionDetail::types()[$type]?></h4>
        <table class="table table-borderless">
            <?php foreach ($prescription as $index => $medicine): ?>
              <tr>
                <td class="column-index text-center"><strong><?=($index+1);?></strong></td>
                <td>
                  <div class=""><strong><?=$medicine->medicine->name?></strong></div>
                </td>
                <td class="column-amount text-center"><strong><?=$medicine->amount?></strong></td>
                <td class="column-dosage-form text-center"><strong><?=@$dosage_form[$medicine->dosage_form_id]?></strong></td>
              </tr>
              <tr>
                <td class="column-index text-center"></td>
                <td colspan="3">
                  <div class="mb-2"><span class="text-sub">Liều dùng</span> <span class="text-mute text-small">(<?= Html::encode(@$dosage_form[$medicine->dosage_form_id]);?>)</span></div>
                  <div>
                    <span class="mr-4"><label class="label-text">Sáng</label><?=(int)$medicine->amount_morning?> <?=Html::encode(@$dosage_form[$medicine->dosage_form_id])?></span>
                    <span class="mr-4"><label class="label-text">Trưa</label><?=(int)$medicine->amount_noon?> <?=Html::encode(@$dosage_form[$medicine->dosage_form_id])?></span>
                    <span class="mr-4"><label class="label-text">Chiều</label><?=(int)$medicine->amount_afternoon?> <?=Html::encode(@$dosage_form[$medicine->dosage_form_id])?></span>
                    <span class="<?= isset($print) ? '' : 'view-time-drug';?>"><label class="label-text"><?=Html::encode(@\common\models\PrescriptionDetail::timeUses()[$medicine->time_of_use])?></label><?= Html::encode(@$drugged_time[$medicine->drugged_time])?></span>
                  </div>
                </td>
              </tr>
            <?php endforeach;?>
        </table>
        <h6 class="total-line">Cộng khoản: <span class="total-line"><?= count($prescription);?></span></h6>
      <?php endforeach;?>
  </div>
  <div class="block-note">
    <h4 class="heading-prescription">Lời dặn của bác sĩ</h4>
    <div>
      <i class="text-sub"><?=\yii\helpers\Html::encode($model->note);?></i>
    </div>
      <?php
      if ($model->re_examination) {
          echo "<p class='mt-2'><i class=\"text-sub\">hẹn tái khám vào ngày &nbsp;{$model->re_examination_date}</i></p>";
      }
      ?>

  </div>
  <div class="row align-items-end block-bottom">
    <div class="col-sm-6 col-5">
      <div><strong>Người nhận thuốc</strong> <span class="text-mute text-small">(Ký tên)</span></div>
    </div>
    <div class="col-sm-6 col-7 text-center">
      <div class="float-right">
        <div class="font-italic"><?= sprintf("Ngày &nbsp; %s &nbsp; tháng  &nbsp; %s &nbsp; năm &nbsp; %s", date('d', $model->created_at), date('m', $model->created_at), date('Y', $model->created_at));?></div>
        <div class="font-weight-bold text-uppercase mt-4">Bác sĩ khám bệnh</div>
        <div class="sign-space"></div>
        <div class="mb-4"><strong>Bác sĩ <span class="text-uppercase"><?= Html::encode($model->doctor->fullname);?></span></strong></div>
        <div><strong>Tổng tiền thuốc </strong><span id="total-amount"><?= Yii::$app->formatter->asInteger($model->total_amount);?></span> VNĐ</div>
      </div>
    </div>
  </div>
</div>