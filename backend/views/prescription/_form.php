<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use common\models\District;
use common\models\Province;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use common\models\Ward;
use common\models\Prescription;
use common\models\PrescriptionDetail;
use common\models\Doctor;
use common\widgets\dynamicform\src\DynamicFormWidget2;
use common\models\DiseaseGroup;
use kartik\widgets\DatePicker;

/**
 * @var yii\web\View $this
 * @var common\models\Patient $modelPatient
 * @var common\models\Prescription $model
 * @var yii\bootstrap4\ActiveForm $form
 */
$provinces = ArrayHelper::map(Province::find()->orderBy('name')->all(), 'id_state', 'name');

$listDistricts = $listWards = [];
if ($modelPatient->province_id) {
    $districts = District::find()->where(['id_state' => $modelPatient->province_id])->orderBy('name')->all();
    $listDistricts = ArrayHelper::map($districts, 'id_district', 'name');
}
if ($modelPatient->district_id) {
    $wards = Ward::find()->where(['id_district' => $modelPatient->district_id])->orderBy('name')->all();
    $listWards = ArrayHelper::map($wards, 'id_ward', 'name');
}

$doctors = ArrayHelper::map(Doctor::find()->orderBy('fullname')->all(), 'id', 'fullname');
$diagnose = ArrayHelper::map(DiseaseGroup::find()->orderBy('name')->all(), 'id', 'name')
?>
<div class="prescription-form custom-form">
    <?php $form = ActiveForm::begin([
        'id' => 'dynamic-form',
        'validateOnBlur' => false,
        'enableAjaxValidation' => false,
        'validateOnChange' => false,
    ]); ?>
  <div class="card my-4">
    <div class="card-header border-0">
      <h2 class="mb-0">Thông tin bệnh nhân</h2>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-sm-6 col-12 ">
            <?php echo $form->field($modelPatient, 'fullname', [
//                'options' => ['class' => 'form-group d-flex align-items-center'],
//                'labelOptions' => [
//                    'class' => 'mb-0',
//                ],
                'template' => '{label} <div class="col p-0">{input}<button class="cancel-autocomplete" type="button"><i class="fas fa-times"></i></button>{error}{hint}</div>'
            ])->widget(\yii\jui\AutoComplete::classname(), [
                'clientOptions' => [
                    'source' => Url::to(['ajax/find-patient']),
                    'minLength'=>'2',
                    'select' => new JsExpression("
                  function( event, ui ) {
                      if(event.type == 'autocompleteselect') {
                          $('#prescription-patient_id').val(ui.item.id);
                          $('#patient-phone').val(ui.item.phone);
                          $('#patient-email').val(ui.item.email);
                          $('#patient-gender').val(ui.item.gender).change();;
                          $('#patient-year_of_birth').val(ui.item.year);
                          $('#sel-province').val(ui.item.province).change();
                          $('#sel-province').trigger('select2:select');
                          $('#sel-district').on('depdrop:afterChange', function(event, id, value, jqXHR, textStatus) {
                                $('#sel-district').val(ui.item.district).change();
                                $('#sel-district').trigger('select2:select');  
                          });
                          $('#sel-ward').on('depdrop:afterChange', function(event, id, value, jqXHR, textStatus) {
                                $('#sel-ward').val(ui.item.ward).change();
                          });
                          $('#patient-address').val(ui.item.address);
                          $('#patient-fullname').attr('readonly', true);
                          $('#patient-year_of_birth').attr('readonly', true);
                          $('.col-gender').addClass('readonly');
                          $('#patient-phone').attr('readonly', true);
                          $('#patient-email').attr('readonly', true);
                          $('#patient-address').attr('readonly', true);
                          $('.col-province').addClass('readonly');
                          $('.col-district').addClass('readonly');
                          $('.col-ward').addClass('readonly');
                      }
                }"),
                ],
                'options'=>[
                    'class' => 'form-control',
                    'autocomplete' => 'chrome-off',
                    'readonly' => $model->patient_id ? true : false
                ]
            ]) ?>
        </div>
        <div class="col-sm-3 col-6">
            <?php echo $form->field($modelPatient, 'year_of_birth', [
//                'options' => ['class' => 'form-group d-sm-flex align-items-center'],
//                'labelOptions' => [
//                    'class' => 'mb-sm-0',
//                ],
//                'template' => '{label} <div class="col p-0">{input}{error}{hint}</div>',
            ])->textInput(['type' => 'number', 'class' => 'form-control', 'min' => '1900', 'placeholder' => '', 'readonly' => $model->patient_id ? true : false]) ?>
        </div>
        <div class="col-sm-3 col-6 <?=$model->patient_id ? 'readonly' : '';?>">
            <?php echo $form->field($modelPatient, 'gender', [
//                'options' => ['class' => 'form-group d-sm-flex align-items-center'],
//                'labelOptions' => [
//                    'class' => 'mb-sm-0',
//                ],
//                'template' => '{label} <div class="col p-0">{input}{error}{hint}</div>',
            ])->widget(Select2::classname(), [
                'data' => $modelPatient::genders(),
                'options' => [
                    'placeholder' => Yii::t('backend', 'Chọn'),
                ],
            ])?>
        </div>
      </div>
      <div class="row">
        <div class="col-6">
            <?php echo $form->field($modelPatient, 'phone', [
//                'options' => ['class' => 'form-group d-sm-flex align-items-center'],
//                'labelOptions' => [
//                    'class' => 'mb-sm-0',
//                ],
//                'template' => '{label} <div class="col p-0">{input}{error}{hint}</div>',
            ])->textInput(['maxlength' => true, 'readonly' => $model->patient_id ? true : false]) ?>
        </div>
        <div class="col-6">
            <?php echo $form->field($modelPatient, 'email', [
//                'options' => ['class' => 'form-group d-sm-flex align-items-center'],
//                'template' => '{label} <div class="col p-0">{input}{error}{hint}</div>',
//                'labelOptions' => [
//                    'class' => 'mb-sm-0',
//                ],
            ])->textInput(['maxlength' => true, 'type' => 'email', 'readonly' => $model->patient_id ? true : false]) ?>
        </div>
      </div>
      <label class="font-weight-bold">Địa chỉ</label>
      <div class="row">
        <div class="col-sm col-6">
            <?php echo $form->field($modelPatient, 'address', [
                'labelOptions' => [
                    'class' => 'small',
                ]
            ])->textInput(['maxlength' => true, 'readonly' => $model->patient_id ? true : false]) ?>
        </div>
        <div class="col-sm col-6 col-province <?=$model->patient_id ? 'readonly' : '';?>">
            <?php echo $form->field($modelPatient, 'province_id', [
                'labelOptions' => [
                    'class' => 'small',
                ]
            ])->widget(Select2::classname(), [
                'data' => $provinces,
                'options' => [
                    'id' => 'sel-province',
                    'placeholder' => Yii::t('backend', 'Chọn Khu Vực')
                ],
            ]);?>
        </div>
        <div class="col-sm col-6 col-district <?=$model->patient_id ? 'readonly' : '';?>">
            <?php echo $form->field($modelPatient, 'district_id', [
                'labelOptions' => [
                    'class' => 'small',
                ]
            ])->widget(DepDrop::classname(), [
                'type' => DepDrop::TYPE_SELECT2,
                'data' => $listDistricts,
                'options' => [
                    'id' => 'sel-district',
                    'placeholder' => Yii::t('backend', 'Chọn Quận')
                ],
                'pluginOptions' => [
                    'depends' => [
                        'sel-province'
                    ],
                    'url' => Url::to([
                        '/ajax/get-districts'
                    ])
                ]
            ]);?>
        </div>
        <div class="col-sm col-6 col-ward <?=$model->patient_id ? 'readonly' : '';?>">
            <?php echo $form->field($modelPatient, 'ward_id', [
                'labelOptions' => [
                    'class' => 'small',
                ]
            ])->widget(DepDrop::classname(), [
                'type' => DepDrop::TYPE_SELECT2,
                'data' => $listWards,
                'options' => [
                    'id' => 'sel-ward',
                    'placeholder' => Yii::t('backend', 'Chọn Phường')
                ],
                'pluginOptions' => [
                    'depends' => [
                        'sel-district'
                    ],
                    'url' => Url::to([
                        '/ajax/get-wards'
                    ])
                ]
            ]);?>
        </div>
      </div>
        <?php echo Html::activeHiddenInput($model, 'patient_id'); ?>
        <div class="row">
          <div class="col-4">
              <?php echo $form->field($model, 'insurance_number', [
//                  'options' => ['class' => 'form-group d-sm-flex align-items-center'],
//                  'template' => '{label} <div class="col p-0">{input}{error}{hint}</div>',
              ])->textInput(['maxlength' => true]) ?>
          </div>
          <div class="col-4">
              <?php echo $form->field($model, 'is_use_insurance', [
//                  'options' => ['class' => 'form-group d-sm-flex align-items-center'],
//                  'labelOptions' => [
//                      'class' => 'mb-sm-0',
//                  ],
//                  'template' => '{label} <div class="col p-0">{input}{error}{hint}</div>',
              ])->widget(Select2::classname(), [
                  'data' => Prescription::yesno(),
                  'options' => [
                      'id' => 'sel-insurance',
                      'placeholder' => Yii::t('backend', 'Chọn')
                  ],
              ]);?>
          </div>
          <div class="col-4">
              <?php echo $form->field($model, 'insurance_rate', [
//                  'options' => ['class' => 'form-group d-sm-flex align-items-center'],
//                  'labelOptions' => [
//                      'class' => 'mb-sm-0 d-sm-none',
//                  ],
//                  'template' => '{label} <div class="col p-0">{input}{error}{hint}</div>',
              ])->widget(Select2::classname(), [
                  'data' => Prescription::insuranceRates(),
                  'options' => [
                      'id' => 'sel-insurance-rate',
                      'placeholder' => Yii::t('backend', 'Chọn %'),
                      'disabled' => $model->is_use_insurance ? false : true
                  ],
              ]);?>
          </div>
        </div>
          <?php echo $form->field($model, 'arr_diagnoses', [
//              'options' => ['class' => 'form-group d-sm-flex align-items-center'],
//              'labelOptions' => [
//                  'class' => 'mb-sm-0',
//              ],
//              'template' => '{label} <div class="col p-0">{input}{error}{hint}</div>',
          ])->widget(Select2::classname(), [
              'data' => $diagnose,
              'options' => [
                  'id' => 'sel-diagnose',
                  'placeholder' => Yii::t('backend', 'Chọn'),
                  'multiple' => true
              ],
          ]);?>
    </div>
  </div>

  <div class="card my-4">
    <div class="card-header border-0">
      <h2 class="mb-0">Toa thuốc BHYT</h2>
    </div>
    <div class="card-body">
        <div id="prescription-insurance-list">
            <?php DynamicFormWidget2::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item-' . PrescriptionDetail::TYPE_MEDICINE_INSURANCE, // required: css class
                'limit' => 20, // the maximum times, an element can be cloned (default 999)
                'min' => $min_insurance, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $modelMedicineInsurance[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'medicine_id',
                    'amount',
                    'dosage_form_id',
                    'amount_morning',
                    'amount_noon',
                    'amount_afternoon',
                    'time_of_use',
                    'drugged_time'
                ],
            ]); ?>

              <div class="container-items"><!-- widgetContainer -->
              <?php
              foreach ($modelMedicineInsurance as $index => $modelDetail) {
                  echo $this->render('form/_form_detail', [
                      'model' => $model,
                      'index' => $index,
                      'modelDetail' => $modelDetail,
                      'form' => $form,
                  ]);
              }
              ?>
              </div>
              <button type="button" class="add-item btn btn-primary">Thêm thuốc</button>
              <h6 class="total-line">Cộng khoản: <span></span></h6>
            <?php DynamicFormWidget2::end(); ?>
        </div>
    </div>
  </div>
  <div class="card my-4">
    <div class="card-header border-0">
      <h2 class="mb-0">Toa thuốc dịch vụ</h2>
    </div>
    <div class="card-body">
        <div id="prescription-service-list">
            <?php DynamicFormWidget2::begin([
                'widgetContainer' => 'dynamicform_wrapper_service', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-service-items', // required: css class selector
                'widgetItem' => '.item-' . PrescriptionDetail::TYPE_MEDICINE_SERVICE, // required: css class
                'limit' => 20, // the maximum times, an element can be cloned (default 999)
                'min' => $min_service, // 0 or 1 (default 1)
                'insertButton' => '.add-service-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $modelMedicineService[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'medicine_id',
                    'amount',
                    'dosage_form_id',
                    'amount_morning',
                    'amount_noon',
                    'amount_afternoon',
                    'time_of_use',
                    'drugged_time'
                ],
            ]); ?>

          <div class="container-service-items"><!-- widgetContainer -->
              <?php
              foreach ($modelMedicineService as $index => $modelDetail) {
                  echo $this->render('form/_form_detail', [
                      'model' => $model,
                      'index' => $index,
                      'modelDetail' => $modelDetail,
                      'form' => $form,
                  ]);
              }
              ?>
          </div>
          <button type="button" class="add-service-item btn btn-primary">Thêm thuốc</button>
          <h6 class="total-line">Cộng khoản: <span></span></h6>
            <?php DynamicFormWidget2::end(); ?>
        </div>
    </div>
  </div>
  <div class="card my-4">
    <div class="card-header border-0">
      <h2 class="mb-0">Thông tin Khám</h2>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-6">
            <?php echo $form->field($model, 'note')->textarea(['maxlength' => true]) ?>
          <div class="d-flex align-items-center">
              <?php echo $form->field($model, 're_examination', ['options' => ['class' => 'form-group mr-4 re-examination']])->checkbox(); ?>
            <div class="re-examination-date" style="display: <?= $model->re_examination ? 'block' : 'none';?>">
                <?php echo $form->field($model, 're_examination_date', [
                    'template' => '{input}',
                ])->widget(\kartik\widgets\DateTimePicker::class, [
                    'type' => \kartik\widgets\DateTimePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd/mm/yyyy hh:ii',
                        'todayHighlight' => true,
                        'todayBtn' => true,
                    ],
                    'options' => [
                        'placeholder' => 'Chọn ngày tái khám'
                    ],
                    'removeButton' => false,
                ])->label(false)?>
            </div>
          </div>
        </div>
        <div class="col-md-6">
            <?php echo $form->field($model, 'doctor_id')->widget(Select2::classname(), [
                'data' => $doctors,
                'options' => [
                    'placeholder' => Yii::t('backend', 'Chọn')
                ],
            ]);?>
            <div class="text-right"><strong>Tổng tiền thuốc </strong><span id="total-amount"><?= Yii::$app->formatter->asInteger($model->total_amount);?></span> VNĐ</div>
        </div>
      </div>
    </div>
    <div class="card-footer">
        <?php echo Html::submitButton(Yii::t('frontend', 'Hoàn tất & Xem toa'), ['value' => 'done',  'name' => 'btn-submit', 'class' => 'btn btn-primary']) ?>
    </div>
  </div>

    <?php ActiveForm::end(); ?>
</div>

<?php
$yes = Prescription::YES;
$url_add_template = Url::to(['prescription/add-template']);
$script = <<< JS
    jQuery(document).ready(function() {
      $(".dynamicform_wrapper .total-line span").html($('.dynamicform_wrapper .medicine-item-line').length);
      $(".dynamicform_wrapper_service .total-line span").html($('.dynamicform_wrapper_service .medicine-item-line').length);
    })
    $('.cancel-autocomplete').on('click', function() {
      $('#patient-fullname').val('').attr('readonly', false);
      $('#patient-year_of_birth').attr('readonly', false);
      $('.col-gender').removeClass('readonly');
      $('#patient-phone').attr('readonly', false);
      $('#patient-email').attr('readonly', false);
      $('#patient-address').attr('readonly', false);
      $('.col-province').removeClass('readonly');
      $('.col-district').removeClass('readonly');
      $('.col-ward').removeClass('readonly');
      $('#prescription-patient_id').val('');
    })
    
    $('#sel-insurance').on('select2:select', function() {
        if ($(this).val() == $yes) {
            $('#sel-insurance-rate').removeAttr('disabled');
        } else {
            $('#sel-insurance-rate').attr('disabled', true);
            $('#sel-insurance-rate').val(null).trigger('change');
        }
    })
    
    $('.add-medicine').on('click', function() {
        var target = $(this).data('target');
        var type = $(this).data('type');
        $.ajax({
          method: "GET",
          url: "$url_add_template",
          data: { type: type}
        }).done(function( html ) {
            $('#'+target).append(html);
        });
    })
    
    jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
        var total = 0
        jQuery(".dynamicform_wrapper .medicine-item-line .serial-number").each(function(index) {
            jQuery(this).html((index + 1));
            total++;
        });
        jQuery(".dynamicform_wrapper .total-line span").html(total);
        
    });
    
    jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {
        var total = 0
        jQuery(".dynamicform_wrapper .medicine-item-line .serial-number").each(function(index) {
            jQuery(this).html((index + 1));
            total++;
        });
        jQuery(".dynamicform_wrapper .total-line span").html(total);
        updateTotalAmount();
    });
    
    jQuery(".dynamicform_wrapper_service").on("afterInsert", function(e, item) {
        var total = 0
        jQuery(".dynamicform_wrapper_service .medicine-item-line .serial-number").each(function(index) {
            jQuery(this).html((index + 1));
            total++;
        });
        jQuery(".dynamicform_wrapper_service .total-line span").html(total);
    });
    
    jQuery(".dynamicform_wrapper_service").on("afterDelete", function(e) {
        var total = 0
        jQuery(".dynamicform_wrapper_service .medicine-item-line .serial-number").each(function(index) {
            jQuery(this).html((index + 1));
                total++;
        });
        jQuery(".dynamicform_wrapper_service .total-line span").html(total);
        updateTotalAmount()
    });
    
    $("#prescription-re_examination").change(function() {
        if($('input[name="Prescription[re_examination]"]:checked').val() == 1){
            $(".re-examination-date").show();
        } else {
            $(".re-examination-date").hide();
        }
          
    });
JS;
$this->registerJs($script);
?>
