<?php

/**
 * @var yii\web\View $this
 * @var common\models\Prescription $model
 */

$this->title = Yii::t('backend', 'Tạo toa thuốc');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Toa thuốc'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prescription-create">

    <?php echo $this->render('_form', [
        'model' => $model,
        'modelPatient' => $modelPatient,
        'modelMedicineInsurance' => $modelMedicineInsurance,
        'modelMedicineService' => $modelMedicineService,
        'min_insurance' => $min_insurance,
        'min_service' => $min_service,
    ]) ?>

</div>
