<?php

use common\components\utils\Html;
use common\components\utils\PermissionConstant;

/**
 * @var yii\web\View $this
 * @var common\models\Prescription $model
 */

$this->title = "DT{$model->id}";
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Toa thuốc'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-body">
      <?php echo $this->render('view/_view', ['model' => $model]);?>
    </div>
    <div class="d-print-none card-footer text-center">
        <?= Html::a('Chỉnh sửa toa', ['update', 'id' => $model->id], ['class' => 'btn btn-dark-blue'], [
            PermissionConstant::UPDATE_PRESCRIPTION
        ])?>
        <?= Html::a('In toa', ['print', 'id' => $model->id], [
            'class' => 'btn btn-primary mx-1',
            'role' => 'modal-remote',
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'title' => 'In toa thuốc',
            'data-confirm-title' => 'Bạn có chắc chắn không?',
            'data-confirm-message' => 'Bạn có chắc muốn in toa thuốc này không?',
            'data-confirm-cancel' => 'Không',
            'data-confirm-ok' => 'Có',
        ], [
            PermissionConstant::PRINT_PRESCRIPTION
        ])?>
        <?= Html::a('Gửi toa', ['delivery-to-store', 'id' => $model->id], ['class' => 'btn btn-orange'])?>
    </div>
  </div>
</div>


