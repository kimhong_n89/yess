<?php

use common\components\utils\Html;
use common\components\utils\PermissionConstant;
use yii\grid\GridView;
use backend\controllers\PrescriptionController;
use yii\helpers\ArrayHelper;
use common\models\Province;
use common\models\District;
use common\models\Prescription;
use kartik\select2\Select2;
/**
 * @var yii\web\View $this
 * @var backend\models\search\PrescriptionSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('backend', 'Toa thuốc');
$this->params['breadcrumbs'][] = $this->title;

$arrDistrict = ['' => Yii::t('backend', 'Tất cả')];
if ($searchModel->patient_province) {
    $arrDistrict = $arrDistrict + ArrayHelper::map(District::find()->where(['id_state' => $searchModel->patient_province])->orderBy('name')->asArray()->all(), 'id_district', 'name');
}
$diagnose = ['' => Yii::t('backend', 'Tất cả')] + ArrayHelper::map(\common\models\DiseaseGroup::find()->orderBy('name')->all(), 'id', 'name');
$classAll = $classDone = $classDraft = $classDelivery = $classDeleted = 'nav-link nav-link mb-sm-3 mb-md-0';

switch ($tab) {
    case PrescriptionController::TAB_DONE:
        $classDone .= ' active';
        break;
    case PrescriptionController::TAB_DRAFT:
        $classDraft .= ' active';
        break;
    case PrescriptionController::TAB_DELIVERY_TO_STORE:
        $classDelivery .= ' active';
        break;
    case PrescriptionController::TAB_DELETED:
        $classDeleted .= ' active';
        break;
    default:
        $classAll .= ' active';
}

?>
<?php \yii\widgets\Pjax::begin(['id' => 'prescription-pjax']);?>
<div class="prescription-index">
  <div class="card">
    <div class="card-header">
      <div class="row">
        <div class="col">
          <ul class="nav nav-pills" id="tabs-icons-text" role="tablist">
            <li class="nav-item">
                <?= Html::a(Yii::t('checkin', 'Tất cả'), ['prescription/index'], ['class' => $classAll])?>
            </li>
            <li class="nav-item">
                <?= Html::a(Yii::t('checkin', 'Hoàn thành'), ['prescription/index', 'tab' => PrescriptionController::TAB_DONE], ['class' => $classDone])?>
            </li>
            <li class="nav-item">
                <?= Html::a(Yii::t('checkin', 'Đang tạo'), ['prescription/index', 'tab' => PrescriptionController::TAB_DRAFT], ['class' => $classDraft])?>
            </li>
            <li class="nav-item">
                <?= Html::a(Yii::t('checkin', 'Chuyển cho nhà thuốc'), ['prescription/index', 'tab' => PrescriptionController::TAB_DELIVERY_TO_STORE], ['class' => $classDelivery])?>
            </li>
            <li class="nav-item">
                <?= Html::a(Yii::t('checkin', 'Đã huỷ'), ['prescription/index', 'tab' => PrescriptionController::TAB_DELETED], ['class' => $classDeleted])?>
            </li>
          </ul>
        </div>
        <div class="col-auto ml-auto">
            <?= Html::a('<i class="fas fa-plus"></i>', ['create'], ['class' => 'btn btn-sm mx-2 btn-primary', 'data-toggle' => 'tooltip', 'title' => Yii::t('checkin', 'Add')], [PermissionConstant::CREATE_PRESCRIPTION])?>
            <?php
            echo Html::a('<i class="fas fa-file-download"></i>',  addParamToUrl(\Yii::$app->request->url, 'act=' . PrescriptionController::ACTION_EXPORT), [
                'class' => 'btn btn-primary btn-sm',
                'data-toggle' => 'tooltip',
                'title' => Yii::t('checkin', 'Export'),
                'data-pjax' => 0
            ], [PermissionConstant::EXPORT_PRESCRIPTION]);
            ?>
        </div>
      </div>
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>


      <?php echo GridView::widget([
          'layout' => "{items}\n{pager}",
          'options' => [
              'class' => 'table-responsive'
          ],
          'tableOptions' => [
              'class' => [
                  'table align-items-center table-striped table-flush',
              ]
          ],
          'headerRowOptions' => [
              'class' => [
                  'thead-light'
              ]
          ],
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
          'columns' => [
              [
                  'attribute' => 'id',
                  'contentOptions' => [
                      'style' => 'width: 40px'
                  ],
              ],
              [
                  'attribute' => 'doctor_name',
                  'contentOptions' => [
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
                  'label' => Yii::t('backend', 'Bác sĩ'),
              ],
              [
                  'attribute' => 'patient_name',
                  'contentOptions' => [
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
                  'label' => Yii::t('backend', 'Bệnh nhân'),
              ],
              [
                  'label' => Yii::t('backend', 'Khu vực'),
                  'contentOptions' => [
                      'width' => '10%',
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
                  'attribute' => 'patient_province',
                  'value' => function($model){
                      return @$model->patient->province->name;
                  },
                  'filter' => Select2::widget([
                      'model' => $searchModel,
                      'attribute' => 'patient_province',
                      'data' => ['' => Yii::t('backend', 'Tất cả')] + ArrayHelper::map(Province::find()->orderBy('name')->asArray()->all(), 'id_state', 'name'),
                  ]),
              ],
              [
                  'label' => Yii::t('backend', 'Quận'),
                  'contentOptions' => [
                      'width' => '10%',
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
                  'attribute' => 'patient_district',
                  'value' => function($model){
                      return @$model->patient->district->name;
                  },
                  'filter' => Select2::widget([
                      'model' => $searchModel,
                      'attribute' => 'patient_district',
                      'data' => $arrDistrict,
                  ]),
              ],
              [
                  'attribute' => 'is_use_insurance',
                  'class' => \common\grid\EnumColumn::class,
                  'enum' => Prescription::yesno(),
                  'filter' => Select2::widget([
                      'model' => $searchModel,
                      'attribute' => 'is_use_insurance',
                      'data' => ['' => Yii::t('backend', 'Tất cả')] + Prescription::yesno(),
                  ]),
                  'contentOptions' => [
                      'width' => '10%',
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
              ],
              [
                  'attribute' => 'arr_diagnoses',
                  'contentOptions' => [
                      'width' => '10%',
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
                  'value' => function($model) {
                      return implode(' / ', ArrayHelper::getColumn($model->diagnoses, 'name'));
                  },
                  'filter' => Select2::widget([
                      'model' => $searchModel,
                      'attribute' => 'arr_diagnoses',
                      'data' => $diagnose,
                      'options' => [
                          'id' => 'sel-diagnose'
                      ],
                  ]),
              ],
              [
                  'class' => \common\grid\EnumColumn::class,
                  'attribute' => 'status',
                  'enum' => Prescription::statuses(),
                  'filter' => Select2::widget([
                      'model' => $searchModel,
                      'attribute' => 'status',
                      'data' => ['' => Yii::t('backend', 'Tất cả')] + Prescription::statuses(),
                  ]),
                  'contentOptions' => [
                      'width' => '10%',
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
              ],
              [
                  'class' => \common\widgets\ActionColumn::class,
                  'contentOptions' => [
                      'class' => 'table-actions',
                      'style' => 'width: 32px'
                  ],
                  'template' => '<div class="dropdown">
                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                      {view}{update}{delete}
                    </div>
                  </div>',
                  'buttons' => [
                      'view' => function ($url, $model) {
                          $attributes = [
                              'class' => 'dropdown-item',
                              'data-toggle' => 'tooltip',
                              'title' => Yii::t('checkin', 'Xem'),
                              'data-pjax' => 0
                          ];
                          return Html::a(Yii::t('checkin', 'Xem'), $url, $attributes, [PermissionConstant::VIEW_PRESCRIPTION]);
                      },
                      'update' => function ($url, $model) {
                          $attributes = [
                              'class' => 'dropdown-item',
                              'data-toggle' => 'tooltip',
                              'title' => Yii::t('checkin', 'Cập nhật'),
                              'data-pjax' => 0
                          ];
                          return Html::a(Yii::t('checkin', 'Cập nhật'), $url, $attributes, [PermissionConstant::UPDATE_PRESCRIPTION]);
                      },
                      'delete' => function ($url, $model) {
                          $attributes = [
                              'class' => 'dropdown-item',
                              'data-toggle' => 'tooltip',
                              'title' => Yii::t('checkin', 'Xóa'),
                              'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                              'data-method' => 'post',
                          ];
                          return Html::a(Yii::t('checkin', 'Xóa'), $url, $attributes, [PermissionConstant::DELETE_PRESCRIPTION]);
                      }
                  ]
              ],
          ],
      ]); ?>
  </div>
</div>
<?php \yii\widgets\Pjax::end();?>
