<?php

/**
 * @var yii\web\View $this
 * @var common\models\Prescription $model
 */

$this->title = Yii::t('backend', 'Cập nhật toa thuốc') . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Toa thuốc'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Cập nhật');
?>
<div class="prescription-update">

    <?php echo $this->render('_form', [
        'model' => $model,
        'modelPatient' => $modelPatient,
        'modelMedicineInsurance' => $modelMedicineInsurance,
        'modelMedicineService' => $modelMedicineService,
        'min_insurance' => $min_insurance,
        'min_service' => $min_service,
    ]) ?>

</div>
