<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use common\models\Province;
use common\models\District;
/**
 * @var yii\web\View $this
 * @var backend\models\search\StoreSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('backend', 'Nhà thuốc');
$this->params['breadcrumbs'][] = $this->title;

$arrDistrict = ['' => Yii::t('backend', 'Tất cả')];
if ($searchModel->province_id) {
    $arrDistrict = $arrDistrict + ArrayHelper::map(District::find()->where(['id_state' => $searchModel->province_id])->orderBy('name')->asArray()->all(), 'id_district', 'name');
}
?>

<?php \yii\widgets\Pjax::begin(['id' => 'store-pjax']);?>
<div class="store-index">
  <div class="card">
    <div class="card-header">
      <div class="row align-items-center">

      </div>
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>


      <?php echo GridView::widget([
          'layout' => "{items}\n{pager}",
          'options' => [
              'class' => 'table-responsive'
          ],
          'tableOptions' => [
              'class' => [
                  'table align-items-center table-striped table-flush',
              ]
          ],
          'headerRowOptions' => [
              'class' => [
                  'thead-light'
              ]
          ],
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
          'columns' => [
              [
                  'attribute' => 'id',
                  'options' => ['style' => 'width: 60px'],
              ],
              [
                  'attribute' => 'image',
                  'value' => function($model) {
                      return Html::img($model->image, ['class' => 'avatar avatar-xl']);
                  },
                  'options' => ['style' => 'width: 60px'],
                  'format' => 'raw',
              ],
              [
                  'attribute' => 'name',
              ],
              [
                  'attribute' => 'phone',
              ],
              [
                  'attribute' => 'province_id',
                  'label' => Yii::t('backend', 'Khu vực'),
                  'contentOptions' => [
                      'width' => '10%',
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
                  'value' => function($model){
                      return @$model->province->name;
                  },
                  'filter' => Select2::widget([
                      'model' => $searchModel,
                      'attribute' => 'province_id',
                      'data' => ['' => Yii::t('backend', 'Tất cả')] + ArrayHelper::map(Province::find()->orderBy('name')->asArray()->all(), 'id_state', 'name'),
                  ]),
              ],
              [
                  'label' => Yii::t('backend', 'Quận'),
                  'contentOptions' => [
                      'width' => '10%',
                      'style' => 'word-break: break-word; white-space: normal;',
                  ],
                  'attribute' => 'district_id',
                  'value' => function($model){
                      return @$model->district->name;
                  },
                  'filter' => Select2::widget([
                      'model' => $searchModel,
                      'attribute' => 'district_id',
                      'data' => $arrDistrict,
                  ]),
              ],
              [
                  'attribute' => 'address',
                  'contentOptions' => ['style' => 'white-space: normal'],
              ],
//              [
//                  'class' => \common\widgets\ActionColumn::class,
//                  'contentOptions' => [
//                      'class' => 'table-actions',
//                      'style' => 'width: 30px'
//                  ],
//                  'template' => "{sync}",
//              ],
          ],
      ]); ?>
  </div>
</div>
<?php \yii\widgets\Pjax::end();?>
