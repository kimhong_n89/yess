<?php
use yii\helpers\Html;
use yii\grid\GridView;
use common\grid\EnumColumn;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 *
 * @var yii\web\View $this
 * @var backend\models\search\DruggedTimeSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */
?>
<div class="template-index">
	<div class="card">
		<div class="card-header card-header-rose card-header-icon">
			<div class="row align-items-center">
				<div class="col">
					<h4 class="card-title"><?= Yii::t('checkin', 'Thời điểm') ?></h4>
				</div>
				<div class="col text-right">
            <?= Html::a('<i class="fas fa-plus"></i>', ['master-data/create-drugged-time', 'pjax-container' => '#master-data-gridview-pjax'], ['class' => 'float-right btn btn-sm btn-primary','role' => 'modal-remote','data-toggle' => 'tooltip','title' => Yii::t('checkin', 'Thêm')]);?>
        </div>
			</div>
		</div>
            <?php
            echo GridView::widget([
                'layout' => "{items}\n{pager}",
                'options' => [
                    'class' => 'table-responsive'
                ],
                'tableOptions' => [
                    'class' => [
                        'table align-items-center table-striped table-flush',
                    ]
                ],
                'headerRowOptions' => [
                    'class' => [
                        'thead-light'
                    ]
                ],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'id',
                        'options' => ['style' => 'width: 5%'],
                    ],
                    'name',
                    [
                        'attribute' => 'status',
                        'class' => \common\grid\EnumColumn::class,
                        'enum' => \common\models\DruggedTime::statuses(),
                        'filter' => \kartik\widgets\Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'status',
                            'data' => ['' => Yii::t('backend', 'Tất cả')] + \common\models\DruggedTime::statuses(),
                        ]),
                        'contentOptions' => [
                            'width' => '15%',
                        ],
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                    ],
                    [
                        'class' => \common\widgets\ActionColumn::class,
                        'contentOptions' => [
                            'class' => 'td-actions',
                            'style' => 'width: 70px'
                        ],
                        'template' => "{update}{delete}",
                        'buttons' => [
                            'update' => function ($url, $model) {
                                $url = Url::to([
                                    'update-drugged-time',
                                    'id' => $model->id,
                                    'pjax-container' => '#master-data-gridview-pjax'
                                ]);
                                $attributes = [
                                    'class' => 'table-action text-warning',
                                    'role' => 'modal-remote',
                                    'data-toggle' => 'tooltip',
                                    'title' => Yii::t('checkin', 'Cập nhật')
                                ];
                                return Html::a('<i class="fa-fw fas fa-edit" aria-hidden=""></i>', $url, $attributes);
                            },
                            'delete' => function ($url, $model) {
                                $url = Url::to([
                                    'delete-drugged-time',
                                    'id' => $model->id,
                                    'pjax-container' => '#master-data-gridview-pjax'
                                ]);
                                $attributes = [
                                    'class' => 'table-action text-danger',
                                    'role' => 'modal-remote',
                                    'data-request-method' => 'post',
                                    'data-toggle' => 'tooltip',
                                    'title' => Yii::t('checkin', 'Xóa'),
                                    'data-confirm-title' => Yii::t('checkin', 'Are you sure?'),
                                    'data-confirm-message' => Yii::t('checkin', 'Are you sure you want to delete this item ')
                                ];
                                return Html::a('<i class="fa-fw fas fa-trash" aria-hidden=""></i>', $url, $attributes);
                            }
                        ]
                    ],
                ],
            ]);
            ?>
    </div>
</div>
