<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use backend\controllers\MasterDataController;
/**
 *
 * @var yii\web\View $this
 */

$this->title = Yii::t('checkin', 'Khác');
$this->params['breadcrumbs'][] = $this->title;

$classBusinessField = $classDosageForm = $classDruggedTime = 'nav-link';

if ($tab == MasterDataController::TAB_BUSINESS_FIELD) {
    $classBusinessField = 'nav-link active';
    $view = '_business_field';
} else if ($tab == MasterDataController::TAB_DOSAGE_FORM) {
    $classDosageForm = 'nav-link active';
    $view = '_dosage_form';
} else {
    $classDruggedTime = 'nav-link active';
    $view = '_drugged_time';
}
?>
<div class="master-data-index">
    <?php \yii\widgets\Pjax::begin(['id' => 'master-data-gridview-pjax']); ?>
	<div class="row">
		<div class="col-md-12 ml-auto mr-auto">
			<div class="nav-tabs-navigation">
				<div class="nav-tabs-wrapper">
					<ul
						class="nav nav-pills nav-pills-warning nav-pills-icons justify-content-center mb-4"
						data-tabs="tabs">
						<li class="nav-item">
                <?= Html::a(Yii::t('checkin', 'Chuyên khoa'), ['master-data/index', 'tab' => MasterDataController::TAB_BUSINESS_FIELD], ['class' => $classBusinessField])?>
						</li>
            <li class="nav-item">
                <?= Html::a(Yii::t('checkin', 'Quy cách'), ['master-data/index', 'tab' => MasterDataController::TAB_DOSAGE_FORM], ['class' => $classDosageForm])?>
            </li>
            <li class="nav-item">
                <?= Html::a(Yii::t('checkin', 'Thời điểm'), ['master-data/index', 'tab' => MasterDataController::TAB_DRUGGEDTIME], ['class' => $classDruggedTime])?>
            </li>
					</ul>
				</div>
			</div>
			<div class="tab-content">
        <div class="tab-pane active show">
            <?= $this->render($view, ['searchModel' => $searchModel, 'dataProvider' => $dataProvider]);?>
        </div>
			</div>
		</div>
	</div>
  <?php \yii\widgets\Pjax::end();?>
</div>
