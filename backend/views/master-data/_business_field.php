<?php
use yii\helpers\Html;
use yii\grid\GridView;
use common\grid\EnumColumn;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 *
 * @var yii\web\View $this
 * @var backend\models\search\BusinessFieldSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */
?>
<div class="template-index">
	<div class="card">
		<div class="card-header card-header-rose card-header-icon">
			<div class="row align-items-center">
				<div class="col">
					<h4 class="card-title"><?= Yii::t('checkin', 'Chuyên khoa') ?></h4>
				</div>
				<div class="col text-right">
            <?php //Html::a('<i class="fas fa-plus"></i>', ['master-data/create-field', 'pjax-container' => '#master-data-gridview-pjax'], ['class' => 'float-right btn btn-sm btn-primary','role' => 'modal-remote','data-toggle' => 'tooltip','title' => Yii::t('checkin', 'Sync')]);?>
        </div>
			</div>
		</div>
            <?php
            echo GridView::widget([
                'layout' => "{items}\n{pager}",
                'options' => [
                    'class' => 'table-responsive'
                ],
                'tableOptions' => [
                    'class' => [
                        'table align-items-center table-striped table-flush',
                    ]
                ],
                'headerRowOptions' => [
                    'class' => [
                        'thead-light'
                    ]
                ],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'id',
                        'options' => ['style' => 'width: 5%'],
                    ],
                    'name',
//                    [
//                        'class' => EnumColumn::class,
//                        'attribute' => 'status',
//                        'options' => ['style' => 'width: 10%'],
//                        'enum' => \common\models\BusinessField::statuses(),
//                        'filter' => BusinessField::statuses(),
//                    ]
                ],
            ]);
            ?>
    </div>
</div>
