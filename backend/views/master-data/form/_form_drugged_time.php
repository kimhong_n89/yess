<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\DruggedTime $model
 * @var yii\bootstrap4\ActiveForm $form
 */
?>

<div class="template-form">
    <?php $form = ActiveForm::begin(); ?>
      <?php echo $form->field($model, 'name')->textInput() ?>
      <?php echo $form->field($model, 'status')->checkbox()->label('Active') ?>
    <?php ActiveForm::end(); ?>
</div>
