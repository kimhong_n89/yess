<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\widgets\DatePicker;
/**
 * @var yii\web\View $this
 * @var backend\models\search\LoginHistorySearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('backend', 'Lịch sử đăng nhập');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-history-index">
    <div class="card">
        <div class="card-body p-0">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
            <?php echo GridView::widget([
                'layout' => "{items}\n{pager}",
                'options' => [
                    'class' => 'table-responsive'
                ],
                'tableOptions' => [
                    'class' => [
                        'table table-striped table-flush',
                    ]
                ],
                'headerRowOptions' => [
                    'class' => [
                        'thead-light'
                    ]
                ],
//                'options' => [
//                    'class' => ['gridview', 'table-responsive'],
//                ],
//                'tableOptions' => [
//                    'class' => ['table', 'text-nowrap', 'table-striped', 'table-bordered', 'mb-0'],
//                ],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'phone_number',
                        'label' => 'Số điện thoại'
                    ],
                    [
                        'attribute' => 'fullname',
                        'label' => 'Bác sĩ'
                    ],
                    'otp_code',
                    [
                        'class' => \common\grid\EnumColumn::class,
                        'attribute' => 'status',
                        'enum' => \common\models\LoginHistory::statuses(),
                        'filter' => \common\models\LoginHistory::statuses(),
                    ],
                    [
                        'attribute' => 'created_at',
                        'label' => 'Đăng nhập lúc',
                        'format' => 'datetime',
                        'filter' => DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'created_at',
                            'type' => DatePicker::TYPE_COMPONENT_APPEND,
                            'pluginOptions' => [
                                'format' => 'dd-mm-yyyy',
                                'endDate' => '0d',
                                'todayHighlight' => true
                            ]
                        ]),
                        'value' => function($model){
                            return str_replace('{%1}', '<br/>', date('H:i:s{%1}d-m-Y', $model->created_at));
                        },
                        'contentOptions' => [
                            'width' => '10%',
                            'style' => 'word-break: break-word; white-space: normal;',
                        ],
                        'format' => 'html'
                    ],
                    'ip',
                    [
                        'attribute' => 'device_agent',
                        'contentOptions' => [
                            'style' => 'word-break: break-word; white-space: normal;',
                        ],
                    ]
                ],
            ]); ?>
    
        </div>
        <div class="card-footer">
            <?php echo getDataProviderSummary($dataProvider) ?>
        </div>
    </div>

</div>
