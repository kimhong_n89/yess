<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/**
 *
 * @var yii\web\View $this
 * @var backend\models\GeneralSettingForm $model
 */

$this->title = Yii::t('checkin', 'Cài đặt');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="checkin-setting-index">
	<div class="row">
		<div class="col-md-8 ml-auto mr-auto">
			<div class="nav-tabs-navigation">
				<div class="nav-tabs-wrapper">
					<ul
						class="nav nav-pills nav-pills-warning nav-pills-icons justify-content-center mb-4"
						data-tabs="tabs">
						<li class="nav-item"><a class="nav-link" href="#general"
							data-hash="general" data-toggle="tab"><?= Yii::t('checkin', 'Cài đặt chung') ?></a>
						</li>
						<li class="nav-item"><a class="nav-link"
							href="#template-email" data-hash="template-email"
							data-toggle="tab"><?= Yii::t('checkin', 'Mẫu Email') ?></a>
						</li>
					</ul>
				</div>
			</div>
			<div class="tab-content">
				<!-- General -->
				<div class="tab-pane" id="general">
                <?php $form = ActiveForm::begin(); ?>
                    <div class="card">
                        <div class="card-header card-header-rose card-header-icon">
                			<h4 class="card-title"><i class="far fa-envelope"></i> <?= Yii::t('checkin', 'Mail server') ?></h4>
                        </div>
						<div class="card-body">
                    		<?php echo $form->field($model, 'smtp_host')->textInput(); ?>
                            <?php echo $form->field($model, 'smtp_port')->textInput(); ?>
                            <?php echo $form->field($model, 'smtp_encryption')->textInput(); ?>
                        <?php echo $form->field($model, 'smtp_username')->textInput(); ?>
                        <?php echo $form->field($model, 'smtp_password')->textInput(); ?>
                            <?php echo $form->field($model, 'server_send_email')->textInput(); ?>
                            <?php echo $form->field($model, 'server_send_name')->textInput(); ?>
                		</div>
					</div>

                    <?php echo Html::submitButton(Yii::t('checkin', 'Cập nhật'), ['class' => 'btn btn-primary']) ?>
                <?php ActiveForm::end(); ?>
            </div>
				<!-- Email -->
			<div class="tab-pane" id="template-email">
                <?= $this->render('template/_email', ['searchModel' => $searchTemplateModel, 'dataProvider' => $emailProvider]); ?>
            </div>
			</div>
		</div>
	</div>
</div>
