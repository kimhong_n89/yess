<?php
use yii\helpers\Html;
use yii\grid\GridView;
use common\grid\EnumColumn;
use common\models\Template;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 *
 * @var yii\web\View $this
 * @var backend\models\search\TemplateSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */
?>
<div class="template-index">
	<div class="card">
		<div class="card-header card-header-rose card-header-icon">
			<div class="row align-items-center">
				<div class="col">
					<h4 class="card-title"><?= Yii::t('checkin', 'Email') ?></h4>
				</div>
				<div class="col text-right">
                    <?=Html::a('<i class="fas fa-plus"></i>', ['setting/create-template','type' => Template::TYPE_EMAIL,'pjax-container' => '#email-gridview-pjax'], ['class' => 'float-right btn btn-sm btn-primary','role' => 'modal-remote','data-toggle' => 'tooltip','title' => Yii::t('checkin', 'Tạo')]);?>
                </div>
			</div>
		</div>
            <?php
            Pjax::begin([
                'id' => 'email-gridview-pjax'
            ]);
            echo GridView::widget([
                'layout' => "{items}\n{pager}",
                'options' => [
                    'class' => 'table-responsive'
                ],
                'tableOptions' => [
                    'class' => [
                        'table align-items-center table-flush',
                    ]
                ],
                'headerRowOptions' => [
                    'class' => [
                        'thead-light'
                    ]
                ],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'options' => [
                            'style' => 'width: 50px'
                        ]
                    ],
                    [
                        'attribute' => 'code',
                        'options' => [
                            'style' => 'width: 20%'
                        ]
                    ],
                    'title',
                    //'content:ntext',
                    [
                        'class' => \common\widgets\ActionColumn::class,
                        'contentOptions' => [
                            'class' => 'td-actions',
                            'style' => 'width: 70px'
                        ],
                        'template' => "{update}{delete}",
                        'buttons' => [
                            'update' => function ($url, $model) {
                                $url = Url::to([
                                    'setting/update-template',
                                    'id' => $model->id,
                                    'pjax-container' => '#email-gridview-pjax'
                                ]);
                                $attributes = [
                                    'class' => 'table-action text-warning',
                                    'role' => 'modal-remote',
                                    'data-toggle' => 'tooltip',
                                    'title' => Yii::t('checkin', 'Cập nhật')
                                ];
                                return Html::a('<i class="fa-fw fas fa-edit" aria-hidden=""></i>', $url, $attributes);
                            },
                            'delete' => function ($url, $model) {
                                $url = Url::to([
                                    'setting/delete-template',
                                    'id' => $model->id,
                                    'pjax-container' => '#email-gridview-pjax'
                                ]);
                                $attributes = [
                                    'class' => 'table-action text-danger',
                                    'role' => 'modal-remote',
                                    'data-request-method' => 'post',
                                    'data-toggle' => 'tooltip',
                                    'title' => Yii::t('checkin', 'Xóa'),
                                    'data-confirm-title' => Yii::t('checkin', 'Are you sure?'),
                                    'data-confirm-message' => Yii::t('checkin', 'Are you sure you want to delete this item ')
                                ];
                                return Html::a('<i class="fa-fw fas fa-trash" aria-hidden=""></i>', $url, $attributes);
                            }
                        ]
                    ],
                ],
            ]);
            Pjax::end();
            ?>
    </div>
</div>
