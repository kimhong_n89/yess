<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use common\models\Template;

/**
 * @var yii\web\View $this
 * @var common\models\Template $model
 * @var yii\bootstrap4\ActiveForm $form
 */
?>

<div class="template-form">
    <?php $form = ActiveForm::begin(); ?>
        <?php echo $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
        <?php if($model->type == Template::TYPE_EMAIL) :?>
            <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        <?php endif;?>
    <?php
    echo $form->field($model, "content")->widget(\yii\imperavi\Widget::class, [
        'plugins' => [
            'fullscreen',
            'fontcolor',
            'video'
        ],
        'options' => [
            'minHeight' => 200,
            'maxHeight' => 300,
            'buttonSource' => true,
            'convertDivs' => false,
            'removeEmptyTags' => true,
            'imageUpload' => Yii::$app->urlManager->createUrl([
                '/file/storage/upload-imperavi'
            ]),
        ],
        'htmlOptions'=>[
            'id'=>'template-content',
        ],
    ])?>
    <?php ActiveForm::end(); ?>
</div>
