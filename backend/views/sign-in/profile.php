<?php

use common\models\UserProfile;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use rmrevin\yii\fontawesome\FAS;

/* @var $this yii\web\View */
/* @var $model common\models\UserProfile */
/* @var $form yii\bootstrap4\ActiveForm */

$this->title = Yii::t('backend', 'Cập nhật thông tin cá nhân')
?>

<?php $form = ActiveForm::begin() ?>
<div class="user-profile-form card">
    <div class="card-body">

        <?php echo $form->field($model, 'picture')->widget(\trntv\filekit\widget\Upload::class, [
            'url'=>['avatar-upload']
        ])->label(Yii::t('backend', 'Avatar')) ?>

        <?php echo $form->field($model, 'firstname')->textInput(['maxlength' => 255])->label(Yii::t('backend', 'Họ')) ?>

        <?php echo $form->field($model, 'middlename')->textInput(['maxlength' => 255])->label(Yii::t('backend', 'Tên đệm')) ?>

        <?php echo $form->field($model, 'lastname')->textInput(['maxlength' => 255])->label(Yii::t('backend', 'Tên')) ?>

        <?php //echo $form->field($model, 'locale')->dropDownlist(Yii::$app->params['availableLocales']) ?>

        <?php echo $form->field($model, 'gender')->dropDownlist([
            UserProfile::GENDER_FEMALE => Yii::t('backend', 'Nữ'),
            UserProfile::GENDER_MALE => Yii::t('backend', 'Nam')
        ]) ?>
    </div>
    <div class="card-footer">
        <?php echo Html::submitButton(FAS::icon('save').' '.Yii::t('backend', 'Cập nhật'), ['class' => 'btn btn-primary']) ?>
    </div>
</div>
<?php ActiveForm::end() ?>