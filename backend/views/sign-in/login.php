<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \backend\models\LoginForm */

$this->title = Yii::t('backend', 'Đăng nhập');
$this->params['breadcrumbs'][] = $this->title;
$this->params['body-class'] = 'login-page';
?>
<div class="header py-5 py-lg-7" style="background: #1F262F !important">
  <div class="container">
    <div class="header-body text-center mb-7">
      <div class="row justify-content-center">
        <div class="col-xl-4 col-lg-5 col-md-7 px-5">
          <img src="/img/logo.png" class="navbar-brand-img img-fluid" alt="Yess">
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container mt--8 pb-5">
  <div class="row justify-content-center">
    <div class="col-lg-5 col-md-7">
      <div class="card border-0 mb-0">
        <div class="card-body px-lg-5 py-lg-5">
          <div class="text-center text-muted mb-4">
          <h1 class="text-main"><?php echo Html::encode($this->title) ?></h1>
            <small><?php echo Yii::t('backend', 'Đăng nhập để vào trang quản trị') ?></small>
          </div>
          <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
          <?php echo $form->errorSummary($model) ?>
            <div class="form-group mb-3">
              <div class="input-group input-group-merge input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                </div>
                <?= Html::activeTextInput($model, 'username', ['class' => 'form-control']);?>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group input-group-merge input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                </div>
                <?= Html::activePasswordInput($model, 'password', ['class' => 'form-control']);?>
              </div>
            </div>
            <?php echo $form->field($model, 'rememberMe')->checkbox([
                'labelOptions' => [
                    'class' => ['widget' => 'custom-control-label text-muted']
                ],
                'template' => "<div class=\"custom-control custom-control-alternative custom-checkbox\">\n{input}\n{label}\n{error}\n{hint}\n</div>"
            ]) ?>
            <div class="text-center">
                <?php echo Html::submitButton(Yii::t('backend', 'Đăng nhập'), [
                    'class' => 'btn btn-main my-4',
                    'name' => 'login-button'
                ]) ?>
            </div>
          <?php ActiveForm::end() ?>
        </div>
      </div>
    </div>
  </div>
</div>