<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\Feedback $model
 * @var yii\bootstrap4\ActiveForm $form
 */
?>

<div class="feedback-form">
    <?php $form = ActiveForm::begin(); ?>
        <div class="card">
            <div class="card-body">
                <div class="mb-2">
                  <label class="font-weight-medium">Bác sĩ:</label> <?=Html::encode($model->doctor->fullname);?>
                </div>
                <div class="mb-2">
                <label class="font-weight-medium">Tiêu đề:</label> <?=Html::encode($model->title);?>
                </div>
                <div class="feedback-content">
                  <label class="label-text">Nội dung</label>
                    <?=\yii\helpers\HtmlPurifier::process($model->content);?>
                </div>
                <?php
                echo $form->field($model, "answer")->widget(\yii\imperavi\Widget::class, [
                    'plugins' => [
                        'fullscreen',
                        'fontcolor',
                        'video'
                    ],
                    'options' => [
                        'minHeight' => 200,
                        'maxHeight' => 200,
                        'buttonSource' => true,
                        'convertDivs' => false,
                        'removeEmptyTags' => true,
                        'imageUpload' => Yii::$app->urlManager->createUrl([
                            '/file/storage/upload-imperavi'
                        ])
                    ],
                    'htmlOptions'=>[
                        'id'=>'feedback-content',
                    ],
                ])?>
            </div>
            <div class="card-footer">
                <?php echo Html::submitButton(Yii::t('backend', 'Cập nhật'), ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
