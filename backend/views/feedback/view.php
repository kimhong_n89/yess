<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var common\models\Feedback $model
 */

$this->title = 'Xem góp ý';
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Góp ý'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->id;
?>
<div class="feedback-view">
    <div class="card">
        <div class="card-header">
            <?php echo Html::a(Yii::t('backend', 'Trả lời'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?php echo Html::a(Yii::t('backend', 'Xoá'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </div>
        <div class="card-body">
            <?php echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    [
                        'attribute' => 'doctor.fullname',
                        'contentOptions' => [
                            'style' => 'word-break: break-word; white-space: normal;',
                        ],
                        'label' => Yii::t('backend', 'Bác sĩ'),
                    ],
                    [
                        'attribute' => 'user.username',
                        'contentOptions' => [
                            'style' => 'word-break: break-word; white-space: normal;',
                        ],
                        'label' => Yii::t('backend', 'Người trả lời'),
                    ],
                    [
                        'attribute' => 'title',
                        'contentOptions' => [
                            'style' => 'word-break: break-word; white-space: normal;',
                        ],
                    ],
                    [
                        'attribute' => 'content',
                        'contentOptions' => [
                            'style' => 'word-break: break-word; white-space: normal;',
                        ],
                    ],
                    [
                        'attribute' => 'answer',
                        'contentOptions' => [
                            'style' => 'word-break: break-word; white-space: normal;',
                        ],
                    ],
                    [
                        'attribute' => 'created_at',
                        'value' => function($model){
                            return date('d-m-Y H:i:s', $model->created_at);
                        },
                    ],
                    [
                        'attribute' => 'updated_at',
                        'value' => function($model){
                            return date('d-m-Y H:i:s', $model->updated_at);
                        },
                    ],
                ],
            ]) ?>
        </div>
    </div>
</div>
