<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\widgets\DatePicker;

/**
 * @var yii\web\View $this
 * @var backend\models\search\FeedbackSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('backend', 'Góp ý');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feedback-index">
    <div class="card">

        <div class="card-body p-0">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
            <?php echo GridView::widget([
                'layout' => "{items}\n{pager}",
                'options' => [
                    'class' => ['gridview', 'table-responsive'],
                ],
                'tableOptions' => [
                    'class' => [
                        'table align-items-center table-striped table-flush',
                    ]
                ],
                'headerRowOptions' => [
                    'class' => [
                        'thead-light'
                    ]
                ],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'options' => [
                            'style' => 'width: 40px'
                        ],
                    ],
                    [
                        'attribute' => 'doctor_name',
                        'contentOptions' => [
                            'style' => 'word-break: break-word; white-space: normal;',
                        ],
                        'label' => Yii::t('backend', 'Bác sĩ'),
                    ],
                    [
                        'attribute' => 'user_name',
                        'contentOptions' => [
                            'style' => 'word-break: break-word; white-space: normal;',
                        ],
                        'label' => Yii::t('backend', 'Người trả lời'),
                    ],
                    [
                        'attribute' => 'title',
                        'contentOptions' => [
                            'style' => 'word-break: break-word; white-space: normal;',
                        ],
                    ],
                    [
                        'attribute' => 'created_at',
                        'format' => 'datetime',
                        'filter' => DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'created_at',
                            'type' => DatePicker::TYPE_COMPONENT_APPEND,
                            'pluginOptions' => [
                                'format' => 'dd-mm-yyyy',
                                'showMeridian' => true,
                                'endDate' => '0d',
                                'todayHighlight' => true
                            ]
                        ]),
                        'value' => function($model){
                            return str_replace('{%1}', '<br/>', date('H:i:s{%1}d-m-Y', $model->created_at));
                        },
                        'contentOptions' => [
                            'width' => '12%',
                            'style' => 'word-break: break-word; white-space: normal;',
                        ],
                        'format' => 'html'
                    ],
                    [
                        'attribute' => 'updated_at',
                        'format' => 'datetime',
                        'filter' => DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'updated_at',
                            'type' => DatePicker::TYPE_COMPONENT_APPEND,
                            'pluginOptions' => [
                                'format' => 'dd-mm-yyyy',
                                'showMeridian' => true,
                                'endDate' => '0d',
                                'todayHighlight' => true
                            ]
                        ]),
                        'value' => function($model){
                            return str_replace('{%1}', '<br/>', date('H:i:s{%1}d-m-Y', $model->updated_at));
                        },
                        'contentOptions' => [
                            'width' => '12%',
                            'style' => 'word-break: break-word; white-space: normal;',
                        ],
                        'format' => 'html'
                    ],
                    [
                        'class' => \common\widgets\ActionColumn::class,
                        'contentOptions' => [
                            'class' => 'table-actions',
                            'style' => 'width: 75px'
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>
