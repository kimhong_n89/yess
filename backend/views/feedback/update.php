<?php

/**
 * @var yii\web\View $this
 * @var common\models\Feedback $model
 */

$this->title = Yii::t('backend', 'Trả lời góp ý');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Góp ý'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
?>
<div class="feedback-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
