$(function() {
	var hash = window.location.hash;
	if(hash) {
	    $('a[data-toggle="tab"][data-hash=' + hash.replace('#', '') + ']').tab('show');
	} else {
	    $('a[data-toggle="tab"]:first').tab('show');
	}
	 $('a[data-toggle="tab"]').on("click", function() {
		window.location.hash = $(this).data('hash');
	});
});

function updateTotalAmount() {
    var totalAmount = 0;
    $('.medicine-item-line').each(function(index) {
        var price = $(this).find('.sel-medicine-name :selected').data('price');
        var amount = $(this).find('.medicine-amount').val();

        var total = parseInt(price) * parseFloat(amount);
        if(isNaN(total)) {
            total = 0;
        }
        totalAmount += total;
    });
    var total = formatAsMoney(totalAmount);
    $('#total-amount').html(total);
}
function formatAsMoney(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function deleteAjax(element) {
    var confirmMessage = jQuery(element).data('confirm-message');

    var result = confirm(confirmMessage);
    if (result) {
        jQuery.ajax({
            method: "POST",
            url: jQuery(element).data('href'),
        }).done(function( resp ) {
            if (resp == 1){
                $.pjax.reload({container: jQuery(element).data('pjax-container')});
            }
        });
    }
}

$(document).on('select2:open', () => {
    setTimeout(function () {  document.querySelector('.select2-container--open .select2-search__field').focus(); }, 10);
});