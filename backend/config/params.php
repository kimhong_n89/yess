<?php
return [
    'dateFormatDisplay' => [
        'phpdatetime' => 'php:Y-m-d H:i:s',
        'phpdate' => 'php:Y-m-d',
        'datetime' => 'Y-m-d H:i:s',
        'date' => 'Y-m-d',
        'time' => 'H:i:s'
    ],
    'ignoreRoute' => [
        'sign-in/profile',
        'sign-in/account',
        'sign-in/logout',
    ],
];