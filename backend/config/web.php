<?php
$config = [
    'homeUrl' => Yii::getAlias('@backendUrl'),
    'controllerNamespace' => 'backend\controllers',
    'defaultRoute' => 'dashboard/index',
    'components' => [
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'request' => [
            'cookieValidationKey' => env('BACKEND_COOKIE_VALIDATION_KEY'),
            'baseUrl' => env('BACKEND_BASE_URL'),
        ],
        'user' => [
            'class' => yii\web\User::class,
            'identityClass' => common\models\User::class,
            'loginUrl' => ['sign-in/login'],
            'enableAutoLogin' => true,
            'as afterLogin' => common\behaviors\LoginTimestampBehavior::class,
        ],
        'assetManager' => [
            'class' => yii\web\AssetManager::class,
            'linkAssets' => env('LINK_ASSETS'),
            'appendTimestamp' => YII_ENV_DEV,
            'bundles' => [
                'yii\bootstrap4\BootstrapAsset' => [
                    'sourcePath' => '@backend/web/bundle',
                    'basePath' => '@backend/web',
                    'css' => [
                        '/css/nucleo.css',
                        '/fonts/fontawesome-free/css/all.min.css',
                        '/css/material-dashboard.min.css',
                        '/css/sweetalert2.min.css'
                    ]
                ],
                'yii\bootstrap4\BootstrapPluginAsset' => [
                    'sourcePath' => '@backend/web/bundle',
                    'basePath' => '@backend/web',
                    'js' => [
                        '/js/popper.min.js',
                        '/js/js.cookie.js',
                        '/js/jquery.scrollbar.min.js',
                        '/js/jquery-scrollLock.min.js',
                        '/js/dropzone.js',
                        '/js/bootstrap-material-design.min.js',
                        '/js/perfect-scrollbar.min.js',
                        '/js/sweetalert2.min.js',
                        '/js/material-dashboard.min.js'
                    ]
                ]
            ],
        ]
    ],
    'modules' => [
        'content' => [
            'class' => backend\modules\content\Module::class,
        ],
        'widget' => [
            'class' => backend\modules\widget\Module::class,
        ],
        'file' => [
            'class' => backend\modules\file\Module::class,
        ],
        'system' => [
            'class' => backend\modules\system\Module::class,
        ],
        'translation' => [
            'class' => backend\modules\translation\Module::class,
        ],
        'rbac' => [
            'class' => backend\modules\rbac\Module::class,
            'defaultRoute' => 'rbac-auth-item/index',
        ],
    ],
//    'as globalAccess' => [
//        'class' => common\behaviors\GlobalAccessBehavior::class,
//        'rules' => [
//            [
//                'controllers' => ['sign-in'],
//                'allow' => true,
//                'roles' => ['?'],
//                'actions' => ['login'],
//            ],
//            [
//                'controllers' => ['sign-in'],
//                'allow' => true,
//                'roles' => ['@'],
//                'actions' => ['logout'],
//            ],
//            [
//                'controllers' => ['site'],
//                'allow' => true,
//                'roles' => ['?', '@'],
//                'actions' => ['error'],
//            ],
//            [
//                'controllers' => ['debug/default'],
//                'allow' => true,
//                'roles' => ['?'],
//            ],
//            [
//                'controllers' => ['user'],
//                'allow' => true,
//                'roles' => ['administrator'],
//            ],
//            [
//                'controllers' => ['user'],
//                'allow' => false,
//            ],
//            [
//                'allow' => true,
//                'roles' => ['manager', 'administrator'],
//            ],
//        ],
//    ],
    'params' => require_once  __DIR__ . '/params.php',
];

if (YII_ENV_DEV) {
    $config['modules']['gii'] = [
        'class' => yii\gii\Module::class,
        'generators' => [
            'crud' => [
                'class' => yii\gii\generators\crud\Generator::class,
                'templates' => [
                    'yii2-starter-kit' => Yii::getAlias('@backend/views/_gii/templates'),
                ],
                'template' => 'yii2-starter-kit',
                'messageCategory' => 'backend',
            ],
        ],
    ];
}

return $config;
