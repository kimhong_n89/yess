<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * StoreForm is the model behind the contact form.
 */
class StoreForm extends Model
{
    public $store_id;
    public $type;

    const TYPE_FAVORITE = 1;
    const TYPE_NEAR = 2;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['type', 'store_id'], 'required'],
            [['type', 'store_id'], 'integer'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'type' => Yii::t('frontend', 'Type'),
            'store_id' => Yii::t('frontend', 'Cửa hàng'),
        ];
    }

    public static function types()
    {
        return [
            self::TYPE_FAVORITE => Yii::t('frontend', 'Chọn từ nhà thuốc yêu thích'),
            self::TYPE_NEAR => Yii::t('frontend', 'Chọn từ vị trí gần bệnh nhân'),
        ];
    }
}
