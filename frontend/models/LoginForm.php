<?php

namespace frontend\models;

use cheatsheet\Time;
use common\models\LoginHistory;
use frontend\models\Doctor;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    const SCENARIO_LOGIN = 'scenario_login';
    public $phone;
    public $otp;
    public $rememberMe = true;

    private $user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['phone'], 'required'],
            [['otp'], 'required', 'on' => self::SCENARIO_LOGIN],
            ['phone', 'match', 'pattern' => Yii::$app->params['phone_pattern']],
            ['otp', 'validateOtp', 'on' => self::SCENARIO_LOGIN],
        ];
    }

    public function attributeLabels()
    {
        return [
            'phone' => Yii::t('frontend', 'Số điện thoại'),
            'otp' => Yii::t('frontend', 'OTP'),
        ];
    }


    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     */
    public function validateOtp()
    {
        if (!$this->hasErrors()) {
            //check otp & phone number
            $valid = LoginHistory::find()->where(['phone_number' => $this->phone, 'otp_code' => $this->otp, 'status' => LoginHistory::STATUS_NOT_ACTIVATE_YET])->one();
            if ($valid) {
                $user = $this->getUser();
                if (!$user) { //create new user
                    $newUser = new Doctor();
                    $newUser->phone = $this->phone;
                    $newUser->status = Doctor::STATUS_PENDING;
                    $newUser->save();
                    $this->user = $newUser;
                } else {
                    if ($user->status == Doctor::STATUS_DELETED) {
                        $this->addError('otp', Yii::t('frontend', 'Tài khoản này đã bị khoá'));
                    }
                }
            } else {
                $this->addError('otp', Yii::t('frontend', 'Mã OTP không đúng hoặc đã hết hạn'));
            }
        }
    }

    /**
     * Finds user by [[phone]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->user === false) {
            $this->user = Doctor::find()
                ->where(['phone' => $this->phone])
                ->one();
        }

        return $this->user;
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            if (Yii::$app->user->login($this->getUser(), Time::SECONDS_IN_A_MONTH)) {
                //update status history login
                $loginHistory = LoginHistory::find()->where(['phone_number' => $this->phone, 'otp_code' => $this->otp, 'status' => LoginHistory::STATUS_NOT_ACTIVATE_YET])->one();
                $loginHistory->status = LoginHistory::STATUS_ACTIVATED;
                $loginHistory->save();
                return true;
            }
        }
        return false;
    }
}
