<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ExaminationSchedule;
use yii\db\Expression;

/**
 * ExaminationScheduleSearch represents the model behind the search form about `common\models\ExaminationSchedule`.
 */
class ExaminationScheduleSearch extends ExaminationSchedule
{
    public $keyword;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'prescription_id', 'doctor_id', 'patient_id', 'created_at', 'updated_at'], 'integer'],
            [['note', 'date', 'keyword'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ExaminationSchedule::find()->joinWith('patient', true, 'INNER JOIN');

        $query->where([
            'doctor_id' => Yii::$app->user->identity->id
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->keyword) {
            $query->andFilterWhere([
                'OR',
                ['like', 'patient.fullname', $this->keyword],
                ['like', 'patient.phone', $this->keyword],
                ['like', 'patient.email', $this->keyword],
                ['like', 'patient.address', $this->keyword],
                ['like', 'note', $this->keyword]
            ]);
        }

        return $dataProvider;
    }

    public function today($params=[])
    {
        $query = ExaminationSchedule::find()->joinWith('patient', true, 'INNER JOIN')->where([
            'date(date)' => new \yii\db\Expression('CURDATE()')
        ]);
        $query->andWhere([
            'doctor_id' => Yii::$app->user->identity->id
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['date' => SORT_ASC]],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->keyword) {
            $query->andFilterWhere([
                'OR',
                ['like', 'patient.fullname', $this->keyword],
                ['like', 'patient.phone', $this->keyword],
                ['like', 'patient.email', $this->keyword],
                ['like', 'patient.address', $this->keyword],
                ['like', 'note', $this->keyword]
            ]);
        }
        return $dataProvider;
    }

    public function thisWeek($params, $includeToday = true)
    {
        $monday = date( 'Y-m-d', strtotime( 'monday this week' ) );
        $sunday = date( 'Y-m-d', strtotime( 'sunday this week' ) );

        $query = ExaminationSchedule::find()->joinWith('patient', true, 'INNER JOIN')->where([
            'between', 'date(date)', $monday, $sunday
        ]);

        if (!$includeToday) {
            $query->andWhere(['>','date(date)', new \yii\db\Expression('CURDATE()') ]);
        }

        $query->andWhere([
            'doctor_id' => Yii::$app->user->identity->id
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['date' => SORT_ASC]],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->keyword) {
            $query->andFilterWhere([
                'OR',
                ['like', 'patient.fullname', $this->keyword],
                ['like', 'patient.phone', $this->keyword],
                ['like', 'patient.email', $this->keyword],
                ['like', 'patient.address', $this->keyword],
                ['like', 'note', $this->keyword]
            ]);
        }

        return $dataProvider;
    }

    public function nextWeek($params)
    {
        $monday = date( 'Y-m-d', strtotime( 'monday next week' ) );
//        $sunday = date( 'Y-m-d', strtotime( 'sunday next week' ) );
//
//        $query = ExaminationSchedule::find()->where([
//            'between', 'date(date)', $monday, $sunday
//        ]);
        $query = ExaminationSchedule::find()->joinWith('patient', true, 'INNER JOIN')->where([
            '>=', 'date(date)', $monday
        ]);
        $query->andWhere([
            'doctor_id' => Yii::$app->user->identity->id
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['date' => SORT_ASC]],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->keyword) {
            $query->andFilterWhere([
                'OR',
                ['like', 'patient.fullname', $this->keyword],
                ['like', 'patient.phone', $this->keyword],
                ['like', 'patient.email', $this->keyword],
                ['like', 'patient.address', $this->keyword],
                ['like', 'note', $this->keyword]
            ]);
        }

        return $dataProvider;
    }
}
