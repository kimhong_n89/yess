<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Patient;

/**
 * PatientSearch represents the model behind the search form about `common\models\Patient`.
 */
class PatientSearch extends Patient
{
    public $doctor_id;
    public $keyword;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'gender', 'province_id', 'district_id', 'ward_id', 'day_of_birth', 'month_of_birth', 'year_of_birth', 'status', 'created_at', 'updated_at'], 'integer'],
            [['fullname', 'phone', 'email', 'address'], 'safe'],
            [['doctor_id', 'keyword'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Patient::find()->select([
            'patient.*',
            'COUNT(prescription.id) as count_prescription'
        ])->joinWith('prescriptions', true, 'INNER JOIN')->groupBy('patient.id');
        $query->where([
            'prescription.doctor_id' => Yii::$app->user->identity->id
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->keyword) {
            $query->andFilterWhere([
                'OR',
                ['like', 'fullname', $this->keyword],
                ['like', 'phone', $this->keyword],
                ['like', 'email', $this->keyword],
                ['like', 'address', $this->keyword]
            ]);
        }

        return $dataProvider;
    }
}
