<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Prescription;

/**
 * PrescriptionSearch represents the model behind the search form about `common\models\Prescription`.
 */
class PrescriptionSearch extends Prescription
{
    public $keyword;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'doctor_id', 'patient_id', 'is_use_insurance', 'total_amount', 'created_at', 'updated_at'], 'integer'],
            [['insurance_number', 'note', 'status'], 'safe'],
            [['insurance_rate'], 'number'],
            [['keyword'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Prescription::find()->joinWith('patient');
        $query->where(['doctor_id' => Yii::$app->user->id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->keyword) {
            $query->andFilterWhere([
                'OR',
                ['like', 'patient.fullname', $this->keyword],
                ['like', 'patient.phone', $this->keyword],
                ['like', 'prescription.id', $this->keyword],
            ]);
        }

        $query->andFilterWhere([
            'prescription.id' => $this->id,
            'prescription.doctor_id' => $this->doctor_id,
            'prescription.patient_id' => $this->patient_id,
            'prescription.is_use_insurance' => $this->is_use_insurance,
            'prescription.insurance_rate' => $this->insurance_rate,
            'prescription.total_amount' => $this->total_amount,
            'prescription.created_at' => $this->created_at,
            'prescription.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere([
            'IN', 'prescription.status', $this->status,
        ]);

        $query->andFilterWhere(['like', 'insurance_number', $this->insurance_number])
            ->andFilterWhere(['like', 'note', $this->note]);
        //dd1($query->createCommand()->rawSql);
        return $dataProvider;
    }
}
