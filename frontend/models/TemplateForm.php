<?php

namespace frontend\models;

use common\models\DoctorSetting;
use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\base\Model;
use common\models\Setting;

class TemplateForm extends Model
{
    //Mail server
    public $logo;
    public $barcode;
    public $medical_name;
    public $clinic_name;
    public $clinic_phone;
    public $medical_code;
    public $no_of_record;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['logo', 'barcode', 'medical_name', 'clinic_name', 'clinic_phone', 'medical_code', 'no_of_record'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'logo' => Yii::t('checkin', 'Logo'),
        ];
    }

    public function loadFromDB()
    {
        $settings = DoctorSetting::find()
            ->where(['doctor_id' => Yii::$app->user->identity->id])
            ->andWhere(['IN', 'key', ['logo', 'barcode', 'medical_name', 'clinic_name', 'clinic_phone', 'medical_code', 'no_of_record']])
            ->all();
        foreach ($settings as $setting) {
            $this->{$setting->key} = $setting->value;
        }
    }
}
