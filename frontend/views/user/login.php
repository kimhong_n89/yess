<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var yii\bootstrap4\ActiveForm $form
 * @var frontend\models\LoginForm $model
 */

$this->title = Yii::t('frontend', 'Đăng nhập');
?>
<div class="container-fluid">
  <div class="site-login mx-auto mt-70 text-center">
    <img src="/img/logo-login.png" class="img-fluid"/>
    <div id="section-phone">
      <?php $form = ActiveForm::begin(['id' => 'phone-form']); ?>
        <h2>Đăng nhập</h2>
          <?= $form->field($modelPhone, 'phone')->textInput(['autofocus' => true, 'placeholder'=> 'Nhập số điện thoại để tiếp tục', 'class' => 'text-center form-control'])->label(false) ?>
        <div class="form-group">
          <div class="col text-center">
              <?= Html::submitButton('Tiếp tục',['class'=>'btn btn-primary btn-rounded','id'=>'request-otp-btn']); ?>
          </div>
        </div>
      <?php ActiveForm::end(); ?>
    </div>
    <div id="section-otp" class="text-center" style="display:none;">
      <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <h2>Mã OTP</h2>
          <?= $form->field($model, 'phone')->hiddenInput(['id' => 'txt-phone'])->label(false); ?>
          <?= $form->field($model, 'otp')->textInput(['autofocus' => true, 'placeholder'=> 'Nhập mã OTP', 'class' => 'text-center form-control'])->label(false) ?>
        <div class="request-again">
          <span class="text-gray">Bạn không nhận được mã?</span> <?= Html::a('Gửi lại', 'javascript:void(0)', ['class' => 'btn-send-back text-black'])?>
        </div>
        <div class="form-group">
          <div class="col">
              <?= Html::submitButton('Hoàn tất', ['class' => 'btn btn-primary btn-rounded', 'id' => 'login-button']) ?>
          </div>
        </div>
        <a  class="back-to-phone"href="javascript:void(0)" class="back-to-phone"><i class="fas fa-arrow-left mr-2"></i>Quay lại</a>
      <?php ActiveForm::end(); ?>
    </div>
  </div>

</div>
<?php
$otpUrl = Url::toRoute(['user/send-otp']);
$loginUrl = Url::toRoute(['user/login']);
$csrf =Yii::$app->request->csrfToken;
$script = <<< JS
var countable;
$('#phone-form').on('beforeSubmit', function(e){
    sendOtp();
    return false;
})
$('.btn-send-back').click(function(){
    sendOtp();
});

$('#login-form').on('beforeSubmit', function(e){
  $.ajax({
     url: '$loginUrl',
     data:$(this).serialize(),
     dataType: 'json',
     method:'POST',
     beforeSend:function(){
        $('button#login-button').prop('disabled',true);
     },
     error:function( xhr, err ) {
             alert('An error occurred, please try again');     
        },
     complete:function(){
            $('button#login-button').prop('disabled',false);
        },
     success: function(data){
        if(data.success==true){
            window.location=data.url;
        }else{
            $('#noti-error').html(data.msg);
              $("#noti-error").fadeTo(5000, 500).fadeIn(500, function(){
              $("#noti-error").fadeOut(500);
            });
        }
       }
  });
  return false;
})
$("#loginform-otp").on("keyup", function(e) {
    if ($(this).val().length == 4) {
       $('#login-form').submit(); 
    }
})
function sendOtp() {
    var phone = $('#loginform-phone').val(); 
    $.ajax({
       url: '$otpUrl',
       data: {phone: phone,_csrf:'$csrf'},
       method:'POST',
       beforeSend:function() {
          $('button#request-otp-btn').prop('disabled',true);
       },
      error:function( xhr, err ) {
          alert('An error occurred, please try again');
      },
       complete:function(){
              $('button#request-otp-btn').prop('disabled',false);
       },
       success: function(data){
            if(data.success==false){
                $('#noti-error').html(data.msg);
                $("#noti-error").fadeTo(5000, 500).fadeIn(500, function(){
                    $("#noti-error").fadeOut(500);
                });
                return false;
            }else{
                $('#noti-success').html(data.msg);
                $("#noti-success").fadeTo(5000, 500).fadeIn(500, function(){
                    $("#noti-success").fadeOut(500);
                });
                $('#section-phone').hide();
                $('#section-otp').show();
                $('#txt-phone').val(phone);
            }
       }
    }); 
    
    var count = 60;
    var spanCount = $(".btn-send-back");
    spanCount.addClass('disabled');
    clearInterval(countable);
    countable= setInterval(function (){
      spanCount.html("Gửi lại sau " + count + " giây");
      if(count > 0){
             count -= 1;
       }else if(count <= 0){
          clearInterval(countable);
          spanCount.html("Gửi lại");
          spanCount.removeClass('disabled');
       }
    }, 1000);
}

$('.back-to-phone').on('click', function() {
  $('#section-otp').hide();
  $('#section-phone').show();
})
JS;
$position = \yii\web\View::POS_READY;
$this->registerJs($script, $position);
?>
