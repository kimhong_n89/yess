<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\Feedback $model
 * @var yii\bootstrap4\ActiveForm $form
 */
?>

<div class="feedback-form">
    <?php $form = ActiveForm::begin(); ?>
      <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
      <?php
      echo $form->field($model, "content")->widget(\yii\imperavi\Widget::class, [
          'plugins' => [
              'fullscreen',
              'fontcolor',
              'video'
          ],
          'options' => [
              'minHeight' => 200,
              'maxHeight' => 200,
              'buttonSource' => true,
              'convertDivs' => false,
              'removeEmptyTags' => true,
              'imageUpload' => Yii::$app->urlManager->createUrl([
                  '/file/storage/upload-imperavi'
              ])
          ],
          'htmlOptions'=>[
              'id'=>'feedback-content',
          ],
      ])?>
    <?php ActiveForm::end(); ?>
</div>
