<?php

use yii\helpers\HtmlPurifier;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Feedback $model
 */

?>
<div class="feedback-view">
  <div class="mb-2">
    <label class="font-weight-medium">Tiêu đề:</label> <?=Html::encode($model->title);?>
  </div>
  <div class="feedback-content">
  <label class="label-text">Trả lời</label>
  <?=HtmlPurifier::process($model->answer);?>
  </div>
</div>
