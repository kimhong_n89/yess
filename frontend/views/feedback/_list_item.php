<?php
use \yii\helpers\Html;
use \yii\helpers\HtmlPurifier;
/**
 * @var \common\models\Feedback $model
 */
?>
<h6 class="feedback-title"><?=Html::encode($model->title);?></h6>
<div class="feedback-content"><?=HtmlPurifier::process($model->content);?></div>
<div class="feedback-date"><?= translate_day_name(date('l, d/m/Y', $model->created_at));?></div>
<?php
  if ($model->answer) {
      echo Html::a('Xem trả lời', ['view-answer', 'id' => $model->id], [
          'class' => 'view-detail btn btn-expand',
          'role' => 'modal-remote',
          'data-toggle' => 'tooltip',
          'title' => 'Xem trả lời',
      ]);
  }
?>