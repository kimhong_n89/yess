<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \common\models\Feedback;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var backend\models\search\StoreSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('backend', 'Góp ý');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row content-top style-primary">
  <div class="col">
    <h2 class="text-white"><?=$this->title;?></h2>
  </div>
  <div class="col">
      <?= \frontend\widgets\FeedbackSearchForm::widget([])?>
  </div>
</div>
<?php \yii\widgets\Pjax::begin(['id' => 'feedback-pjax']);?>
<div class="feedback-index">
  <p class="before-feedback-list">
      <?php
      echo Html::a('Gửi góp ý', ['create', 'pjax-container' => '#feedback-pjax'], [
          'class' => 'btn btn-update btn-send-feedback',
          'role' => 'modal-remote',
          'data-request-method' => 'post',
          'data-toggle' => 'tooltip',
          'title' => 'Cập nhật',
      ]);
      ?>
  </p>

    <?php
    echo \yii\widgets\ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'feedback-item'],
        'itemView' => '_list_item',
        'summary' => '',
        'pager' => [
            'firstPageLabel' => '<div class="double-icon"><i class="fas fa-chevron-left"></i><i class="fas fa-chevron-left"></i></div>',
            'lastPageLabel' => '<div class="double-icon"><i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i></div>',
            'prevPageLabel' => '<i class="fas fa-chevron-left"></i>',
            'nextPageLabel' => '<i class="fas fa-chevron-right"></i>',
            'options' => ['class' => 'table-responsive']
        ],
    ]);?>
</div>
<?php \yii\widgets\Pjax::end();?>
