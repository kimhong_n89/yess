<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use backend\controllers\PatientController;

/**
 * @var yii\web\View $this
 * @var common\models\Patient $model
 */

$this->title = 'Thông tin bệnh nhân';
$rate = @\common\models\Prescription::insuranceRates()[$lastPrescription->insurance_rate];
?>
<div class="row content-top style-primary">
  <div class="col">
    <h2 class="text-white"><?=$this->title;?></h2>
  </div>
  <div class="col">
      <?= \frontend\widgets\PatientSearchForm::widget([])?>
  </div>
</div>
<div class="patient-view">
  <div class="block-patient">
    <div class="right-actions">
        <?php
            echo Html::a('Sửa', ['update', 'id' => $model->id, 'pjax-container' => '#patient-info-pjax'], [
                'class' => 'btn btn-update mx-1',
                'role' => 'modal-remote',
                'data-request-method' => 'post',
                'data-toggle' => 'tooltip',
                'title' => 'Cập nhật',
            ]);
            echo Html::a('Xoá', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-delete mx-1',
//                'role' => 'modal-remote',
//                'data-request-method' => 'post',
//                'data-toggle' => 'tooltip',
//                'title' => 'Xoá',
//                'data-confirm-title' => 'Bạn có chắc chắn không?',
//                'data-confirm-message' => 'Bạn có chắc muốn xoá bệnh nhân này không?',
//                'data-confirm-cancel' => 'Không',
//                'data-confirm-ok' => 'Có',
                'data-method' => 'post',
                'data-confirm' => 'Bạn có chắc muốn xoá bệnh nhân này không?',
            ]);
        ?>
    </div>
    <?php \yii\widgets\Pjax::begin(['id' => 'patient-info-pjax']);?>
    <div class="row">
      <div class="col-sm-6 col-patient-name mb-20"><label class="label-text">Bệnh nhân:</label><?= Html::encode($model->fullname);?></div>
      <div class="col-sm-3 col-year-old mb-20"><label class="label-text">Độ tuổi:</label><?= Html::encode($model->getYearOld());?></div>
      <div class="col-sm-3 col-patient-gender mb-20"><label class="label-text">Giới tính:</label><?= Html::encode(@$model::genders()[$model->gender])?></div>
    </div>
    <div class="row">
      <div class="col-sm-6 mb-20"><label class="label-text">Mã hồ sơ:</label><?= $model->id;?></div>
      <div class="col-sm-6 mb-20"><label class="label-text">Tình trạng:</label><?= @\common\models\Patient::statuses()[$model->status];?></div>
<!--      <div class="col-sm-6"><label class="label-text">Số toa:</label>--><?//= $dataProvider->totalCount;?><!--</div>-->
    </div>
    <div class="row">
      <div class="col-12 mb-20"><label class="min-width label-text">Địa chỉ</label><?=Html::encode($model->getFullAddress())?></div>
      <div class="col-sm-4 col-6 mb-20"><label class="min-width label-text">Số BHYT</label><?=Html::encode($lastPrescription->insurance_number)?></div>
      <div class="col-sm-4 col-6 mb-20"><label class="label-text"><?=$lastPrescription->is_use_insurance ? "Có BHYT ({$rate})" : 'Không BHYT';?></label></div>
      <div class="col-12 mb-20"><label class="label-text">Chẩn đoán</label><?= implode(' / ', \yii\helpers\ArrayHelper::getColumn($model->getAllDiagnoses(), 'name'));?></div>
    </div>
    <?php Pjax::end();?>
    <div class="list-prescription">
        <div class="label-text">Lịch sử toa thuốc</div>
            <?php
            echo \yii\widgets\ListView::widget([
                'dataProvider' => $dataProvider,
                'itemOptions' => ['class' => 'item'],
                'itemView' => '_prescription',
                'summary' => '',
                'pager' => [
                    'firstPageLabel' => '<div class="double-icon"><i class="fas fa-chevron-left"></i><i class="fas fa-chevron-left"></i></div>',
                    'lastPageLabel' => '<div class="double-icon"><i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i></div>',
                    'prevPageLabel' => '<i class="fas fa-chevron-left"></i>',
                    'nextPageLabel' => '<i class="fas fa-chevron-right"></i>',
                ]
            ]);?>
        </div>
  </div>
</div>
