<?php
use common\models\Prescription;
use \yii\helpers\Html;
/**
 * @var \common\models\Patient $model
 */

?>

<div class="serial-number"><?=$index+1?></div>
<div class="block-patient">
  <a class="view-detail btn btn-expand" href="<?= \yii\helpers\Url::to(['view', 'id' => $model->id]);?>">Chi tiết</a>
  <div class="row">
    <div class="col-sm-6 col-patient-name mb-20"><label class="label-text">Bệnh nhân:</label><?= Html::encode($model->fullname);?></div>
    <div class="col-sm-3 col-year-old mb-20"><label class="label-text">Độ tuổi:</label><?= Html::encode($model->getYearOld());?></div>
    <div class="col-sm-3 col-patient-gender mb-20"><label class="label-text">Giới tính:</label><?= Html::encode(@$model::genders()[$model->gender])?></div>
  </div>
  <div class="row">
    <div class="col-sm-6 mb-20"><label class="label-text">Mã hồ sơ:</label><?= $model->id;?></div>
    <div class="col-sm-6 mb-20"><label class="label-text">Tình trạng:</label><?= @\common\models\Patient::statuses()[$model->status];?></div>
<!--    <div class="col-sm-6"><label class="label-text">Số toa:</label>--><?//= $model->count_prescription;?><!--</div>-->
  </div>
</div>

