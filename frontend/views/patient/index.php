<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var backend\models\search\PrescriptionSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('backend', 'Quản lý bệnh nhân');
?>
<div class="row content-top style-primary">
  <div class="col">
    <h2 class="text-white"><?=$this->title;?></h2>
  </div>
  <div class="col">
      <?= \frontend\widgets\PatientSearchForm::widget([])?>
  </div>
</div>

<?php //\yii\widgets\Pjax::begin(['id' => 'prescription-pjax']);?>
<div class="patient-index">
    <?php
    echo \yii\widgets\ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'patient-item'],
        'itemView' => '_list_item',
        'summary' => '',
        'pager' => [
            'firstPageLabel' => '<div class="double-icon"><i class="fas fa-chevron-left"></i><i class="fas fa-chevron-left"></i></div>',
            'lastPageLabel' => '<div class="double-icon"><i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i></div>',
            'prevPageLabel' => '<i class="fas fa-chevron-left"></i>',
            'nextPageLabel' => '<i class="fas fa-chevron-right"></i>',
            'options' => ['class' => 'table-responsive']
        ]
    ]);?>
</div>
<?php //\yii\widgets\Pjax::end();?>
