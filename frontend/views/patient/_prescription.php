<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Prescription;
/**
 * @var \common\models\Prescription $model
 */
?>
<div class="serial-number"><?=$index+1;?></div>
<a class="view-detail btn btn-expand" href="<?= \yii\helpers\Url::to(['/prescription/view', 'id' => $model->id]);?>">Chi tiết</a>
<div class="row">
    <div class="col-sm-6 text-left mb-20">
        <span class="label-text">Mã đơn thuốc</span>DT<?=$model->id?>-<?=date('dmY', $model->created_at);?>
    </div>
    <div class="col-sm-6 text-sm-right mb-20">
        <span class="label-text">Ngày tạo</span><?=date('d/m/Y', $model->created_at);?><span class="ml-3"><?= str_replace('{%1}', 'h', date('H{%1}i', $model->created_at));?></span>
    </div>
</div>
