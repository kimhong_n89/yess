<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var frontend\models\search\ExaminationScheduleSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('backend', 'Lịch khám bệnh');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row content-top style-primary">
  <div class="col">
    <h2 class="text-white"><?=$this->title;?></h2>
  </div>
  <div class="col">
      <?= \frontend\widgets\ScheduleSearchForm::widget([])?>
  </div>
</div>
<div class="examination-schedule-index">
  <h3>Hôm nay</h3>
  <div class="today-block row">
  <?php if (empty($dataProviderToday->getModels())) :?>
    <div class="col"><p class="empty-schedule">Không có lịch hẹn trong ngày hôm nay</p></div>
  <?php else:?>
    <?php foreach ($dataProviderToday->getModels() as $schedule) :?>
    <div class="col-sm-6 my-3">
      <div class="schedule">
        <div class="div-padding-left"><label class="font-weight-medium label-patient"><span class="material-icons">person</span>Bệnh nhân:</label><?= Html::encode($schedule->patient->fullname);?></div>
        <div class="div-padding-left"><label class="font-weight-medium">Ghi chú:</label><?= Html::encode($schedule->note);?></div>
        <div class="div-padding-left"><label class="font-weight-medium label-time"><span class="material-icons">watch_later</span>Giờ hẹn:</label><?= date('H:i', strtotime($schedule->date));?></div>
        <div class="div-padding-left"><label class="font-weight-medium">Ngày hẹn:</label>Hôm nay, <?= date('d/m/Y', strtotime($schedule->date));?></div>
        <div class="div-padding-left"><label class="font-weight-medium label-doctor"><i class="fas fa-stethoscope"></i>Bác sĩ:</label><?= Html::encode($schedule->doctor->fullname);?></div>
        <div class="div-padding-left"><label class="font-weight-medium">Chuyên khoa:</label><?= implode(', ', \yii\helpers\ArrayHelper::getColumn($schedule->doctor->businessFields, 'name'));?></div>
      </div>
    </div>
    <?php endforeach;?>
  <?php endif;?>
  </div>
  <h3>Trong tuần</h3>
  <div class="this-week-block row">
      <?php if (empty($dataProviderThisweek->getModels())) :?>
        <div class="col"><p class="empty-schedule">Không có lịch hẹn trong ngày hôm nay</p></div>
      <?php else:?>
          <?php foreach ($dataProviderThisweek->getModels() as $schedule) :?>
          <div class="col-sm-6 my-3">
            <div class="schedule">
              <div class="div-padding-left"><label class="font-weight-medium label-patient"><span class="material-icons">person</span>Bệnh nhân:</label><?= Html::encode($schedule->patient->fullname);?></div>
              <div class="div-padding-left"><label class="font-weight-medium">Ghi chú:</label><?= Html::encode($schedule->note);?></div>
              <div class="div-padding-left"><label class="font-weight-medium label-time"><span class="material-icons">watch_later</span>Giờ hẹn:</label><?= date('H:i', strtotime($schedule->date));?></div>
              <div class="div-padding-left"><label class="font-weight-medium">Ngày hẹn:</label><?= translate_day_name(date('l, d/m/Y', strtotime($schedule->date)));?></div>
              <div class="div-padding-left"><label class="font-weight-medium label-doctor"><i class="fas fa-stethoscope"></i>Bác sĩ:</label><?= Html::encode($schedule->doctor->fullname);?></div>
              <div class="div-padding-left"><label class="font-weight-medium">Chuyên khoa:</label><?= implode(', ', \yii\helpers\ArrayHelper::getColumn($schedule->doctor->businessFields, 'name'));?></div>
            </div>
          </div>
          <?php endforeach;?>
      <?php endif;?>
  </div>
  <h3>Tuần kế tiếp</h3>
  <div class="next-week-block row">
      <?php if (empty($dataProviderNextweek->getModels())) :?>
        <div class="col"><p class="empty-schedule">Không có lịch hẹn trong ngày hôm nay</p></div>
      <?php else:?>
          <?php foreach ($dataProviderNextweek->getModels() as $schedule) :?>
          <div class="col-sm-6 my-3">
            <div class="schedule">
              <div class="div-padding-left"><label class="font-weight-medium label-patient"><span class="material-icons">person</span>Bệnh nhân:</label><?= Html::encode($schedule->patient->fullname);?></div>
              <div class="div-padding-left"><label class="font-weight-medium">Ghi chú:</label><?= Html::encode($schedule->note);?></div>
              <div class="div-padding-left"><label class="font-weight-medium label-time"><span class="material-icons">watch_later</span>Giờ hẹn:</label><?= date('H:i', strtotime($schedule->date));?></div>
              <div class="div-padding-left"><label class="font-weight-medium">Ngày hẹn:</label><?= translate_day_name(date('l, d/m/Y', strtotime($schedule->date)));?></div>
              <div class="div-padding-left"><label class="font-weight-medium label-doctor"><i class="fas fa-stethoscope"></i>Bác sĩ:</label><?= Html::encode($schedule->doctor->fullname);?></div>
              <div class="div-padding-left"><label class="font-weight-medium">Chuyên khoa:</label><?= implode(', ', \yii\helpers\ArrayHelper::getColumn($schedule->doctor->businessFields, 'name'));?></div>
            </div>
          </div>
          <?php endforeach;?>
      <?php endif;?>
  </div>

</div>
