<?php
/**
 * @var yii\web\View $this
 * @var string $content
 */

use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use rmrevin\yii\fontawesome\FAS;
$this->beginContent('@frontend/views/layouts/_clear.php');
$avatar = Yii::$app->user->identity->getAvatarUrl('/img/default-avatar.png');
?>
  <header>
    <nav class="navbar-main navbar">
      <div class="container-fluid p-0">
        <a class="navbar-brand" href="<?= Yii::$app->homeUrl;?>">
            <?=\yii\helpers\Html::img('/img/logo.png', [
                'class' => 'img-fluid'
            ])?>
        </a>
        <div class="navbar-nav justify-content-end ml-auto nav d-md-none">
          <div class="media align-items-center position-relative">
            <div class="media-body text-right">
              <p class="doctor-name font-weight-bold">Bác sĩ <?= \yii\helpers\Html::encode(Yii::$app->user->identity->fullname);?></p>
            </div>
            <a role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <span class="avatar avatar-sm rounded-circle">
              <img alt="Avatar" src="<?=$avatar?>">
            </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right position-absolute" aria-labelledby="navbarDropdownProfile">
              <a class="dropdown-item" href="<?=\yii\helpers\Url::to(['setting/index'])?>">Thông tin cá nhân</a>
              <?php if (Yii::$app->user->identity->status == \common\models\Doctor::STATUS_APPROVED) :?>
                <a class="dropdown-item" href="<?= \yii\helpers\Url::to(['/feedback'])?>">Góp ý</a>
              <?php endif;?>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="<?= \yii\helpers\Url::to(['/user/logout'])?>" data-method="post">Đăng xuất</a>
            </div>

            <button class="navbar-toggler sidebarIconToggle" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
              <div class="spinner diagonal part-1"></div>
              <div class="spinner horizontal"></div>
              <div class="spinner diagonal part-2"></div>
            </button>
          </div>
        </div>
      </div>
    </nav>
  </header>
  <div class="container-fluid">
    <div class="row">
      <nav class="col sidebar d-print-none" id="sidebarMenu">
          <?php echo \frontend\widgets\MainSidebarMenu::widget([
              'options' => [
                  'class' => [
                      'nav',
                      'nav-pills',
                      'nav-sidebar',
                      'flex-column',
                  ],
                  'role' => 'menu',
                  'encode' => false
              ],
              'items' => [
                  [
                      'label' => Yii::t('backend', 'Tổng quan'),
                      'options' => ['class' => 'nav-item li-bg-dashboard'],
                      'icon' => \yii\helpers\Html::img('/img/dashboard.png'),
                      'url' => Yii::$app->homeUrl,
                      'visible' => Yii::$app->user->identity->status == \common\models\Doctor::STATUS_APPROVED
                  ],
                  [
                      'label' => Yii::t('backend', 'Tạo toa<br>thuốc mới'),
                      'options' => ['class' => 'nav-item li-bg-create'],
                      'icon' => \yii\helpers\Html::img('/img/prescription-create.png'),
                      'url' => ['prescription/create'],
                      'encode' => false,
                      'visible' => Yii::$app->user->identity->status == \common\models\Doctor::STATUS_APPROVED
                  ],
                  [
                      'label' => Yii::t('backend', 'Quản lý<br>toa thuốc'),
                      'options' => ['class' => 'nav-item li-bg-prescription'],
                      'icon' => \yii\helpers\Html::img('/img/manage-prescription.png'),
                      'url' => ['prescription/index'],
                      'encode' => false,
                      'visible' => Yii::$app->user->identity->status == \common\models\Doctor::STATUS_APPROVED
                  ],
                  [
                      'label' => Yii::t('backend', 'Nhà thuốc<br>yêu thích'),
                      'options' => ['class' => 'nav-item li-bg-store'],
                      'icon' => \yii\helpers\Html::img('/img/store.png'),
                      'url' => ['store/index'],
                      'encode' => false,
                      'visible' => Yii::$app->user->identity->status == \common\models\Doctor::STATUS_APPROVED
                  ],
                  [
                      'label' => Yii::t('backend', 'Lịch hẹn<br>khám'),
                      'options' => ['class' => 'nav-item li-bg-schedule'],
                      'icon' => \yii\helpers\Html::img('/img/schedule.png'),
                      'url' => ['schedule/index'],
                      'encode' => false,
                      'visible' => Yii::$app->user->identity->status == \common\models\Doctor::STATUS_APPROVED
                  ],
                  [
                      'label' => Yii::t('backend', 'Danh sách<br>bệnh nhân'),
                      'options' => ['class' => 'nav-item li-bg-patient'],
                      'icon' => \yii\helpers\Html::img('/img/patient.png'),
                      'url' => ['patient/index'],
                      'encode' => false,
                      'visible' => Yii::$app->user->identity->status == \common\models\Doctor::STATUS_APPROVED
                  ],
                  [
                      'label' => Yii::t('backend', 'Cài đặt<br>chung'),
                      'options' => ['class' => 'nav-item li-bg-setting'],
                      'icon' => \yii\helpers\Html::img('/img/setting.png'),
                      'url' => ['setting/index'],
                      'encode' => false
                  ],
              ],
          ]) ?>
      </nav>
      <div class="col main-content">
          <?php echo $content ?>
      </div>
    </div>
  </div>
<?php $this->endContent() ?>