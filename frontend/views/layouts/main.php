<?php
/**
 * @var yii\web\View $this
 * @var string $content
 */

use yii\helpers\ArrayHelper;
use yii\bootstrap4\Breadcrumbs;

$this->beginContent('@frontend/views/layouts/base.php')
?>
    <?php if(Yii::$app->session->hasFlash('alert')):?>
        <?php echo \yii\bootstrap4\Alert::widget([
            'body'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
            'options'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
        ])?>
    <?php endif; ?>
    <?php echo $content ?>
<?php $this->endContent() ?>