<?php
/**
 * @var yii\web\View $this
 * @var string $content
 */

use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

$this->beginContent('@frontend/views/layouts/_clear.php')
?>
<div class="login-notification">
  <div class="alert alert-primary mb-2 text-center" style="display: none" id="noti-success" role="alert"></div>
  <div class="alert alert-danger mb-2 text-center" role="alert" style="display: none" id="noti-error"></div>
</div>

<main class="flex-shrink-0" role="main">
    <?php echo $content ?>
</main>


<?php $this->endContent() ?>