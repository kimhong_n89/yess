<?php
use \yii\helpers\Html;
/**
 * @var \common\models\Store $model
 */

?>
<?php
$is_favorited = in_array($model->id, $favorites);
?>
<div class="row ">
  <div class="col col-image">
    <img src="<?=Html::encode($model->image);?>" class="img-fluid" alt="<?=Html::encode($model->name);?>">
  </div>
  <div class="col col-info">
      <h6 class="store-name"><?=Html::encode($model->name);?></h6>
      <p class="store-address"><label class="label-text mb-0">Địa chỉ:</label><?=Html::encode($model->address);?></p>
      <p class="store-phone"><label class="label-text mb-0">Liên hệ:</label><?=Html::encode($model->phone);?></p>
      <a href="javascript:void(0)" title="<?=$is_favorited ? 'Thêm vào yêu thích' : 'Đã yêu thích';?>" data-store="<?= $model->id;?>" class="store-favorite <?= $is_favorited ? 'favorited' : ''?>"><i class="far fa-star"></i></a>
  </div>
</div>