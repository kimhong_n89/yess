<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \common\models\FavoriteStore;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var backend\models\search\StoreSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('backend', 'Nhà thuốc yêu thích');
$this->params['breadcrumbs'][] = $this->title;
$favorites = ArrayHelper::getColumn(FavoriteStore::find()->where(['doctor_id' => Yii::$app->user->identity->id])->asArray()->all(), 'store_id');
?>
<div class="row content-top style-primary">
  <div class="col">
    <h2 class="text-white"><?=$this->title;?></h2>
  </div>
  <div class="col">
      <?= \frontend\widgets\StoreSearchForm::widget([])?>
  </div>
</div>
<div class="store-index">
  <?php
  $is_favorited = in_array($model->id, $favorites);
  ?>
  <div class="item-store">
    <div class="row ">
      <div class="col col-image">
        <img src="<?=Html::encode($model->image);?>" class="img-fluid" alt="<?=Html::encode($model->name);?>">
      </div>
      <div class="col col-info">
        <h6 class="store-name"><?=Html::encode($model->name);?></h6>
        <p class="store-address"><label class="label-text mb-0">Địa chỉ:</label><?=Html::encode($model->address);?></p>
        <p class="store-phone"><label class="label-text mb-0">Liên hệ:</label><?=Html::encode($model->phone);?></p>
        <a href="javascript:void(0)" title="<?=$is_favorited ? 'Thêm vào yêu thích' : 'Đã yêu thích';?>" data-store="<?= $model->id;?>" class="store-favorite <?= $is_favorited ? 'favorited' : ''?>"><i class="far fa-star"></i></a>
      </div>
    </div>
  </div>
</div>

<?php
$url_store_favorite = \yii\helpers\Url::to(['ajax/store-favorite']);
$script = <<< JS
$('.store-favorite').on('click', function(e) {
  var store = $(this).data('store');
  var elem = this;
  $.ajax({
      method: "POST",
      url: "$url_store_favorite",
      data: { id:store}
  }).done(function( res ) {
      if (res.success) {
          if(res.remove == 1) {
            $(elem).removeClass('favorited');
            $(elem).prop('title', 'Thêm vào yêu thích');
          } else{
              $(elem).addClass('favorited');
            $(elem).prop('title', 'Đã yêu thích');
          } 
      }
      
  });
});
JS;
$this->registerJs($script);
?>
