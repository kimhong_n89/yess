<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \common\models\FavoriteStore;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var backend\models\search\StoreSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('backend', 'Nhà thuốc yêu thích');
$this->params['breadcrumbs'][] = $this->title;
$favorites = ArrayHelper::getColumn(FavoriteStore::find()->where(['doctor_id' => Yii::$app->user->identity->id])->asArray()->all(), 'store_id');
?>
<div class="row content-top style-primary">
  <div class="col">
    <h2 class="text-white"><?=$this->title;?></h2>
  </div>
  <div class="col">
      <?= \frontend\widgets\StoreSearchForm::widget([])?>
  </div>
</div>

<?php //\yii\widgets\Pjax::begin(['id' => 'store-pjax']);?>
<div class="store-index">
    <?php
    echo \yii\widgets\ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item-store'],
        'itemView' => '_list_item',
        'viewParams' => ['favorites' => $favorites],
        'summary' => '',
        'pager' => [
            'firstPageLabel' => '<div class="double-icon"><i class="fas fa-chevron-left"></i><i class="fas fa-chevron-left"></i></div>',
            'lastPageLabel' => '<div class="double-icon"><i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i></div>',
            'prevPageLabel' => '<i class="fas fa-chevron-left"></i>',
            'nextPageLabel' => '<i class="fas fa-chevron-right"></i>',
            'options' => ['class' => 'table-responsive']
        ],
    ]);?>
</div>
<?php //\yii\widgets\Pjax::end();?>

<?php
$url_store_favorite = \yii\helpers\Url::to(['ajax/store-favorite']);
$script = <<< JS
$('.store-favorite').on('click', function(e) {
  var store = $(this).data('store');
  var elem = this;
  $.ajax({
      method: "POST",
      url: "$url_store_favorite",
      data: { id:store}
  }).done(function( res ) {
      if (res.success) {
          if(res.remove == 1) {
            $(elem).removeClass('favorited');
            $(elem).prop('title', 'Thêm vào yêu thích');
          } else{
              $(elem).addClass('favorited');
            $(elem).prop('title', 'Đã yêu thích');
          } 
      }
      
  });
});
JS;
$this->registerJs($script);
?>
