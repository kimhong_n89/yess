<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use common\models\District;
use common\models\Province;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use common\models\Ward;
use common\models\Prescription;
use common\models\PrescriptionDetail;
use common\models\Doctor;
use common\widgets\dynamicform\src\DynamicFormWidget2;
use common\models\DiseaseGroup;
use kartik\widgets\DatePicker;

/**
 * @var yii\web\View $this
 * @var common\models\Patient $modelPatient
 * @var common\models\Prescription $model
 * @var yii\bootstrap4\ActiveForm $form
 */
$provinces = ArrayHelper::map(Province::find()->orderBy('name')->all(), 'id_state', 'name');

$listDistricts = $listWards = [];
if ($modelPatient->province_id) {
    $districts = District::find()->where(['id_state' => $modelPatient->province_id])->orderBy('name')->all();
    $listDistricts = ArrayHelper::map($districts, 'id_district', 'name');
}
if ($modelPatient->district_id) {
    $wards = Ward::find()->where(['id_district' => $modelPatient->district_id])->orderBy('name')->all();
    $listWards = ArrayHelper::map($wards, 'id_ward', 'name');
}

$diagnose = ArrayHelper::map(DiseaseGroup::find()->orderBy('name')->all(), 'id', 'name');
?>
<div class="row">
  <div class="col form-content">
    <div class="prescription-form custom-form text-black">
        <div class="row block-info">
          <div class="col-6">
            <span class="label-text">Mã đơn thuốc</span>DT<?=$model->id?>-<?=date('dmY', $model->created_at ? $model->created_at : time());?>
          </div>
          <div class="col-6 text-sm-right">
            <span class="label-text">Ngày tạo</span><?=date('d/m/Y', $model->created_at ? $model->created_at : time());?><span class="ml-3"><?= str_replace('{%1}', 'h', date('H{%1}i', $model->created_at ? $model->created_at : time()));?></span>
          </div>
          <div class="col-sm-12">
            <h1 class="text-center font-weight-bold text-uppercase">Toa thuốc</h1>
          </div>
        </div>
        <?php $form = ActiveForm::begin([
            'id' => 'dynamic-form',
            'validateOnBlur' => false,
            'enableAjaxValidation' => false,
            'validateOnChange' => false,
        ]); ?>
          <div class="row">
            <div class="col-sm col-12 ">
                <?php echo $form->field($modelPatient, 'fullname', [
                    'options' => ['class' => 'form-group d-sm-flex align-items-center'],
                    'labelOptions' => [
                        'class' => 'mb-sm-0',
                    ],
                    'template' => '{label} <div class="col p-0">{input}<button class="cancel-autocomplete" type="button"><i class="fas fa-times"></i></button></div>'
                ])->widget(\yii\jui\AutoComplete::classname(), [
                    'clientOptions' => [
                        'source' => Url::to(['ajax/find-patient']),
                        'minLength'=>'2',
                        'select' => new JsExpression("
                      function( event, ui ) {
                          if(event.type == 'autocompleteselect') {
                              $('#prescription-patient_id').val(ui.item.id);
                              $('#patient-phone').val(ui.item.phone);
                              $('#patient-email').val(ui.item.email);
                              $('#patient-gender').val(ui.item.gender).change();
                              $('#patient-year_of_birth').val(ui.item.year);
                              $('#sel-province').val(ui.item.province).change();
                              $('#sel-province').trigger('select2:select');
                              $('#sel-district').on('depdrop:afterChange', function(event, id, value, jqXHR, textStatus) {
                                    $('#sel-district').val(ui.item.district).change();
                                    $('#sel-district').trigger('select2:select');  
                              });
                              $('#sel-ward').on('depdrop:afterChange', function(event, id, value, jqXHR, textStatus) {
                                    $('#sel-ward').val(ui.item.ward).change();
                              });
                              $('#patient-address').val(ui.item.address);
                              $('#patient-fullname').attr('readonly', true);
                              $('#patient-year_of_birth').attr('readonly', true);
                              $('.col-gender').addClass('readonly');
                              $('#patient-phone').attr('readonly', true);
                              $('#patient-email').attr('readonly', true);
                              $('#patient-address').attr('readonly', true);
                              $('.col-province').addClass('readonly');
                              $('.col-district').addClass('readonly');
                              $('.col-ward').addClass('readonly');
                          }
                    }"),
                    ],
                    'options'=>[
                        'class' => 'form-control',
                        'autocomplete' => 'chrome-off',
                        'readonly' => $model->patient_id ? true : false
                    ]
                ]) ?>
            </div>
            <div class="col p-sm-0 col-year-age">
                <?php echo $form->field($modelPatient, 'year_of_birth', [
                    'options' => ['class' => 'form-group d-sm-flex align-items-center'],
                    'labelOptions' => [
                        'class' => 'mb-sm-0',
                    ],
                    'template' => '{label} <div class="col p-0">{input}</div>',
                ])->textInput(['type' => 'number', 'class' => 'form-control', 'min' => '1900', 'placeholder' => '', 'readonly' => $model->patient_id ? true : false]) ?>
            </div>
            <div class="col col-gender <?=$model->patient_id ? 'readonly' : '';?>">
                <?php echo $form->field($modelPatient, 'gender', [
                    'options' => ['class' => 'form-group d-sm-flex align-items-center'],
                    'labelOptions' => [
                        'class' => 'mb-sm-0',
                    ],
                    'template' => '{label} <div class="col p-0">{input}</div>',
                ])->widget(Select2::classname(), [
                    'data' => $modelPatient::genders(),
                    'options' => [
                        'placeholder' => Yii::t('backend', 'Chọn'),
                    ],
                ])?>
            </div>
          </div>
          <div class="row">
            <div class="col-6">
                <?php echo $form->field($modelPatient, 'phone', [
                    'options' => ['class' => 'form-group d-sm-flex align-items-center'],
                    'labelOptions' => [
                        'class' => 'mb-sm-0',
                    ],
                    'template' => '{label} <div class="col p-0">{input}{error}{hint}</div>',
                ])->textInput(['maxlength' => true, 'readonly' => $model->patient_id ? true : false]) ?>
            </div>
            <div class="col-6">
                <?php echo $form->field($modelPatient, 'email', [
                    'options' => ['class' => 'form-group d-sm-flex align-items-center'],
                    'template' => '{label} <div class="col p-0">{input}{error}{hint}</div>',
                    'labelOptions' => [
                        'class' => 'mb-sm-0',
                    ],
                ])->textInput(['maxlength' => true, 'type' => 'email', 'readonly' => $model->patient_id ? true : false]) ?>
            </div>
          </div>
          <label class="font-weight-bold">Địa chỉ</label>
          <div class="row">
            <div class="col-sm col-6">
                <?php echo $form->field($modelPatient, 'address', [
                    'labelOptions' => [
                        'class' => 'small',
                    ]
                ])->textInput(['maxlength' => true, 'readonly' => $model->patient_id ? true : false]) ?>
            </div>
            <div class="col-sm col-6 col-province <?=$model->patient_id ? 'readonly' : '';?>">
                <?php echo $form->field($modelPatient, 'province_id', [
                    'labelOptions' => [
                        'class' => 'small',
                    ]
                ])->widget(Select2::classname(), [
                    'data' => $provinces,
                    'options' => [
                        'id' => 'sel-province',
                        'placeholder' => Yii::t('backend', 'Chọn Khu Vực')
                    ],
                ]);?>
            </div>
            <div class="col-sm col-6 col-district <?=$model->patient_id ? 'readonly' : '';?>">
                <?php echo $form->field($modelPatient, 'district_id', [
                    'labelOptions' => [
                        'class' => 'small',
                    ]
                ])->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'data' => $listDistricts,
                    'options' => [
                        'id' => 'sel-district',
                        'placeholder' => Yii::t('backend', 'Chọn Quận')
                    ],
                    'pluginOptions' => [
                        'depends' => [
                            'sel-province'
                        ],
                        'url' => Url::to([
                            '/ajax/get-districts'
                        ])
                    ]
                ]);?>
            </div>
            <div class="col-sm col-6 col-ward <?=$model->patient_id ? 'readonly' : '';?>">
                <?php echo $form->field($modelPatient, 'ward_id', [
                    'labelOptions' => [
                        'class' => 'small',
                    ]
                ])->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'data' => $listWards,
                    'options' => [
                        'id' => 'sel-ward',
                        'placeholder' => Yii::t('backend', 'Chọn Phường')
                    ],
                    'pluginOptions' => [
                        'depends' => [
                            'sel-district'
                        ],
                        'url' => Url::to([
                            '/ajax/get-wards'
                        ])
                    ]
                ]);?>
            </div>
          </div>
            <?php echo Html::activeHiddenInput($model, 'patient_id'); ?>
            <div class="row">
              <div class="col col-insurance-number">
                  <?php echo $form->field($model, 'insurance_number', [
                      'options' => ['class' => 'form-group d-sm-flex align-items-center'],
                      'template' => '{label} <div class="col p-0">{input}</div>',
                  ])->textInput(['maxlength' => true]) ?>
              </div>
              <div class="col p-0 col-use-insurance">
                  <?php echo $form->field($model, 'is_use_insurance', [
                      'options' => ['class' => 'form-group d-sm-flex align-items-center'],
                      'labelOptions' => [
                          'class' => 'mb-sm-0',
                      ],
                      'template' => '{label} <div class="col p-0">{input}</div>',
                  ])->widget(Select2::classname(), [
                      'data' => Prescription::yesno(),
                      'options' => [
                          'id' => 'sel-insurance',
                          'placeholder' => Yii::t('backend', 'Chọn')
                      ],
                  ]);?>
              </div>
              <div class="col col-insurance-rate">
                  <?php echo $form->field($model, 'insurance_rate', [
                      'options' => ['class' => 'form-group d-sm-flex align-items-center'],
                      'labelOptions' => [
                          'class' => 'mb-sm-0 d-sm-none',
                      ],
                      'template' => '{label} <div class="col p-0">{input}</div>',
                  ])->widget(Select2::classname(), [
                      'data' => Prescription::insuranceRates(),
                      'options' => [
                          'id' => 'sel-insurance-rate',
                          'placeholder' => Yii::t('backend', 'Chọn %'),
                          'disabled' => $model->is_use_insurance ? false : true
                      ],
                  ]);?>
              </div>
            </div>
              <?php echo $form->field($model, 'arr_diagnoses', [
                  'options' => ['class' => 'form-group d-sm-flex align-items-center'],
                  'labelOptions' => [
                      'class' => 'mb-sm-0',
                  ],
                  'template' => '{label} <div class="col p-0">{input}{error}{hint}</div>',
              ])->widget(Select2::classname(), [
                  'data' => $diagnose,
                  'options' => [
                      'id' => 'sel-diagnose',
                      'placeholder' => Yii::t('backend', 'Chọn'),
                      'multiple' => true
                  ],
                  'pluginEvents' => [
                      "change" => "function(e) {
                          var data = $('#sel-diagnose').val();
                          $.pjax.reload({container: '#product-suggestion-pjax', replace: false, url: '" . addParamToUrl(\Yii::$app->request->url, 'suggest=1') . "&diagnose=' + data });
                      }"
                  ]
              ]);?>


            <div id="prescription-insurance-list" class="my-5">
              <h3 class="heading-prescription">Toa thuốc BHYT</h3>
                <?php DynamicFormWidget2::begin([
                    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                    'widgetBody' => '.container-items', // required: css class selector
                    'widgetItem' => '.item-' . PrescriptionDetail::TYPE_MEDICINE_INSURANCE, // required: css class
                    'limit' => 20, // the maximum times, an element can be cloned (default 999)
                    'min' => $min_insurance, // 0 or 1 (default 1)
                    'insertButton' => '.add-item', // css class
                    'deleteButton' => '.remove-item', // css class
                    'model' => $modelMedicineInsurance[0],
                    'formId' => 'dynamic-form',
                    'formFields' => [
                        'medicine_id',
                        'amount',
                        'dosage_form_id',
                        'amount_morning',
                        'amount_noon',
                        'amount_afternoon',
                        'time_of_use',
                        'drugged_time'
                    ],
                ]); ?>

                  <div class="container-items"><!-- widgetContainer -->
                  <?php
                  foreach ($modelMedicineInsurance as $index => $modelDetail) {
                      echo $this->render('form/_form_detail', [
                          'model' => $model,
                          'index' => $index,
                          'modelDetail' => $modelDetail,
                          'form' => $form,
                      ]);
                  }
                  ?>
                  </div>
                  <button type="button" class="add-item btn btn-primary">Thêm thuốc</button>
                  <h6 class="total-line">Cộng khoản: <span></span></h6>
                <?php DynamicFormWidget2::end(); ?>
            </div>


            <div id="prescription-service-list" class="my-5">
              <h3 class="heading-prescription">Toa thuốc dịch vụ</h3>
                <?php DynamicFormWidget2::begin([
                    'widgetContainer' => 'dynamicform_wrapper_service', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                    'widgetBody' => '.container-service-items', // required: css class selector
                    'widgetItem' => '.item-' . PrescriptionDetail::TYPE_MEDICINE_SERVICE, // required: css class
                    'limit' => 20, // the maximum times, an element can be cloned (default 999)
                    'min' => $min_service, // 0 or 1 (default 1)
                    'insertButton' => '.add-service-item', // css class
                    'deleteButton' => '.remove-item', // css class
                    'model' => $modelMedicineService[0],
                    'formId' => 'dynamic-form',
                    'formFields' => [
                        'medicine_id',
                        'amount',
                        'dosage_form_id',
                        'amount_morning',
                        'amount_noon',
                        'amount_afternoon',
                        'time_of_use',
                        'drugged_time'
                    ],
                ]); ?>

              <div class="container-service-items"><!-- widgetContainer -->
                  <?php
                  foreach ($modelMedicineService as $index => $modelDetail) {
                      echo $this->render('form/_form_detail', [
                          'model' => $model,
                          'index' => $index,
                          'modelDetail' => $modelDetail,
                          'form' => $form,
                      ]);
                  }
                  ?>
              </div>
              <button type="button" class="add-service-item btn btn-primary">Thêm thuốc</button>
              <h6 class="total-line">Cộng khoản: <span></span></h6>
                <?php DynamicFormWidget2::end(); ?>
            </div>
            <?php echo $form->field($model, 'note', [
                'labelOptions' => [
                    'class' => 'heading-prescription',
                ],
            ])->textarea(['maxlength' => true]) ?>
            <div class="d-flex align-items-center">
                <?php echo $form->field($model, 're_examination', ['options' => ['class' => 'form-group mr-4 re-examination']])->checkbox(); ?>
                <div class="re-examination-date" style="display: <?= $model->re_examination ? 'block' : 'none';?>">
                    <?php echo $form->field($model, 're_examination_date', [
                        'template' => '{label}{input}',
                    ])->widget(\kartik\widgets\DateTimePicker::class, [
                        'type' => \kartik\widgets\DateTimePicker::TYPE_INPUT,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd/mm/yyyy hh:ii',
                            'todayHighlight' => true,
                            'todayBtn' => true,
                        ],
                        'options' => [
                            'placeholder' => 'Chọn ngày tái khám'
                        ],
                        'removeButton' => false,
                    ])->label(false)?>
                </div>
            </div>

              <div class="row align-items-end block-bottom">
                <div class="col-sm-6 col-5">
                  <div><strong>Người nhận thuốc</strong> <span class="text-mute text-small">(Ký tên)</span></div>
                </div>
                <div class="col-sm-6 col-7 text-center">
                    <div class="float-right">
                      <div class="font-italic"><?= sprintf("Ngày &nbsp; %s &nbsp; tháng  &nbsp; %s &nbsp; năm &nbsp; %s", date('d'), date('m'), date('Y'));?></div>
                      <div class="font-weight-bold text-uppercase mt-4">Bác sĩ khám bệnh</div>
                      <div class="sign-space"></div>
                      <div class="mb-4"><strong>Bác sĩ <span class="text-uppercase"><?= Html::encode($model->doctor->fullname);?></span></strong></div>
                      <div><strong>Tổng tiền thuốc </strong><span id="total-amount"><?= Yii::$app->formatter->asInteger($model->total_amount);?></span> VNĐ</div>
                    </div>
                </div>
              </div>
              <?php if(empty($hideButton)):?>
                <div class="text-center my-5">
                    <?php echo Html::submitButton(Yii::t('frontend', 'Lưu toa thuốc'), ['value' => 'save', 'name' => 'btn-submit', 'class' => 'btn btn-dark-blue mx-1']) ?>
                    <?php echo Html::submitButton(Yii::t('frontend', 'Hoàn tất & Xem toa'), ['value' => 'done',  'name' => 'btn-submit', 'class' => 'btn btn-primary']) ?>
                </div>
              <?php endif;?>
        <?php ActiveForm::end(); ?>
    </div>
  </div>
  <div class="col space-right">
      <?php \yii\widgets\Pjax::begin([
          'id' => 'product-suggestion-pjax',
          'enablePushState' => false,
          'enableReplaceState' => false
      ]);?>
  <?php
      if (isset($_GET['diagnose'])) {
          $_diagnose = Yii::$app->request->get('diagnose');
          $model->arr_diagnoses = explode(',', $_diagnose);
      }
      $activeTab = $activeTabPane = true;
      if ($model->arr_diagnoses) {
        foreach ($model->arr_diagnoses as $diagnose) {
          $diseaseGroup = DiseaseGroup::findOne($diagnose);
          if (!$diseaseGroup) continue;
          echo "<h6 class='diagnose-name'>{$diseaseGroup->name}</h6>";
          $feature_values = \common\models\FeatureValue::find()
              ->innerJoin(\common\models\DiseaseFeatureValue::tableName(), 'feature_value.id=disease_feature_value.feature_value_id')
              ->andWhere(['disease_id' => $diagnose])->all();
          if (!$feature_values) continue;
          ?>
          <ul class="nav flex-column feature-nav" role="tablist">
            <?php foreach ($feature_values as $feature_value): ?>
              <li class="nav-item" role="presentation">
                <a class="nav-link feature-tab <?= $activeTab ? 'active' : ''?>" id="tab-feature-<?=$feature_value->id?>" data-toggle="tab" href="#feature-<?=$feature_value->id?>" role="tab" aria-controls="feature-<?=$feature_value->id?>" aria-selected="true"><?= $feature_value->value;?></a>
              </li>
              <?php
              $activeTab = false;
              ?>
            <?php endforeach;?>
          </ul>

          <div class="tab-content">
            <h6 >Thuốc tương ứng</h6>
            <?php
            foreach ($feature_values as $feature_value) {
                $productList = \common\models\Product::find()
                    ->innerJoin(\common\models\ProductFeature::tableName(), 'product.id=product_feature.product_id')
                    ->andWhere(['feature_value_id' => $feature_value->id])->all();
                ?>
              <div class="tab-pane fade <?= $activeTabPane ? 'active show' : ''?>" id="feature-<?=$feature_value->id?>" role="tabpanel" aria-labelledby="tab-feature-<?=$feature_value->id?>">
                  <?php if (empty($productList)) :?>
                      <p class="no-product">Không có thuốc tương ứng với <?= $feature_value->value;?></p>
                  <?php else :?>
                    <?php $productLists = array_chunk($productList,3)?>
                    <div id="carouselProduct<?=$feature_value->id?>" class="carousel slide">
                      <div class="carousel-inner">
                      <?php foreach ($productLists as $index => $products): ?>
                          <div class="carousel-item <?= $index === 0 ? 'active' : ''?>">
                            <?php foreach ($products as $product): ?>
                              <div class="product-item clearfix">
                                <div class="product-name"><?=$product->name?></div>
                                <img class="product-img img-fluid" src="<?= @json_decode($product->images)[0]?>"/>
                                <a href="<?=$product->rewrite?>" target="_blank" class="product-link">Chi tiết</a>
                              </div>
                            <?php endforeach;?>
                          </div>
                      <?php endforeach;?>
                      </div>
                      <ol class="carousel-indicators position-relative">
                          <?php foreach ($productLists  as $index => $products): ?>
                            <li data-target="#carouselProduct<?=$feature_value->id?>" data-slide-to="<?=$index?>" class="<?= $index === 0 ? 'active' : ''?>"></li>
                          <?php endforeach;?>
                      </ol>
                    </div>
                  <?php endif;?>
              </div>
              <?php
                $activeTabPane = false;
              ?>
            <?php } ?>
          </div>
       <?php }
        }
      ?>
      <?php \yii\widgets\Pjax::end();?>
  </div>
</div>
<?php
$yes = Prescription::YES;
$url_add_template = Url::to(['prescription/add-template']);
$script = <<< JS
    jQuery(document).ready(function() {
      $(".dynamicform_wrapper .total-line span").html($('.dynamicform_wrapper .medicine-item-line').length);
      $(".dynamicform_wrapper_service .total-line span").html($('.dynamicform_wrapper_service .medicine-item-line').length);
    })
    $('.cancel-autocomplete').on('click', function() {
      $('#patient-fullname').val('').attr('readonly', false);
      $('#patient-year_of_birth').attr('readonly', false);
      $('.col-gender').removeClass('readonly');
      $('#patient-phone').attr('readonly', false);
      $('#patient-email').attr('readonly', false);
      $('#patient-address').attr('readonly', false);
      $('.col-province').removeClass('readonly');
      $('.col-district').removeClass('readonly');
      $('.col-ward').removeClass('readonly');
      $('#prescription-patient_id').val('');
    })
    
    $('#prescription-insurance_number').on('change', function() {
        if ($(this).val().length > 0) {
            $('#sel-insurance').val(1).change();
        } else {
            $('#sel-insurance').val(0).change();
        }
        $('#sel-insurance').trigger('select2:select');
    })
    
    $('#sel-insurance').on('select2:select', function() {
        if ($(this).val() == $yes) {
            $('#sel-insurance-rate').removeAttr('disabled');
        } else {
            $('#sel-insurance-rate').attr('disabled', true);
            $('#sel-insurance-rate').val(null).trigger('change');
            $('#prescription-insurance_number').val('');
        }
    })
    
    $('.add-medicine').on('click', function() {
        var target = $(this).data('target');
        var type = $(this).data('type');
        $.ajax({
          method: "GET",
          url: "$url_add_template",
          data: { type: type}
        }).done(function( html ) {
            $('#'+target).append(html);
        });
    })
    
    jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
        var total = 0
        jQuery(".dynamicform_wrapper .medicine-item-line .serial-number").each(function(index) {
            jQuery(this).html((index + 1));
            total++;
        });
        jQuery(".dynamicform_wrapper .total-line span").html(total);
        
    });
    
    jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {
        var total = 0
        jQuery(".dynamicform_wrapper .medicine-item-line .serial-number").each(function(index) {
            jQuery(this).html((index + 1));
            total++;
        });
        jQuery(".dynamicform_wrapper .total-line span").html(total);
        updateTotalAmount();
    });
    
    jQuery(".dynamicform_wrapper_service").on("afterInsert", function(e, item) {
        var total = 0
        jQuery(".dynamicform_wrapper_service .medicine-item-line .serial-number").each(function(index) {
            jQuery(this).html((index + 1));
            total++;
        });
        jQuery(".dynamicform_wrapper_service .total-line span").html(total);
    });
    
    jQuery(".dynamicform_wrapper_service").on("afterDelete", function(e) {
        var total = 0
        jQuery(".dynamicform_wrapper_service .medicine-item-line .serial-number").each(function(index) {
            jQuery(this).html((index + 1));
                total++;
        });
        jQuery(".dynamicform_wrapper_service .total-line span").html(total);
        updateTotalAmount()
    });
    
    $("#prescription-re_examination").change(function() {
        if($('input[name="Prescription[re_examination]"]:checked').val() == 1){
            $(".re-examination-date").show();
        } else {
            $(".re-examination-date").hide();
        }
          
    });
JS;
$this->registerJs($script);
?>