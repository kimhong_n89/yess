<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
/**
 * @var yii\web\View $this
 */

$this->title = 'Yess - Toa thuốc online';
?>
<style>
  @media print {
    @page { margin: 0; box-shadow: none}
    body { margin: 15px 15px; }
    .main-content {
      max-width: 100%;
      flex: 100%;
    }
  }
</style>
<div class="view-printed">
<?php if(!$model->mail_sent):?>
  <div class="d-print-none alert alert-warning mb-2 text-center" role="alert">Toa thuốc chưa được gửi qua email của khách hàng. Vui lòng gửi thử lại sau.</div>
<?php endif; ?>
<?= $model->html; ?>
</div>
<div class="d-print-none py-4 text-center">
    <?= Html::a('In toa', 'javascript:void(0)', [
        'class' => 'btn btn-primary mx-1',
        'onclick' => 'window.print()'
    ])?>

    <?php
    if (!$model->mail_sent) {
        echo Html::a('Gửi toa qua mail', ['send-via-mail', 'token' => $token], [
            'class' => 'btn btn-dark-blue',
            'role' => 'modal-remote',
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'title' => 'Gửi toa qua mail',
            'data-confirm-title' => 'Bạn có chắc chắn không?',
            'data-confirm-message' => 'Bạn có chắc muốn gửi toa thuốc này cho khách hàng không?',
            'data-confirm-cancel' => 'Không',
            'data-confirm-ok' => 'Có',
        ]);
    }
    ?>
</div>
<?php
$script = <<< JS
    jQuery(document).ready(function() {
      window.print();
    })
JS;
$this->registerJs($script);
?>