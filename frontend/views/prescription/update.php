<?php

/**
 * @var yii\web\View $this
 * @var common\models\Prescription $model
 */

$this->title = Yii::t('frontend', 'Sửa toa thuốc');
?>
<div class="prescription-update">
  <div class="row content-top style-primary">
    <div class="col">
      <h2 class="text-white"><?=$this->title;?></h2>
    </div>
    <div class="col">
        <?= \frontend\widgets\GlobalSearch::widget([])?>
    </div>
  </div>
    <?php echo $this->render('_form', [
        'model' => $model,
        'modelPatient' => $modelPatient,
        'modelMedicineInsurance' => $modelMedicineInsurance,
        'modelMedicineService' => $modelMedicineService,
        'min_insurance' => $min_insurance,
        'min_service' => $min_service,
    ]) ?>

</div>
