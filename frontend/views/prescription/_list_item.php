<?php
use common\models\Prescription;
use \yii\helpers\Html;
/**
 * @var \common\models\Prescription $model
 */

?>

<div class="card text-black">
    <div class="card-header bg-transparent">
      <div class="row">
        <div class="col-sm-6"><label class="label-text">Họ và tên</label><span class="text-uppercase"><?= Html::encode($model->patient->fullname);?></span></div>
        <div class="col-sm-6 text-sm-right">
        <label class="label-text">Mã đơn thuốc</label>DT<?=$model->id?>-<?=date('dmY', $model->created_at);?>
        </div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-sm col-8 col-patient font-weight-bold"><?= implode(' / ', \yii\helpers\ArrayHelper::getColumn($model->diagnoses, 'name'));?></div>
        <div class="col-sm col-4 col-total-amount">
          <?= number_format($model->total_amount, 0, '.','.')?>
          <div>VNĐ</div>
        </div>
        <div class="col-sm col-12 col-status">
          <strong><?= @Prescription::statuses()[$model->status];?></strong>
          <div class="text-mute"><?= @Prescription::statusNotes()[$model->status];?></div>
        </div>
        <div class="col-sm col-12 col-provider">
          <?php if ($model->store):?>
            <div><?=$model->store->name?></div>
            <?= Html::a('Xem chi tiết', ['/store/view', 'id' => $model->store->id], ['class' => 'text-orange d-inline-block mt-1']);?>
          <?php endif;?>
        </div>
        <div class="col-sm col-12 col-actions text-center">
          <div class="row justify-content-center align-items-end h-100">
              <div class="mb-2 col-12">
                  <?php
                      if ($model->status != Prescription::STATUS_DRAFT) {
                          echo Html::a('<img class="" src="/img/icons/print-icon.png">In toa', ['print', 'id' => $model->id], [
                              'class' => 'btn btn-print',
//                              'role' => 'modal-remote',
//                              'data-request-method' => 'post',
//                              'data-toggle' => 'tooltip',
//                              'title' => 'In toa thuốc',
//                              'data-confirm-title' => 'Bạn có chắc chắn không?',
//                              'data-confirm-message' => 'Bạn có chắc muốn in toa thuốc này không?',
//                              'data-confirm-cancel' => 'Không',
//                              'data-confirm-ok' => 'Có',
                              'target' => '_blank',
                              'data-method' => 'post',
                              'data-confirm' => 'Bạn có chắc muốn in toa thuốc này không?',
                          ]);
                      }
                  ?>
              </div>
              <div class="">
                <?php
                  if ($model->status != Prescription::STATUS_DELIVERY_TO_STORE && $model->status != Prescription::STATUS_DELETED) {
                    echo Html::a('Sửa', ['update', 'id' => $model->id], ['class' => 'btn btn-update mx-1']);
                    echo Html::a('Xoá', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-delete mx-1',
//                        'role' => 'modal-remote',
//                        'data-request-method' => 'post',
//                        'data-toggle' => 'tooltip',
//                        'title' => 'Xoá toa thuốc',
//                        'data-confirm-title' => 'Bạn có chắc chắn không?',
//                        'data-confirm-message' => 'Bạn có chắc muốn xoá toa thuốc này không?',
//                        'data-confirm-cancel' => 'Không',
//                        'data-confirm-ok' => 'Có',
                        'data-method' => 'post',
                        'data-confirm' => 'Bạn có chắc muốn xoá toa thuốc này không?',
                    ]);
                  }
                ?>
              </div>
          </div>
        </div>
      </div>
    </div>
</div>
