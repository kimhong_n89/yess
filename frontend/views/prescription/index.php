<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\controllers\PrescriptionController;

/**
 * @var yii\web\View $this
 * @var backend\models\search\PrescriptionSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = Yii::t('backend', 'Toa thuốc');
$classDraft = $classDone = $classDeleted = 'nav-link nav-link mb-sm-3 mb-md-0';
$params = ['prescription/index'];
if ($searchModel->keyword) {
    $params['PrescriptionSearch[keyword]'] = $searchModel->keyword;
}

switch ($tab) {
    case PrescriptionController::TAB_DONE:
        $classDone .= ' active';
        break;
    case PrescriptionController::TAB_DELETED:
        $classDeleted .= ' active';
        break;
    default:
        $classDraft .= ' active';
}

?>
<div class="row content-top style-primary">
  <div class="col align-self-end">
    <div class="row align-items-center">
      <div class="col-md-12">
        <ul class="nav" role="tablist">
          <li class="nav-item">
              <?= Html::a(Yii::t('checkin', 'Đang tạo'), $params + ['tab' => PrescriptionController::TAB_DRAFT], ['class' => $classDraft]);?>
          </li>
          <li class="nav-item">
              <?= Html::a(Yii::t('checkin', 'Hoàn thành'), $params + ['tab' => PrescriptionController::TAB_DONE], ['class' => $classDone]);?>
          </li>
          <li class="nav-item">
              <?= Html::a(Yii::t('checkin', 'Đã huỷ'), $params + ['tab' => PrescriptionController::TAB_DELETED], ['class' => $classDeleted]);?>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="col d-sm-block d-none">
      <?= \frontend\widgets\GlobalSearch::widget([])?>
  </div>
</div>

<?php //\yii\widgets\Pjax::begin(['id' => 'prescription-pjax']);?>
<div class="prescription-index">
    <div class="card list-header sticky-top d-sm-block d-none">
      <div class="card-body">
      <div class="row">
        <div class="col my-auto col-patient">Khách hàng</div>
        <div class="col my-auto col-total-amount">Tổng<br/>thanh toán</div>
        <div class="col my-auto col-status">Trạng thái</div>
        <div class="col my-auto col-provider">Cung cấp</div>
        <div class="col my-auto col-actions">Thao tác</div>
      </div>
      </div>
    </div>
    <div class="d-sm-none d-block mb-20 mt-20">
        <?= \frontend\widgets\GlobalSearch::widget(['options' => ['class' => 'input-center border-primary']])?>
    </div>
    <?php
    echo \yii\widgets\ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'prescription-item'],
        'itemView' => '_list_item',
        'summary' => '',
        'pager' => [
            'firstPageLabel' => '<div class="double-icon"><i class="fas fa-chevron-left"></i><i class="fas fa-chevron-left"></i></div>',
            'lastPageLabel' => '<div class="double-icon"><i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i></div>',
            'prevPageLabel' => '<i class="fas fa-chevron-left"></i>',
            'nextPageLabel' => '<i class="fas fa-chevron-right"></i>',
            'options' => ['class' => 'table-responsive']
        ],
//        'pager' => [
//            'class' => \kop\y2sp\ScrollPager::className(),
//            'next' => '.page-item.next:not(".disabled") a',
//            'triggerOffset' => 3,
//            'noneLeftText' => ''
//        ]
    ]);?>
</div>
<?php //\yii\widgets\Pjax::end();?>
