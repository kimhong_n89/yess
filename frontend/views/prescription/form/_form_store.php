<?php
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;

/**
 * @var yii\web\View $this
 * @var \frontend\models\StoreForm $model
 * @var yii\bootstrap4\ActiveForm $form
 */
?>


<div class="template-form">
    <?php $form = \kartik\form\ActiveForm::begin(); ?>
    <?php echo $form->field($model, 'type')->widget(Select2::classname(), [
        'data' => \frontend\models\StoreForm::types(),
        'options' => [
            'id' => 'sel-store',
            'class' => 'form-control',
        ],
        'pluginOptions' => [
            'minimumResultsForSearch' => -1,
        ],
        'pluginEvents' => [
            //"change" => "function() { updateTotalAmount(); }"
        ]
    ])->label(false);?>
    <?php \yii\widgets\Pjax::begin([
        'id' => 'favorite-pjax',
        'enablePushState' => false,
        'enableReplaceState' => false
    ]);?>
    <?php
    echo \yii\widgets\ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => ''],
        'itemView' => function ($store, $key, $index, $widget) use ($form, $model){
            return  $form->field($model, 'store_id')->radio([
                'id' => "_rad_{$store->id}",
                'class' => 'custom-control-input',
                'value' => $store->id,
                'uncheck' => null,
                'template' => "{input}<label class='d-block custom-control-label center-radio' for='_rad_{$store->id}'><div class='item-store'>
                    <div class='row'>
                      <div class='col col-image'>
                        <img src='". Html::encode($store->image) ."' class='img-fluid' alt='". Html::encode($store->name) ."'>
                      </div>
                      <div class='col col-info'>
                          <h6 class='store-name'>". Html::encode($store->name) ."</h6>
                          <p class='store-address'><label class='label-text mb-0'>Địa chỉ:</label>". Html::encode($store->address) ."</p>
                          <p class='store-phone'><label class='label-text mb-0'>Liên hệ:</label>". Html::encode($store->phone)."</p>
                      </div>
                    </div>
                </div></label>"
            ]);
        },
        'summary' => '',
        'pager' => [
//            'firstPageLabel' => '<div class="double-icon"><i class="fas fa-chevron-left"></i><i class="fas fa-chevron-left"></i></div>',
//            'lastPageLabel' => '<div class="double-icon"><i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i></div>',
            'prevPageLabel' => '<i class="fas fa-chevron-left"></i>',
            'nextPageLabel' => '<i class="fas fa-chevron-right"></i>',
            'options' => ['class' => 'table-responsive']
        ],
    ]);?>
    <?php \yii\widgets\Pjax::end();?>
    <?php \kartik\form\ActiveForm::end(); ?>
</div>
