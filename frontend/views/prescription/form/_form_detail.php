<?php
use yii\helpers\Html;
use kartik\select2\Select2;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use common\models\Product;
use common\models\DosageForm;
use common\models\DruggedTime;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

/**
 * @var yii\web\View $this
 * @var \common\models\Prescription $model
 * @var common\models\PrescriptionDetail $modelDetail
 * @var yii\bootstrap4\ActiveForm $form
 */

$medicines = $productsExtraData = [];
if ($modelDetail->medicine_id) {
    $selectedProduct = Product::findOne($modelDetail->medicine_id);
    if($selectedProduct) {
        $medicines[$selectedProduct->id] = $selectedProduct->name;
        $productsExtraData[$selectedProduct->id] = ['data-price' => $selectedProduct->price];
    }
}

$dosage_forms = ArrayHelper::map(DosageForm::find()->asArray()->all(), 'id', 'name');
$drugged_times = ArrayHelper::map(DruggedTime::find()->asArray()->all(), 'id', 'name');
?>


<div class="card medicine-item-line item-<?=$modelDetail->type;?>">
    <button type="button" class="remove-item btn">Xoá</button>
    <div class="card-body">
        <div class="serial-number"><?=$index+1?></div>
        <div class="row">
            <div class="col-sm col-12 col-medicine">
                <?php echo $form->field($modelDetail, "[$index]medicine_id")->widget(Select2::classname(), [
                    'data' => $medicines,
                    'options' => [
                        'placeholder' => Yii::t('backend', 'Nhập chọn thuốc'),
                        'class' => 'sel-medicine-name form-control',
                        'options' => $productsExtraData
                    ],
                    'pluginOptions' => [
                        'ajax' => [
                            'url' => Url::to([
                                '/ajax/search-products'
                            ]),
                            'dataType' => 'json',
                            'delay' => 250,
                            'data' => new JsExpression('function(params) { 
                              var diagnose = $("#sel-diagnose").val();
                              return {q:params.term, page: params.page, diagnoses: diagnose}; }
                            '),
                            'processResults' => new JsExpression("function (data, params) {
                                params.page = params.page || 1;
                                return {
                                    results: data.items,
                                    pagination: {
                                        more: (params.page * 30) < data.total_count
                                    }
                                };
                            }"),
                            'cache' => true
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) {return markup; }'),
                        'templateResult' => new JsExpression('function(resp) { return resp.text; }'),
                        'templateSelection' => new JsExpression('function (resp) { 
                            if (resp.options != undefined) {
                                $(resp.element).attr("data-price", resp.options.price);
                            }
                            return resp.text; 
                        }'),
                    ],
                    'pluginEvents' => [
                        "change" => "function() { updateTotalAmount(); }"
                    ]
                ])->label(false);?>
            </div>
            <div class="col-sm col-6 col-amount">
                <?= $form->field($modelDetail, "[$index]amount", [
                    'options' => ['class' => 'form-group d-flex align-items-center'],
                    'template' => '{label} <div class="col p-0">{input}</div>',
                ])->textInput(['type' => 'number', 'min' => 1, 'placeholder' => 'số lượng', 'class' => 'medicine-amount form-control', 'onChange' =>'updateTotalAmount()'])->label(false);?>
            </div>
            <div class="col-sm col-6 col-dosage-form">
                <?= $form->field($modelDetail, "[$index]dosage_form_id", [
                    'options' => ['class' => 'form-group d-flex align-items-center'],
                    'template' => '{label} <div class="col p-0">{input}</div>',
                ])->widget(Select2::classname(), [
                    'data' => $dosage_forms,
                    'options' => [
                        'placeholder' => Yii::t('backend', 'Quy cách'),
                    ],
                ])->label(false);?>
            </div>
        </div>
        <div class="row">
          <div class="col-md-12 mb-2"><span class="text-sub">Liều dùng</span> <span class="text-mute text-small line-dosage-form"></span></div>

          <div class="col-sm col-4">
              <?= $form->field($modelDetail, "[$index]amount_morning", [
                  'options' => ['class' => 'hide-md-placeholder form-group d-sm-flex align-items-center'],
                  'labelOptions' => [
                      'class' => 'mb-sm-0',
                  ],
                  'template' => '{label} <div class="col p-0">{input}</div>',
              ])->textInput(['type' => 'number', 'min' => 1, 'placeholder' => 'số lượng']);?>
          </div>
          <div class="col-sm col-4">
              <?= $form->field($modelDetail, "[$index]amount_noon", [
                  'options' => ['class' => 'hide-md-placeholder form-group d-sm-flex align-items-center'],
                  'labelOptions' => [
                      'class' => 'mb-sm-0',
                  ],
                  'template' => '{label} <div class="col p-0">{input}</div>',
              ])->textInput(['type' => 'number', 'min' => 1, 'placeholder' => 'số lượng']);?>
          </div>
          <div class="col-sm col-4">
              <?= $form->field($modelDetail, "[$index]amount_afternoon", [
                  'options' => ['class' => 'hide-md-placeholder form-group d-sm-flex align-items-center'],
                  'labelOptions' => [
                      'class' => 'mb-sm-0',
                  ],
                  'template' => '{label} <div class="col p-0">{input}</div>',
              ])->textInput(['type' => 'number', 'min' => 1, 'placeholder' => 'số lượng']);?>
          </div>

          <div class="col-sm col-6 col-time-of-use">
              <?= $form->field($modelDetail, "[$index]time_of_use", [
                  'options' => ['class' => 'form-group d-sm-flex align-items-center'],
                  'labelOptions' => [
                      'class' => 'mb-sm-0',
                  ],
                  'template' => '<div class="col p-0">{input}</div>',
              ])->radioList(\common\models\PrescriptionDetail::timeUses())->label(false);?>
          </div>
          <div class="col-sm col-6 col-drugged-time">
              <?= $form->field($modelDetail, "[$index]drugged_time")->widget(Select2::classname(), [
                  'data' => $drugged_times,
                  'options' => [
                      'placeholder' => Yii::t('backend', 'Chọn thời điểm'),
                  ],
              ])->label(false);?>
          </div>

        </div>
    </div>
</div>
