<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use \common\models\DruggedTime;
use \common\models\DosageForm;
use common\models\PrescriptionDetail;
use common\models\Prescription;

/**
 * @var yii\web\View $this
 * @var common\models\Prescription $model
 */

$this->title = "Toa thuốc - DT{$model->id}-" . date('dmY', $model->created_at);
?>
<style>
  @media print {
    @page { margin: 0; box-shadow: none}
    body { margin: 1.6cm 1.6cm; }
    .main-content {
      max-width: 100%;
      flex: 100%;
      border: 0;
    }
  }
</style>
<div class="row content-top style-primary d-print-none">
  <div class="col">
    <h2 class="text-white"><?=$this->title;?></h2>
  </div>
  <div class="col">
      <?= \frontend\widgets\GlobalSearch::widget([])?>
  </div>
</div>
<?php echo $this->render('view/_view', ['model' => $model]);?>
<div class="d-print-none my-5 text-center">
  <?php if ($model->status != Prescription::STATUS_DELIVERY_TO_STORE):?>
      <?= Html::a('Chỉnh sửa toa', ['update', 'id' => $model->id], ['class' => 'btn btn-dark-blue'])?>
  <?php endif;?>
      <?= Html::a('In toa', ['print', 'id' => $model->id], [
          'class' => 'btn btn-primary mx-1',
          'target' => '_blank',
          'data-method' => 'post',
          'data-confirm' => 'Bạn có chắc muốn in toa thuốc này không?',
      ])?>
  <?php if ($model->status == Prescription::STATUS_DONE):?>
      <?= Html::a('Gửi toa', ['send-to-store', 'id' => $model->id], [
          'class' => 'btn btn-orange',
          'role' => 'modal-remote',
          'data-request-method' => 'post',
          'data-toggle' => 'tooltip',
          'title' => 'Gửi toa thuốc',
  ])?>
  <?php endif;?>
</div>


