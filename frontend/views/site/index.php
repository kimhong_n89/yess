<?php
use yii\widgets\Pjax;
use \yii\web\JsExpression;
use \yii\helpers\Json;
/**
 * @var yii\web\View $this
 */
$this->title = Yii::t('frontend', 'Trang chủ');
$defaultHigh = 200;
?>
<div class="dashboard">
  <div id="statistic">
<!--    <h5>--><?//= Yii::t('backend', 'Thống kê') ?><!--</h5>-->
    <div class="row">
      <div class="col-sm-4 col-6">
        <div class="statistic-block">
          <div>
            <h6>Toa thuốc<br>đang tạo</h6>
            <div class="text-dark-blue count"><?= $prescription_draft_count?></div>
          </div>
        </div>
      </div>
      <div class="col-sm-4 col-6">
        <div class="statistic-block">
          <div>
            <h6>Khách hàng<br>đã tiếp nhận</h6>
            <div class="text-primary count"><?= $customer_count?></div>
          </div>
        </div>
      </div>
      <div class="col-sm-4 col-6">
        <div class="statistic-block">
          <div>
            <h6>Toa thuốc<br>đã tạo</h6>
            <div class="text-blue count"><?= $prescription_done_count?></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php Pjax::begin(['id' => 'revenue-chart']) ?>
      <div class="chart">
<!--          <h5>--><?//= Yii::t('backend', 'Tổng quan') ?><!--</h5>-->
            <?=
            \common\widgets\chartist\Chartist::widget([
                'tagName' => 'div',
                'data' => new JsExpression(Json::encode([
                    'labels' => $chartData['labels'],
                    'series' => [
                        $chartData['series'],
                    ]
                ])),
                'chartOptions' => [
                    'options' => [
                        'high' => @$chartData['highest'] > $defaultHigh ? @$chartData['highest'] : $defaultHigh,
                        'seriesBarDistance' => 15,
                        'fullWidth' => true,
                        'chartPadding' => [
                            'top' => 20,
                            'right' => 20,
                            'bottom' => 20,
                        ],
                        'height' => '300px',
                        'plugins' => [
                            new JsExpression("Chartist.plugins.tooltip()"),
                            new JsExpression("Chartist.plugins.ctPointLabels({
                              textAnchor: 'middle',
                              color: '#ffa726'
                            })")
                        ]
                    ],
                ],
                'widgetOptions' => [
                    'type' => 'Line', // Bar, Line, or Pie, i.e. the chart types supported by Chartist.js
                    'useClass' => 'chartist-chart' // optional parameter, needs to be included in the htmlOptions class string as well if set! Forces the widget to use this class name as reference point for Chartist.js instead of an id
                ],
                'htmlOptions' => [
                    'class' => 'chartist-chart ct-chart', // ct-chart for CSS references; size of the charting area needs to be assigned as well
                    //...
                ]
            ]);
            ?>
      </div>
    <?php Pjax::end()?>
</div>
<?php
$script = <<< JS
    jQuery(document).ready(function() {
        var cw = $('.statistic-block').outerWidth();
        $('.statistic-block').css({'height':cw+'px'});
        $('.statistic-block').css('visibility', 'visible');
    })
    window.onresize = function() {
        var cw = $('.statistic-block').outerWidth();
        $('.statistic-block').css({'height':cw+'px'});
        $('.statistic-block').css('visibility', 'visible');
    }
JS;
$this->registerJs($script);
?>
