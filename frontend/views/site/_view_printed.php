<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 */

$this->title = 'Yess - Toa thuốc online';
?>
<style>
  @media print {
    @page { margin: 0; box-shadow: none}
    body { margin: 15px 15px; }
    .main-content {
      max-width: 100%;
      flex: 100%;
    }
  }
</style>
<div class="view-printed">
<?= $model->html; ?>
</div>
