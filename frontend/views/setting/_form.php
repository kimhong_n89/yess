<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use trntv\filekit\widget\Upload;
use yii\helpers\ArrayHelper;
use common\models\District;
use common\models\Province;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\Ward;
/**
 *
 * @var yii\web\View $this
 * @var common\models\Doctor $model
 * @var yii\bootstrap4\ActiveForm $form
 */

$provinces = ArrayHelper::map(Province::find()->orderBy('name')->all(), 'id_state', 'name');
$businessFields = ArrayHelper::map(\common\models\BusinessField::find()->orderBy('name')->all(), 'id', 'name');
$listDistricts = $listWards = [];
if ($model->province_id) {
    $districts = District::find()->where(['id_state' => $model->province_id])->orderBy('name')->all();
    $listDistricts = ArrayHelper::map($districts, 'id_district', 'name');
}
if ($model->district_id) {
    $wards = Ward::find()->where(['id_district' => $model->district_id])->orderBy('name')->all();
    $listWards = ArrayHelper::map($wards, 'id_ward', 'name');
}
?>
<div class="profile-form">
    <?php $form = ActiveForm::begin(); ?>
        <div class="row">
          <div class="col-md-12">
            <?php
                echo $form->field($model, 'avatar', [
                    'options' => ['class' => ['form-group', 'upload-single']]
                ])->widget(Upload::class, [
                    'url' => [
                        '/file/storage/upload'
                    ],
                    'maxFileSize' => 5000000, // 5 MiB,
                    'acceptFileTypes' => new JsExpression('/(\.|\/)(gif|jpe?g|png)$/i')
                ])->label('Cập nhật ảnh đại diện')?>

            <h6 class="label-group">Thông tin cá nhân</h6>
            <?php echo $form->field($model, 'fullname')->textInput(['maxlength' => true]) ?>
            <?php echo $form->field($model, 'phone', [
                'options' => ['class' => 'form-group relative'],
                'template' => '<div class="row"><div class="col">{label}</div><div class="col text-light-green text-right">Đã xác thực</div></div>{input}{error}{hint}',
            ])->textInput(['maxlength' => true, 'disabled' => true]) ?>
            <?php echo $form->field($model, 'email')->textInput(['maxlength' => true, 'type' => 'email']) ?>
            <div class="row">
              <div class="col col-gender">
                  <?php echo $form->field($model, 'gender', [
                      'options' => ['class' => 'form-group'],
                      'template' => '{label}{input}',
                  ])->widget(Select2::classname(), [
                      'data' => $model::genders(),
                      'options' => [
                          'placeholder' => Yii::t('backend', 'Chọn')
                      ],
                  ]);?>
              </div>
              <div class="col">
                <div class="form-group">
                  <label for="doctor-day_of_birth">Ngày sinh</label>
                  <div class="row">
                      <?php echo $form->field($model, 'day_of_birth', [
                          'options' => ['class' => 'col'],
                          'template' => '{input}',
                      ])->textInput(['type' => 'number', 'class' => 'form-control text-center', 'placeholder' => 'DD', 'max' => '31', 'min' => '1']) ?>
                      <?php echo $form->field($model, 'month_of_birth', [
                          'options' => ['class' => 'col p-0'],
                          'template' => '{input}',
                      ])->textInput(['type' => 'number', 'class' => 'form-control text-center', 'placeholder' => 'MM', 'max' => '12', 'min' => '1']) ?>
                      <?php echo $form->field($model, 'year_of_birth', [
                          'options' => ['class' => 'col col-year-of-birth'],
                          'template' => '{input}',
                      ])->textInput(['type' => 'number', 'class' => 'form-control text-center', 'placeholder' => 'YYYY', 'max' => date('Y'), 'min' => '1900']) ?>
                  </div>
                  <div class="invalid-feedback"></div>
                </div>
              </div>
            </div>
            <h6 class="label-group">Địa chỉ</h6>
            <div class="row">
              <div class="col-6 pr-sm-2">
                  <?php echo $form->field($model, 'province_id')->widget(Select2::classname(), [
                      'data' => $provinces,
                      'options' => [
                          'id' => 'sel-province',
                          'placeholder' => Yii::t('backend', 'Chọn Tỉnh/Thành')
                      ],
                  ]);?>
              </div>
              <div class="col-6 pl-sm-2">
                  <?php echo $form->field($model, 'district_id')->widget(DepDrop::classname(), [
                      'type' => DepDrop::TYPE_SELECT2,
                      'data' => $listDistricts,
                      'options' => [
                          'id' => 'sel-district',
                          'placeholder' => Yii::t('backend', 'Chọn Quận/Huyện')
                      ],
                      'pluginOptions' => [
                          'depends' => [
                              'sel-province'
                          ],
                          'url' => Url::to([
                              '/ajax/get-districts'
                          ])
                      ]
                  ]);?>
              </div>
              <div class="col-6 pr-sm-2">
                  <?php echo $form->field($model, 'ward_id')->widget(DepDrop::classname(), [
                      'type' => DepDrop::TYPE_SELECT2,
                      'data' => $listWards,
                      'options' => [
                          'id' => 'sel-ward',
                          'placeholder' => Yii::t('backend', 'Chọn Phường/Xã')
                      ],
                      'pluginOptions' => [
                          'depends' => [
                              'sel-district'
                          ],
                          'url' => Url::to([
                              '/ajax/get-wards'
                          ])
                      ]
                  ]);?>
              </div>
              <div class="col-6 pl-sm-2">
                  <?php echo $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
              </div>
            </div>
            <h6 class="label-group">Thông tin chuyên môn</h6>
            <?php echo $form->field($model, 'arr_business_field')->widget(Select2::classname(), [
                'data' => $businessFields,
                'options' => [
                    'placeholder' => Yii::t('backend', 'Chọn'),
                    'multiple' => true
                ],
            ]);?>
            <?php echo $form->field($model, 'attachments')->widget(
                Upload::class,
                [
                    'url' => ['/file/storage/upload'],
                    'sortable' => true,
                    'maxFileSize' => 10 * 1024 * 1024, // 10Mb
                    'maxNumberOfFiles' => 10, // default 1,
                    'acceptFileTypes' => new \yii\web\JsExpression('/(\.|\/)(pdf|jpe?g|png)$/i'),
                ]
            ) ?>
            <div class="text-center">
                <?php echo Html::submitButton( Yii::t('checkin', 'Cập nhật'), ['class' => 'btn btn-primary btn-rounded']) ?>
            </div>
          </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
