<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 20/08/2021
 * Time: 17:15
 */
use yii\web\JsExpression;

?>
<div class="alert alert-info text-center" role="alert">
  Cấu hình template toa thuốc sẽ được lưu tự động
</div>
<div class="form-header">
  <div class="row">
    <div class="col-7 col-sm-6 view-left-header">
      <div class="row align-items-center">
          <div class="col col-logo">
            <?php
            echo \trntv\filekit\widget\Upload::widget([
                'name' => 'setting_logo',
                'value' => $setting->logo ? [
                    'path' => $setting->logo,
                    'base_url' => \Yii::$app->params['baseUrl'],
                    'delete_url' => \yii\helpers\Url::to([
                        '/setting/upload-delete', 'key' => 'logo', 'path' => $setting->logo
                    ])
                ] : null,
                'hiddenInputId' => 'setting_logo', // must for not use model
                'url' => [
                    '/setting/upload', 'key' => 'logo'
                ],
                'maxFileSize' => 5000000, // 5 MiB,
                'acceptFileTypes' => new JsExpression('/(\.|\/)(gif|jpe?g|png)$/i'),
            ]);
            ?>
          </div>
          <div class="col">
            <div class="mb-20">
                <?php
                  echo \kartik\editable\Editable::widget([
                      'formOptions' => [
                          'action' => \yii\helpers\Url::to([
                              'setting/inline-update',
                              'key' => 'medical_name',
                          ])
                      ],
                      'model' => $setting,
                      'attribute'=>'medical_name',
                      'asPopover' => true,
                      'header' => ' ',
                      'placement' => \kartik\popover\PopoverX::ALIGN_AUTO_HORIZONTAL,
                      'options' => ['class'=>'form-control'],
                      'valueIfNull' => 'Sở Y Tế ...'
                  ]);
                ?>
            </div>
            <div class="mb-20">
                <?php
                echo \kartik\editable\Editable::widget([
                    'formOptions' => [
                        'action' => \yii\helpers\Url::to([
                            'setting/inline-update',
                            'key' => 'clinic_name',
                        ])
                    ],
                    'model' => $setting,
                    'attribute'=>'clinic_name',
                    'asPopover' => true,
                    'header' => ' ',
                    'placement' => \kartik\popover\PopoverX::ALIGN_AUTO_HORIZONTAL,
                    'options' => ['class'=>'form-control'],
                    'valueIfNull' => 'Phòng Khám ...'
                ]);
                ?>
            </div>
            <div class="mb-20"><label class="label-text">Liên hệ:</label>
                <?php
                echo \kartik\editable\Editable::widget([
                    'formOptions' => [
                        'action' => \yii\helpers\Url::to([
                            'setting/inline-update',
                            'key' => 'clinic_phone',
                        ])
                    ],
                    'model' => $setting,
                    'attribute'=>'clinic_phone',
                    'asPopover' => true,
                    'header' => ' ',
                    'placement' => \kartik\popover\PopoverX::ALIGN_AUTO_HORIZONTAL,
                    'options' => ['class'=>'form-control'],
                    'valueIfNull' => 'Số điện thoại'
                ]);
                ?>
            </div>
          </div>
      </div>
    </div>
    <div class="col-5 col-sm-6 text-center view-right-header">
      <div class="col-barcode">
          <?php
          echo \trntv\filekit\widget\Upload::widget([
              'name' => 'setting_barcode',
              'value' => $setting->barcode ? [
                  'path' => $setting->barcode,
                  'base_url' => \Yii::$app->params['baseUrl'],
                  'delete_url' => \yii\helpers\Url::to([
                      '/setting/upload-delete', 'key' => 'barcode', 'path' => $setting->barcode
                  ])
              ] : null,
              'hiddenInputId' => 'setting_barcode', // must for not use model
              'url' => [
                  '/setting/upload', 'key' => 'barcode'
              ],
              'maxFileSize' => 5000000, // 5 MiB,
              'acceptFileTypes' => new JsExpression('/(\.|\/)(gif|jpe?g|png)$/i'),
          ]);
          ?>
        <div class="mb-20"><label class="label-text">Mã Y Tế</label>
            <?php
            echo \kartik\editable\Editable::widget([
                'formOptions' => [
                    'action' => \yii\helpers\Url::to([
                        'setting/inline-update',
                        'key' => 'medical_code',
                    ])
                ],
                'model' => $setting,
                'attribute'=>'medical_code',
                'asPopover' => true,
                'header' => ' ',
                'placement' => \kartik\popover\PopoverX::ALIGN_AUTO_HORIZONTAL,
                'options' => ['class'=>'form-control'],
                'valueIfNull' => 'Nhập mã'
            ]);
            ?>
        </div>
        <div class="mb-20"><label class="label-text">Số hồ sơ</label>
            <?php
            echo \kartik\editable\Editable::widget([
                'formOptions' => [
                    'action' => \yii\helpers\Url::to([
                        'setting/inline-update',
                        'key' => 'no_of_record',
                    ])
                ],
                'model' => $setting,
                'attribute'=>'no_of_record',
                'asPopover' => true,
                'header' => ' ',
                'placement' => \kartik\popover\PopoverX::ALIGN_AUTO_HORIZONTAL,
                'options' => ['class'=>'form-control'],
                'valueIfNull' => 'Nhập số hồ sơ'
            ]);
            ?>
        </div>
      </div>
    </div>
  </div>
</div>
<div class ="readonly opacity-25">
  <?php
    echo $this->render('/prescription/_form', [
        'model' => $model,
        'modelPatient' => $modelPatient,
        'modelMedicineInsurance' => $modelMedicineInsurance,
        'modelMedicineService' => $modelMedicineService,
        'hideButton' => true,
        'min_insurance' => 1,
        'min_service' => 1,
    ]);
  ?>
</div>
