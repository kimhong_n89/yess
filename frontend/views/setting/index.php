<?php
use yii\helpers\Html;
use frontend\controllers\SettingController;
/**
 *
 * @var yii\web\View $this
 */

$this->title = Yii::t('checkin', 'Cài đặt chung');
$this->params['breadcrumbs'][] = $this->title;

$classProfile = $classTemplate = 'nav-link';

if ($tab == SettingController::TAB_TEMPLATE) {
    $classTemplate = 'nav-link active';
} else {
    $classProfile = 'nav-link active';
}
?>
<div class="setting-index">
  <div class="row content-top style-primary">
    <div class="col">
      <h2 class="text-white"><?=$this->title;?></h2>
    </div>
    <div class="col">
        <?= \frontend\widgets\GlobalSearch::widget([])?>
    </div>
  </div>
	<div class="row">
		<div class="col-md-12">
			<div class="nav-tabs-navigation">
				<div class="nav-tabs-wrapper">
					<ul class="row nav nav-pills-icons justify-content-center mb-4"
						data-tabs="tabs">
						<li class="nav-item">
                <?= Html::a(Yii::t('checkin', 'Thông tin cá nhân'), ['setting/index', 'tab' => SettingController::TAB_PROFILE], ['class' => $classProfile])?>
						</li>
            <?php if (Yii::$app->user->identity->status == \common\models\Doctor::STATUS_APPROVED) :?>
            <li class="nav-item">
                <?= Html::a(Yii::t('checkin', 'Cấu hình template toa thuốc'), ['setting/index', 'tab' => SettingController::TAB_TEMPLATE], ['class' => $classTemplate])?>
            </li>
            <?php endif;?>
					</ul>
				</div>
			</div>
			<div class="tab-content">
        <div class="tab-pane active show">
            <?php
            if ($tab == SettingController::TAB_TEMPLATE) {
                  echo $this->render($view, [
                      'setting' => $setting,
                      'model' => $model,
                      'modelPatient' => $modelPatient,
                      'modelMedicineInsurance' => $modelMedicineInsurance,
                      'modelMedicineService' => $modelMedicineService,
                  ]);
            } else {
                echo $this->render($view, ['model' => $model]);
            }
            ?>
        </div>
			</div>
		</div>
	</div>
</div>
