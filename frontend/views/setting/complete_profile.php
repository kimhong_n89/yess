<?php
use yii\helpers\Html;
/**
 *
 * @var yii\web\View $this
 */

$this->title = Yii::t('checkin', 'Hoàn tất cập nhật tài khoản!');

?>
<main class="flex-shrink-0" role="main">
  <div class="container-fluid">
    <div class="page-complete-profile mx-auto text-center">
      <img src="/img/complete-profile.png" class="img-fluid img-checked">
      <h3>Cám ơn bạn đã hoàn tất cập nhật tài khoản!</h3>
      <p>Chúng tôi sẽ gửi phản hồi qua Email khi tài khoản của bạn được kích hoạt thành công.</p>
      <img src="/img/pana.png" class="img-fluid img-bg-complete">
    </div>
    <div class="text-center mt-20">
        <?php echo Html::a( Yii::t('checkin', 'Cập nhật thông tin cá nhân'), ['/setting/index'], ['class' => 'btn btn-primary btn-rounded']) ?>
        <?php echo Html::a( Yii::t('checkin', 'Đăng xuất'), ['/user/logout'],['class' => 'btn btn-default btn-rounded mx-1 mt-2', 'data-method' => 'post']) ?>
    </div>
  </div>
</main>

