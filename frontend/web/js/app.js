function updateTotalAmount() {
	var totalAmount = 0;
    $('.medicine-item-line').each(function(index) {
    	var price = $(this).find('.sel-medicine-name :selected').data('price');
        var amount = $(this).find('.medicine-amount').val();

        var total = parseInt(price) * parseFloat(amount);
        if(isNaN(total)) {
            total = 0;
        }
        totalAmount += total;
    });
    var total = formatAsMoney(totalAmount);
	$('#total-amount').html(total);
}
function formatAsMoney(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

$(document).on('select2:open', () => {
    setTimeout(function () {  document.querySelector('.select2-container--open .select2-search__field').focus(); }, 10);
});