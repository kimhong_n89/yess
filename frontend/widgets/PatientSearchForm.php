<?php

namespace frontend\widgets;

use frontend\models\search\PatientSearch;
use yii\base\Widget;

/**
 * Render products tab panel
 */
class PatientSearchForm extends Widget
{
    public $options;
    /**
     * @inheritdoc
     */
    public function run()
    {
        $searchModel = new PatientSearch();
        $searchModel->search(\Yii::$app->request->queryParams);
        $options = $this->options ? $this->options : [];
        return $this->render('_search_patient', [
            'model' => $searchModel,
            'options' => $options
        ]);
    }
}