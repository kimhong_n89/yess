<?php

namespace frontend\widgets;

use frontend\models\search\PrescriptionSearch;
use yii\base\Widget;

/**
 * Render products tab panel
 */
class GlobalSearch extends Widget
{
    public $options;
    /**
     * @inheritdoc
     */
    public function run()
    {
        $searchModel = new PrescriptionSearch();
        $searchModel->search(\Yii::$app->request->queryParams);
        $options = $this->options ? $this->options : [];
        return $this->render('_search_global', [
            'model' => $searchModel,
            'options' => $options
        ]);
    }
}