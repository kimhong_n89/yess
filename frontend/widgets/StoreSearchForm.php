<?php

namespace frontend\widgets;

use frontend\models\search\StoreSearch;
use yii\base\Widget;

/**
 * Render products tab panel
 */
class StoreSearchForm extends Widget
{
    public $options;
    /**
     * @inheritdoc
     */
    public function run()
    {
        $searchModel = new StoreSearch();
        $searchModel->search(\Yii::$app->request->queryParams);
        $options = $this->options ? $this->options : [];
        return $this->render('_search_store', [
            'model' => $searchModel,
            'options' => $options
        ]);
    }
}