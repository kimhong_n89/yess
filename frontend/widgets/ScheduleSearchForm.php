<?php

namespace frontend\widgets;

use frontend\models\search\ExaminationScheduleSearch;
use yii\base\Widget;

/**
 * Render products tab panel
 */
class ScheduleSearchForm extends Widget
{
    public $options;
    /**
     * @inheritdoc
     */
    public function run()
    {
        $searchModel = new ExaminationScheduleSearch();
        $searchModel->search(\Yii::$app->request->queryParams);
        $options = $this->options ? $this->options : [];
        return $this->render('_search_schedule', [
            'model' => $searchModel,
            'options' => $options
        ]);
    }
}