<?php

namespace frontend\widgets;

use frontend\models\search\FeedbackSearch;
use yii\base\Widget;

/**
 * Render products tab panel
 */
class FeedbackSearchForm extends Widget
{
    public $options;
    /**
     * @inheritdoc
     */
    public function run()
    {
        $searchModel = new FeedbackSearch();
        $searchModel->search(\Yii::$app->request->queryParams);
        $options = $this->options ? $this->options : [];
        return $this->render('_search_feedback', [
            'model' => $searchModel,
            'options' => $options
        ]);
    }
}