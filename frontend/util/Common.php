<?php
namespace frontend\util;

Class Common
{
    /**
     * Generate random number for OTP code
     * @return string
     */
    static function generateOtpCode()
    {
        $length = env("SMS_CODE_LENGTH");
        if(empty($length)) $length = 4;
        $min_value = pow(10, $length - 1);
        $max_value = pow(10, $length) - 1;
        return rand($min_value, $max_value);
    }
}