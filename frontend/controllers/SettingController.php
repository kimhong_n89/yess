<?php

namespace frontend\controllers;

use common\models\Doctor;
use common\models\DoctorSetting;
use common\models\MedicineForm;
use common\models\MedicineServiceForm;
use common\models\Patient;
use common\models\Prescription;
use frontend\models\TemplateForm;
use trntv\filekit\actions\DeleteAction;
use trntv\filekit\actions\UploadAction;
use Yii;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use rmrevin\yii\fontawesome\FAS;

class SettingController extends FrontendController
{
    const TAB_PROFILE = 'profile';
    const TAB_TEMPLATE = 'template';

    /** @inheritdoc */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'delete' => ['post'],
                        'upload-delete' => ['delete'],
                    ],
                ],
            ]
        );
    }

    /** @inheritdoc */
    public function actions()
    {
        return [
            'upload' => [
                'class' => UploadAction::class,
                'deleteRoute' => 'upload-delete',
                'on afterSave' => function($event) {
                    /* @var $file \League\Flysystem\File */
                    $file = $event->file;
                    $model = new DoctorSetting();
                    $model->doctor_id = Yii::$app->user->identity->id;
                    $model->key = Yii::$app->request->get('key');
                    $model->value = $file->getPath();
                    $model->save();

                }
            ],
            'upload-delete' => [
                'class' => DeleteAction::class,
                'on afterDelete' => function($event) {
                    DoctorSetting::deleteAll(['doctor_id' => Yii::$app->user->identity->id, 'value' => Yii::$app->request->get('path')]);
                }
            ],
        ];
    }


    /**
     * Lists all SpaSetting models.
     * @return mixed
     */
    public function actionIndex($tab = self::TAB_PROFILE)
    {
        switch ($tab) {
            case self::TAB_TEMPLATE :
                $setting = new TemplateForm();
                $setting->loadFromDB();
                $model = new Prescription();
                $model->doctor_id = Yii::$app->user->id;
                $modelPatient = new Patient();
                $modelMedicineInsurance = [new MedicineForm()];
                $modelMedicineService = [new MedicineServiceForm()];
                $view = 'template';
                return $this->render('index', [
                    'setting' => $setting,
                    'model' => $model,
                    'modelPatient' => $modelPatient,
                    'modelMedicineInsurance' => $modelMedicineInsurance,
                    'modelMedicineService' => $modelMedicineService,
                    'view' => $view,
                    'tab' => $tab,
                ]);
                break;
            case self::TAB_PROFILE :
            default:
                $model = Doctor::findOne(Yii::$app->user->id);
                $model->scenario = Doctor::SCENARIO_UPDATE_PROFILE;
                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    if ($model->status == Doctor::STATUS_APPROVED) {
                        Yii::$app->session->setFlash('alert', [
                            'options' => ['class' => 'tetx-center alert-success'],
                            'body' => Yii::t('frontend', 'Tài khoản của bạn đã được cập nhật thành công')
                        ]);
                        return $this->refresh();
                    }
                    return $this->redirect(['complete-profile']);
                }
                $view = '_form';
        }
        return $this->render('index', [
            'model' => $model,
            'view' => $view,
            'tab' => $tab
        ]);
    }


    public function actionCompleteProfile()
    {
        $this->layout = '_clear';
        return $this->render('complete_profile');

    }

    public function actionInlineUpdate($key)
    {
        $req = Yii::$app->request;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($req->isAjax && $req->isPost && isset($req->post('TemplateForm')[$key])) {
            $model = DoctorSetting::findOne(['key' => $key, 'doctor_id' => Yii::$app->user->identity->id]);
            if (!$model) {
                $model = new DoctorSetting();
                $model->doctor_id = Yii::$app->user->identity->id;
                $model->key = $key;
            }
            $model->value = $req->post('TemplateForm')[$key];
            $model->save();

            return ['output' => $model->value, 'message' => ''];
        }
    }
}
