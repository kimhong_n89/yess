<?php

namespace frontend\controllers;

use common\models\LoginHistory;
use common\models\SmsHistory;
use frontend\models\Doctor;
use frontend\models\LoginForm;
use frontend\util\Common;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class UserController
 */
class UserController extends \yii\web\Controller
{
    public $layout = 'login';
    /**
     * @return array
     */

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'login', 'send-otp'
                        ],
                        'allow' => true,
                        'roles' => ['?']
                    ],
                    [
                        'actions' => [
                            'login', 'send-otp'
                        ],
                        'allow' => false,
                        'roles' => ['@'],
                        'denyCallback' => function () {
                            return Yii::$app->controller->redirect(['/site/index']);
                        }
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post']
                ]
            ]
        ];
    }

    /**
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $this->layout = 'main';
        $model = $this->findModel(Yii::$app->user->id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('forceUpdateLocale');
            Yii::$app->session->setFlash('alert', [
                'options' => ['class' => 'alert-success'],
                'body' => Yii::t('frontend', 'Tài khoản của bạn đã được cập nhật thành công')
            ]);
            return $this->refresh();
        }
        return $this->render('_form', [
            'model' => $model,
        ]);

    }

    /**
     * @return array|string|Response
     */
    public function actionLogin()
    {
        $modelPhone = new LoginForm();
        $model = new LoginForm();
        $model->scenario = LoginForm::SCENARIO_LOGIN;
        if (\Yii::$app->request->isAjax){
            \Yii::$app->response->format = 'json';
            $model->load(Yii::$app->request->post());
            if ($model->login()) {
                $url = Yii::$app->getUser()->getReturnUrl();
                if ($model->getUser()->status !== Doctor::STATUS_APPROVED) {
                    if (!empty($model->getUser()->fullname)) { //waiting for approve
                        $url = Url::to(['setting/complete-profile']);
                    } else {
                        $url = Url::to(['setting/index']);
                        Yii::$app->session->setFlash('alert', [
                            'options' => ['class' => 'text-center alert-warning'],
                            'body' => Yii::t('frontend', 'Chúc mừng bạn đã đăng nhập thành công! <br> Vui lòng hoàn tất cập nhật hồ sơ')
                        ]);
                    }
                }
                $response = [
                    'success' => true,
                    'msg' => 'Login Successful',
                    'url' => $url
                ];
            } else {
                $error = implode(", ", \yii\helpers\ArrayHelper::getColumn($model->errors, 0, false)); // Model's Errors string
                Yii::info($model->errors, 'login');
                $response = [
                    'success' => false,
                    'msg' => $error
                ];
            }
            return $response;
        }
        return $this->render('login', [
            'model' => $model,
            'modelPhone' => $modelPhone
        ]);
    }

    /**
     * @return Response
     */
    public function actionLogout()
    {
        $session = Yii::$app->session;
        $session->set('countSendOtp', 0);

        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * Send OTP to user
     * @return array
     */
    public function actionSendOtp()
    {
        $phone = \Yii::$app->request->post('phone');
        \Yii::$app->response->format = 'json';
        $response = [];
        if ($phone)
        {
            $session = Yii::$app->session;
            $countSendOtp = $session->get('countSendOtp', 0);
            if ($countSendOtp > Yii::$app->params['max_send_otp']) {
                $response = [
                    'success' => false,
                    'msg' => "Bạn đã yêu cầu gửi OTP quá nhiều, vui lòng thử lại sau."
                ];
            } else {
                $otp = Common::generateOtpCode();
                //disabled old otp code
                LoginHistory::updateAll(['status' => LoginHistory::STATUS_DELETED], ['phone_number' => $phone, 'status' => LoginHistory::STATUS_NOT_ACTIVATE_YET]);
                // Generate login history
                $history = new LoginHistory();
                $history->otp_code = $otp;
                $history->phone_number = $phone;
                $history->status = LoginHistory::STATUS_NOT_ACTIVATE_YET;
                $history->device_agent = \Yii::$app->request->getUserAgent();
                $history->ip = \Yii::$app->request->getUserIP();
                $history->created_at = $history->updated_at = time();
                if($history->save()) {
                    @SmsHistory::sendOTPSMS($phone, $otp);
                    $response = [
                        'success' => true,
                        'msg' => 'Mã OTP đã được gửi đến số điện thoại của bạn'
                    ];
                    $countSendOtp++;
                    $session->set('countSendOtp', $countSendOtp);
                } else {
                    $response = [
                        'success' => false,
                        'msg' => print_r($history->getFirstErrors())
                    ];
                }
            }
        }
        else
        {
            $response = [
                'success' => false,
                'msg' => 'Vui lòng nhập số điện thoại của bạn.'
            ];
        }
        return $response;
    }

    /**
     * @inheritDoc
     * @param $id
     * @return Doctor|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Doctor::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
