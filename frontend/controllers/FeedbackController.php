<?php

namespace frontend\controllers;

use Yii;
use common\models\Feedback;
use frontend\models\search\FeedbackSearch;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FeedbackController implements the CRUD actions for Feedback model.
 */
class FeedbackController extends Controller
{

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Feedback models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FeedbackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Feedback model.
     * @param int $id ID
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Feedback model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Feedback();
        $request = Yii::$app->request;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return [
                'forceReload' => $request->get('pjax-container', '#crud-datatable-pjax'),
                'forceClose' => true,
                'push' => false,
                'replace' => false
            ];
            exit();
        }
        return [
            'title'=> Yii::t('checkin', 'Gửi góp ý'),
            'content'=>$this->renderAjax('_form', [
                'model' => $model,
            ]),
            'footer' => Html::submitButton(Yii::t('backend', 'Gửi'), ['class' => 'btn btn-primary'])
        ];
    }

    /**
     * ViewAnswer
     * @param int $id ID
     * @return mixed
     */
    public function actionViewAnswer($id)
    {
        $model = $this->findModel($id);
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            'title'=> Yii::t('checkin', 'Xem trả lời'),
            'content'=>$this->renderAjax('_view', [
                'model' => $model,
            ]),
            'footer' => Html::button(Yii::t('backend', 'Đóng'), ['class' => 'btn btn-default', 'data-dismiss'=>"modal"])
        ];
    }

    /**
     * Deletes an existing Feedback model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Feedback model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Feedback the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Feedback::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
