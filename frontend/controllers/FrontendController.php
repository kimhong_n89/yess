<?php
namespace frontend\controllers;

use frontend\models\Doctor;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Class FrontendController
 * @package frontend\controllers
 */
class FrontendController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'except' => ['get-districts', 'get-wards', 'get-products-by-diagnose'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            // Check if user is deactivated or not
                            if (\Yii::$app->user->identity->status !== Doctor::STATUS_APPROVED) {
                                $controller = Yii::$app->controller->id;
                                if ($controller != 'setting') {
                                    if (\Yii::$app->user->identity->fullname) {
                                        return $this->redirect(['/setting/complete-profile']);
                                    } else {
                                        return $this->redirect(['/setting/index']);
                                    }
                                    exit();
                                } else {
                                    return true;
                                }
                            }
                            return true;
                        },
                        'denyCallback' => function () {
                            return Yii::$app->controller->redirect(['/user/login']);
                        }
                    ]
                ]
            ],
        ];
    }
}
