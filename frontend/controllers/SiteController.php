<?php

namespace frontend\controllers;

use cheatsheet\Time;
use common\models\DoctorSetting;
use common\models\Patient;
use common\models\Prescription;
use common\models\PrintedReport;
use common\sitemap\UrlsIterator;
use frontend\models\ContactForm;
use frontend\models\search\ExaminationScheduleSearch;
use Sitemaped\Element\Urlset\Urlset;
use Sitemaped\Sitemap;
use Yii;
use yii\filters\AccessControl;
use yii\filters\PageCache;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'rules' => [
                    [
                        'actions' => [
                            'view-prescription', 'error'
                        ],
                        'allow' => true,
                        'roles' => ['?']
                    ]
                ]
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'login'
            ],
//            'captcha' => [
//                'class' => 'yii\captcha\CaptchaAction',
//                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
//            ],
            'set-locale' => [
                'class' => 'common\actions\SetLocaleAction',
                'locales' => array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    /**
     * @return string
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $this->layout = 'dashboard';
        $prescriptions= Prescription::find()->where(['doctor_id' => Yii::$app->user->id])->asArray()->all();
        $by_status = ArrayHelper::index($prescriptions, 'id', 'status');
        $by_patient = ArrayHelper::index($prescriptions, 'id', 'patient_id');

        $prescription_deleted_count = isset($by_status[Prescription::STATUS_DELETED]) ? count($by_status[Prescription::STATUS_DELETED]) : 0;
        $prescription_draft_count = isset($by_status[Prescription::STATUS_DRAFT]) ? count($by_status[Prescription::STATUS_DRAFT]) : 0;
        $prescription_done_count = count($prescriptions) - $prescription_draft_count - $prescription_deleted_count;
        $customer_count = count($by_patient);
        $chartData = $this->getPrescriptionData();

        $searchModel = new ExaminationScheduleSearch();
        $dataProviderToday = $searchModel->today();
        $this->view->params['dataProviderScheduleToday'] = $dataProviderToday;
        $this->view->params['doctorSetting'] = ArrayHelper::map(DoctorSetting::find()->where(['doctor_id' => Yii::$app->user->identity->id])->asArray()->all(), 'key', 'value');
        return $this->render('index', [
            'customer_count' => $customer_count,
            'prescription_done_count' => $prescription_done_count,
            'prescription_draft_count' => $prescription_draft_count,
            'chartData' => $chartData,
        ]);
    }

    /**
     * @param $token
     * @return string
     */
    public function actionViewPrescription($token)
    {
        $this->layout = '_clear';
        $hash = PrintedReport::getDecodeToken($token);
        $model = PrintedReport::findOne(['hash' => $hash]);
        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $this->render('_view_printed', [
            'model' => $model
        ]);
    }

    /**
     * @param null $month
     * @param null $year
     * @return array
     * @throws \yii\db\Exception
     */
    public function getPrescriptionData($month =null, $year = null)
    {
        //cal_days_in_month();
        if (!$month) {
            $month = date('n');
        }
        if (!$year) {
            $year = date('Y');
        }
        $select = "SELECT count(id) as prescription, DAY(FROM_UNIXTIME(created_at)) as `day` FROM " . Prescription::tableName();
        $where = sprintf("WHERE MONTH(FROM_UNIXTIME(created_at)) = %1s AND YEAR(FROM_UNIXTIME(created_at)) = %2s", $month, $year);
        $where .= ' AND doctor_id=' . Yii::$app->user->id;

        $groupBy = 'GROUP BY `created_at`';
        $result = ArrayHelper::map(Yii::$app->db->createCommand("$select $where $groupBy")->queryAll(), 'day', 'prescription');
        $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $highest = 0;
        $labels = $series = [];
        for ($i = 1; $i <= $days; $i++) {
            $value = isset($result[$i]) ? $result[$i] : 0;
            $highest = ($highest < $value) ? $value : $highest;
            $labels[] = $i;
            $series[] = ['meta' => "$i-$month", 'value' => isset($result[$i]) ? $result[$i] : 0];
        }

        return [
            'labels' => $labels,
            'series' => $series,
            'highest' => $highest
        ];
    }
}
