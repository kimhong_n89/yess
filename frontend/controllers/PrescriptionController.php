<?php

namespace frontend\controllers;

use common\models\FavoriteStore;
use common\models\MedicineForm;
use common\models\GeneralModel;
use common\models\MedicineServiceForm;
use common\models\Patient;
use common\models\PrescriptionDetail;
use common\models\PrintedReport;
use common\models\Product;
use common\models\Store;
use frontend\models\StoreForm;
use Yii;
use common\models\Prescription;
use frontend\models\search\PrescriptionSearch;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * PrescriptionController implements the CRUD actions for Prescription model.
 */
class PrescriptionController extends FrontendController
{
    const TAB_DONE = 'done';
    const TAB_DRAFT = 'draft';
    const TAB_DELIVERY_TO_STORE = 'delivery_to_store';
    const TAB_DELETED = 'deleted';

    /** @inheritdoc */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'delete' => ['post'],
                        'print' => ['post'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Prescription models.
     * @return mixed
     */
    public function actionIndex($tab = self::TAB_DRAFT)
    {
        $searchModel = new PrescriptionSearch();
        switch ($tab) {
            case self::TAB_DONE:
                $params = ArrayHelper::merge(Yii::$app->request->queryParams, ['PrescriptionSearch' => ['status' => [Prescription::STATUS_DONE, Prescription::STATUS_DELIVERY_TO_STORE]]]);
                break;
            case self::TAB_DELETED:
                $params = ArrayHelper::merge(Yii::$app->request->queryParams, ['PrescriptionSearch' => ['status' => [Prescription::STATUS_DELETED]]]);
                break;
            case self::TAB_DRAFT:
            default:
                $params = ArrayHelper::merge(Yii::$app->request->queryParams, ['PrescriptionSearch' => ['status' => [Prescription::STATUS_DRAFT]]]);
        }

        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tab' => $tab
        ]);
    }

    /**
     * Displays a single Prescription model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Prescription model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        $model = new Prescription();
        $model->doctor_id = Yii::$app->user->id;
        $modelPatient = new Patient();
        $modelMedicineInsurance = [new MedicineForm()];
        $modelMedicineService = [new MedicineServiceForm()];

        if ($model->load(Yii::$app->request->post()) && $modelPatient->load(Yii::$app->request->post())) {

            $modelMedicineInsurance = GeneralModel::createMultiple(MedicineForm::classname(), $modelMedicineInsurance);
            Model::loadMultiple($modelMedicineInsurance, Yii::$app->request->post());

            $modelMedicineService = GeneralModel::createMultiple(MedicineServiceForm::classname(), $modelMedicineService);
            Model::loadMultiple($modelMedicineService, Yii::$app->request->post());

            $valid = true;
            $transaction = \Yii::$app->db->beginTransaction();
            if (!$model->patient_id) {
               //create new patient
                $valid = $modelPatient->save();
                $model->patient_id = $modelPatient->id;
            }

            // validate all models
            $valid = $model->validate() && $valid;
            if ($modelMedicineInsurance) {
                $valid = Model::validateMultiple($modelMedicineInsurance) && $valid;
            }
            if ($modelMedicineService) {
                $valid = Model::validateMultiple($modelMedicineService) && $valid;
            }
            if ($valid) {
                try {
                    if ($flag = $model->save()) {
                        //calculate total_amount
                        $total_amount = 0;
                        foreach ($modelMedicineInsurance as $modelDetail) {
                            $modelDetail->prescription_id = $model->id;
                            $price = Product::findOne($modelDetail->medicine_id)->price ;
                            $total_amount += $price * $modelDetail->amount;
                            if (! ($flag = $modelDetail->save())) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                        foreach ($modelMedicineService as $modelDetail) {
                            $modelDetail->prescription_id = $model->id;
                            $price = Product::findOne($modelDetail->medicine_id)->price ;
                            $total_amount += $price * $modelDetail->amount;
                            if (! ($flag = $modelDetail->save())) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                        $model->total_amount = $total_amount;
                        $model->save(false);
                    }
                    if ($flag) {
                        $transaction->commit();
                        $action = Yii::$app->request->post('btn-submit', 'save');
                        if ($action == 'save') {
                            Yii::$app->session->setFlash('alert', [
                                'options' => ['class' => 'alert-success'],
                                'body' => Yii::t('frontend', 'Toa thuốc của bạn đã được tạo thành công')
                            ]);
                            return $this->redirect(['index']);
                        }
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            } else {
                $transaction->rollBack();
            }
        }

        $min_insurance = count($modelMedicineInsurance) > 0 ? 1 : 0;
        $min_service = count($modelMedicineService) > 0 ? 1 : 0;
        return $this->render('create', [
            'model' => $model,
            'modelPatient' => $modelPatient,
            'modelMedicineInsurance' => (empty($modelMedicineInsurance)) ? [new MedicineForm()] : $modelMedicineInsurance,
            'modelMedicineService' => (empty($modelMedicineService)) ? [new MedicineServiceForm()] : $modelMedicineService,
            'min_insurance' => $min_insurance,
            'min_service' => $min_service,
        ]);
    }

    /**
     * Updates an existing Prescription model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionUpdate($id)
    {
        $min_insurance = $min_service = 0;
        $model = $this->findModel($id);
        if ($model->status == $model::STATUS_DELIVERY_TO_STORE) {
            return $this->redirect(['view', 'id' => $id]);
        }
        $modelPatient = $model->patient;
        $prescriptionDetails = $model->prescriptionDetails;
        $modelDetails = ArrayHelper::index($prescriptionDetails, null, 'type');
        if (!empty($modelDetails[PrescriptionDetail::TYPE_MEDICINE_INSURANCE])) {
            foreach ($modelDetails[PrescriptionDetail::TYPE_MEDICINE_INSURANCE] as $modelDetail) {
                $modelMedicineInsurance[] = (new MedicineForm())->setModel($modelDetail);
            }
            $min_insurance = 1;
        } else {
            $modelMedicineInsurance = [new MedicineForm()];
        }
        if (!empty($modelDetails[PrescriptionDetail::TYPE_MEDICINE_SERVICE])) {
            foreach ($modelDetails[PrescriptionDetail::TYPE_MEDICINE_SERVICE] as $modelDetail) {
                $modelMedicineService[] = (new MedicineServiceForm())->setModel($modelDetail);
            }
            $min_service = 1;
        } else {
            $modelMedicineService = [new MedicineServiceForm()];
        }

        $oldDetails = ArrayHelper::getColumn($prescriptionDetails, 'id');

        if ($model->load(Yii::$app->request->post()) && $modelPatient->load(Yii::$app->request->post())) {

            $modelMedicineInsurance = GeneralModel::createMultiple(MedicineForm::classname(), $modelMedicineInsurance);
            Model::loadMultiple($modelMedicineInsurance, Yii::$app->request->post());

            $modelMedicineService = GeneralModel::createMultiple(MedicineServiceForm::classname(), $modelMedicineService);
            Model::loadMultiple($modelMedicineService, Yii::$app->request->post());

            $min_insurance = count($modelMedicineInsurance) > 0 ? 1 : 0;
            $min_service = count($modelMedicineService) > 0 ? 1 : 0;

            $valid = true;
            $transaction = \Yii::$app->db->beginTransaction();
            if (!$model->patient_id) {
                //create new patient
                $valid = $modelPatient->save();
                $model->patient_id = $modelPatient->id;
            }

            // validate all models
            $valid = $model->validate() && $valid;
            if ($modelMedicineInsurance) {
                $valid = Model::validateMultiple($modelMedicineInsurance) && $valid;
            }
            if ($modelMedicineService) {
                $valid = Model::validateMultiple($modelMedicineService) && $valid;
            }

            if ($valid) {
                try {
                    if ($flag = $model->save()) {
                        //calculate total_amount
                        $total_amount = 0;
                        foreach ($modelMedicineInsurance as $modelDetail) {
                            $modelDetail->prescription_id = $model->id;
                            $price = Product::findOne($modelDetail->medicine_id)->price ;
                            $total_amount += $price * $modelDetail->amount;
                            if (! ($flag = $modelDetail->save())) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                        foreach ($modelMedicineService as $modelDetail) {
                            $modelDetail->prescription_id = $model->id;
                            $price = Product::findOne($modelDetail->medicine_id)->price ;
                            $total_amount += $price * $modelDetail->amount;
                            if (! ($flag = $modelDetail->save())) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                        $model->total_amount = $total_amount;
                        $model->save(false);
                    }
                    //delete old prescription detail
                    PrescriptionDetail::deleteAll(['IN', 'id', $oldDetails]);
                    if ($flag) {
                        $transaction->commit();
                        $action = Yii::$app->request->post('btn-submit', 'save');
                        if ($action == 'save') {
                            Yii::$app->session->setFlash('alert', [
                                'options' => ['class' => 'alert-success'],
                                'body' => Yii::t('frontend', 'Toa thuốc của bạn đã được cập nhật thành công')
                            ]);
                            return $this->redirect('index');
                        }
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            } else {
                $transaction->rollBack();
            }
        }
        return $this->render('update', [
            'model' => $model,
            'modelPatient' => $modelPatient,
            'modelMedicineInsurance' => (empty($modelMedicineInsurance)) ? [new MedicineForm()] : $modelMedicineInsurance,
            'modelMedicineService' => (empty($modelMedicineService)) ? [new MedicineServiceForm()] : $modelMedicineService,
            'min_insurance' => $min_insurance,
            'min_service' => $min_service,
        ]);
    }

    /**
     * Deletes an existing Prescription model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->softDelete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Prescription model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Prescription the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Prescription::find()->where(['id' => $id, 'doctor_id' => Yii::$app->user->identity->id])->one()) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Save print html to db
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionPrint($id)
    {
        $model = Prescription::findOne(['id' => $id, 'doctor_id' => Yii::$app->user->identity->id]);
        if ($model) {
            $printed = PrintedReport::findOne(['version' => $model->updated_at]);
            if (!$printed) {
                $printed = new PrintedReport();
                $printed->prescription_id = $id;
                $printed->html = $this->renderPartial('view/_view', ['model' => $model, 'print' => true]);
                $printed->version = $model->updated_at;
                $printed->save();
                $printed->sendEmailPrescription();
            }

            return $this->redirect(['view-print', 'token' => PrintedReport::getEncodeToken($printed->hash)]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionViewPrint($token)
    {
        $this->layout = '_clear';
        $hash = PrintedReport::getDecodeToken($token);
        $model = PrintedReport::findOne(['hash' => $hash]);
        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $this->render('_view_printed', [
            'model' => $model,
            'token' => $token
        ]);
    }

    public function actionSendViaMail($token)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $hash = PrintedReport::getDecodeToken($token);
        $model = PrintedReport::findOne(['hash' => $hash]);
        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        if($model->sendEmailPrescription()) {
            return [
                'title'=> Yii::t('checkin', 'Thành công!'),
                'content' => 'Toa thuốc của bạn đã được gửi đến khách hàng',
                'footer' => ''
            ];
        } else {
            return [
                'title'=> Yii::t('checkin', 'Thất bại!'),
                'content' => 'Đã có lỗi trong quá trình gửi toa, vui lòng thử lại sau',
                'footer' => ''
            ];
        }
        exit();
    }

    public function actionSendToStore($id)
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $modelPrescription = $this->findModel($id);
        if ($modelPrescription->status != Prescription::STATUS_DONE) {
            return [
                'title'=> Yii::t('checkin', 'Lỗi!'),
                'content' => 'Vui lòng hoàn thành toa thuốc trước khi gửi đến nhà thuốc',
                'footer' => ''
            ];
            exit();
        }

        $query = Store::find()->innerJoin(FavoriteStore::tableName(), 'store.id=favorite_store.store_id')->where(['favorite_store.doctor_id' => Yii::$app->user->identity->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $model = new StoreForm();
        $model->store_id = $modelPrescription->store_id;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $modelPrescription->store_id = $model->store_id;
            $modelPrescription->status = Prescription::STATUS_DELIVERY_TO_STORE;
            $modelPrescription->save(false);
            return [
                'title'=> Yii::t('checkin', 'Thành công!'),
                'content' => 'Toa thuốc của bạn đã được gửi đến nhà thuốc',
                'footer' => ''
            ];
            exit();
        }
        return [
            'title'=> Yii::t('checkin', 'Chọn nhà thuốc để gửi toa'),
            'content'=>$this->renderAjax('form/_form_store', [
                'model' => $model,
                'dataProvider' => $dataProvider
            ]),
            'footer' => "<div class='col-12 text-center'>". Html::submitButton('Gửi toa', ['class' => 'btn btn-orange']) . "</div>"
        ];
    }
}
